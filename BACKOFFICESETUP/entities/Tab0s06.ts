import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s06", ["cdcobrador"], { unique: true })
@Entity("tab0s06", { schema: "dbo" })
export class Tab0s06 {
  @Column("char", { primary: true, name: "cdcobrador", length: 10 })
  cdcobrador: string;

  @Column("char", { name: "dscobrador", nullable: true, length: 60 })
  dscobrador: string | null;

  @Column("char", { name: "drcobrador", nullable: true, length: 60 })
  drcobrador: string | null;

  @Column("char", { name: "ruccobrador", nullable: true, length: 15 })
  ruccobrador: string | null;

  @Column("char", { name: "tlfcobrador", nullable: true, length: 15 })
  tlfcobrador: string | null;

  @Column("char", { name: "tlfcobrador1", nullable: true, length: 15 })
  tlfcobrador1: string | null;
}
