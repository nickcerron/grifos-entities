import { Column, Entity } from "typeorm";

@Entity("generador_sunat", { schema: "dbo" })
export class GeneradorSunat {
  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "tipodoc", nullable: true, length: 2 })
  tipodoc: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("int", { name: "estado", nullable: true })
  estado: number | null;

  @Column("varchar", { name: "descripcion", nullable: true, length: 60 })
  descripcion: string | null;

  @Column("int", { name: "estado_sunat", nullable: true })
  estadoSunat: number | null;

  @Column("varchar", { name: "descripcion_sunat", nullable: true, length: 120 })
  descripcionSunat: string | null;
}
