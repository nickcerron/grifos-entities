import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s01", ["cdempresa"], { unique: true })
@Entity("tab0s01", { schema: "dbo" })
export class Tab0s01 {
  @Column("char", { primary: true, name: "cdempresa", length: 2 })
  cdempresa: string;

  @Column("varchar", { name: "dsempresa", nullable: true, length: 220 })
  dsempresa: string | null;

  @Column("varchar", { name: "drempresa", nullable: true, length: 220 })
  drempresa: string | null;

  @Column("char", { name: "tlfempresa", nullable: true, length: 15 })
  tlfempresa: string | null;

  @Column("char", { name: "rucempresa", nullable: true, length: 15 })
  rucempresa: string | null;

  @Column("char", { name: "conexion", nullable: true, length: 10 })
  conexion: string | null;

  @Column("char", { name: "ruta_import_interfase", nullable: true, length: 10 })
  rutaImportInterfase: string | null;

  @Column("char", { name: "depempresa", nullable: true, length: 60 })
  depempresa: string | null;

  @Column("char", { name: "provempresa", nullable: true, length: 60 })
  provempresa: string | null;

  @Column("char", { name: "disempresa", nullable: true, length: 60 })
  disempresa: string | null;

  @Column("char", { name: "cdubigeo", nullable: true, length: 60 })
  cdubigeo: string | null;

  @Column("varchar", { name: "paginaweb", nullable: true, length: 220 })
  paginaweb: string | null;

  @Column("char", { name: "autorizacion", nullable: true, length: 60 })
  autorizacion: string | null;

  @Column("varchar", { name: "urbempresa", nullable: true, length: 220 })
  urbempresa: string | null;

  @Column("varchar", { name: "correo", nullable: true, length: 220 })
  correo: string | null;

  @Column("varchar", { name: "dsSucursal", nullable: true, length: 120 })
  dsSucursal: string | null;

  @Column("varchar", { name: "drSucursal", nullable: true, length: 120 })
  drSucursal: string | null;

  @Column("varchar", { name: "fe_rutaservidor", nullable: true, length: 50 })
  feRutaservidor: string | null;

  @Column("bit", { name: "facturacion_electronica", nullable: true })
  facturacionElectronica: boolean | null;

  @Column("bit", { name: "flg_detraccion", nullable: true })
  flgDetraccion: boolean | null;

  @Column("varchar", { name: "Cuenta_BN", nullable: true, length: 20 })
  cuentaBn: string | null;

  @Column("bit", { name: "flg_Percepcion", nullable: true })
  flgPercepcion: boolean | null;

  @Column("bit", { name: "flg_Retencion", nullable: true })
  flgRetencion: boolean | null;

  @Column("varchar", { name: "conexionPGSQL", nullable: true, length: 20 })
  conexionPgsql: string | null;

  @Column("varchar", { name: "dslocal", nullable: true, length: 50 })
  dslocal: string | null;
}
