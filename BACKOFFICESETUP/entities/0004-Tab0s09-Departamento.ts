import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s09", ["cddepartamento"], { unique: true })
@Entity("tab0s09", { schema: "dbo" })
export class Tab0s09 {
  //id:  ahora sera identity
  //bigint
  @Column("char", { primary: true, name: "cddepartamento", length: 2 })
  cddepartamento: string;

  //name: nombre del distrito
  //varchar(80)
  //nullable: false
  @Column("char", { name: "dsdepartamento", nullable: true, length: 20 })
  dsdepartamento: string | null;


  //este campo muere
  @Column("bit", { name: "flgafecto", nullable: true })
  flgafecto: boolean | null;

  //ubigeo
  //varchar(10)
  @Column("char", { name: "codUbigeo", nullable: true, length: 2 })
  codUbigeo: string | null;

  //se agregara el campo 
  //zip_code (codigo postal)
  zip_code: string | null;
  //varchar(10)
  //nullable: true

  //se agregara el campo
  //pais: referencia a la tabla pais
  //el nombre en la db sera country_id
  country: string | null;
}
