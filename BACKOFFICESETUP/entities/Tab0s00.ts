import { Column, Entity, Index } from "typeorm";

@Index("id", ["id"], { unique: true })
@Index("pk_tab0s00", ["id"], { unique: true })
@Entity("tab0s00", { schema: "dbo" })
export class Tab0s00 {

  //? REPORTES? 
  @Column("char", { name: "monpais", nullable: true, length: 1 })
  monpais: string | null;

  @Column("char", { primary: true, name: "id", length: 1 })
  id: string;

  @Column("char", { name: "path_repo", nullable: true, length: 50 })
  pathRepo: string | null;

  @Column("char", { name: "path_formato", nullable: true, length: 50 })
  pathFormato: string | null;

  @Column("char", { name: "path_gasboy", nullable: true, length: 50 })
  pathGasboy: string | null;

  @Column("char", { name: "path_loteria", nullable: true, length: 50 })
  pathLoteria: string | null;

  @Column("numeric", {
    name: "numempresa",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  numempresa: number | null;

  @Column("char", { name: "tpfactura", nullable: true, length: 5 })
  tpfactura: string | null;

  @Column("char", { name: "tpboleta", nullable: true, length: 5 })
  tpboleta: string | null;

  @Column("char", { name: "tpticket", nullable: true, length: 5 })
  tpticket: string | null;

  @Column("char", { name: "tppromocion", nullable: true, length: 5 })
  tppromocion: string | null;

  @Column("char", { name: "tpntavta", nullable: true, length: 5 })
  tpntavta: string | null;

  @Column("char", { name: "tpncredito", nullable: true, length: 5 })
  tpncredito: string | null;

  @Column("char", { name: "tpndebito", nullable: true, length: 5 })
  tpndebito: string | null;

  @Column("char", { name: "tpletra", nullable: true, length: 5 })
  tpletra: string | null;

  @Column("char", { name: "tpletraprot", nullable: true, length: 5 })
  tpletraprot: string | null;

  @Column("char", { name: "tpguia", nullable: true, length: 5 })
  tpguia: string | null;

  @Column("char", { name: "tpvale", nullable: true, length: 5 })
  tpvale: string | null;

  @Column("char", { name: "tpanticipo", nullable: true, length: 5 })
  tpanticipo: string | null;

  @Column("char", { name: "tpcheque", nullable: true, length: 5 })
  tpcheque: string | null;

  @Column("char", { name: "tpproforma", nullable: true, length: 5 })
  tpproforma: string | null;

  @Column("char", { name: "tpseparacion", nullable: true, length: 5 })
  tpseparacion: string | null;

  @Column("char", { name: "tpcomanda", nullable: true, length: 5 })
  tpcomanda: string | null;

  @Column("char", { name: "tppedido", nullable: true, length: 5 })
  tppedido: string | null;

  @Column("char", { name: "tppgoefectivo", nullable: true, length: 5 })
  tppgoefectivo: string | null;

  @Column("char", { name: "tppgotarjeta", nullable: true, length: 5 })
  tppgotarjeta: string | null;

  @Column("char", { name: "tppgocheque", nullable: true, length: 5 })
  tppgocheque: string | null;

  @Column("char", { name: "tppgocredito", nullable: true, length: 5 })
  tppgocredito: string | null;

  @Column("char", { name: "tpaplicncred", nullable: true, length: 5 })
  tpaplicncred: string | null;

  @Column("char", { name: "tpaplicantic", nullable: true, length: 5 })
  tpaplicantic: string | null;

  @Column("char", { name: "tppgocanje", nullable: true, length: 5 })
  tppgocanje: string | null;

  @Column("char", { name: "cdarticulocontable", nullable: true, length: 20 })
  cdarticulocontable: string | null;

  @Column("char", { name: "calculoigv", nullable: true, length: 1 })
  calculoigv: string | null;

  @Column("bit", { name: "flgdesgtkfact", nullable: true })
  flgdesgtkfact: boolean | null;

  @Column("bit", { name: "flgmolotov", nullable: true })
  flgmolotov: boolean | null;

  @Column("char", { name: "path_clicorp", nullable: true, length: 60 })
  pathClicorp: string | null;

  @Column("char", { name: "path_bonus", nullable: true, length: 60 })
  pathBonus: string | null;

  @Column("char", { name: "central", nullable: true, length: 1 })
  central: string | null;

  @Column("char", { name: "path_transfer", nullable: true, length: 60 })
  pathTransfer: string | null;

  @Column("bit", { name: "cambioturno" })
  cambioturno: boolean;

  @Column("char", { name: "tppgotransferencia", nullable: true, length: 5 })
  tppgotransferencia: string | null;

  @Column("char", { name: "tpticketfac", nullable: true, length: 5 })
  tpticketfac: string | null;

  @Column("char", { name: "tpcliente_corporativo", nullable: true, length: 5 })
  tpclienteCorporativo: string | null;

  @Column("varchar", { name: "nom_ventaplaya", nullable: true, length: 250 })
  nomVentaplaya: string | null;

  @Column("varchar", { name: "fe_ipremoto", nullable: true, length: 15 })
  feIpremoto: string | null;

  @Column("varchar", { name: "fe_puertoremoto", nullable: true, length: 10 })
  fePuertoremoto: string | null;

  @Column("varchar", { name: "fe_proveedor", nullable: true, length: 3 })
  feProveedor: string | null;

  @Column("varchar", {
    name: "fe_dbz_rutasuitepos",
    nullable: true,
    length: 50,
  })
  feDbzRutasuitepos: string | null;

  @Column("varchar", {
    name: "fe_dbz_rutasuitesuc",
    nullable: true,
    length: 500,
  })
  feDbzRutasuitesuc: string | null;

  @Column("varchar", {
    name: "fe_snt_rutaprocesamiento",
    nullable: true,
    length: 50,
  })
  feSntRutaprocesamiento: string | null;

  @Column("varchar", {
    name: "fe_snt_rutafacturador",
    nullable: true,
    length: 50,
  })
  feSntRutafacturador: string | null;

  @Column("varchar", {
    name: "fe_snt_rutagenerador",
    nullable: true,
    length: 50,
  })
  feSntRutagenerador: string | null;

  @Column("varchar", {
    name: "fe_mst_rutawebservice",
    nullable: true,
    length: 100,
  })
  feMstRutawebservice: string | null;

  @Column("varchar", {
    name: "fe_sge_rutawebservice",
    nullable: true,
    length: 100,
  })
  feSgeRutawebservice: string | null;

  @Column("varchar", {
    name: "fe_sge_clavecertificado",
    nullable: true,
    length: 100,
  })
  feSgeClavecertificado: string | null;

  @Column("varchar", {
    name: "fe_asp_rutaprocesamiento",
    nullable: true,
    length: 100,
  })
  feAspRutaprocesamiento: string | null;

  @Column("varchar", {
    name: "fe_inc_rutaprocesamiento",
    nullable: true,
    length: 100,
  })
  feIncRutaprocesamiento: string | null;

  @Column("int", { name: "fe_bzl_tiempoespera", nullable: true })
  feBzlTiempoespera: number | null;

  @Column("varchar", {
    name: "fe_act_rutaprocesamiento",
    nullable: true,
    length: 100,
  })
  feActRutaprocesamiento: string | null;

  @Column("varchar", {
    name: "fe_tci_rutaprocesamiento",
    nullable: true,
    length: 100,
  })
  feTciRutaprocesamiento: string | null;

  @Column("varchar", { name: "fe_act_ruta_ws", nullable: true, length: 150 })
  feActRutaWs: string | null;

  @Column("varchar", { name: "fe_act_clave_ws", nullable: true, length: 30 })
  feActClaveWs: string | null;

  @Column("varchar", {
    name: "fe_act_ruta_ws_CDR",
    nullable: true,
    length: 150,
  })
  feActRutaWsCdr: string | null;

  @Column("char", { name: "tpdinterno", nullable: true, length: 5 })
  tpdinterno: string | null;

  @Column("varchar", {
    name: "fe_cvr_ruta_in_nota",
    nullable: true,
    length: 20,
  })
  feCvrRutaInNota: string | null;

  @Column("varchar", {
    name: "fe_tci_rutaanulacion",
    nullable: true,
    length: 250,
  })
  feTciRutaanulacion: string | null;

  @Column("bit", { name: "Fe_Tab01", nullable: true })
  feTab01: boolean | null;

  @Column("numeric", {
    name: "porcretencion",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porcretencion: number | null;

  @Column("numeric", {
    name: "MtoBaseRetencion",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  mtoBaseRetencion: number | null;

  @Column("bit", { name: "ActRucEmpresa", nullable: true })
  actRucEmpresa: boolean | null;
}
