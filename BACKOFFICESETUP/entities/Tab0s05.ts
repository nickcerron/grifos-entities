import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s05", ["cdvendedor"], { unique: true })
@Entity("tab0s05", { schema: "dbo" })
export class Tab0s05 {
  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", { name: "dsvendedor", nullable: true, length: 60 })
  dsvendedor: string | null;

  @Column("char", { name: "drvendedor", nullable: true, length: 60 })
  drvendedor: string | null;

  @Column("char", { name: "rucvendedor", nullable: true, length: 15 })
  rucvendedor: string | null;

  @Column("char", { name: "tlfvendedor", nullable: true, length: 15 })
  tlfvendedor: string | null;

  @Column("char", { name: "tlfvendedor1", nullable: true, length: 15 })
  tlfvendedor1: string | null;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;
}
