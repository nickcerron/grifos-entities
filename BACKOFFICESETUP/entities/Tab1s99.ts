import { Column, Entity, Index } from "typeorm";

@Index("pk_tab1s99", ["tipo", "codigo", "cdempresa"], { unique: true })
@Entity("tab1s99", { schema: "dbo" })
export class Tab1s99 {
  @Column("char", { primary: true, name: "tipo", length: 3 })
  tipo: string;

  @Column("char", { primary: true, name: "codigo", length: 20 })
  codigo: string;

  @Column("char", { primary: true, name: "cdempresa", length: 2 })
  cdempresa: string;

  @Column("char", { name: "cdnivel", nullable: true, length: 2 })
  cdnivel: string | null;
}
