import { Column, Entity } from "typeorm";

@Entity("TmpVendedorWebS", { schema: "dbo" })
export class TmpVendedorWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "VendedorId", length: 10 })
  vendedorId: string;

  @Column("varchar", { name: "Nombres", nullable: true, length: 60 })
  nombres: string | null;

  @Column("varchar", { name: "Direccion", nullable: true, length: 60 })
  direccion: string | null;

  @Column("varchar", { name: "RUC", nullable: true, length: 15 })
  ruc: string | null;

  @Column("varchar", { name: "Telefono1", nullable: true, length: 15 })
  telefono1: string | null;

  @Column("varchar", { name: "Telefono2", nullable: true, length: 15 })
  telefono2: string | null;
}
