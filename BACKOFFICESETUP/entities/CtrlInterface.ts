import { Column, Entity, Index } from "typeorm";

@Index("pk_ctrlInterface", ["localId"], { unique: true })
@Entity("ctrlInterface", { schema: "dbo" })
export class CtrlInterface {
  @Column("bit", { name: "data", nullable: true })
  data: boolean | null;

  @Column("varchar", { name: "progPath", length: 50 })
  progPath: string;

  @Column("varchar", { name: "progBat", length: 50 })
  progBat: string;

  @Column("bit", { name: "procLun", nullable: true })
  procLun: boolean | null;

  @Column("bit", { name: "procMar", nullable: true })
  procMar: boolean | null;

  @Column("bit", { name: "procMie", nullable: true })
  procMie: boolean | null;

  @Column("bit", { name: "procJue", nullable: true })
  procJue: boolean | null;

  @Column("bit", { name: "procVie", nullable: true })
  procVie: boolean | null;

  @Column("bit", { name: "procSab", nullable: true })
  procSab: boolean | null;

  @Column("bit", { name: "procDom", nullable: true })
  procDom: boolean | null;

  @Column("int", { name: "procHour" })
  procHour: number;

  @Column("char", { name: "procStar", length: 5 })
  procStar: string;

  @Column("bit", { name: "sinCreden" })
  sinCreden: boolean;

  @Column("varchar", { name: "sendSmtp", nullable: true, length: 50 })
  sendSmtp: string | null;

  @Column("int", { name: "sendPort", nullable: true })
  sendPort: number | null;

  @Column("varchar", { name: "sendUser", nullable: true, length: 100 })
  sendUser: string | null;

  @Column("varchar", { name: "sendPass", nullable: true, length: 50 })
  sendPass: string | null;

  @Column("varchar", { name: "sendUser1", length: 100 })
  sendUser1: string;

  @Column("varchar", { name: "sendUser2", nullable: true, length: 100 })
  sendUser2: string | null;

  @Column("varchar", { name: "sendUser3", nullable: true, length: 100 })
  sendUser3: string | null;

  @Column("varchar", { name: "sendUser4", nullable: true, length: 100 })
  sendUser4: string | null;

  @Column("varchar", { name: "sendUser5", nullable: true, length: 100 })
  sendUser5: string | null;

  @Column("varchar", { name: "localName", nullable: true, length: 100 })
  localName: string | null;

  @Column("int", { primary: true, name: "localId" })
  localId: number;

  @Column("datetime", { name: "fecReg" })
  fecReg: Date;

  @Column("datetime", { name: "fecUpd", nullable: true })
  fecUpd: Date | null;

  @Column("bit", { name: "procError", nullable: true })
  procError: boolean | null;

  @Column("bit", { name: "procStock", nullable: true })
  procStock: boolean | null;

  @Column("bit", { name: "conOffice", nullable: true })
  conOffice: boolean | null;

  @Column("varchar", { name: "logProc", nullable: true, length: 60 })
  logProc: string | null;
}
