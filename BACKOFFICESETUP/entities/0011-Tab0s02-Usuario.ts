import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s02", ["cdusuario"], { unique: true })
@Entity("tab0s02", { schema: "dbo" })
export class Tab0s02 {
  @Column("char", { primary: true, name: "cdusuario", length: 10 })
  cdusuario: string;

  @Column("char", { name: "dsusuario", nullable: true, length: 60 })
  dsusuario: string | null;

  @Column("char", { name: "drusuario", nullable: true, length: 60 })
  drusuario: string | null;

  @Column("char", { name: "rucusuario", nullable: true, length: 15 })
  rucusuario: string | null;

  @Column("char", { name: "tlfusuario", nullable: true, length: 15 })
  tlfusuario: string | null;

  @Column("char", { name: "tlfusuario1", nullable: true, length: 15 })
  tlfusuario1: string | null;

  @Column("numeric", {
    name: "password",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  password: number | null;

  @Column("bit", { name: "flgdscto", nullable: true })
  flgdscto: boolean | null;

  @Column("bit", { name: "flgborraritem", nullable: true })
  flgborraritem: boolean | null;

  @Column("bit", { name: "flganular", nullable: true })
  flganular: boolean | null;

  @Column("bit", { name: "flgTurno", nullable: true })
  flgTurno: boolean | null;

  @Column("bit", { name: "flgCierreX", nullable: true })
  flgCierreX: boolean | null;

  @Column("bit", { name: "flgCierreZ", nullable: true })
  flgCierreZ: boolean | null;

  @Column("char", { name: "dniusuario", nullable: true, length: 15 })
  dniusuario: string | null;

  @Column("bit", { name: "flgAutoVtaManual", nullable: true })
  flgAutoVtaManual: boolean | null;

  @Column("bit", { name: "flgInactivo", nullable: true })
  flgInactivo: boolean | null;

  @Column("bit", { name: "flgAfiliar", nullable: true })
  flgAfiliar: boolean | null;

  @Column("bit", { name: "flgModIngSal", nullable: true })
  flgModIngSal: boolean | null;
}
