import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s12", ["cdnivel", "cdmodulo"], { unique: true })
@Entity("tab0s12", { schema: "dbo" })
export class Tab0s12 {
  @Column("char", { primary: true, name: "cdnivel", length: 2 })
  cdnivel: string;

  @Column("char", { primary: true, name: "cdmodulo", length: 10 })
  cdmodulo: string;
}
