import { Column, Entity, Index } from "typeorm";
// *los length cambiaran
@Index("pk_tab0s07", ["cdbanco"], { unique: true })
@Entity("tab0s07", { schema: "dbo" })
export class Tab0s07_Banco {
  // el id, sera reemplazado por un id autogenerado
  // este campo 'muere'
  @Column("char", { primary: true, name: "cdbanco", length: 4 })
  cdbanco: string;

  //se le añadira este campo: nombre
  // length => 100
  // nullable => false
  name: string;

  //descripcion
  //length => 500
  //nullable => true
  @Column("char", { name: "dsbanco", nullable: true, length: 40 })
  descripcion: string | null;

  //direccion
  //length => 250
  //nullable => true
  @Column("char", { name: "drbanco", nullable: true, length: 60 })
  address: string | null;

  //telefono => sera un array de telefonos ahora
  //length => 20 (cada telefono) 
  @Column("char", { name: "tlfbanco", nullable: true, length: 15 })
  telefonos: string | null;// telefonos: string[]

  //esta campo muere
  @Column("char", { name: "tlfbanco1", nullable: true, length: 15 })
  tlfbanco1: string | null;

  //ruc 
  // unique
  //length => 30
  //nullable => false
  @Column("char", { name: "rucbanco", nullable: true, length: 15 })
  ruc: string | null;
}
