import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Department } from './department.entity';
@Entity()
export class Country {
  @ApiProperty({ uniqueItems: true })
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @ApiProperty({ uniqueItems: true, maxLength: 80, title: 'Nombre' })
  @Column('varchar', { unique: true, length: 80 })
  name: string;

  @ApiProperty({ uniqueItems: true, maxLength: 3, title: 'Acrónimo' })
  @Column('varchar', { unique: true, length: 3, nullable: true })
  acronym: string;

  @ApiProperty({ uniqueItems: true, maxLength: 80, title: 'Nacionalidad' })
  @Column('varchar', { unique: true, length: 80, nullable: true })
  nationality: string;

  @ApiProperty({ default: true, title: 'Estado' })
  @Column('boolean', { default: true })
  isActive: boolean;

  @ApiProperty({ type: () => [Department], title: 'Departamentos' })
  @OneToMany(() => Department, (department) => department.country, {
    cascade: true,
  })
  departments?: Department[];
}
