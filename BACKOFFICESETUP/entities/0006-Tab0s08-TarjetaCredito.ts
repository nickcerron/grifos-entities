import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s08", ["cdtarjeta"], { unique: true })
@Entity("tab0s08", { schema: "dbo" })
export class Tab0s08 {

  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column("char", { primary: true, name: "cdtarjeta", length: 2 })
  cdtarjeta: string;

  //name
  //length => 50
  //nullable => true
  //unique  => true
  @Column("char", { name: "dstarjeta", nullable: true, length: 20 })
  dstarjeta: string | null;

  //?en la bd todos los datos son nulos
  //?muere
  @Column("varchar", { name: "c_cuenta", nullable: true, length: 12 })
  cCuenta: string | null;

  //*code_pos
  //cod_pos
  //length => 10
  //nullable =>true
  @Column("char", { name: "cdpos", nullable: true, length: 10 })
  cdpos: string | null;
}
