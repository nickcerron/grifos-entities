import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s15", ["cdtranspor"], { unique: true })
@Entity("tab0s15", { schema: "dbo" })
export class Tab0s15 {
  @Column("char", { primary: true, name: "cdtranspor", length: 50 })
  cdtranspor: string;

  @Column("char", { name: "dstranspor", nullable: true, length: 60 })
  dstranspor: string | null;

  @Column("char", { name: "drtranspor", nullable: true, length: 60 })
  drtranspor: string | null;

  @Column("char", { name: "ructranspor", nullable: true, length: 15 })
  ructranspor: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "tlftranspor", nullable: true, length: 15 })
  tlftranspor: string | null;

  @Column("char", { name: "marcavehic", nullable: true, length: 15 })
  marcavehic: string | null;

  @Column("char", { name: "inscripcion", nullable: true, length: 15 })
  inscripcion: string | null;
}
