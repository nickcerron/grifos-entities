import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s14", ["cdmoneda"], { unique: true })
@Entity("tab0s14", { schema: "dbo" })
export class Tab0s14 {
  @Column("char", { name: "orden", nullable: true, length: 1 })
  orden: string | null;

  @Column("char", { primary: true, name: "cdmoneda", length: 1 })
  cdmoneda: string;

  @Column("char", { name: "dsmoneda", nullable: true, length: 40 })
  dsmoneda: string | null;

  @Column("char", { name: "smbmoneda", nullable: true, length: 4 })
  smbmoneda: string | null;

  @Column("varchar", { name: "cdiso", nullable: true, length: 3 })
  cdiso: string | null;
}
