import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s13", ["cdtipodoc", "cdformato"], { unique: true })
@Entity("tab0s13", { schema: "dbo" })
export class Tab0s13 {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "cdformato", length: 20 })
  cdformato: string;

  @Column("char", { name: "dsformato", nullable: true, length: 60 })
  dsformato: string | null;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nroitem: number | null;

  @Column("tinyint", { name: "maxND_linea", nullable: true })
  maxNdLinea: number | null;

  @Column("tinyint", { name: "maxlineas", nullable: true })
  maxlineas: number | null;
}
