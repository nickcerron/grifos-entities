import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s11", ["cdnivel"], { unique: true })
@Entity("tab0s11", { schema: "dbo" }) 
//*ADMINISTRADOR,CAJERO, (ROLES CREO)
export class Tab0s11 {
  @Column("char", { primary: true, name: "cdnivel", length: 2 })
  cdnivel: string;

  @Column("char", { name: "dsnivel", nullable: true, length: 40 })
  dsnivel: string | null;
}
