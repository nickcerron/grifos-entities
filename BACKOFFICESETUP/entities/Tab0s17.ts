import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s17", ["cdmodulo"], { unique: true })
@Entity("tab0s17", { schema: "dbo" })
export class Tab0s17 {
  @Column("char", { primary: true, name: "cdmodulo", length: 10 })
  cdmodulo: string;

  @Column("char", { name: "dsmodulo", nullable: true, length: 60 })
  dsmodulo: string | null;
}
