import { Column, Entity, Index } from 'typeorm';

@Index('pk_tab0s03', ['cdtipodoc'], { unique: true })
@Entity('tab0s03', { schema: 'dbo' })
export class Tab0s03 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtipodoc', length: 5 })
  cdtipodoc: string;

  //name
  //nullable => false
  @Column('char', { name: 'dstipodoc', nullable: true, length: 40 })
  dstipodoc: string | null;

  //abbreviation
  //length => 10
  @Column('char', { name: 'abrtipodoc', nullable: true, length: 5 })
  abrtipodoc: string | null;

  //business_doc
  //nullable => false
  //default => true
  @Column('bit', { name: 'flgcomercial', nullable: true })
  flgcomercial: boolean | null;

  //* NO SE VISUALIZA EN REMOTO
  //?muere
  //los datos en la db son true (ALL)
  @Column('bit', { name: 'flgsistema', nullable: true })
  flgsistema: boolean | null;

  //?muere
  // los datos en la db son null (ALL)
  @Column('numeric', {
    name: 'MtoMaximo',
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtoMaximo: number | null;

  //sele añadira una relacion con compañia
  //campo en db: company_id
}
