import { Column, Entity, Index } from "typeorm";

@Index("pk_tab0s18", ["cdProvincia"], { unique: true })
@Entity("tab0s18", { schema: "dbo" })
export class Tab0s18 {
  //id:  ahora sera identity
  //bigint
  @Column("char", { primary: true, name: "cdProvincia", length: 5 })
  cdProvincia: string;

  //name: nombre del distrito
  //varchar(80)
  //nullable: false
  @Column("varchar", { name: "dsProvincia", nullable: true, length: 80 })
  dsProvincia: string | null;

  //ahora sera una referencia a la tabla Departamento
  //nombre en la db departamento_id
  @Column("char", { name: "cdDepartamento", nullable: true, length: 2 })
  cdDepartamento: string | null;

  //ubigeo
  //varchar(10)
  @Column("char", { name: "codUbigeo", nullable: true, length: 2 })
  codUbigeo: string | null;

  //se agregara el campo 
  //zip_code (codigo postal)
  zip_code: string | null;
  //varchar(10)
  //nullable: true
}
