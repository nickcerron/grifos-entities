import { Column, Entity, Index } from 'typeorm';

@Index('pk_tab0s04', ['fecha'], { unique: true })
@Entity('tab0s04', { schema: 'dbo' })
export class Tab0s04 {
  //*id, sera agregara por un id autogenerado
  //*id campo será bigint

  //date
  //type => date
  //nullable => false
  //default => now()
  @Column('datetime', { name: 'fecha' })
  fecha: Date;

  //amount
  //nullable => false
  //precision => 10
  @Column('numeric', {
    name: 'cambioc',
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cambioc: number | null;

  //sele añadira una relacion con compañia
  //campo en db: company_id
}
