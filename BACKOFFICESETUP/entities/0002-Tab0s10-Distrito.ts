import { Column, Entity, Index } from "typeorm";

@Index("PK_tab0s10", ["cddistrito"], { unique: true })
@Entity("tab0s10", { schema: "dbo" })
export class Tab0s10 {
  //id:  ahora sera identity
  //bigint
  @Column("char", { primary: true, name: "cddistrito", length: 5 })
  cddistrito: string;

  //name: nombre del distrito
  //varchar(80)
  //nullable: false
  @Column("varchar", { name: "dsdistrito", nullable: true, length: 80 })
  dsdistrito: string | null;

  //ahora sera una referencia a la tabla Provincia
  //nombre en la db provincia_id
  @Column("char", { name: "cdProvincia", nullable: true, length: 5 })
  cdProvincia: string | null;


  //ubigeo
  //varchar(10)
  @Column("char", { name: "codUbigeo", nullable: true, length: 2 })
  codUbigeo: string | null;

  //se agregara el campo 
  //zip_code (codigo postal)
  zip_code: string | null;
  //varchar(10)
  //nullable: true
}
