import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_NDebitoD_Auditoria",
  ["fecelimina", "cdlocal", "nrondebito", "nroitem"],
  { unique: true }
)
@Entity("NDebitoD_Auditoria", { schema: "dbo" })
export class NDebitoDAuditoria {
  @Column("datetime", { primary: true, name: "fecelimina" })
  fecelimina: Date;

  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nrondebito", length: 10 })
  nrondebito: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("char", { name: "dsarticulo", nullable: true, length: 60 })
  dsarticulo: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;
}
