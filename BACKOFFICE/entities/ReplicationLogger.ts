import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_ReplicationLogger", ["id"], { unique: true })
@Entity("ReplicationLogger", { schema: "dbo" })
export class ReplicationLogger {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("smalldatetime", { name: "CreationTime" })
  creationTime: Date;

  @Column("varchar", { name: "Module", length: 255 })
  module: string;

  @Column("text", { name: "Message", nullable: true })
  message: string | null;
}
