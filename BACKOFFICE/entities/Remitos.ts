import { Column, Entity, Index } from "typeorm";

@Index("PK_remitos", ["cdlocal", "cdremito", "nroremito"], { unique: true })
@Entity("Remitos", { schema: "dbo" })
export class Remitos {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdremito", length: 5 })
  cdremito: string;

  @Column("char", { primary: true, name: "nroremito", length: 12 })
  nroremito: string;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 8,
    scale: 4,
  })
  tcambio: number | null;

  @Column("int", { name: "nropaquetes", nullable: true })
  nropaquetes: number | null;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolares",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 200 })
  observacion: string | null;

  @Column("datetime", { name: "fecregistra", nullable: true })
  fecregistra: Date | null;

  @Column("datetime", { name: "fecmodifica", nullable: true })
  fecmodifica: Date | null;

  @Column("varchar", { name: "usuregistra", nullable: true, length: 20 })
  usuregistra: string | null;

  @Column("varchar", { name: "usumodifica", nullable: true, length: 20 })
  usumodifica: string | null;

  @Column("char", { name: "estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("int", { name: "nroPaqSoles", nullable: true })
  nroPaqSoles: number | null;

  @Column("int", { name: "nroPaqDolares", nullable: true })
  nroPaqDolares: number | null;

  @Column("char", { name: "nrobolsa", nullable: true, length: 15 })
  nrobolsa: string | null;

  @Column("char", { name: "nrocuentadolares", nullable: true, length: 20 })
  nrocuentadolares: string | null;
}
