import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("SeguimientoMargenDiario", { schema: "dbo" })
export class SeguimientoMargenDiario {
  @PrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @Column("smalldatetime", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "ARTICULO", nullable: true, length: 50 })
  articulo: string | null;

  @Column("varchar", { name: "MENSAJE", nullable: true, length: 1000 })
  mensaje: string | null;

  @Column("smalldatetime", { name: "FECHA_ERROR", nullable: true })
  fechaError: Date | null;
}
