import { Column, Entity, Index } from 'typeorm';

@Index('PK_articulo', ['cdarticulo'], { unique: true })
@Entity('articulo', { schema: 'dbo' })
//*Ahora es producto
export class Articulo {
  //!!no se usa, todo esta en '' blanco
  @Column('text', { name: 'glosa', nullable: true })
  glosa: string | null;

  //ahora 'id', sera uuid, pero debera aceptar los que ya existen
  @Column('char', { primary: true, name: 'cdarticulo', length: 20 })
  cdarticulo: string;

  //name, varchar not null
  @Column('char', { name: 'dsarticulo', nullable: true, length: 60 })
  dsarticulo: string | null;
  //description varchar 120
  @Column('char', { name: 'dsarticulo1', nullable: true, length: 60 })
  dsarticulo1: string | null;

  @Column('char', { name: 'cdgrupo01', nullable: true, length: 5 })
  cdgrupo01: string | null;

  //! estos tablas se convertiran en una sola con un campo extra
  //! que diga de que tipo de grupo es, relacion many to many
  // @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  // cdgrupo02: string | null;

  // @Column("char", { name: "cdgrupo03", nullable: true, length: 5 })
  // cdgrupo03: string | null;

  // @Column("char", { name: "cdgrupo04", nullable: true, length: 5 })
  // cdgrupo04: string | null;

  // @Column("char", { name: "cdgrupo05", nullable: true, length: 5 })
  // cdgrupo05: string | null;
  //! hasta aqui

  //referencia a unidad de medida
  @Column('char', { name: 'cdunimed', nullable: true, length: 5 })
  cdunimed: string | null;

  //referencia a talla o tamaño de producto
  @Column('char', { name: 'cdtalla', nullable: true, length: 5 })
  cdtalla: string | null;

  //indica si es un producto compuesto, como un cafe que en este caso ya no baja el stock principal sino, digamos, 200ml de leche, etc, compuestos
  //en la db todos los datos estan en 'A' para indicar lo anterior
  //el resto en ''
  //*ahora sera boolean
  @Column('char', { name: 'tpformula', nullable: true, length: 1 })
  tpformula: string | null;

  @Column('numeric', {
    name: 'impuesto',
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column('numeric', {
    name: 'impuesto1',
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto1: number | null;


  @Column('bit', { name: 'movimiento', nullable: true })
  movimiento: boolean | null;

  @Column('bit', { name: 'vtaxmonto', nullable: true })
  vtaxmonto: boolean | null;

  @Column('smalldatetime', { name: 'fecinicial', nullable: true })
  fecinicial: Date | null;

  @Column('char', { name: 'monctoinicial', nullable: true, length: 1 })
  monctoinicial: string | null;

  @Column('numeric', {
    name: 'ctoinicial',
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinicial: number | null;

  @Column('smalldatetime', { name: 'fecinventario', nullable: true })
  fecinventario: Date | null;

  @Column('char', { name: 'monctoinventario', nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column('numeric', {
    name: 'ctoinventario',
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column('smalldatetime', { name: 'fecedicion', nullable: true })
  fecedicion: Date | null;

  //equivalence
  @Column('numeric', {
    name: 'equivalencia',
    nullable: true,
    precision: 11,
    scale: 3,
  })
  equivalencia: number | null;

  //referencia a unidad de medida
  @Column('char', { name: 'cdmedequiv', nullable: true, length: 5 })
  cdmedequiv: string | null;

  @Column('bit', { name: 'flgpromocion', nullable: true })
  flgpromocion: boolean | null;

  @Column('char', { name: 'tpconversion', nullable: true, length: 1 })
  tpconversion: string | null;

  @Column('numeric', {
    name: 'valorconversion',
    nullable: true,
    precision: 10,
    scale: 5,
  })
  valorconversion: number | null;


  @Column('numeric', {
    name: 'PrecioAfiliacion',
    nullable: true,
    precision: 10,
    scale: 0,
  })
  precioAfiliacion: number | null;

  @Column('varchar', { name: 'c_cuenta', nullable: true, length: 12 })
  cCuenta: string | null;

  @Column('bit', { name: 'Is_EasyTaxi', nullable: true, default: () => '(0)' })
  isEasyTaxi: boolean | null;

  @Column('char', { name: 'ctacompra', nullable: true, length: 12 })
  ctacompra: string | null;

  @Column('char', { name: 'ctacosto', nullable: true, length: 12 })
  ctacosto: string | null;

  @Column('char', { name: 'ctaalmacen', nullable: true, length: 12 })
  ctaalmacen: string | null;

  @Column('numeric', {
    name: 'PuntosFidelizacion',
    nullable: true,
    precision: 10,
    scale: 0,
  })
  puntosFidelizacion: number | null;

  @Column('numeric', {
    name: 'CantFidelizacion',
    nullable: true,
    precision: 10,
    scale: 0,
  })
  cantFidelizacion: number | null;

  @Column('numeric', {
    name: 'MontoFidelizacion',
    nullable: true,
    precision: 10,
    scale: 0,
  })
  montoFidelizacion: number | null;

  @Column('char', { name: 'Ticketfactura', nullable: true, length: 10 })
  ticketfactura: string | null;



  @Column('bit', { name: 'Flg_Exonerado', nullable: true })
  flgExonerado: boolean | null;

  @Column('bit', { name: 'Flg_Inafecto', nullable: true })
  flgInafecto: boolean | null;


  //flag flete ahora sera boolean, no null
  //*pendiente funcionalidad
  @Column('bit', { name: 'Flete', nullable: true })
  flete: boolean | null;

  //impuesto al plastico, ahora sera boolean, no null
  @Column('bit', { name: 'ImpPlastico', nullable: true })
  impPlastico: boolean | null;

  //transferencia gratuita, ahora sera boolean, no null
  //*pendiente funcionalidad
  @Column('bit', { name: 'trfgratuita', nullable: true })
  trfgratuita: boolean | null;

  //bloqueo de venta, ahora sera boolean, no null
  @Column('bit', { name: 'bloqvta', nullable: true })
  bloqvta: boolean | null;

  //bloqueo de compra, ahora sera boolean, no null
  @Column('bit', { name: 'bloqcom', nullable: true })
  bloqcom: boolean | null;

  //bloqueo general, ahora sera boolean, no null
  @Column('bit', { name: 'bloqgral', nullable: true })
  bloqgral: boolean | null;

  // virtual, ahora sera boolean, no null
  //*pendiente funcionalidadm para saber si tiene venta virtual?
  @Column('bit', { name: 'virtual', nullable: true })
  virtual: boolean | null;

  //ahora sera boolean, no null, la mayoria de valores en 0
  //*pendiente funcionalidad
  @Column('bit', { name: 'usadecimales', nullable: true })
  usadecimales: boolean | null;

  //Puede ser la flag GenerarStock
  //*pendiente funcionalidad
  //Flag Mover Stock ahora sera boolean, no null
  @Column('bit', { name: 'moverstock', nullable: true })
  moverstock: boolean | null;

  //*pendiente funcionalidad
  //Flag Glosa ahora sera boolean, no null
  @Column('bit', { name: 'flgglosa', nullable: true })
  flgglosa: boolean | null;

  //*pendiente funcionalidad
  //Consignacion ahora sera boolean, no null
  @Column('bit', { name: 'consignacion', nullable: true })
  consignacion: boolean | null;

  //*pendiente funcionalidad<
  //flav Producto en Venta, ahora sera boolean, no null
  @Column('bit', { name: 'venta', nullable: true })
  venta: boolean | null;

  //*pendiente funcionalidad
  //! todo en null, para eliminar
  @Column('char', { name: 'ctaventa', nullable: true, length: 12 })
  ctaventa: string | null;

  //*pendiente funcionalidad
  //! codigo articulo sunat, funcionalidad pendiente
  @Column('varchar', { name: 'cdarticulosunat', nullable: true, length: 20 })
  cdarticulosunat: string | null;

  //TODO
  //! Tal vez sea Codigo articulo Proliat, si es asi todo en ''
  //*pendiente funcionalidad
  @Column('varchar', { name: 'cdarticulovulcano', nullable: true, length: 12 })
  cdarticulovulcano: string | null;

  //todo
  //! todo en '', para eliminar?
  //*pendiente funcionalidad
  @Column('varchar', { name: 'c_cuenta_ventas', nullable: true, length: 12 })
  cCuentaVentas: string | null;

  //referencia a tipo de percepcion
  //*pendiente funcionalidad
  @Column('char', { name: 'cdtppercepcion', nullable: true, length: 3 })
  cdtppercepcion: string | null;

  //! todos los valores en '0.00'
  //porcentaje de percepcion 6 y 3
  //*pendiente funcionalidad
  @Column('numeric', {
    name: 'porcpercepcion',
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porcpercepcion: number | null;

  //referencia a tipo de detraccion
  //! todo en null
  //*pendiente funcionalidad
  @Column('char', { name: 'cdtpdetraccion', nullable: true, length: 3 })
  cdtpdetraccion: string | null;

  //monto de detraccion 10 y 3
  // Base Aplica Detraccion
  //*pendiente funcionalidad
  //! todo en '0.00'
  @Column('numeric', {
    name: 'MtoBaseDetraccion',
    nullable: true,
    precision: 10,
    scale: 2,
  })
  mtoBaseDetraccion: number | null;

  //porcentaje de detraccion 6 y 3
  //*pendiente funcionalidad
  //! todo en null
  @Column('numeric', {
    name: 'porcdetraccion',
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porcdetraccion: number | null;

  //*pendiente funcionalidad
  //utily margin 6 y 3
  @Column('numeric', {
    name: 'mgutilidad',
    nullable: true,
    precision: 6,
    scale: 2,
  })
  mgutilidad: number | null;

  //*pendiente funcionalidad
  //! todo en la db esta como '', no se usa, para eliminar
  @Column('varchar', { name: 'c_cuenta_compras', nullable: true, length: 12 })
  cCuentaCompras: string | null;

  //*pendiente funcionalidad
  //! todo en la db esta como '', no se usa, para eliminar
  @Column('varchar', { name: 'c_centrocosto', nullable: true, length: 12 })
  cCentrocosto: string | null;

  //*pendiente funcionalidad
  //moneda currency ahora sera una referencia a la tabla moneda
  @Column('char', { name: 'monctorepo', nullable: true, length: 1 })
  monctorepo: string | null;

  //*pendiente funcionalidad
  //costo reposicion
  @Column('numeric', {
    name: 'ctoreposicion',
    nullable: true,
    precision: 16,
    scale: 8,
  })
  ctoreposicion: number | null;

  //*pendiente funcionalidad
  //moneda currency ahora sera una referencia a la tabla moneda
  @Column('char', { name: 'monctoprom', nullable: true, length: 1 })
  monctoprom: string | null;

  //*pendiente funcionalidad
  //costo promedio 16 y 4
  @Column('numeric', {
    name: 'ctopromedio',
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctopromedio: number | null;

  //*pendiente funcionalidad
  //SKU o codigo de barras varchar 10
  @Column('char', { name: 'cdamarre', nullable: true, length: 6 })
  cdamarre: string | null;

  //*pendiente funcionalidad
  //weight
  @Column('numeric', { name: 'peso', nullable: true, precision: 18, scale: 5 })
  peso: number | null;
}
