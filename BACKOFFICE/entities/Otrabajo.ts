import { Column, Entity, Index } from "typeorm";

@Index("PK_otrabajo", ["nrootrabajo"], { unique: true })
@Entity("otrabajo", { schema: "dbo" })
export class Otrabajo {
  @Column("char", { primary: true, name: "nrootrabajo", length: 10 })
  nrootrabajo: string;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("smalldatetime", { name: "fecotrabajo", nullable: true })
  fecotrabajo: Date | null;

  @Column("smalldatetime", { name: "fecentrega", nullable: true })
  fecentrega: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  tcambio: number | null;

  @Column("char", { name: "cdtipootrabajo", nullable: true, length: 2 })
  cdtipootrabajo: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("numeric", {
    name: "kilometraje",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  kilometraje: number | null;

  @Column("bit", { name: "flgconcluida", nullable: true })
  flgconcluida: boolean | null;

  @Column("bit", { name: "flgcortesia", nullable: true })
  flgcortesia: boolean | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtoventa",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoventa: number | null;
}
