import { Column, Entity } from "typeorm";

@Entity("TmpSalidaDWebS", { schema: "dbo" })
export class TmpSalidaDWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "SalidaId", length: 5 })
  salidaId: string;

  @Column("char", { name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("numeric", { name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("int", { name: "AlmacenId" })
  almacenId: number;

  @Column("char", {
    name: "ArticuloId_Alternativo",
    nullable: true,
    length: 20,
  })
  articuloIdAlternativo: string | null;

  @Column("varchar", { name: "NroIngreso", length: 10 })
  nroIngreso: string;

  @Column("float", { name: "cantidad", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("float", { name: "costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "mtosubtotal", nullable: true, precision: 53 })
  mtosubtotal: number | null;

  @Column("float", { name: "mtoimpuesto", nullable: true, precision: 53 })
  mtoimpuesto: number | null;

  @Column("float", { name: "mtoimpuesto1", nullable: true, precision: 53 })
  mtoimpuesto1: number | null;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
