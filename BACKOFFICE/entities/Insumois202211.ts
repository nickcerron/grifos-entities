import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Insumois202211",
  [
    "cdlocal",
    "nroseriemaq",
    "movimiento",
    "cdtpmov",
    "nromov",
    "cdtipodoc",
    "nrodocumento",
    "nroitem",
    "cdarticulo",
    "talla",
  ],
  { unique: true }
)
@Entity("Insumois202211", { schema: "dbo" })
export class Insumois202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroseriemaq", length: 20 })
  nroseriemaq: string;

  @Column("char", { primary: true, name: "cdtpmov", length: 5 })
  cdtpmov: string;

  @Column("char", { primary: true, name: "nromov", length: 10 })
  nromov: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 15 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "movimiento", length: 1 })
  movimiento: string;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  ctoreposicion: number | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  ctopromedio: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("bit", { name: "flganulacion", nullable: true })
  flganulacion: boolean | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;
}
