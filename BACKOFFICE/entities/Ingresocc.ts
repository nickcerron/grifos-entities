import { Column, Entity, Index } from "typeorm";

@Index("PK_ingresocc", ["cdtpingreso", "fechora"], { unique: true })
@Entity("ingresocc", { schema: "dbo" })
export class Ingresocc {
  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "dsproveedor", nullable: true, length: 60 })
  dsproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("char", { name: "observacion", nullable: true, length: 50 })
  observacion: string | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;
}
