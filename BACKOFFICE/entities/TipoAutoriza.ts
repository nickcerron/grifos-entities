import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoAutoriza", ["cdtpAutoriza"], { unique: true })
@Entity("TipoAutoriza", { schema: "dbo" })
export class TipoAutoriza {
  @Column("char", { primary: true, name: "cdtpAutoriza", length: 2 })
  cdtpAutoriza: string;

  @Column("varchar", { name: "dstpAutoriza", length: 120 })
  dstpAutoriza: string;

  @Column("bit", { name: "FlgMostrar", nullable: true })
  flgMostrar: boolean | null;
}
