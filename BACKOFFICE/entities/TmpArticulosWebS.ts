import { Column, Entity } from "typeorm";

@Entity("TmpArticulosWebS", { schema: "dbo" })
export class TmpArticulosWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 60 })
  descripcion: string | null;

  @Column("char", { name: "GrupoId01", length: 5 })
  grupoId01: string;

  @Column("char", { name: "GrupoId02", length: 5 })
  grupoId02: string;

  @Column("char", { name: "GrupoId03", length: 5 })
  grupoId03: string;

  @Column("char", { name: "GrupoId04", length: 5 })
  grupoId04: string;

  @Column("bit", { name: "tpformula", nullable: true })
  tpformula: boolean | null;

  @Column("bit", { name: "flgpromocion", nullable: true })
  flgpromocion: boolean | null;

  @Column("bit", { name: "trfgratuita", nullable: true })
  trfgratuita: boolean | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("char", { name: "Medida", nullable: true, length: 5 })
  medida: string | null;
}
