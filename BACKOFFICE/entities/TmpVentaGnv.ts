import { Column, Entity } from "typeorm";

@Entity("Tmp_VentaGNV", { schema: "dbo" })
export class TmpVentaGnv {
  @Column("varchar", { name: "RECIBO", nullable: true, length: 15 })
  recibo: string | null;

  @Column("smalldatetime", { name: "FECHA_HORA_DESPACHO", nullable: true })
  fechaHoraDespacho: Date | null;

  @Column("varchar", { name: "HORA_DESPACHO", nullable: true, length: 10 })
  horaDespacho: string | null;

  @Column("smalldatetime", { name: "FECHA_TURNO", nullable: true })
  fechaTurno: Date | null;

  @Column("varchar", { name: "RUC", nullable: true, length: 11 })
  ruc: string | null;

  @Column("varchar", { name: "TIPODOC", nullable: true, length: 30 })
  tipodoc: string | null;

  @Column("varchar", { name: "NRODOCUMENTO", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("varchar", { name: "PLACA", nullable: true, length: 10 })
  placa: string | null;

  @Column("varchar", { name: "SURTIDOR", nullable: true, length: 11 })
  surtidor: string | null;

  @Column("varchar", { name: "CARA", nullable: true, length: 10 })
  cara: string | null;

  @Column("numeric", {
    name: "LECTURA_INICIAL",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  lecturaInicial: number | null;

  @Column("numeric", {
    name: "LECTURA_FINAL",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  lecturaFinal: number | null;

  @Column("numeric", {
    name: "PRECIO_M3",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  precioM3: number | null;

  @Column("numeric", {
    name: "CANT_M3",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  cantM3: number | null;

  @Column("numeric", {
    name: "CANT_M3_MOD",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  cantM3Mod: number | null;

  @Column("varchar", { name: "FORMA_PAGO", nullable: true, length: 30 })
  formaPago: string | null;

  @Column("numeric", {
    name: "DESCUENTO",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  descuento: number | null;

  @Column("numeric", {
    name: "INCREMENTO",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  incremento: number | null;

  @Column("numeric", {
    name: "RECAUDO",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  recaudo: number | null;

  @Column("numeric", { name: "VALOR", nullable: true, precision: 12, scale: 2 })
  valor: number | null;

  @Column("numeric", {
    name: "redondea_indecopi",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  redondeaIndecopi: number | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "dsusuario", nullable: true, length: 60 })
  dsusuario: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "RAZON_SOCIAL", nullable: true, length: 120 })
  razonSocial: string | null;
}
