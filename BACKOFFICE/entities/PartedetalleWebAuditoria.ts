import { Column, Entity } from "typeorm";

@Entity("PartedetalleWeb_auditoria", { schema: "dbo" })
export class PartedetalleWebAuditoria {
  @Column("char", { name: "cdlocal", nullable: true, length: 4 })
  cdlocal: string | null;

  @Column("datetime", { name: "Fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "Grupo", nullable: true, length: 1 })
  grupo: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "dsarticulo", nullable: true, length: 40 })
  dsarticulo: string | null;

  @Column("numeric", {
    name: "inicial",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  inicial: number | null;

  @Column("numeric", {
    name: "compra",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  compra: number | null;

  @Column("numeric", {
    name: "Consumo",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumo: number | null;

  @Column("numeric", { name: "Venta", nullable: true, precision: 12, scale: 3 })
  venta: number | null;

  @Column("numeric", { name: "Final", nullable: true, precision: 12, scale: 3 })
  final: number | null;

  @Column("numeric", {
    name: "varillaje",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  varillaje: number | null;

  @Column("numeric", {
    name: "difdia",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difdia: number | null;

  @Column("numeric", {
    name: "difmes",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difmes: number | null;

  @Column("numeric", {
    name: "Variacion",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  variacion: number | null;

  @Column("numeric", { name: "Total", nullable: true, precision: 12, scale: 2 })
  total: number | null;

  @Column("smalldatetime", {
    name: "Fecsistema",
    nullable: true,
    default: () => "getdate()",
  })
  fecsistema: Date | null;

  @Column("varchar", { name: "proceso", nullable: true, length: 15 })
  proceso: string | null;
}
