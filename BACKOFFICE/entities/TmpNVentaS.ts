import { Column, Entity } from "typeorm";

@Entity("TMP_NVentaS", { schema: "dbo" })
export class TmpNVentaS {
  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nroseriemaq", length: 15 })
  nroseriemaq: string;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", { name: "NroItem", precision: 3, scale: 0 })
  nroItem: number;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 18,
    scale: 8,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 19,
    scale: 8,
  })
  mtoimpuesto: number | null;
}
