import { Column, Entity, Index } from "typeorm";

@Index("PK_vale", ["nrovale"], { unique: true })
@Entity("vale", { schema: "dbo" })
export class Vale {
  @Column("char", { primary: true, name: "nrovale", length: 10 })
  nrovale: string;

  @Column("smalldatetime", { name: "fecvale", nullable: true })
  fecvale: Date | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtovale",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtovale: number | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("bit", { name: "flgbloqueado", nullable: true })
  flgbloqueado: boolean | null;
}
