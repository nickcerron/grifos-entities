import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('PRECIOGRUPOCLI', { schema: 'dbo' })
export class Preciogrupocli {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @PrimaryGeneratedColumn({ type: 'int', name: 'cdPrecioGCliente' })
  cdPrecioGCliente: number;

  // groupClient
  // nullable => false,
  // type => Grupocliente (relation)
  @Column('char', { name: 'CDGRUPOCLI', nullable: true, length: 5 })
  cdgrupocli: string | null;

  // paymentMethod
  @Column('char', { name: 'TIPOCLI', nullable: true, length: 3 })
  tipocli: string | null;

  // product
  // nullable => false
  // type => Product (relation)
  @Column('char', { name: 'CDARTICULO', nullable: true, length: 20 })
  cdarticulo: string | null;

  //discount_price
  // precision: 10,
  // scale: 4,
  @Column('numeric', {
    name: 'PRECIO',
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  //discountType
  //nullable => false
  @Column('char', { name: 'TIPODES', nullable: true, length: 3 })
  tipodes: string | null;
}
