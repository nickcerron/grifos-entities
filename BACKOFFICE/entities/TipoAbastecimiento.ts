import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoAbastecimiento", ["cdTpAbastecimiento"], { unique: true })
@Entity("TipoAbastecimiento", { schema: "dbo" })
export class TipoAbastecimiento {
  @Column("char", { primary: true, name: "cdTpAbastecimiento", length: 10 })
  cdTpAbastecimiento: string;

  @Column("char", { name: "dsAbastecimiento", length: 50 })
  dsAbastecimiento: string;
}
