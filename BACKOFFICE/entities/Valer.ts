import { Column, Entity, Index } from "typeorm";

@Index("PK_valer", ["nrovale"], { unique: true })
@Entity("valer", { schema: "dbo" })
export class Valer {
  @Column("char", { primary: true, name: "nrovale", length: 10 })
  nrovale: string;
}
