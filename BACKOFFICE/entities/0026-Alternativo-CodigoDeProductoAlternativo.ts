import { Column, Entity, Index } from "typeorm";

@Index("PK_alternativo", ["cdalterna"], { unique: true })
@Entity("alternativo", { schema: "dbo" })
export class Alternativo {
  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
  @Column("char", { primary: true, name: "cdalterna", length: 20 })
  cdalterna: string;
}
