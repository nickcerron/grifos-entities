import { Column, Entity } from "typeorm";

@Entity("TmpRepPrecioClienteWebS", { schema: "dbo" })
export class TmpRepPrecioClienteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("varchar", { name: "TipoCli", length: 3 })
  tipoCli: string;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("numeric", {
    name: "Precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("varchar", { name: "TipoDes", nullable: true, length: 3 })
  tipoDes: string | null;

  @Column("varchar", { name: "NroContrato", length: 60 })
  nroContrato: string;

  @Column("bit", { name: "Licitacion", nullable: true })
  licitacion: boolean | null;
}
