import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_F__7CFB65B28ED81CA1", ["periodo"], { unique: true })
@Entity("Kardex_Fisico_Periodo_Inicial", { schema: "dbo" })
export class KardexFisicoPeriodoInicial {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { primary: true, name: "Periodo", length: 6 })
  periodo: string;
}
