import { Column, Entity, Index } from "typeorm";

@Index("PK_proformadm", ["cdlocal", "nroproforma", "cdarticulo", "talla"], {
  unique: true,
})
@Entity("proformadm", { schema: "dbo" })
export class Proformadm {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { primary: true, name: "nroproforma", length: 10 })
  nroproforma: string;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nroitem: number | null;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("text", { name: "glosa", nullable: true })
  glosa: string | null;
}
