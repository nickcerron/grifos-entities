import { Column, Entity } from "typeorm";

@Entity("GrupoRutas", { schema: "dbo" })
export class GrupoRutas {
  @Column("char", { name: "CodigoGrupoRuta", nullable: true, length: 2 })
  codigoGrupoRuta: string | null;

  @Column("nchar", { name: "Descrip", nullable: true, length: 50 })
  descrip: string | null;

  @Column("nchar", { name: "DescripcionCorta", nullable: true, length: 5 })
  descripcionCorta: string | null;
}
