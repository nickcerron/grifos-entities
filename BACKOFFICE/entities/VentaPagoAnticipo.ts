import { Column, Entity, Index } from "typeorm";

@Index(
  "pk_Venta_PagoAnticipo",
  ["cdtipodoc", "nrodocumento", "ruccliente", "tipo", "fecha"],
  { unique: true }
)
@Entity("Venta_PagoAnticipo", { schema: "dbo" })
export class VentaPagoAnticipo {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "ruccliente", length: 15 })
  ruccliente: string;

  @Column("char", { primary: true, name: "tipo", length: 2 })
  tipo: string;

  @Column("numeric", {
    name: "consumo",
    nullable: true,
    precision: 16,
    scale: 2,
  })
  consumo: number | null;

  @Column("smalldatetime", { primary: true, name: "fecha" })
  fecha: Date;
}
