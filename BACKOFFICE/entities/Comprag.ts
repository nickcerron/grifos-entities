import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_comprag",
  [
    "cdlocal",
    "nrocompra",
    "cdtipodoc",
    "nrodocumento",
    "cdtipodoc1",
    "nrodocumento1",
    "cdproveedor",
  ],
  { unique: true }
)
@Entity("comprag", { schema: "dbo" })
export class Comprag {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nrocompra", length: 10 })
  nrocompra: string;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "cdtipodoc1", length: 5 })
  cdtipodoc1: string;

  @Column("char", { primary: true, name: "nrodocumento1", length: 10 })
  nrodocumento1: string;

  @Column("char", { primary: true, name: "cdproveedor", length: 15 })
  cdproveedor: string;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;
}
