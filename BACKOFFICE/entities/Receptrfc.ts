import { Column, Entity, Index } from "typeorm";

@Index("PK_receptrfc", ["cdlocorigen", "movimiento", "nromov"], {
  unique: true,
})
@Entity("receptrfc", { schema: "dbo" })
export class Receptrfc {
  @Column("char", { primary: true, name: "cdlocorigen", length: 3 })
  cdlocorigen: string;

  @Column("char", { primary: true, name: "movimiento", length: 1 })
  movimiento: string;

  @Column("char", { primary: true, name: "nromov", length: 10 })
  nromov: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fectransferencia", nullable: true })
  fectransferencia: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("char", { name: "nroingreso", nullable: true, length: 10 })
  nroingreso: string | null;

  @Column("bit", { name: "flgingreso", nullable: true })
  flgingreso: boolean | null;
}
