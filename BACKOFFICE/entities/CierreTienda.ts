import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_CierreTienda",
  ["cdlocal", "cdalmacen", "fecproceso", "turno", "cdarticulo"],
  { unique: true }
)
@Entity("CierreTienda", { schema: "dbo" })
export class CierreTienda {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "ventas",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ventas: number | null;

  @Column("numeric", {
    name: "ingresos",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ingresos: number | null;

  @Column("numeric", {
    name: "salidas",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  salidas: number | null;

  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;
}
