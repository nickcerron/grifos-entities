import { Column, Entity, Index } from "typeorm";

@Index("PK_proveedor", ["cdproveedor"], { unique: true })
@Entity("proveedor", { schema: "dbo" })
export class Proveedor {
  //ahora ser id, bigint, autoincremental
  @Column("char", { primary: true, name: "cdproveedor", length: 20 })
  cdproveedor: string;

  //name
  @Column("varchar", { name: "dsproveedor", nullable: true, length: 120 })
  dsproveedor: string | null;

  //ruc
  //varchar 20
  @Column("char", { name: "rucproveedor", nullable: true, length: 15 })
  rucproveedor: string | null;

  //address
  @Column("varchar", { name: "drproveedor", nullable: true, length: 120 })
  drproveedor: string | null;

  //*phones varchar[]
  // @Column("char", { name: "tlfproveedor", nullable: true, length: 15 })
  // tlfproveedor: string | null;

  // @Column("char", { name: "tlfproveedor1", nullable: true, length: 15 })
  // tlfproveedor1: string | null;

  //fax varchar 15
  @Column("char", { name: "faxproveedor", nullable: true, length: 15 })
  faxproveedor: string | null;

  //email varchar 120
  @Column("varchar", { name: "emproveedor", nullable: true, length: 120 })
  emproveedor: string | null;

  //* Esto se transformara en la entidad TipoProveedor
  //? pendiente en la db todo esta en null
  //@Column("bit", { name: "Tipo_Proveedor", nullable: true })
  //tipoProveedor: boolean | null;

  //? descripcionTipoProveedor todo esta en null en la db
  //*ahora sera name: nombre del tipo de proveedor unique not null
  //@Column("varchar", { name: "dstipo_proveedor", nullable: true, length: 60 })
  //dstipoProveedor: string | null;

  //? todo esta en null en la db
  @Column("varchar", { name: "contacto_proveedor", nullable: true, length: 60 })
  contactoProveedor: string | null;
}
