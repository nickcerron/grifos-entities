import { Column, Entity } from "typeorm";

@Entity("HVale_WEBMIS", { schema: "dbo" })
export class HValeWebmis {
  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "nrovale", length: 10 })
  nrovale: string;

  @Column("smalldatetime", { name: "fecvale", nullable: true })
  fecvale: Date | null;

  @Column("varchar", { name: "Moneda", nullable: true, length: 3 })
  moneda: string | null;

  @Column("numeric", {
    name: "mtovale",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtovale: number | null;

  @Column("bigint", { name: "ClienteId", nullable: true })
  clienteId: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "NroSerieMaq", nullable: true, length: 15 })
  nroSerieMaq: string | null;

  @Column("char", { name: "CdTipoDoc", nullable: true, length: 5 })
  cdTipoDoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "NroSerieMaqFac", nullable: true, length: 15 })
  nroSerieMaqFac: string | null;

  @Column("char", { name: "cdtipodocfac", nullable: true, length: 5 })
  cdtipodocfac: string | null;

  @Column("char", { name: "nrodocumentofac", nullable: true, length: 10 })
  nrodocumentofac: string | null;

  @Column("smalldatetime", { name: "fecdocumentofac", nullable: true })
  fecdocumentofac: Date | null;

  @Column("smalldatetime", { name: "fecprocesofac", nullable: true })
  fecprocesofac: Date | null;

  @Column("char", { name: "placa", nullable: true, length: 10 })
  placa: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;
}
