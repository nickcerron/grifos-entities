import { Column, Entity, Index } from "typeorm";
import { Zona } from './0004-Zona-Zona';

@Index("PK_local", ["cdlocal", "cdzona"], { unique: true })
@Entity("local", { schema: "dbo" })
export class Local {
  //ahora ser id, bigint, autoincremental
  @Column("char", { primary: true, name: "cdlocal", length: 4 })
  cdlocal: string;

  //name
  @Column("char", { name: "dslocal", nullable: true, length: 50 })
  dslocal: string | null;

  //direccion
  @Column("char", { name: "drlocal", nullable: true, length: 60 })
  drlocal: string | null;

  //*telefonos varchar[]
  //@Column("char", { name: "tlflocal", nullable: true, length: 20 })
  //tlflocal: string | null;
  //*se fusionan
  //@Column("char", { name: "tlflocal1", nullable: true, length: 20 })
  //tlflocal1: string | null;

  //referencia a Zona, zone_id
  @Column("char", { primary: true, name: "cdzona", length: 5 })
  cdzona: string | Zona;

  //? pendiente, solo hay un dato en la dg y esta como ''
  //nroCentralizacion ahora se llamara centralization_number
  @Column("varchar", { name: "NRO_CENTRALIZACION", nullable: true, length: 50 })
  nroCentralizacion: string | null;

  //? referencia a la entidad Distrito
  @Column("char", { name: "dislocal", nullable: true, length: 60 })
  dislocal: string | null;

  //! esto se elimina pq con el distrito se saca la provincia
  @Column("char", { name: "provlocal", nullable: true, length: 60 })
  provlocal: string | null;
  //! esto se elimina pq con el distrito se saca el departamento tambien
  //!el campo se repite, pero este es el usado
  @Column("char", { name: "deplocal", nullable: true, length: 60 })
  deplocal: string | null;

  //? pendiente saber si hacer referencia a una entidad, en la db el unico valor que tiene '0060'
  @Column("char", { name: "cdsunat", nullable: true, length: 4 })
  cdsunat: string | null;

  //! esto se elimina pq con el distrito se saca el departamento tambien
  //! este campo se repite y no se usa, muere
  @Column("char", { name: "cdDepartamento", nullable: true, length: 2 })
  cdDepartamento: string | null;

  @Column("char", { name: "cdProvincia", nullable: true, length: 5 })
  cdProvincia: string | null;

  @Column("char", { name: "cdDistrito", nullable: true, length: 5 })
  cdDistrito: string | null;
}
