import { Column, Entity, Index } from "typeorm";

@Index("PK_IslaPlaya", ["isla", "lado", "manguera", "cdarticulo"], {
  unique: true,
})
@Entity("IslaPlaya", { schema: "dbo" })
export class IslaPlaya {
  @Column("char", { primary: true, name: "isla", length: 2 })
  isla: string;

  @Column("char", { primary: true, name: "lado", length: 2 })
  lado: string;

  @Column("char", { primary: true, name: "manguera", length: 1 })
  manguera: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;
}
