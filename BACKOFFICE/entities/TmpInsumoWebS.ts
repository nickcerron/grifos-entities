import { Column, Entity } from "typeorm";

@Entity("TmpInsumoWebS", { schema: "dbo" })
export class TmpInsumoWebS {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("char", { name: "Movimiento", nullable: true, length: 1 })
  movimiento: string | null;

  @Column("int", { name: "NroItem" })
  nroItem: number;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("float", { name: "Cantidad", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("bit", { name: "Anulado", nullable: true })
  anulado: boolean | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;
}
