import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("log_errores_fe", { schema: "dbo" })
export class LogErroresFe {
  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("datetime", { name: "fecregistro", nullable: true })
  fecregistro: Date | null;

  @Column("varchar", { name: "mensaje", nullable: true, length: 120 })
  mensaje: string | null;

  @Column("varchar", { name: "doc_entrada", nullable: true, length: 120 })
  docEntrada: string | null;

  @Column("varchar", { name: "doc_salida", nullable: true, length: 120 })
  docSalida: string | null;

  @Column("varchar", { name: "doc_respuesta", nullable: true, length: 120 })
  docRespuesta: string | null;

  @Column("varchar", { name: "cdhash", nullable: true, length: 50 })
  cdhash: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @PrimaryGeneratedColumn({ type: "int", name: "Consecutivo" })
  consecutivo: number;
}
