import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_F__3214EC076A626D1E", ["id"], { unique: true })
@Entity("Kardex_Fisico_Productos_Todos_UpdateStock", { schema: "dbo" })
export class KardexFisicoProductosTodosUpdateStock {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
}
