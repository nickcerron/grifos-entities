import { Column, Entity, Index } from "typeorm";

@Index("pbcatt_x", ["pbtTnam", "pbtOwnr"], {})
@Entity("pbcattbl", { schema: "dbo" })
export class Pbcattbl {
  @Column("varchar", { name: "pbt_tnam", length: 129 })
  pbtTnam: string;

  @Column("int", { name: "pbt_tid", nullable: true })
  pbtTid: number | null;

  @Column("varchar", { name: "pbt_ownr", length: 129 })
  pbtOwnr: string;

  @Column("smallint", { name: "pbd_fhgt", nullable: true })
  pbdFhgt: number | null;

  @Column("smallint", { name: "pbd_fwgt", nullable: true })
  pbdFwgt: number | null;

  @Column("varchar", { name: "pbd_fitl", nullable: true, length: 1 })
  pbdFitl: string | null;

  @Column("varchar", { name: "pbd_funl", nullable: true, length: 1 })
  pbdFunl: string | null;

  @Column("smallint", { name: "pbd_fchr", nullable: true })
  pbdFchr: number | null;

  @Column("smallint", { name: "pbd_fptc", nullable: true })
  pbdFptc: number | null;

  @Column("varchar", { name: "pbd_ffce", nullable: true, length: 18 })
  pbdFfce: string | null;

  @Column("smallint", { name: "pbh_fhgt", nullable: true })
  pbhFhgt: number | null;

  @Column("smallint", { name: "pbh_fwgt", nullable: true })
  pbhFwgt: number | null;

  @Column("varchar", { name: "pbh_fitl", nullable: true, length: 1 })
  pbhFitl: string | null;

  @Column("varchar", { name: "pbh_funl", nullable: true, length: 1 })
  pbhFunl: string | null;

  @Column("smallint", { name: "pbh_fchr", nullable: true })
  pbhFchr: number | null;

  @Column("smallint", { name: "pbh_fptc", nullable: true })
  pbhFptc: number | null;

  @Column("varchar", { name: "pbh_ffce", nullable: true, length: 18 })
  pbhFfce: string | null;

  @Column("smallint", { name: "pbl_fhgt", nullable: true })
  pblFhgt: number | null;

  @Column("smallint", { name: "pbl_fwgt", nullable: true })
  pblFwgt: number | null;

  @Column("varchar", { name: "pbl_fitl", nullable: true, length: 1 })
  pblFitl: string | null;

  @Column("varchar", { name: "pbl_funl", nullable: true, length: 1 })
  pblFunl: string | null;

  @Column("smallint", { name: "pbl_fchr", nullable: true })
  pblFchr: number | null;

  @Column("smallint", { name: "pbl_fptc", nullable: true })
  pblFptc: number | null;

  @Column("varchar", { name: "pbl_ffce", nullable: true, length: 18 })
  pblFfce: string | null;

  @Column("varchar", { name: "pbt_cmnt", nullable: true, length: 254 })
  pbtCmnt: string | null;
}
