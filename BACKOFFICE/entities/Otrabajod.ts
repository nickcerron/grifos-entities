import { Column, Entity } from "typeorm";

@Entity("otrabajod", { schema: "dbo" })
export class Otrabajod {
  @Column("char", { name: "nrootrabajo", nullable: true, length: 10 })
  nrootrabajo: string | null;

  @Column("char", { name: "descripcion", nullable: true, length: 250 })
  descripcion: string | null;
}
