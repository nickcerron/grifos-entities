import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepAnomaliasDetalle", { schema: "dbo" })
export class TmpRepAnomaliasDetalle {
  @PrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "NROANOMALIA", nullable: true, length: 12 })
  nroanomalia: string | null;

  @Column("char", { name: "NROPOS", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "CDMONEDA", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "MTOFALTANTE",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltante: number | null;

  @Column("numeric", {
    name: "MTOSOBRANTE",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobrante: number | null;

  @Column("char", { name: "CDVENDEDOR", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("datetime", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("int", { name: "TURNO", nullable: true })
  turno: number | null;

  @Column("varchar", { name: "DSFECHATURNO", nullable: true, length: 20 })
  dsfechaturno: string | null;
}
