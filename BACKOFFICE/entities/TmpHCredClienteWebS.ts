import { Column, Entity } from "typeorm";

@Entity("TmpHCredClienteWebS", { schema: "dbo" })
export class TmpHCredClienteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "DocPago", length: 1 })
  docPago: string;

  @Column("varchar", { name: "TipoDocId", length: 5 })
  tipoDocId: string;

  @Column("varchar", { name: "NroDocumento", length: 15 })
  nroDocumento: string;

  @Column("datetime", { name: "FecSistema" })
  fecSistema: Date;

  @Column("varchar", { name: "NroPos", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("datetime", { name: "FecDocumento", nullable: true })
  fecDocumento: Date | null;

  @Column("smalldatetime", { name: "FecVencimiento", nullable: true })
  fecVencimiento: Date | null;

  @Column("smalldatetime", { name: "FecPago", nullable: true })
  fecPago: Date | null;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("char", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("float", { name: "MtoTotal", nullable: true, precision: 53 })
  mtoTotal: number | null;

  @Column("float", { name: "MtoEmision", nullable: true, precision: 53 })
  mtoEmision: number | null;

  @Column("float", { name: "MtoSoles", nullable: true, precision: 53 })
  mtoSoles: number | null;

  @Column("float", { name: "MtoDolares", nullable: true, precision: 53 })
  mtoDolares: number | null;

  @Column("float", { name: "TCambio", nullable: true, precision: 53 })
  tCambio: number | null;

  @Column("char", { name: "DocAplicaId", nullable: true, length: 5 })
  docAplicaId: string | null;

  @Column("varchar", { name: "NroDocAplica", nullable: true, length: 10 })
  nroDocAplica: string | null;

  @Column("varchar", { name: "CobradorId", nullable: true, length: 20 })
  cobradorId: string | null;

  @Column("int", { name: "NroPago", nullable: true })
  nroPago: number | null;

  @Column("char", { name: "TipoPagoId", nullable: true, length: 5 })
  tipoPagoId: string | null;

  @Column("char", { name: "BancoId", nullable: true, length: 5 })
  bancoId: string | null;

  @Column("varchar", { name: "NroCuenta", nullable: true, length: 20 })
  nroCuenta: string | null;

  @Column("varchar", { name: "NroPlanilla", nullable: true, length: 10 })
  nroPlanilla: string | null;

  @Column("varchar", { name: "NroRecibo", nullable: true, length: 10 })
  nroRecibo: string | null;

  @Column("varchar", { name: "NroCheque", nullable: true, length: 20 })
  nroCheque: string | null;

  @Column("char", { name: "TarjetaCreditoId", nullable: true, length: 2 })
  tarjetaCreditoId: string | null;

  @Column("varchar", { name: "NroTarjetaCredito", nullable: true, length: 20 })
  nroTarjetaCredito: string | null;

  @Column("varchar", { name: "Referencia", nullable: true, length: 60 })
  referencia: string | null;

  @Column("smalldatetime", { name: "FecProceso", nullable: true })
  fecProceso: Date | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
