import { Column, Entity, Index } from "typeorm";

@Index("PK_clienteplaca", ["cdcliente", "nroplaca"], { unique: true })
@Entity("clienteplaca", { schema: "dbo" })
export class Clienteplaca {
  @Column("char", { primary: true, name: "cdcliente", length: 20 })
  cdcliente: string;

  @Column("char", { primary: true, name: "nroplaca", length: 20 })
  nroplaca: string;

  @Column("varchar", { name: "marca", nullable: true, length: 50 })
  marca: string | null;

  @Column("varchar", { name: "modelo", nullable: true, length: 50 })
  modelo: string | null;

  @Column("varchar", { name: "color", nullable: true, length: 50 })
  color: string | null;

  @Column("numeric", { name: "ano", nullable: true, precision: 4, scale: 0 })
  ano: number | null;

  @Column("varchar", { name: "combustible", nullable: true, length: 30 })
  combustible: string | null;

  @Column("varchar", { name: "clase_vehiculo", nullable: true, length: 30 })
  claseVehiculo: string | null;

  @Column("varchar", { name: "nroserie", nullable: true, length: 40 })
  nroserie: string | null;

  @Column("varchar", { name: "nromotor", nullable: true, length: 40 })
  nromotor: string | null;

  @Column("numeric", {
    name: "kilometraje",
    nullable: true,
    precision: 8,
    scale: 0,
  })
  kilometraje: number | null;

  @Column("varchar", { name: "codigo", nullable: true, length: 20 })
  codigo: string | null;
}
