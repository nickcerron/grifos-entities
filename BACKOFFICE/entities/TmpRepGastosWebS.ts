import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepGastosWebS", { schema: "dbo" })
export class TmpRepGastosWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "Numero_Gasto", nullable: true, length: 10 })
  numeroGasto: string | null;

  @Column("smalldatetime", { name: "Fecha_Proceso" })
  fechaProceso: Date;

  @Column("int", { name: "TipoRegistro" })
  tipoRegistro: number;

  @Column("char", { name: "Tipo_Gasto", nullable: true, length: 5 })
  tipoGasto: string | null;

  @Column("char", { name: "TipoDocId", nullable: true, length: 5 })
  tipoDocId: string | null;

  @Column("varchar", { name: "NroDocumento", length: 15 })
  nroDocumento: string;

  @Column("char", { name: "ProveedorId", length: 15 })
  proveedorId: string;

  @Column("varchar", { name: "Encargado", nullable: true, length: 40 })
  encargado: string | null;

  @Column("decimal", {
    name: "Impuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  impuesto: number | null;

  @Column("decimal", {
    name: "MtoSubTotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoSubTotal: number | null;

  @Column("decimal", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoImpuesto: number | null;

  @Column("decimal", {
    name: "MtoTotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoTotal: number | null;

  @Column("varchar", { name: "Observaciones", nullable: true, length: 250 })
  observaciones: string | null;

  @Column("bit", { name: "Cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
