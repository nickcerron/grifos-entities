import { Column, Entity } from "typeorm";

@Entity("TmpAnuladosWeb", { schema: "dbo" })
export class TmpAnuladosWeb {
  @Column("char", { name: "CdTipoDoc", nullable: true, length: 5 })
  cdTipoDoc: string | null;

  @Column("varchar", { name: "Moneda", length: 3 })
  moneda: string;

  @Column("varchar", { name: "Estado", length: 1 })
  estado: string;

  @Column("varchar", { name: "NroDocumento", nullable: true, length: 10 })
  nroDocumento: string | null;

  @Column("varchar", { name: "TipoTran", nullable: true, length: 1 })
  tipoTran: string | null;

  @Column("int", { name: "EmpresaID" })
  empresaId: number;

  @Column("int", { name: "LocalID", nullable: true })
  localId: number | null;

  @Column("char", { name: "CdProducto", length: 20 })
  cdProducto: string;

  @Column("smallint", { name: "Item", nullable: true })
  item: number | null;

  @Column("char", { name: "NroSerieMaq", length: 20 })
  nroSerieMaq: string;

  @Column("char", { name: "TarjAfiliacion", nullable: true, length: 20 })
  tarjAfiliacion: string | null;

  @Column("char", { name: "ClienteId", nullable: true, length: 15 })
  clienteId: string | null;

  @Column("char", { name: "NroPos", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("smalldatetime", { name: "FechaDoc", nullable: true })
  fechaDoc: Date | null;

  @Column("smalldatetime", { name: "FechaSis", nullable: true })
  fechaSis: Date | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tCambio: number | null;

  @Column("numeric", {
    name: "Impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "Precio",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precio: number | null;

  @Column("numeric", {
    name: "Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "MtoDescto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoDescto: number | null;

  @Column("numeric", {
    name: "MtoSubTotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoSubTotal: number | null;

  @Column("numeric", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoImpuesto: number | null;

  @Column("numeric", {
    name: "mToTotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mToTotal: number | null;

  @Column("numeric", { name: "Puntos", precision: 10, scale: 0 })
  puntos: number;

  @Column("int", { name: "ValorAcumula" })
  valorAcumula: number;

  @Column("nchar", { name: "Usuario", nullable: true, length: 10 })
  usuario: string | null;

  @Column("numeric", { name: "Turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("nvarchar", { name: "Glosa", nullable: true, length: 150 })
  glosa: string | null;

  @Column("bit", { name: "Enviado", nullable: true })
  enviado: boolean | null;

  @Column("int", { name: "Almacen", nullable: true })
  almacen: number | null;

  @Column("int", { name: "Credito" })
  credito: number;

  @Column("varchar", { name: "NroDocumentoAnul", nullable: true, length: 10 })
  nroDocumentoAnul: string | null;
}
