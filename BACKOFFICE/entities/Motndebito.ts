import { Column, Entity, Index } from "typeorm";

@Index("PK__motndebi__90D129EE27570652", ["cdmotndebito"], { unique: true })
@Entity("motndebito", { schema: "dbo" })
export class Motndebito {
  @Column("varchar", { primary: true, name: "cdmotndebito", length: 5 })
  cdmotndebito: string;

  @Column("varchar", { name: "dsmotndebito", nullable: true, length: 40 })
  dsmotndebito: string | null;
}
