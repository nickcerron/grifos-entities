import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_GuiasD",
  [
    "cdlocal",
    "cdtipodoc",
    "nrodocumento",
    "cdtpsalida",
    "nrosalida",
    "nroitem",
    "cdarticulo",
  ],
  { unique: true }
)
@Entity("GuiasD", { schema: "dbo" })
export class GuiasD {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("char", { primary: true, name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  cantidad: number | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;
}
