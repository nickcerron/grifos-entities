import { Column, Entity } from "typeorm";

@Entity("TmpVentaWebS", { schema: "dbo" })
export class TmpVentaWebS {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "SerieMaq", length: 20 })
  serieMaq: string;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "FecProceso", nullable: true })
  fecProceso: Date | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("varchar", { name: "NroPOS", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("varchar", { name: "ClienteId", nullable: true, length: 20 })
  clienteId: string | null;

  @Column("varchar", { name: "RucCliente", nullable: true, length: 15 })
  rucCliente: string | null;

  @Column("varchar", { name: "RsCliente", nullable: true, length: 120 })
  rsCliente: string | null;

  @Column("varchar", { name: "Moneda", nullable: true, length: 3 })
  moneda: string | null;

  @Column("float", { name: "MtoDescuento", nullable: true, precision: 53 })
  mtoDescuento: number | null;

  @Column("float", { name: "mtosubtotal", nullable: true, precision: 53 })
  mtosubtotal: number | null;

  @Column("float", { name: "mtoimpuesto", nullable: true, precision: 53 })
  mtoimpuesto: number | null;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("varchar", { name: "TransportistaId", nullable: true, length: 50 })
  transportistaId: string | null;

  @Column("varchar", { name: "NroPlaca", nullable: true, length: 250 })
  nroPlaca: string | null;

  @Column("varchar", { name: "VendedorId", nullable: true, length: 15 })
  vendedorId: string | null;

  @Column("bit", { name: "Anulado", nullable: true })
  anulado: boolean | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tCambio: number | null;

  @Column("int", { name: "Turno", nullable: true })
  turno: number | null;

  @Column("varchar", { name: "Odometro", nullable: true, length: 10 })
  odometro: string | null;

  @Column("char", { name: "TipoFactura", nullable: true, length: 1 })
  tipoFactura: string | null;

  @Column("char", { name: "TipoVenta", nullable: true, length: 1 })
  tipoVenta: string | null;

  @Column("smalldatetime", { name: "FecAnula", nullable: true })
  fecAnula: Date | null;

  @Column("varchar", { name: "c_centralizacion", nullable: true, length: 50 })
  cCentralizacion: string | null;

  @Column("varchar", { name: "MotNotaID", nullable: true, length: 5 })
  motNotaId: string | null;

  @Column("varchar", { name: "MotNotaDes", nullable: true, length: 100 })
  motNotaDes: string | null;

  @Column("varchar", {
    name: "NroTarjeta",
    nullable: true,
    length: 20,
    default: () => "''",
  })
  nroTarjeta: string | null;

  @Column("numeric", {
    name: "mtoimp_otros",
    precision: 12,
    scale: 4,
    default: () => "(0)",
  })
  mtoimpOtros: number;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("datetime", { name: "FecVencimiento", nullable: true })
  fecVencimiento: Date | null;
}
