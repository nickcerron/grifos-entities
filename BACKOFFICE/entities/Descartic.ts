import { Column, Entity } from "typeorm";

@Entity("descartic", { schema: "dbo" })
export class Descartic {
  @Column("char", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", { name: "precio", precision: 12, scale: 3 })
  precio: number;
}
