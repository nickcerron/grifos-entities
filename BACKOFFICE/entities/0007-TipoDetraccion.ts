import { Column, Entity, Index } from 'typeorm';

@Index('PK_TipoDetraccion', ['cdtpdetraccion'], { unique: true })
@Entity('TipoDetraccion', { schema: 'dbo' })
export class TipoDetraccion {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtpdetraccion', length: 3 })
  cdtpdetraccion: string;

  //name
  //nullable => false
  @Column('varchar', { name: 'dstpdetraccion', length: 100 })
  dstpdetraccion: string;

  //rate
  //precision => 6
  @Column('numeric', { name: 'tasa', precision: 5, scale: 2 })
  tasa: number;

  //view
  //default => true
  @Column('bit', { name: 'flg_mostrar', nullable: true })
  flgMostrar: boolean | null;

  //sele añadira una relacion con compañia
  //campo en db: company_id
}
