import { Column, Entity } from "typeorm";

@Entity("inventario2", { schema: "dbo" })
export class Inventario2 {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "talla", length: 10 })
  talla: string;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "stockteorico",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockteorico: number | null;

  @Column("numeric", {
    name: "stockfisico",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockfisico: number | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;

  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("numeric", {
    name: "stockinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinventario: number | null;

  @Column("char", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column("char", { name: "monctoprom", nullable: true, length: 1 })
  monctoprom: string | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctopromedio: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;
}
