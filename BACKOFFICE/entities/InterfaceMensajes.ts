import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_interface_mensajes", ["id"], { unique: true })
@Entity("interface_mensajes", { schema: "dbo" })
export class InterfaceMensajes {
  @PrimaryGeneratedColumn({ type: "bigint", name: "id" })
  id: string;

  @Column("varchar", { name: "mensaje", length: 500 })
  mensaje: string;

  @Column("smalldatetime", { name: "fecha", default: () => "getdate()" })
  fecha: Date;
}
