import { Column, Entity } from "typeorm";

@Entity("TmpRepDsctoContadoPorConfirmar", { schema: "dbo" })
export class TmpRepDsctoContadoPorConfirmar {
  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "CLIENTEID", nullable: true, length: 20 })
  clienteid: string | null;

  @Column("bit", { name: "FLGCONFIRMADOCENTRAL", nullable: true })
  flgconfirmadocentral: boolean | null;
}
