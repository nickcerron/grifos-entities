import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepSalidaDWebS", { schema: "dbo" })
export class TmpRepSalidaDWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "empresaid" })
  empresaid: number;

  @Column("int", { name: "localid" })
  localid: number;

  @Column("varchar", { name: "salidaid", length: 5 })
  salidaid: string;

  @Column("varchar", { name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("int", { name: "nroitem" })
  nroitem: number;

  @Column("varchar", { name: "articuloid", length: 20 })
  articuloid: string;

  @Column("int", { name: "almacenid" })
  almacenid: number;

  @Column("varchar", {
    name: "articuloid_alternativo",
    nullable: true,
    length: 20,
  })
  articuloidAlternativo: string | null;

  @Column("varchar", { name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 27, scale: 5 })
  costo: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtototal: number | null;

  @Column("varchar", { name: "user_registra", length: 20 })
  userRegistra: string;
}
