import { Column, Entity } from "typeorm";

@Entity("TmpDiaDepVendWebS", { schema: "dbo" })
export class TmpDiaDepVendWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "fecproceso" })
  fecproceso: Date;

  @Column("int", { name: "turno" })
  turno: number;

  @Column("varchar", { name: "VendedorId", length: 20 })
  vendedorId: string;

  @Column("char", { name: "TipoPagoId", length: 5 })
  tipoPagoId: string;

  @Column("bigint", { name: "nrodeposito" })
  nrodeposito: string;

  @Column("int", { name: "TipoRegistro" })
  tipoRegistro: number;

  @Column("float", { name: "mtosoles", nullable: true, precision: 53 })
  mtosoles: number | null;

  @Column("float", { name: "mtodolar", nullable: true, precision: 53 })
  mtodolar: number | null;

  @Column("float", { name: "tcambio", nullable: true, precision: 53 })
  tcambio: number | null;

  @Column("char", { name: "TarjetaCreditoId", nullable: true, length: 2 })
  tarjetaCreditoId: string | null;

  @Column("varchar", { name: "NroTarjetaCredito", nullable: true, length: 20 })
  nroTarjetaCredito: string | null;

  @Column("bit", { name: "glp", nullable: true })
  glp: boolean | null;

  @Column("int", { name: "nrosobres", nullable: true })
  nrosobres: number | null;

  @Column("varchar", { name: "nrodocumento", length: 15 })
  nrodocumento: string;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
