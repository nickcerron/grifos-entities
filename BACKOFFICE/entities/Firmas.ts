import { Column, Entity, Index } from "typeorm";

@Index("PK_firmas", ["cdfirma"], { unique: true })
@Entity("firmas", { schema: "dbo" })
export class Firmas {
  @Column("char", { primary: true, name: "cdfirma", length: 5 })
  cdfirma: string;

  @Column("char", { name: "dsfirma", nullable: true, length: 50 })
  dsfirma: string | null;

  @Column("char", { name: "cargo", nullable: true, length: 30 })
  cargo: string | null;

  @Column("image", { name: "grffirma", nullable: true })
  grffirma: Buffer | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;
}
