import { Column, Entity, Index } from "typeorm";

@Index("PK_talla", ["cdtalla"], { unique: true })
@Entity("talla", { schema: "dbo" })
export class Talla {
  @Column("char", { primary: true, name: "cdtalla", length: 5 })
  cdtalla: string;

  @Column("char", { name: "dstalla", nullable: true, length: 60 })
  dstalla: string | null;
}
