import { Column, Entity } from "typeorm";

@Entity("PRODUCTOS$", { schema: "dbo" })
export class Productos {
  @Column("nvarchar", { name: "020010010   ", nullable: true, length: 255 })
  020010010: string | null;

  @Column("nvarchar", {
    name: "PETROLUBE SUPERIOR HD-40                                    ",
    nullable: true,
    length: 255,
  })
  petrolubeSuperiorHd_40: string | null;

  @Column("float", { name: "F3", nullable: true, precision: 53 })
  f3: number | null;
}
