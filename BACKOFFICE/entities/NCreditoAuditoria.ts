import { Column, Entity, Index } from "typeorm";

@Index("PK_NCredito_Auditoria", ["fecelimina", "cdlocal", "nroncredito"], {
  unique: true,
})
@Entity("NCredito_Auditoria", { schema: "dbo" })
export class NCreditoAuditoria {
  @Column("datetime", { primary: true, name: "fecelimina" })
  fecelimina: Date;

  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroncredito", length: 10 })
  nroncredito: string;

  @Column("smalldatetime", { name: "fecncredito", nullable: true })
  fecncredito: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "NroSerie1", nullable: true, length: 15 })
  nroSerie1: string | null;

  @Column("char", { name: "NroSerie2", nullable: true, length: 10 })
  nroSerie2: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("numeric", {
    name: "tcambioorigen",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambioorigen: number | null;

  @Column("char", { name: "observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("bit", { name: "flgdevolucion", nullable: true })
  flgdevolucion: boolean | null;

  @Column("char", { name: "Tiponcredito", nullable: true, length: 1 })
  tiponcredito: string | null;
}
