import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_VentaP202302",
  [
    "cdlocal",
    "nroseriemaq",
    "cdtipodoc",
    "nrodocumento",
    "cdtppago",
    "cdbanco",
    "nrocuenta",
    "nrocheque",
    "cdtarjeta",
    "nrotarjeta",
  ],
  { unique: true }
)
@Entity("VentaP202302", { schema: "dbo" })
export class VentaP202302 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroseriemaq", length: 20 })
  nroseriemaq: string;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "cdtppago", length: 5 })
  cdtppago: string;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { primary: true, name: "cdbanco", length: 4 })
  cdbanco: string;

  @Column("char", { primary: true, name: "nrocuenta", length: 20 })
  nrocuenta: string;

  @Column("char", { primary: true, name: "nrocheque", length: 20 })
  nrocheque: string;

  @Column("char", { primary: true, name: "cdtarjeta", length: 2 })
  cdtarjeta: string;

  @Column("char", { primary: true, name: "nrotarjeta", length: 20 })
  nrotarjeta: string;

  @Column("numeric", {
    name: "mtopagosol",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtopagosol: number | null;

  @Column("numeric", {
    name: "mtopagodol",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtopagodol: number | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nroncredito", nullable: true, length: 10 })
  nroncredito: string | null;

  @Column("numeric", {
    name: "valoracumula",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  valoracumula: number | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
