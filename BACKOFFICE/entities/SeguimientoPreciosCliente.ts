import { Column, Entity } from "typeorm";

@Entity("SeguimientoPreciosCliente", { schema: "dbo" })
export class SeguimientoPreciosCliente {
  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("char", { name: "CDCLIENTE", length: 15 })
  cdcliente: string;

  @Column("char", { name: "TIPOCLI", length: 3 })
  tipocli: string;

  @Column("char", { name: "CDARTICULO", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "NROCONTRATO", length: 60 })
  nrocontrato: string;

  @Column("varchar", { name: "MENSAJE", nullable: true, length: 250 })
  mensaje: string | null;

  @Column("datetime", { name: "FECHAERROR", nullable: true })
  fechaerror: Date | null;
}
