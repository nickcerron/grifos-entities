import { Column, Entity } from "typeorm";

@Entity("TmpMovfactura", { schema: "dbo" })
export class TmpMovfactura {
  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "LocalID", nullable: true, length: 4 })
  localId: string | null;

  @Column("char", { name: "cdproducto", nullable: true, length: 20 })
  cdproducto: string | null;

  @Column("smallint", { name: "Item", nullable: true })
  item: number | null;

  @Column("char", { name: "Almacen", nullable: true, length: 4 })
  almacen: string | null;

  @Column("char", { name: "ClienteId", nullable: true, length: 15 })
  clienteId: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("smalldatetime", { name: "FechaDoc", nullable: true })
  fechaDoc: Date | null;

  @Column("smalldatetime", { name: "FechaSis", nullable: true })
  fechaSis: Date | null;

  @Column("char", { name: "Moneda", nullable: true, length: 3 })
  moneda: string | null;

  @Column("float", { name: "TCambio", nullable: true, precision: 53 })
  tCambio: number | null;

  @Column("float", { name: "Impuesto", nullable: true, precision: 53 })
  impuesto: number | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "cantidad", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("float", { name: "MtoDescto", nullable: true, precision: 53 })
  mtoDescto: number | null;

  @Column("float", { name: "MtoSubtotal", nullable: true, precision: 53 })
  mtoSubtotal: number | null;

  @Column("float", { name: "MtoImpuesto", nullable: true, precision: 53 })
  mtoImpuesto: number | null;

  @Column("float", { name: "MtoTotal", nullable: true, precision: 53 })
  mtoTotal: number | null;

  @Column("float", { name: "Puntos", nullable: true, precision: 53 })
  puntos: number | null;

  @Column("float", { name: "ValorAcumulado", nullable: true, precision: 53 })
  valorAcumulado: number | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("bit", { name: "enviado", nullable: true })
  enviado: boolean | null;

  @Column("tinyint", { name: "Turno", nullable: true })
  turno: number | null;

  @Column("bit", { name: "Credito", nullable: true })
  credito: boolean | null;

  @Column("nvarchar", { name: "Glosa", nullable: true, length: 150 })
  glosa: string | null;

  @Column("nvarchar", { name: "Referencia", nullable: true, length: 25 })
  referencia: string | null;

  @Column("nchar", { name: "Usuario", nullable: true, length: 10 })
  usuario: string | null;

  @Column("timestamp", { name: "TmsTamp", nullable: true })
  tmsTamp: Date | null;

  @Column("float", { name: "costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("date", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "drcliente", nullable: true, length: 100 })
  drcliente: string | null;
}
