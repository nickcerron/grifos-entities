import { Column, Entity, Index } from "typeorm";

@Index("PK_interface_no_Contabilizado", ["cTurno", "numero", "fechaDia"], {
  unique: true,
})
@Entity("interface_no_Contabilizado", { schema: "dbo" })
export class InterfaceNoContabilizado {
  @Column("bigint", { primary: true, name: "c_turno" })
  cTurno: string;

  @Column("char", { primary: true, name: "numero", length: 4 })
  numero: string;

  @Column("smalldatetime", { primary: true, name: "fecha_dia" })
  fechaDia: Date;

  @Column("numeric", { name: "soles", nullable: true, precision: 11, scale: 2 })
  soles: number | null;

  @Column("char", { name: "producto", nullable: true, length: 2 })
  producto: string | null;

  @Column("numeric", { name: "precio", nullable: true, precision: 6, scale: 2 })
  precio: number | null;

  @Column("numeric", {
    name: "galones",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  galones: number | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("nchar", { name: "MANGUERA", nullable: true, length: 1 })
  manguera: string | null;

  @Column("smalldatetime", { name: "trans_fecha_hora", nullable: true })
  transFechaHora: Date | null;

  @Column("char", { name: "turno", nullable: true, length: 1 })
  turno: string | null;

  @Column("datetime", {
    name: "fecha_hora_servidor",
    nullable: true,
    default: () => "getdate()",
  })
  fechaHoraServidor: Date | null;

  @Column("char", {
    name: "trasferido",
    nullable: true,
    length: 1,
    default: () => "'N'",
  })
  trasferido: string | null;
}
