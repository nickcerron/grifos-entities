import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaContomTurno202302",
  ["cdlocal", "fecproceso", "turno", "lado", "manguera"],
  { unique: true }
)
@Entity("DiaContomTurno202302", { schema: "dbo" })
export class DiaContomTurno202302 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "lado", length: 2 })
  lado: string;

  @Column("char", { primary: true, name: "manguera", length: 2 })
  manguera: string;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "Inicial",
    nullable: true,
    precision: 14,
    scale: 3,
  })
  inicial: number | null;

  @Column("numeric", { name: "final", nullable: true, precision: 14, scale: 3 })
  final: number | null;

  @Column("numeric", {
    name: "galones",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  galones: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 12, scale: 2 })
  total: number | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;
}
