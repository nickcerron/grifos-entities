import { Column, Entity } from "typeorm";

@Entity("loteriaaviso", { schema: "dbo" })
export class Loteriaaviso {
  @Column("char", { name: "linea1", nullable: true, length: 39 })
  linea1: string | null;

  @Column("char", { name: "linea2", nullable: true, length: 39 })
  linea2: string | null;

  @Column("char", { name: "linea3", nullable: true, length: 39 })
  linea3: string | null;

  @Column("char", { name: "linea4", nullable: true, length: 39 })
  linea4: string | null;

  @Column("char", { name: "linea5", nullable: true, length: 39 })
  linea5: string | null;
}
