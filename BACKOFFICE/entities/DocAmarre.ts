import { Column, Entity } from "typeorm";

@Entity("DocAmarre", { schema: "dbo" })
export class DocAmarre {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "nroproforma", nullable: true, length: 10 })
  nroproforma: string | null;

  @Column("char", { name: "cdtpguia", nullable: true, length: 5 })
  cdtpguia: string | null;

  @Column("char", { name: "nroguia", nullable: true, length: 10 })
  nroguia: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;
}
