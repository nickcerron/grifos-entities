import { Column, Entity, Index } from "typeorm";

@Index("PK_zona", ["cdzona"], { unique: true })
@Entity("zona", { schema: "dbo" })
export class Zona {
  //id:  ahora sera identity
  //bigint
  @Column("char", { primary: true, name: "cdzona", length: 5 })
  cdzona: string;

  //nombre de la zona
  //unique
  //varchar(50)
  //nullable: false
  @Column("char", { name: "dszona", nullable: true, length: 40 })
  dszona: string | null;

  //descripcion de la zona
  //varchar(100)
  @Column("varchar", { name: "dazona", nullable: true, length: 100 })
  dazona: string | null;
}
