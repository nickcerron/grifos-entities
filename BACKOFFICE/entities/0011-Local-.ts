import { Column, Entity, Index } from 'typeorm';

@Index('PK_local', ['cdlocal', 'cdzona'], { unique: true })
@Entity('local', { schema: 'dbo' })
export class Local {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdlocal', length: 4 })
  cdlocal: string;

  //name
  //length => 70
  //unique => true
  @Column('char', { name: 'dslocal', nullable: true, length: 50 })
  dslocal: string | null;

  //address
  //length => 100
  @Column('char', { name: 'drlocal', nullable: true, length: 60 })
  drlocal: string | null;

  //phones
  //type: string []
  @Column('char', { name: 'tlflocal', nullable: true, length: 20 })
  tlflocal: string | null;

  //?muere
  @Column('char', { name: 'tlflocal1', nullable: true, length: 20 })
  tlflocal1: string | null;

  //?muere
  @Column('varchar', { name: 'NRO_CENTRALIZACION', nullable: true, length: 50 })
  nroCentralizacion: string | null;

  //?muere
  @Column('char', { name: 'dislocal', nullable: true, length: 60 })
  dislocal: string | null;

  //?muere
  @Column('char', { name: 'provlocal', nullable: true, length: 60 })
  provlocal: string | null;

  //?muere
  @Column('char', { name: 'deplocal', nullable: true, length: 60 })
  deplocal: string | null;

  //cod_sunat
  //length => 10
  @Column('char', { name: 'cdsunat', nullable: true, length: 4 })
  cdsunat: string | null;

  //department
  @Column('char', { name: 'cdDepartamento', nullable: true, length: 2 })
  cdDepartamento: string | null;

  //province
  @Column('char', { name: 'cdProvincia', nullable: true, length: 5 })
  cdProvincia: string | null;

  //district
  @Column('char', { name: 'cdDistrito', nullable: true, length: 5 })
  cdDistrito: string | null;

  //zone
  //type : Zone
  //?muere
  @Column('char', { primary: true, name: 'cdzona', length: 5 })
  cdzona: string;
}
