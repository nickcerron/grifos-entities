import { Column, Entity, Index } from 'typeorm';

@Index('PK_tipoingreso', ['cdtpingreso'], { unique: true })
@Entity('tipoingreso', { schema: 'dbo' })
export class Tipoingreso {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtpingreso', length: 5 })
  cdtpingreso: string;

  //name
  //nullable:false
  @Column('varchar', { name: 'dstpingreso', nullable: true, length: 40 })
  dstpingreso: string | null;

  //nro_correlative
  @Column('char', { name: 'nrocorrelativo', nullable: true, length: 10 })
  nrocorrelativo: string | null;

  //valued
  @Column('bit', { name: 'flgvalorizado', nullable: true })
  flgvalorizado: boolean | null;

  //system
  //?muere
  //? TODOS LOS DATOS SON => bit(0)
  @Column('bit', { name: 'flgsistema', nullable: true })
  flgsistema: boolean | null;

  //show
  //default 1
  @Column('bit', { name: 'flgmostrar', nullable: true })
  flgmostrar: boolean | null;

  //cal_cost
   //?NO SE PARA QUE SIRVE
  @Column('bit', { name: 'flgcalcosto', nullable: true })
  flgcalcosto: boolean | null;

  //nro_resume
  //?muere => TODOS LOS DATOS SON '000'
  @Column('char', { name: 'Bzl_nroresumen', nullable: true, length: 3 })
  bzlNroresumen: string | null;
}
