import { Column, Entity } from "typeorm";

@Entity("interface_config", { schema: "dbo" })
export class InterfaceConfig {
  @Column("bigint", { name: "ultimoregistro" })
  ultimoregistro: string;

  @Column("int", { name: "ultimorecno" })
  ultimorecno: number;

  @Column("tinyint", { name: "cambioturno" })
  cambioturno: number;

  @Column("tinyint", { name: "cierredia", nullable: true })
  cierredia: number | null;

  @Column("tinyint", { name: "cierrepventa", nullable: true })
  cierrepventa: number | null;

  @Column("tinyint", { name: "cierrelado", nullable: true })
  cierrelado: number | null;

  @Column("tinyint", { name: "cambiodia", nullable: true })
  cambiodia: number | null;

  @Column("int", { name: "n_turno", nullable: true })
  nTurno: number | null;

  @Column("varchar", { name: "c_punto_venta", nullable: true, length: 10 })
  cPuntoVenta: string | null;

  @Column("int", { name: "CodLado", nullable: true })
  codLado: number | null;

  @Column("tinyint", { name: "intervalo" })
  intervalo: number;

  @Column("tinyint", { name: "reconexion" })
  reconexion: number;

  @Column("tinyint", { name: "calcula_galon" })
  calculaGalon: number;

  @Column("varchar", { name: "tipo_controlador", length: 50 })
  tipoControlador: string;

  @Column("varchar", { name: "ruta_origen", nullable: true, length: 100 })
  rutaOrigen: string | null;

  @Column("varchar", { name: "ruta_destino", nullable: true, length: 100 })
  rutaDestino: string | null;

  @Column("smallint", { name: "nro_lados" })
  nroLados: number;

  @Column("tinyint", { name: "redondeo" })
  redondeo: number;

  @Column("bigint", { name: "c_turno", nullable: true })
  cTurno: string | null;

  @Column("tinyint", { name: "nivel_acesso", nullable: true })
  nivelAcesso: number | null;

  @Column("nchar", { name: "clave_interface", nullable: true, length: 10 })
  claveInterface: string | null;

  @Column("nchar", { name: "clave_estadisco", nullable: true, length: 10 })
  claveEstadisco: string | null;

  @Column("nchar", { name: "clave_contralador", nullable: true, length: 10 })
  claveContralador: string | null;

  @Column("char", { name: "tras_archivo1", nullable: true, length: 1 })
  trasArchivo1: string | null;

  @Column("char", { name: "tras_archivo2", nullable: true, length: 1 })
  trasArchivo2: string | null;

  @Column("smalldatetime", {
    name: "fecha_contometros_electronicos",
    nullable: true,
  })
  fechaContometrosElectronicos: Date | null;

  @Column("smalldatetime", { name: "fecha_dia", nullable: true })
  fechaDia: Date | null;

  @Column("varchar", { name: "host_interfase", length: 20 })
  hostInterfase: string;

  @Column("varchar", { name: "serie_disco", nullable: true, length: 20 })
  serieDisco: string | null;
}
