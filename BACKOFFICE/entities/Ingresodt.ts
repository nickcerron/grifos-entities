import { Column, Entity, Index } from "typeorm";

@Index("PK_ingresodt", ["ubicacion", "cdarticulo"], { unique: true })
@Entity("ingresodt", { schema: "dbo" })
export class Ingresodt {
  @Column("char", { primary: true, name: "ubicacion", length: 10 })
  ubicacion: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;
}
