import { Column, Entity } from "typeorm";

@Entity("ingreso_dajota", { schema: "dbo" })
export class IngresoDajota {
  @Column("varchar", { name: "cdarticulo", nullable: true, length: 60 })
  cdarticulo: string | null;

  @Column("varchar", { name: "descripcion", nullable: true, length: 60 })
  descripcion: string | null;

  @Column("numeric", {
    name: "stockfisico",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  stockfisico: number | null;

  @Column("varchar", { name: "cdgrupoid01", nullable: true, length: 10 })
  cdgrupoid01: string | null;

  @Column("varchar", { name: "dsgrupo", nullable: true, length: 60 })
  dsgrupo: string | null;

  @Column("numeric", {
    name: "costoglobal",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  costoglobal: number | null;

  @Column("numeric", {
    name: "importe",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  importe: number | null;
}
