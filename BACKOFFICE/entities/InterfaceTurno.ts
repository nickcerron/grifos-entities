import { Column, Entity, Index } from "typeorm";

@Index("PK_interface_turno", ["cTurno"], { unique: true })
@Entity("interface_turno", { schema: "dbo" })
export class InterfaceTurno {
  @Column("bigint", { primary: true, name: "c_turno" })
  cTurno: string;

  @Column("int", { name: "n_turno", nullable: true })
  nTurno: number | null;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("smalldatetime", { name: "hora", nullable: true })
  hora: Date | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("nchar", { name: "responsable", nullable: true, length: 30 })
  responsable: string | null;

  @Column("char", { name: "maquina", nullable: true, length: 30 })
  maquina: string | null;

  @Column("smalldatetime", {
    name: "fecha_hora_servidor",
    nullable: true,
    default: () => "getdate()",
  })
  fechaHoraServidor: Date | null;

  @Column("datetime", { name: "fecha_cierre", nullable: true })
  fechaCierre: Date | null;
}
