import { Column, Entity } from "typeorm";

@Entity("provarticulohist", { schema: "dbo" })
export class Provarticulohist {
  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  @Column("varchar", { name: "dscompra", nullable: true, length: 60 })
  dscompra: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtocosto",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  mtocosto: number | null;

  @Column("char", { name: "cdmonfob", nullable: true, length: 1 })
  cdmonfob: string | null;

  @Column("numeric", {
    name: "mtocostofob",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  mtocostofob: number | null;

  @Column("smalldatetime", { name: "fecmodifica", nullable: true })
  fecmodifica: Date | null;

  @Column("char", { name: "usumodifica", nullable: true, length: 10 })
  usumodifica: string | null;
}
