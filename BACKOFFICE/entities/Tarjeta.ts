import { Column, Entity, Index } from "typeorm";

@Index("PK_Tarjeta", ["nroDocumento", "fecDocumento"], { unique: true })
@Entity("tarjeta", { schema: "dbo" })
export class Tarjeta {
  @Column("varchar", { name: "nropos", length: 10 })
  nropos: string;

  @Column("char", { name: "tipo", length: 20 })
  tipo: string;

  @Column("char", { name: "nroTarjeta", length: 20 })
  nroTarjeta: string;

  @Column("smalldatetime", { primary: true, name: "fecDocumento" })
  fecDocumento: Date;

  @Column("smalldatetime", { name: "fecProceso" })
  fecProceso: Date;

  @Column("numeric", {
    name: "totalS",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  totalS: number | null;

  @Column("numeric", {
    name: "totalD",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  totalD: number | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "tipoDoc", length: 5 })
  tipoDoc: string;

  @Column("char", { primary: true, name: "nroDocumento", length: 10 })
  nroDocumento: string;

  @Column("smalldatetime", {
    name: "fecConciliacion",
    nullable: true,
    default: () => "getdate()",
  })
  fecConciliacion: Date | null;

  @Column("int", { name: "logical", default: () => "(0)" })
  logical: number;

  @Column("int", { name: "flagerror", default: () => "(0)" })
  flagerror: number;
}
