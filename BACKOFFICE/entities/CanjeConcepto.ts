import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { CnfgValorPunto } from "./CnfgValorPunto";

@Index("PK_Canje_Concepto", ["valorId", "concepto", "codProducto"], {
  unique: true,
})
@Entity("Canje_Concepto", { schema: "dbo" })
export class CanjeConcepto {
  @Column("int", { primary: true, name: "ValorId" })
  valorId: number;

  @Column("int", { primary: true, name: "Concepto" })
  concepto: number;

  @Column("char", { primary: true, name: "CodProducto", length: 20 })
  codProducto: string;

  @Column("int", { name: "TipoPunto", nullable: true })
  tipoPunto: number | null;

  @Column("float", { name: "ValorPtoCanje", nullable: true, precision: 53 })
  valorPtoCanje: number | null;

  @ManyToOne(
    () => CnfgValorPunto,
    (cnfgValorPunto) => cnfgValorPunto.canjeConceptos
  )
  @JoinColumn([{ name: "ValorId", referencedColumnName: "valorId" }])
  valor: CnfgValorPunto;
}
