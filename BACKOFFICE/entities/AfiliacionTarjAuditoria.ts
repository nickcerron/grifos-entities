import { Column, Entity } from "typeorm";

@Entity("AfiliacionTarj_Auditoria", { schema: "dbo" })
export class AfiliacionTarjAuditoria {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "TarjAfiliacion", length: 20 })
  tarjAfiliacion: string;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("char", { name: "drcliente", nullable: true, length: 120 })
  drcliente: string | null;

  @Column("char", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null;

  @Column("char", { name: "tlfcliente1", nullable: true, length: 15 })
  tlfcliente1: string | null;

  @Column("char", { name: "emcliente", nullable: true, length: 60 })
  emcliente: string | null;

  @Column("numeric", {
    name: "Puntos",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  puntos: number | null;

  @Column("numeric", {
    name: "Canjeado",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  canjeado: number | null;

  @Column("numeric", {
    name: "Disponible",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  disponible: number | null;

  @Column("bit", { name: "Bloqueado", nullable: true })
  bloqueado: boolean | null;

  @Column("smalldatetime", { name: "fechamodif", nullable: true })
  fechamodif: Date | null;

  @Column("char", { name: "usuario", nullable: true, length: 10 })
  usuario: string | null;

  @Column("varchar", { name: "tipo_modif", nullable: true, length: 50 })
  tipoModif: string | null;

  @Column("char", { name: "provtelefonia", nullable: true, length: 2 })
  provtelefonia: string | null;

  @Column("char", { name: "provtelefonia1", nullable: true, length: 2 })
  provtelefonia1: string | null;

  @Column("smalldatetime", { name: "Fechaultconsumo", nullable: true })
  fechaultconsumo: Date | null;
}
