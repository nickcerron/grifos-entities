import { Column, Entity, Index } from "typeorm";

@Index("pk_PagoAnticipo", ["operacion", "ruccliente"], { unique: true })
@Entity("PagoAnticipo", { schema: "dbo" })
export class PagoAnticipo {
  @Column("char", { primary: true, name: "operacion", length: 40 })
  operacion: string;

  @Column("char", { primary: true, name: "ruccliente", length: 15 })
  ruccliente: string;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "cdBanco", nullable: true, length: 4 })
  cdBanco: string | null;

  @Column("char", { name: "NroCuenta", nullable: true, length: 25 })
  nroCuenta: string | null;

  @Column("numeric", {
    name: "mtoanticipo",
    nullable: true,
    precision: 16,
    scale: 2,
  })
  mtoanticipo: number | null;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  @Column("smalldatetime", { name: "fecSistema", nullable: true })
  fecSistema: Date | null;
}
