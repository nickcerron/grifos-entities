import { Column, Entity } from "typeorm";

@Entity("TmpRepAnuladosWebS", { schema: "dbo" })
export class TmpRepAnuladosWebS {
  @Column("int", { name: "EMPRESA" })
  empresa: number;

  @Column("char", { name: "RUCEMPRESA", length: 15 })
  rucempresa: string;

  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("varchar", { name: "SERIEMAQ", length: 20 })
  seriemaq: string;

  @Column("varchar", { name: "TIPOD", length: 5 })
  tipod: string;

  @Column("char", { name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("bit", { name: "ANULADO" })
  anulado: boolean;

  @Column("smalldatetime", { name: "FECANULA" })
  fecanula: Date;
}
