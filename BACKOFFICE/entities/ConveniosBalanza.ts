import { Column, Entity } from "typeorm";

@Entity("ConveniosBalanza", { schema: "dbo" })
export class ConveniosBalanza {
  @Column("char", { name: "convenio", nullable: true, length: 3 })
  convenio: string | null;

  @Column("varchar", { name: "descripcion", nullable: true, length: 60 })
  descripcion: string | null;

  @Column("bit", { name: "flghabilitado", nullable: true })
  flghabilitado: boolean | null;
}
