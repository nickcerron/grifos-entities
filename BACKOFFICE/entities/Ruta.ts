import { Column, Entity, Index } from "typeorm";

@Index("PK_ruta", ["cdruta"], { unique: true })
@Entity("ruta", { schema: "dbo" })
export class Ruta {
  @Column("char", { primary: true, name: "cdruta", length: 10 })
  cdruta: string;

  @Column("varchar", { name: "dsruta", nullable: true, length: 70 })
  dsruta: string | null;
}
