import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_Mensajes_Predeterminados", ["idMensaje"], { unique: true })
@Entity("Mensajes_Predeterminados", { schema: "dbo" })
export class MensajesPredeterminados {
  @PrimaryGeneratedColumn({ type: "int", name: "Id_Mensaje" })
  idMensaje: number;

  @Column("varchar", { name: "Descripcion", length: 200 })
  descripcion: string;

  @Column("bit", { name: "Predeterminado", nullable: true })
  predeterminado: boolean | null;
}
