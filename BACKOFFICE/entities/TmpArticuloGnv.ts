import { Column, Entity } from "typeorm";

@Entity("TMP_ArticuloGNV", { schema: "dbo" })
export class TmpArticuloGnv {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "dsarticulo", nullable: true, length: 60 })
  dsarticulo: string | null;

  @Column("char", { name: "dsarticulo1", nullable: true, length: 30 })
  dsarticulo1: string | null;

  @Column("char", { name: "cdgrupo01", nullable: true, length: 5 })
  cdgrupo01: string | null;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("char", { name: "cdgrupo03", nullable: true, length: 5 })
  cdgrupo03: string | null;

  @Column("char", { name: "cdgrupo04", nullable: true, length: 5 })
  cdgrupo04: string | null;

  @Column("char", { name: "cdgrupo05", nullable: true, length: 5 })
  cdgrupo05: string | null;

  @Column("char", { name: "cdunimed", nullable: true, length: 5 })
  cdunimed: string | null;

  @Column("char", { name: "cdtalla", nullable: true, length: 5 })
  cdtalla: string | null;

  @Column("char", { name: "tpformula", nullable: true, length: 1 })
  tpformula: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "impuesto1",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto1: number | null;

  @Column("bit", { name: "bloqvta", nullable: true })
  bloqvta: boolean | null;

  @Column("bit", { name: "bloqcom", nullable: true })
  bloqcom: boolean | null;

  @Column("bit", { name: "flgglosa", nullable: true })
  flgglosa: boolean | null;

  @Column("bit", { name: "moverstock", nullable: true })
  moverstock: boolean | null;

  @Column("bit", { name: "venta", nullable: true })
  venta: boolean | null;

  @Column("bit", { name: "consignacion", nullable: true })
  consignacion: boolean | null;

  @Column("bit", { name: "bloqgral", nullable: true })
  bloqgral: boolean | null;

  @Column("bit", { name: "movimiento", nullable: true })
  movimiento: boolean | null;

  @Column("bit", { name: "vtaxmonto", nullable: true })
  vtaxmonto: boolean | null;

  @Column("smalldatetime", { name: "fecinicial", nullable: true })
  fecinicial: Date | null;

  @Column("char", { name: "monctoinicial", nullable: true, length: 1 })
  monctoinicial: string | null;

  @Column("numeric", {
    name: "ctoinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinicial: number | null;

  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("char", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column("char", { name: "monctoprom", nullable: true, length: 1 })
  monctoprom: string | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctopromedio: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;

  @Column("smalldatetime", { name: "fecedicion", nullable: true })
  fecedicion: Date | null;

  @Column("numeric", {
    name: "mgutilidad",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  mgutilidad: number | null;

  @Column("numeric", {
    name: "equivalencia",
    nullable: true,
    precision: 8,
    scale: 2,
  })
  equivalencia: number | null;

  @Column("char", { name: "cdmedequiv", nullable: true, length: 5 })
  cdmedequiv: string | null;

  @Column("char", { name: "cdamarre", nullable: true, length: 6 })
  cdamarre: string | null;

  @Column("bit", { name: "flgpromocion", nullable: true })
  flgpromocion: boolean | null;

  @Column("char", { name: "tpconversion", nullable: true, length: 1 })
  tpconversion: string | null;

  @Column("numeric", {
    name: "valorconversion",
    nullable: true,
    precision: 10,
    scale: 5,
  })
  valorconversion: number | null;

  @Column("char", { name: "ctacompra", nullable: true, length: 12 })
  ctacompra: string | null;

  @Column("char", { name: "ctaventa", nullable: true, length: 12 })
  ctaventa: string | null;

  @Column("char", { name: "ctacosto", nullable: true, length: 12 })
  ctacosto: string | null;

  @Column("char", { name: "ctaalmacen", nullable: true, length: 12 })
  ctaalmacen: string | null;

  @Column("bit", { name: "usadecimales", nullable: true })
  usadecimales: boolean | null;

  @Column("bit", { name: "virtual", nullable: true })
  virtual: boolean | null;

  @Column("bit", { name: "TrfGratuita", nullable: true })
  trfGratuita: boolean | null;

  @Column("numeric", {
    name: "PrecioAfiliacion",
    nullable: true,
    precision: 5,
    scale: 0,
  })
  precioAfiliacion: number | null;

  @Column("varchar", { name: "cdarticulovulcano", nullable: true, length: 20 })
  cdarticulovulcano: string | null;

  @Column("varchar", { name: "c_cuenta_compras", nullable: true, length: 12 })
  cCuentaCompras: string | null;

  @Column("varchar", { name: "c_centrocosto", nullable: true, length: 12 })
  cCentrocosto: string | null;

  @Column("varchar", { name: "c_cuenta_ventas", nullable: true, length: 12 })
  cCuentaVentas: string | null;

  @Column("varchar", { name: "c_cuenta", nullable: true, length: 12 })
  cCuenta: string | null;
}
