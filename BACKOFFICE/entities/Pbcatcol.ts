import { Column, Entity, Index } from "typeorm";

@Index("pbcatc_x", ["pbcTnam", "pbcOwnr", "pbcCnam"], {})
@Entity("pbcatcol", { schema: "dbo" })
export class Pbcatcol {
  @Column("varchar", { name: "pbc_tnam", length: 129 })
  pbcTnam: string;

  @Column("int", { name: "pbc_tid", nullable: true })
  pbcTid: number | null;

  @Column("varchar", { name: "pbc_ownr", length: 129 })
  pbcOwnr: string;

  @Column("varchar", { name: "pbc_cnam", length: 129 })
  pbcCnam: string;

  @Column("smallint", { name: "pbc_cid", nullable: true })
  pbcCid: number | null;

  @Column("varchar", { name: "pbc_labl", nullable: true, length: 254 })
  pbcLabl: string | null;

  @Column("smallint", { name: "pbc_lpos", nullable: true })
  pbcLpos: number | null;

  @Column("varchar", { name: "pbc_hdr", nullable: true, length: 254 })
  pbcHdr: string | null;

  @Column("smallint", { name: "pbc_hpos", nullable: true })
  pbcHpos: number | null;

  @Column("smallint", { name: "pbc_jtfy", nullable: true })
  pbcJtfy: number | null;

  @Column("varchar", { name: "pbc_mask", nullable: true, length: 31 })
  pbcMask: string | null;

  @Column("smallint", { name: "pbc_case", nullable: true })
  pbcCase: number | null;

  @Column("smallint", { name: "pbc_hght", nullable: true })
  pbcHght: number | null;

  @Column("smallint", { name: "pbc_wdth", nullable: true })
  pbcWdth: number | null;

  @Column("varchar", { name: "pbc_ptrn", nullable: true, length: 31 })
  pbcPtrn: string | null;

  @Column("varchar", { name: "pbc_bmap", nullable: true, length: 1 })
  pbcBmap: string | null;

  @Column("varchar", { name: "pbc_init", nullable: true, length: 254 })
  pbcInit: string | null;

  @Column("varchar", { name: "pbc_cmnt", nullable: true, length: 254 })
  pbcCmnt: string | null;

  @Column("varchar", { name: "pbc_edit", nullable: true, length: 31 })
  pbcEdit: string | null;

  @Column("varchar", { name: "pbc_tag", nullable: true, length: 254 })
  pbcTag: string | null;
}
