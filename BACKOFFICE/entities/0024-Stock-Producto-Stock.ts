import { Column, Entity, Index } from "typeorm";

@Index("PK_stock_1", ["cdlocal", "cdalmacen", "cdarticulo"], { unique: true })
@Entity("stock", { schema: "dbo" })
export class Stock {
  //! se combinaran los campos cdlocal y cdalmacen en un solo campo que hara 
  //! referencia a la entidad LocalXAlmacen
  //@Column("char", { primary: true, name: "cdlocal", length: 3 })
  //cdlocal: string;

  //@Column("char", { primary: true, name: "cdalmacen", length: 3 })
  //cdalmacen: string;

  //referencia a Producto
  // ya no sera primary key
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;
  //! hasta aqui

  //! todo en la db esta en null, deberia morir
  //! ya se indica en la entidad Producto
  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;


  //! fecha inicial, parece ser la fecha de creacion del registro, pero hay otro campo que igual puede serlo
  @Column("smalldatetime", { name: "fecinicial", nullable: true })
  fecinicial: Date | null;

  //! eliminar
  //? stock inicial parece ser el stock de creacion del registro
  @Column("numeric", {
    name: "stockinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinicial: number | null;

  //! eliminar
  // referencia a moneda
  //? moneda del costo inicial
  @Column("char", { name: "monctoinicial", nullable: true, length: 1 })
  monctoinicial: string | null;

  //! eliminar
  @Column("numeric", {
    name: "ctoinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinicial: number | null;

  // ! eliminar  
  //? fecha de inventario, parace
  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  // ! eliminar  
  //? stock de inventario, cuanto se relleno de stock? o cuanto hay despues de un inventario o rellando?
  @Column("numeric", {
    name: "stockinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinventario: number | null;

  // ! eliminar  
  //? moneda del costo de inventario
  @Column("char", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  //! eliminar
  //? costo de inventario
  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  //? stock minimo a lo largo del tiempo?
  @Column("numeric", {
    name: "stockminimo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockminimo: number | null;

  //* stock actual, el mas importante
  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;


  //? stock separado, pendiente uso
  @Column("numeric", {
    name: "stockseparado",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockseparado: number | null;

  //? stock maximo a lo largo del tiempo?
  @Column("numeric", {
    name: "stockmaximo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockmaximo: number | null;


  // referencia a moneda
  //? moneda del costo de reposicion,  que es reposicion?
  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  //? costo de reposicion, que es reposicion? 
  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;

  //! eliminar
  // usuario de creacion del registro? o el que ingreso el ultimo inventario o stock?
  @Column("char", { name: "usuingreso", nullable: true, length: 10 })
  usuingreso: string | null;


  //! eliminar
  //! fecha de ingreso, no se que sera, puede ser la fecha de creacion del registro
  //! pero hay otro campo que igual puede serlo, fecha inicial
  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  //! eliminar  
  //? usuario de salida, que saco stock o lo traslado?
  @Column("char", { name: "ususalida", nullable: true, length: 10 })
  ususalida: string | null;

  //! eliminar
  //? fecha de salida, cuando se saco stock o se traslado?
  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  //! eliminar
  //? usuario de venta, quien vendio el producto por ultima vez?
  @Column("char", { name: "usuventa", nullable: true, length: 10 })
  usuventa: string | null;

  //! eliminar
  //? fecha de venta, cuando se vendio el producto por ultima vez?
  @Column("smalldatetime", { name: "fecventa", nullable: true })
  fecventa: Date | null;

  //! eliminar
  //? es recalculo, todo en null, pendiente uso
  @Column("bit", { name: "IS_RECALCULO", nullable: true })
  isRecalculo: boolean | null;
}
