import { Column, Entity } from "typeorm";

@Entity("credcliente_Log", { schema: "dbo" })
export class CredclienteLog {
  @Column("char", { name: "docpago", length: 1 })
  docpago: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { name: "renovacion", length: 2 })
  renovacion: string;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("smalldatetime", { name: "fecpago", nullable: true })
  fecpago: Date | null;

  @Column("smalldatetime", { name: "fecsistema" })
  fecsistema: Date;

  @Column("char", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtoemision",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtoemision: number | null;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolares",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("numeric", {
    name: "mtodifcambio",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodifcambio: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cddocaplica", nullable: true, length: 5 })
  cddocaplica: string | null;

  @Column("char", { name: "nrodocaplica", nullable: true, length: 10 })
  nrodocaplica: string | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("char", { name: "cdcobrador", nullable: true, length: 10 })
  cdcobrador: string | null;

  @Column("numeric", {
    name: "nropago",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nropago: number | null;

  @Column("varchar", { name: "nroplanilla", nullable: true, length: 15 })
  nroplanilla: string | null;

  @Column("char", { name: "nrorecibo", nullable: true, length: 10 })
  nrorecibo: string | null;

  @Column("char", { name: "cdtppago", nullable: true, length: 5 })
  cdtppago: string | null;

  @Column("char", { name: "cdbanco", nullable: true, length: 5 })
  cdbanco: string | null;

  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("char", { name: "nrocheque", nullable: true, length: 20 })
  nrocheque: string | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "referencia", nullable: true, length: 60 })
  referencia: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("datetime", { name: "FechaCreacion", nullable: true })
  fechaCreacion: Date | null;

  @Column("numeric", {
    name: "nrocuota",
    precision: 3,
    scale: 0,
    default: () => "(1)",
  })
  nrocuota: number;

  @Column("numeric", { name: "Dias", nullable: true, precision: 3, scale: 0 })
  dias: number | null;

  @Column("char", { name: "UsuarioModifico", nullable: true, length: 10 })
  usuarioModifico: string | null;

  @Column("smalldatetime", { name: "fecModifica", nullable: true })
  fecModifica: Date | null;
}
