import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_Boleta", ["identifier"], { unique: true })
@Entity("Boleta", { schema: "dbo" })
export class Boleta {
  @PrimaryGeneratedColumn({
    type: "numeric",
    name: "id",
    precision: 18,
    scale: 0,
  })
  id: number;

  @Column("nvarchar", { name: "ruc", length: 11 })
  ruc: string;

  @Column("nvarchar", { name: "documentType", length: 2 })
  documentType: string;

  @Column("nvarchar", { primary: true, name: "identifier", length: 13 })
  identifier: string;

  @Column("datetime", { name: "sentTime" })
  sentTime: Date;

  @Column("datetime", { name: "sunatTime", nullable: true })
  sunatTime: Date | null;

  @Column("nvarchar", { name: "email", nullable: true, length: 100 })
  email: string | null;

  @Column("numeric", {
    name: "status",
    nullable: true,
    precision: 3,
    scale: 0,
    default: () => "'100'",
  })
  status: number | null;

  @Column("nvarchar", { name: "cdrFile", nullable: true })
  cdrFile: string | null;

  @Column("nvarchar", { name: "pdfFile", nullable: true })
  pdfFile: string | null;

  @Column("nvarchar", { name: "xmlSigned", nullable: true })
  xmlSigned: string | null;

  @Column("nvarchar", { name: "digestValue", nullable: true, length: 60 })
  digestValue: string | null;
}
