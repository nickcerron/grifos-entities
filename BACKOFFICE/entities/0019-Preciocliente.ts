import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index(
  'PK_preciocliente',
  ['cdcliente', 'tipocli', 'cdarticulo', 'nrocontrato'],
  { unique: true }
)
@Entity('preciocliente', { schema: 'dbo' })
export class Preciocliente {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @PrimaryGeneratedColumn({ type: 'int', name: 'cdPrecioCliente' })
  cdPrecioCliente: number;

  // client
  // nullable => false,
  // type => Client (relation)
  @Column('char', { primary: true, name: 'cdcliente', length: 15 })
  cdcliente: string;

  // paymentMethod
  @Column('char', { primary: true, name: 'tipocli', length: 3 })
  tipocli: string;

  // product
  // nullable => false
  // type => Product (relation)
  @Column('char', { primary: true, name: 'cdarticulo', length: 20 })
  cdarticulo: string;

  //discount_price
  // precision: 10,
  // scale: 4,
  @Column('numeric', {
    name: 'precio',
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  //discountType
  //nullable => false
  @Column('char', { name: 'tipodes', nullable: true, length: 3 })
  tipodes: string | null;

  //?muere
  //? nro_contract
  //? todos los datos en blanco
  @Column('varchar', {
    primary: true,
    name: 'nrocontrato',
    length: 60,
    default: () => "' '",
  })
  nrocontrato: string;

  //?muere
  //? datos en la db => 0
  @Column('bit', { name: 'licitacion', nullable: true })
  licitacion: boolean | null;
}
