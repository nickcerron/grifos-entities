import { Column, Entity, Index } from "typeorm";

@Index("PK_importacioncc", ["fechora"], { unique: true })
@Entity("importacioncc", { schema: "dbo" })
export class Importacioncc {
  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "dsproveedor", nullable: true, length: 60 })
  dsproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecimportacion", nullable: true })
  fecimportacion: Date | null;

  @Column("char", { name: "observacion", nullable: true, length: 50 })
  observacion: string | null;
}
