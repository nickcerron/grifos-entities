import { Column, Entity, Index } from "typeorm";

@Index("PK_transferencia", ["cdlocal", "movimiento", "nromov"], {
  unique: true,
})
@Entity("transferencia", { schema: "dbo" })
export class Transferencia {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "movimiento", length: 1 })
  movimiento: string;

  @Column("char", { primary: true, name: "nromov", length: 10 })
  nromov: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdlocdest", nullable: true, length: 3 })
  cdlocdest: string | null;

  @Column("smalldatetime", { name: "fectransferencia", nullable: true })
  fectransferencia: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("char", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("bit", { name: "enviado", nullable: true })
  enviado: boolean | null;
}
