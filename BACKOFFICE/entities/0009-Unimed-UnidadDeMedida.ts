import { Column, Entity, Index } from "typeorm";

@Index("PK_unimed", ["cdunimed"], { unique: true })
@Entity("unimed", { schema: "dbo" })
export class Unimed {
  //se creara un nuevo campo id, bigint, autoincremental

  //ahora sera acronym, length 5 unique not null
  @Column("char", { primary: true, name: "cdunimed", length: 5 })
  cdunimed: string;

  //name unique varchar
  @Column("char", { name: "dsunimed", nullable: true, length: 40 })
  dsunimed: string | null;

  //description
  @Column("varchar", { name: "daunimed", nullable: true, length: 100 })
  daunimed: string | null;

  //un_ece varchar 3 unique not null
  @Column("char", { name: "cdunece", nullable: true, length: 3 })
  cdunece: string | null;

  //se le añadira compañia
  //y el campo isActive
}
