import { Column, Entity } from "typeorm";

@Entity("comisiones", { schema: "dbo" })
export class Comisiones {
  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("numeric", {
    name: "rangoini",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  rangoini: number | null;

  @Column("numeric", {
    name: "rangofin",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  rangofin: number | null;

  @Column("numeric", {
    name: "porcomision",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porcomision: number | null;
}
