import { Column, Entity } from "typeorm";

@Entity("LOG_GrupoProdclienteDes", { schema: "dbo" })
export class LogGrupoProdclienteDes {
  @Column("int", { name: "cdgrupoID" })
  cdgrupoId: number;

  @Column("varchar", { name: "descripcion", nullable: true, length: 50 })
  descripcion: string | null;

  @Column("char", { name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { name: "tipogrupo", length: 10 })
  tipogrupo: string;

  @Column("char", { name: "tipodes", length: 3 })
  tipodes: string;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("int", { name: "cdGrupoProCli", nullable: true })
  cdGrupoProCli: number | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
