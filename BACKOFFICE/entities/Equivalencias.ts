import { Column, Entity } from "typeorm";

@Entity("Equivalencias", { schema: "dbo" })
export class Equivalencias {
  @Column("char", { name: "Tipo", length: 10 })
  tipo: string;

  @Column("char", { name: "CodSIGES", length: 5 })
  codSiges: string;

  @Column("nchar", { name: "CodExterno", length: 10 })
  codExterno: string;

  @Column("char", { name: "CodExterno2", nullable: true, length: 2 })
  codExterno2: string | null;

  @Column("smalldatetime", { name: "FechaInicio", nullable: true })
  fechaInicio: Date | null;

  @Column("smalldatetime", { name: "FechaFinal", nullable: true })
  fechaFinal: Date | null;
}
