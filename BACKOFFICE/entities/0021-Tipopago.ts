import { Column, Entity, Index } from 'typeorm';

@Index('PK_tipopago', ['cdtppago'], { unique: true })
@Entity('tipopago', { schema: 'dbo' })
export class Tipopago {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtppago', length: 5 })
  cdtppago: string;

  //name
  //unique
  //nullable=>false
  @Column('char', { name: 'dstppago', nullable: true, length: 60 })
  dstppago: string | null;

  //pagoCredito
  //default => false
  //nullable =>false
  @Column('bit', { name: 'flgpago', nullable: true })
  flgpago: boolean | null;

  //isActive
  //default  => true
  //nullable => false
  @Column('bit', { name: 'flgsistema', nullable: true })
  flgsistema: boolean | null;
}
