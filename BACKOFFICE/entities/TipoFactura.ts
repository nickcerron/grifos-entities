import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoFactura", ["cdTpFactura"], { unique: true })
@Entity("TipoFactura", { schema: "dbo" })
export class TipoFactura {
  @Column("char", { primary: true, name: "cdTpFactura", length: 5 })
  cdTpFactura: string;

  @Column("varchar", { name: "dsTpFactura", length: 30 })
  dsTpFactura: string;

  @Column("bit", { name: "flgPredefinido", nullable: true })
  flgPredefinido: boolean | null;
}
