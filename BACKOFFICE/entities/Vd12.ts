import { Column, Entity } from "typeorm";

@Entity("vd12", { schema: "dbo" })
export class Vd12 {
  @Column("numeric", {
    name: "totald",
    nullable: true,
    precision: 38,
    scale: 4,
  })
  totald: number | null;

  @Column("char", { name: "nrod", length: 10 })
  nrod: string;

  @Column("char", { name: "tipod", length: 5 })
  tipod: string;
}
