import { Column, Entity, Index } from "typeorm";

@Index("PK_clientem", ["cdcliente"], { unique: true })
@Entity("clientem", { schema: "dbo" })
export class Clientem {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("text", { name: "memcliente", nullable: true })
  memcliente: string | null;

  @Column("image", { name: "imgcliente", nullable: true })
  imgcliente: Buffer | null;
}
