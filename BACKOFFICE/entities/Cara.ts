import { Column, Entity } from "typeorm";

@Entity("cara", { schema: "dbo" })
export class Cara {
  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;
}
