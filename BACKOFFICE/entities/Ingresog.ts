import { Column, Entity, Index } from "typeorm";

@Index("PK_ingresog", ["cdlocal", "cdtpingreso", "nroingreso", "rucempresa"], {
  unique: true,
})
@Entity("ingresog", { schema: "dbo" })
export class Ingresog {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("char", { primary: true, name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("char", { name: "cdtpingrefe", nullable: true, length: 5 })
  cdtpingrefe: string | null;

  @Column("char", { name: "nroringrefe", nullable: true, length: 10 })
  nroringrefe: string | null;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;
}
