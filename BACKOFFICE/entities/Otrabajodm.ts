import { Column, Entity, Index } from "typeorm";

@Index("PK_otrabajodm", ["nrootrabajo"], { unique: true })
@Entity("otrabajodm", { schema: "dbo" })
export class Otrabajodm {
  @Column("char", { primary: true, name: "nrootrabajo", length: 10 })
  nrootrabajo: string;

  @Column("text", { name: "observacion", nullable: true })
  observacion: string | null;
}
