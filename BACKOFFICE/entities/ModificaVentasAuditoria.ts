import { Column, Entity } from "typeorm";

@Entity("Modifica_Ventas_Auditoria", { schema: "dbo" })
export class ModificaVentasAuditoria {
  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nropos_nuevo", nullable: true, length: 10 })
  nroposNuevo: string | null;

  @Column("char", { name: "cdusuario_nuevo", nullable: true, length: 10 })
  cdusuarioNuevo: string | null;

  @Column("numeric", {
    name: "turno_nuevo",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  turnoNuevo: number | null;

  @Column("char", { name: "cdusuario_modifica", nullable: true, length: 10 })
  cdusuarioModifica: string | null;

  @Column("smalldatetime", { name: "fecha_modifica", nullable: true })
  fechaModifica: Date | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 300 })
  observacion: string | null;
}
