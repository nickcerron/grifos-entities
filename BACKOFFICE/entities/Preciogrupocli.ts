import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("PRECIOGRUPOCLI", { schema: "dbo" })
export class Preciogrupocli {
  @Column("char", { name: "CDGRUPOCLI", nullable: true, length: 5 })
  cdgrupocli: string | null;

  @Column("char", { name: "TIPOCLI", nullable: true, length: 3 })
  tipocli: string | null;

  @Column("char", { name: "CDARTICULO", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "PRECIO",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "TIPODES", nullable: true, length: 3 })
  tipodes: string | null;

  @PrimaryGeneratedColumn({ type: "int", name: "cdPrecioGCliente" })
  cdPrecioGCliente: number;
}
