import { Column, Entity } from "typeorm";

@Entity("despachos", { schema: "dbo" })
export class Despachos {
  @Column("char", { name: "codvehiculo", nullable: true, length: 20 })
  codvehiculo: string | null;

  @Column("char", { name: "codruta", nullable: true, length: 10 })
  codruta: string | null;

  @Column("date", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "hora", nullable: true, length: 12 })
  hora: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 12, scale: 2 })
  total: number | null;
}
