import { Column, Entity } from "typeorm";

@Entity("TmpTarjetasWebMIS", { schema: "dbo" })
export class TmpTarjetasWebMis {
  @Column("char", { name: "TarjAfiliacion", length: 20 })
  tarjAfiliacion: string;

  @Column("bigint", { name: "ClienteID", nullable: true })
  clienteId: string | null;

  @Column("int", { name: "Local", nullable: true })
  local: number | null;

  @Column("smalldatetime", { name: "FechaRegistro", nullable: true })
  fechaRegistro: Date | null;

  @Column("numeric", {
    name: "PtosAcumulado",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  ptosAcumulado: number | null;

  @Column("numeric", {
    name: "PtosCanjeados",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  ptosCanjeados: number | null;

  @Column("numeric", {
    name: "PtosDisponibles",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  ptosDisponibles: number | null;

  @Column("numeric", {
    name: "ValorAcumula",
    nullable: true,
    precision: 18,
    scale: 2,
  })
  valorAcumula: number | null;

  @Column("bit", { name: "Status", nullable: true })
  status: boolean | null;

  @Column("smalldatetime", { name: "Fechaultconsumo", nullable: true })
  fechaultconsumo: Date | null;
}
