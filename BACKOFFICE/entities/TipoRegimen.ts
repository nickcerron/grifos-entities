import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoRegimen", ["cdtpRegimen"], { unique: true })
@Entity("TipoRegimen", { schema: "dbo" })
export class TipoRegimen {
  @Column("char", { primary: true, name: "cdtpRegimen", length: 3 })
  cdtpRegimen: string;

  @Column("varchar", { name: "dstpRegimen", length: 30 })
  dstpRegimen: string;

  @Column("bit", { name: "flg_mostrar", nullable: true })
  flgMostrar: boolean | null;
}
