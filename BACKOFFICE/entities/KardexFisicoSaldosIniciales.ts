import { Column, Entity } from "typeorm";

@Entity("Kardex_Fisico_Saldos_Iniciales", { schema: "dbo" })
export class KardexFisicoSaldosIniciales {
  @Column("varchar", { name: "Periodo", nullable: true, length: 6 })
  periodo: string | null;

  @Column("varchar", { name: "CdArticulo", nullable: true, length: 20 })
  cdArticulo: string | null;

  @Column("numeric", {
    name: "cantidadi",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadi: number | null;
}
