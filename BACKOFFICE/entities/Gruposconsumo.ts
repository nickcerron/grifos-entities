import { Column, Entity, Index } from "typeorm";

@Index("pk_gruposconsumo", ["cdgrupos", "cdgrupo02"], { unique: true })
@Entity("gruposconsumo", { schema: "dbo" })
export class Gruposconsumo {
  @Column("char", { name: "cdgrupos", length: 5 })
  cdgrupos: string;

  @Column("char", { name: "cdgrupo02", length: 5 })
  cdgrupo02: string;

  @Column("char", { name: "dsgrupos", nullable: true, length: 40 })
  dsgrupos: string | null;
}
