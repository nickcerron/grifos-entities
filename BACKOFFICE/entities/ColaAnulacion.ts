import { Column, Entity, Index } from "typeorm";

@Index("PK_COLAANULACION", ["cdtipodoc", "nrodocumento", "nropos"], {
  unique: true,
})
@Entity("COLA_ANULACION", { schema: "dbo" })
export class ColaAnulacion {
  @Column("varchar", { primary: true, name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("varchar", { primary: true, name: "CDTIPODOC", length: 5 })
  cdtipodoc: string;

  @Column("varchar", { primary: true, name: "NROPOS", length: 50 })
  nropos: string;

  @Column("varchar", { name: "POS_ID", nullable: true, length: 50 })
  posId: string | null;

  @Column("varchar", { name: "RUC_EMISOR", length: 11 })
  rucEmisor: string;

  @Column("smalldatetime", { name: "FECHA_EMISION" })
  fechaEmision: Date;

  @Column("smalldatetime", { name: "FECHA_ENVIO", nullable: true })
  fechaEnvio: Date | null;

  @Column("varchar", { name: "LOCAL_ID", length: 11 })
  localId: string;

  @Column("varchar", {
    name: "DOCUMENTO_REFERENCIA",
    nullable: true,
    length: 15,
  })
  documentoReferencia: string | null;

  @Column("smallint", { name: "ESTADO", nullable: true, default: () => "(0)" })
  estado: number | null;

  @Column("varchar", { name: "OBSERVACION", nullable: true, length: 1000 })
  observacion: string | null;

  @Column("smalldatetime", { name: "TIMESTAMP" })
  timestamp: Date;

  @Column("bit", { name: "CORREOENVIADO", nullable: true })
  correoenviado: boolean | null;
}
