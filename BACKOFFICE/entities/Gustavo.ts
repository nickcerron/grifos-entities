import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_gustavo", ["idTabla"], { unique: true })
@Entity("gustavo", { schema: "dbo" })
export class Gustavo {
  @PrimaryGeneratedColumn({ type: "int", name: "id_tabla" })
  idTabla: number;

  @Column("nchar", { name: "campo1", nullable: true, length: 10 })
  campo1: string | null;

  @Column("smalldatetime", { name: "campo2", nullable: true })
  campo2: Date | null;

  @Column("numeric", {
    name: "campo3",
    nullable: true,
    precision: 18,
    scale: 0,
  })
  campo3: number | null;

  @Column("char", { name: "campo5", nullable: true, length: 10 })
  campo5: string | null;
}
