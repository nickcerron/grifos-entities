import { Column, Entity } from "typeorm";

@Entity("TmpHValeWebS", { schema: "dbo" })
export class TmpHValeWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "NroVale", length: 15 })
  nroVale: string;

  @Column("smalldatetime", { name: "fecvale", nullable: true })
  fecvale: Date | null;

  @Column("char", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("float", { name: "mtovale", nullable: true, precision: 53 })
  mtovale: number | null;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("char", { name: "nroplaca", nullable: true, length: 250 })
  nroplaca: string | null;

  @Column("varchar", { name: "NroSerieMaq", length: 15 })
  nroSerieMaq: string;

  @Column("varchar", { name: "TipoDocId", length: 5 })
  tipoDocId: string;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "NroSerieMaqFac", nullable: true, length: 15 })
  nroSerieMaqFac: string | null;

  @Column("char", { name: "cdtipodocfac", nullable: true, length: 5 })
  cdtipodocfac: string | null;

  @Column("varchar", { name: "nrodocumentofac", nullable: true, length: 15 })
  nrodocumentofac: string | null;

  @Column("smalldatetime", { name: "fecdocumentofac", nullable: true })
  fecdocumentofac: Date | null;

  @Column("smalldatetime", { name: "fecprocesofac", nullable: true })
  fecprocesofac: Date | null;

  @Column("char", { name: "placa", nullable: true, length: 10 })
  placa: string | null;

  @Column("int", { name: "turno", nullable: true })
  turno: number | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;

  @Column("varchar", { name: "DocVale", nullable: true, length: 10 })
  docVale: string | null;

  @Column("numeric", {
    name: "ConsumoMtoIni",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumoMtoIni: number | null;

  @Column("numeric", {
    name: "ConsumoMtoFin",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumoMtoFin: number | null;

  @Column("bit", { name: "Licitacion", nullable: true })
  licitacion: boolean | null;

  @Column("varchar", { name: "NroContrato", nullable: true, length: 60 })
  nroContrato: string | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;

  @Column("char", { name: "AreaId", nullable: true, length: 5 })
  areaId: string | null;
}
