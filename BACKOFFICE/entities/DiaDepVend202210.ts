import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaDepVend202210",
  ["cdlocal", "nrodeposito", "fecproceso", "turno", "cdvendedor", "cdtppago"],
  { unique: true }
)
@Entity("DiaDepVend202210", { schema: "dbo" })
export class DiaDepVend202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", { primary: true, name: "cdtppago", length: 5 })
  cdtppago: string;

  @Column("numeric", {
    primary: true,
    name: "nrodeposito",
    precision: 10,
    scale: 0,
  })
  nrodeposito: number;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolar",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolar: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("bit", { name: "glp", nullable: true })
  glp: boolean | null;

  @Column("numeric", {
    name: "nrosobres",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nrosobres: number | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "nroremito", nullable: true, length: 11 })
  nroremito: string | null;

  @Column("char", { name: "cdremito", nullable: true, length: 5 })
  cdremito: string | null;

  @Column("bit", { name: "detalle_deposito", nullable: true })
  detalleDeposito: boolean | null;

  @Column("int", { name: "cantmoneda10cen", nullable: true })
  cantmoneda10cen: number | null;

  @Column("int", { name: "cantmoneda20cen", nullable: true })
  cantmoneda20cen: number | null;

  @Column("int", { name: "cantmoneda50cen", nullable: true })
  cantmoneda50cen: number | null;

  @Column("int", { name: "cantmoneda1", nullable: true })
  cantmoneda1: number | null;

  @Column("int", { name: "cantmoneda2", nullable: true })
  cantmoneda2: number | null;

  @Column("int", { name: "cantmoneda5", nullable: true })
  cantmoneda5: number | null;

  @Column("int", { name: "cantbillete10", nullable: true })
  cantbillete10: number | null;

  @Column("int", { name: "cantbillete20", nullable: true })
  cantbillete20: number | null;

  @Column("int", { name: "cantbillete50", nullable: true })
  cantbillete50: number | null;

  @Column("int", { name: "cantbillete100", nullable: true })
  cantbillete100: number | null;

  @Column("int", { name: "cantbillete200", nullable: true })
  cantbillete200: number | null;

  @Column("int", { name: "cantmoneda1dol", nullable: true })
  cantmoneda1dol: number | null;

  @Column("int", { name: "cantmoneda2dol", nullable: true })
  cantmoneda2dol: number | null;

  @Column("int", { name: "cantbillete5dol", nullable: true })
  cantbillete5dol: number | null;

  @Column("int", { name: "cantbillete10dol", nullable: true })
  cantbillete10dol: number | null;

  @Column("int", { name: "cantbillete20dol", nullable: true })
  cantbillete20dol: number | null;

  @Column("int", { name: "cantbillete50dol", nullable: true })
  cantbillete50dol: number | null;

  @Column("int", { name: "cantbillete100dol", nullable: true })
  cantbillete100dol: number | null;

  @Column("int", { name: "cantmonedas", nullable: true })
  cantmonedas: number | null;

  @Column("int", { name: "cantbilletes", nullable: true })
  cantbilletes: number | null;

  @Column("numeric", {
    name: "mtomonedas",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtomonedas: number | null;

  @Column("numeric", {
    name: "mtobilletessol",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtobilletessol: number | null;

  @Column("numeric", {
    name: "mtobilletesdol",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtobilletesdol: number | null;
}
