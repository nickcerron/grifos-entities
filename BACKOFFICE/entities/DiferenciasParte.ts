import { Column, Entity, Index } from "typeorm";

@Index("PK_Diferencias_parte", ["fecproceso"], { unique: true })
@Entity("Diferencias_parte", { schema: "dbo" })
export class DiferenciasParte {
  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { name: "Contometropec", precision: 12, scale: 2 })
  contometropec: number;

  @Column("numeric", { name: "Contometromanual", precision: 12, scale: 2 })
  contometromanual: number;

  @Column("numeric", { name: "diferenciacontom", precision: 12, scale: 2 })
  diferenciacontom: number;

  @Column("numeric", { name: "Depbanco", precision: 12, scale: 2 })
  depbanco: number;

  @Column("numeric", { name: "Ventas", precision: 12, scale: 2 })
  ventas: number;

  @Column("numeric", { name: "diferenciaventas", precision: 12, scale: 2 })
  diferenciaventas: number;
}
