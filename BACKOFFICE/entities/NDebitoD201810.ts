import { Column, Entity, Index } from "typeorm";

@Index("PK_NDebitoD201810", ["cdlocal", "nrondebito", "nroitem"], {
  unique: true,
})
@Entity("NDebitoD201810", { schema: "dbo" })
export class NDebitoD201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nrondebito", length: 10 })
  nrondebito: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("varchar", { name: "dsarticulo", length: 60 })
  dsarticulo: string;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;
}
