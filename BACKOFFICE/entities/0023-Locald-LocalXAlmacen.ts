import { Column, Entity, Index } from "typeorm";
//la tabla no se usa pero se comenzara a usar
@Index("PK_locald", ["cdlocal", "cdalmacen"], { unique: true })
@Entity("locald", { schema: "dbo" })
export class Locald {
  //referencia a local
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  //referencia a almacen
  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;
}
