import { Column, Entity, Index } from "typeorm";

@Index("PK_Stock_Ventas", ["cdAlmacen", "cdArticulo"], { unique: true })
@Entity("Stock_Ventas", { schema: "dbo" })
export class StockVentas {
  @Column("varchar", { primary: true, name: "cdAlmacen", length: 3 })
  cdAlmacen: string;

  @Column("varchar", { primary: true, name: "cdArticulo", length: 20 })
  cdArticulo: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidad: number | null;
}
