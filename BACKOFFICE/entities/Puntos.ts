import { Column, Entity, Index } from "typeorm";

@Index("PK_PUNTOS", ["codigo", "programa"], { unique: true })
@Entity("PUNTOS", { schema: "dbo" })
export class Puntos {
  @Column("varchar", { primary: true, name: "Codigo", length: 20 })
  codigo: string;

  @Column("numeric", {
    name: "Puntos",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  puntos: number | null;

  @Column("varchar", { primary: true, name: "Programa", length: 50 })
  programa: string;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  @Column("char", { name: "CdLocal", nullable: true, length: 3 })
  cdLocal: string | null;

  @Column("char", { name: "NroSerieMaq", nullable: true, length: 20 })
  nroSerieMaq: string | null;

  @Column("char", { name: "CdTipoDoc", nullable: true, length: 5 })
  cdTipoDoc: string | null;

  @Column("char", { name: "NroDocumento", nullable: true, length: 10 })
  nroDocumento: string | null;
}
