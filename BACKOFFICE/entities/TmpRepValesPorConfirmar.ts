import { Column, Entity } from "typeorm";

@Entity("TmpRepValesPorConfirmar", { schema: "dbo" })
export class TmpRepValesPorConfirmar {
  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "NROVALE", nullable: true, length: 12 })
  nrovale: string | null;

  @Column("bit", { name: "FLGCONFIRMADOCENTRAL", nullable: true })
  flgconfirmadocentral: boolean | null;
}
