import { Column, Entity } from "typeorm";

@Entity("ContometosZeta", { schema: "dbo" })
export class ContometosZeta {
  @Column("numeric", { name: "nrozeta", precision: 15, scale: 0 })
  nrozeta: number;

  @Column("char", { name: "cara", length: 2 })
  cara: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("numeric", {
    name: "VolumenFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  volumenFinal: number | null;

  @Column("numeric", {
    name: "MontoFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  montoFinal: number | null;
}
