import { Column, Entity } from "typeorm";

@Entity("TmpClientesWebMIS", { schema: "dbo" })
export class TmpClientesWebMis {
  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("bigint", { name: "ClienteID", nullable: true })
  clienteId: string | null;

  @Column("int", { name: "TipoCliente" })
  tipoCliente: number;

  @Column("varchar", { name: "RazSoc", nullable: true, length: 50 })
  razSoc: string | null;

  @Column("char", { name: "Ruc", nullable: true, length: 15 })
  ruc: string | null;

  @Column("char", { name: "drcliente", nullable: true, length: 60 })
  drcliente: string | null;

  @Column("char", { name: "cddistrito", nullable: true, length: 2 })
  cddistrito: string | null;

  @Column("char", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null;

  @Column("varchar", { name: "provtelef1", nullable: true, length: 50 })
  provtelef1: string | null;

  @Column("char", { name: "tlfcliente1", nullable: true, length: 15 })
  tlfcliente1: string | null;

  @Column("varchar", { name: "provtelef2", nullable: true, length: 50 })
  provtelef2: string | null;

  @Column("varchar", { name: "Correo", nullable: true, length: 30 })
  correo: string | null;

  @Column("varchar", { name: "Contacto", length: 1 })
  contacto: string;

  @Column("int", { name: "Afil" })
  afil: number;

  @Column("smalldatetime", { name: "FechaAf", nullable: true })
  fechaAf: Date | null;

  @Column("varchar", { name: "FecNac", length: 10 })
  fecNac: string;

  @Column("char", { name: "tipocli", length: 1 })
  tipocli: string;

  @Column("int", { name: "LmtCrd" })
  lmtCrd: number;

  @Column("int", { name: "Status" })
  status: number;
}
