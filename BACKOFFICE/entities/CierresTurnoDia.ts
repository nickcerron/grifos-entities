import { Column, Entity } from "typeorm";

@Entity("cierres_turno_dia", { schema: "dbo" })
export class CierresTurnoDia {
  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "turno", nullable: true, length: 10 })
  turno: string | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "seriehd", nullable: true, length: 10 })
  seriehd: string | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 50 })
  observacion: string | null;
}
