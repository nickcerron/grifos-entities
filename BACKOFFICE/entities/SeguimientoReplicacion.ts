import { Column, Entity } from "typeorm";

@Entity("SeguimientoReplicacion", { schema: "dbo" })
export class SeguimientoReplicacion {
  @Column("char", { name: "CDLOCAL", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "NROSERIEMAQ", nullable: true, length: 15 })
  nroseriemaq: string | null;

  @Column("char", { name: "CDTIPODOC", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "NRODOCUMENTO", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("int", { name: "NUMERO", nullable: true })
  numero: number | null;

  @Column("varchar", { name: "CODIGO", nullable: true, length: 20 })
  codigo: string | null;

  @Column("varchar", { name: "MENSAJE", nullable: true, length: 250 })
  mensaje: string | null;

  @Column("datetime", { name: "FECHAERROR", nullable: true })
  fechaerror: Date | null;
}
