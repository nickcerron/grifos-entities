import { Column, Entity, Index } from "typeorm";

@Index("pk_Vehiculo_Guia", ["cdplaca", "cdtpGuia", "nroGuia"], { unique: true })
@Entity("Vehiculo_Guia", { schema: "dbo" })
export class VehiculoGuia {
  @Column("char", { primary: true, name: "cdplaca", length: 20 })
  cdplaca: string;

  @Column("char", { primary: true, name: "cdtpGuia", length: 5 })
  cdtpGuia: string;

  @Column("char", { primary: true, name: "nroGuia", length: 10 })
  nroGuia: string;
}
