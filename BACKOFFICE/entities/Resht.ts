import { Column, Entity } from "typeorm";

@Entity("RESHT", { schema: "dbo" })
export class Resht {
  @Column("varchar", { name: "Codempleado", nullable: true, length: 10 })
  codempleado: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 1, scale: 0 })
  turno: number | null;

  @Column("datetime", { name: "fechaI", nullable: true })
  fechaI: Date | null;

  @Column("datetime", { name: "fechaS", nullable: true })
  fechaS: Date | null;

  @Column("numeric", { name: "MT", nullable: true, precision: 5, scale: 0 })
  mt: number | null;

  @Column("numeric", { name: "HD", nullable: true, precision: 8, scale: 2 })
  hd: number | null;

  @Column("numeric", { name: "HE", nullable: true, precision: 8, scale: 2 })
  he: number | null;

  @Column("numeric", { name: "HT", nullable: true, precision: 8, scale: 2 })
  ht: number | null;
}
