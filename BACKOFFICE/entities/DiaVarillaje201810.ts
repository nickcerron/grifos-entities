import { Column, Entity, Index } from "typeorm";

@Index("PK_DiaVarillaje201810", ["cdlocal", "fecproceso", "cdarticulo"], {
  unique: true,
})
@Entity("DiaVarillaje201810", { schema: "dbo" })
export class DiaVarillaje201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "inicial",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  inicial: number | null;

  @Column("numeric", {
    name: "compra",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  compra: number | null;

  @Column("numeric", { name: "venta", nullable: true, precision: 15, scale: 3 })
  venta: number | null;

  @Column("numeric", { name: "final", nullable: true, precision: 15, scale: 3 })
  final: number | null;

  @Column("numeric", {
    name: "varillaje",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  varillaje: number | null;

  @Column("numeric", {
    name: "difdia",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difdia: number | null;

  @Column("numeric", {
    name: "difmes",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difmes: number | null;

  @Column("numeric", {
    name: "variacion",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  variacion: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 15, scale: 2 })
  total: number | null;
}
