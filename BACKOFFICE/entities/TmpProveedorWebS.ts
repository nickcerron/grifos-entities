import { Column, Entity } from "typeorm";

@Entity("TmpProveedorWebS", { schema: "dbo" })
export class TmpProveedorWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("varchar", { name: "ProveedorId", length: 15 })
  proveedorId: string;

  @Column("varchar", { name: "RazonSocial", nullable: true, length: 60 })
  razonSocial: string | null;

  @Column("varchar", { name: "RUC", nullable: true, length: 15 })
  ruc: string | null;

  @Column("varchar", { name: "Direccion", nullable: true, length: 60 })
  direccion: string | null;

  @Column("varchar", { name: "Telefono1", nullable: true, length: 15 })
  telefono1: string | null;

  @Column("varchar", { name: "Telefono2", nullable: true, length: 15 })
  telefono2: string | null;

  @Column("varchar", { name: "Fax", nullable: true, length: 15 })
  fax: string | null;

  @Column("varchar", { name: "Correo", nullable: true, length: 50 })
  correo: string | null;
}
