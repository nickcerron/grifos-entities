import { Column, Entity } from "typeorm";

@Entity("TmpLocalWebS", { schema: "dbo" })
export class TmpLocalWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "NombreLocal", nullable: true, length: 50 })
  nombreLocal: string | null;
}
