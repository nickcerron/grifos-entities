import { Column, Entity, Index } from "typeorm";

@Index("PK_profencabezado", ["cdencabezado"], { unique: true })
@Entity("profencabezado", { schema: "dbo" })
export class Profencabezado {
  @Column("char", { primary: true, name: "cdencabezado", length: 5 })
  cdencabezado: string;

  @Column("text", { name: "memencabezado", nullable: true })
  memencabezado: string | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;
}
