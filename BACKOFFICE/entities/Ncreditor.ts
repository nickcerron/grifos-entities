import { Column, Entity, Index } from "typeorm";

@Index("PK_ncreditor", ["cdlocal", "fecncredito", "fecproceso"], {
  unique: true,
})
@Entity("ncreditor", { schema: "dbo" })
export class Ncreditor {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecncredito" })
  fecncredito: Date;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;
}
