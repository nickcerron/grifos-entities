import { Column, Entity, Index } from "typeorm";

@Index("pk_Transportista", ["cdcliente", "cdtranspor"], { unique: true })
@Entity("Transportista", { schema: "dbo" })
export class Transportista {
  @Column("char", { primary: true, name: "cdcliente", length: 20 })
  cdcliente: string;

  @Column("char", { primary: true, name: "cdtranspor", length: 20 })
  cdtranspor: string;

  @Column("varchar", { name: "dstranspor", nullable: true, length: 120 })
  dstranspor: string | null;

  @Column("varchar", { name: "drtranspor", nullable: true, length: 120 })
  drtranspor: string | null;

  @Column("varchar", { name: "ructranspor", nullable: true, length: 20 })
  ructranspor: string | null;

  @Column("varchar", { name: "nroplaca", nullable: true, length: 20 })
  nroplaca: string | null;

  @Column("varchar", { name: "tlftranspor", nullable: true, length: 30 })
  tlftranspor: string | null;

  @Column("varchar", { name: "marcavehic", nullable: true, length: 20 })
  marcavehic: string | null;

  @Column("varchar", { name: "inscripcion", nullable: true, length: 20 })
  inscripcion: string | null;

  @Column("varchar", { name: "nrolicencia", nullable: true, length: 15 })
  nrolicencia: string | null;

  @Column("varchar", { name: "vehiculo", nullable: true, length: 50 })
  vehiculo: string | null;

  @Column("varchar", { name: "NroRegMTCTrans", nullable: true, length: 20 })
  nroRegMtcTrans: string | null;

  @Column("varchar", { name: "NroAutorizaTrans", nullable: true, length: 50 })
  nroAutorizaTrans: string | null;

  @Column("char", { name: "cdAutorizaTrans", nullable: true, length: 2 })
  cdAutorizaTrans: string | null;
}
