import { Column, Entity } from "typeorm";

@Entity("insumoistemp", { schema: "dbo" })
export class Insumoistemp {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "nroseriemaq", length: 15 })
  nroseriemaq: string;

  @Column("char", { name: "cdtpmov", length: 5 })
  cdtpmov: string;

  @Column("char", { name: "nromov", length: 10 })
  nromov: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nrodocumento", length: 15 })
  nrodocumento: string;

  @Column("char", { name: "movimiento", length: 1 })
  movimiento: string;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("numeric", { name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  ctoreposicion: number | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  ctopromedio: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("bit", { name: "flganulacion", nullable: true })
  flganulacion: boolean | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;
}
