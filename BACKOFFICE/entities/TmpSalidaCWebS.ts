import { Column, Entity } from "typeorm";

@Entity("TmpSalidaCWebS", { schema: "dbo" })
export class TmpSalidaCWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "SalidaId", length: 5 })
  salidaId: string;

  @Column("char", { name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("char", { name: "TipoDocId", nullable: true, length: 5 })
  tipoDocId: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("char", { name: "ProveedorId", nullable: true, length: 15 })
  proveedorId: string | null;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("char", { name: "AlmacenId_Destino", nullable: true, length: 3 })
  almacenIdDestino: string | null;

  @Column("char", { name: "nroingreso", nullable: true, length: 10 })
  nroingreso: string | null;

  @Column("char", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("float", { name: "mtosubtotal", nullable: true, precision: 53 })
  mtosubtotal: number | null;

  @Column("float", { name: "mtoimpuesto", nullable: true, precision: 53 })
  mtoimpuesto: number | null;

  @Column("float", { name: "mtoimpuesto1", nullable: true, precision: 53 })
  mtoimpuesto1: number | null;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("float", { name: "tcambio", nullable: true, precision: 53 })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("varchar", { name: "UserIdReg", nullable: true, length: 20 })
  userIdReg: string | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("varchar", { name: "UserIdAnula", nullable: true, length: 20 })
  userIdAnula: string | null;

  @Column("bit", { name: "IsSAP", nullable: true })
  isSap: boolean | null;
}
