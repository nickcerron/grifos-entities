import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_ingresodev",
  ["cdlocal", "cdtpingreso", "nroingreso", "nroitem", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("ingresodev", { schema: "dbo" })
export class Ingresodev {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("char", { primary: true, name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("bit", { name: "flgfaltante", nullable: true })
  flgfaltante: boolean | null;

  @Column("bit", { name: "flgdeterioro", nullable: true })
  flgdeterioro: boolean | null;

  @Column("bit", { name: "flgotros", nullable: true })
  flgotros: boolean | null;

  @Column("char", { name: "observacion", nullable: true, length: 70 })
  observacion: string | null;

  @Column("bit", { name: "flgdevuelto", nullable: true })
  flgdevuelto: boolean | null;

  @Column("char", { name: "movimiento", nullable: true, length: 1 })
  movimiento: string | null;

  @Column("char", { name: "nromov", nullable: true, length: 10 })
  nromov: string | null;

  @Column("char", { name: "movimiento1", nullable: true, length: 1 })
  movimiento1: string | null;

  @Column("char", { name: "nromov1", nullable: true, length: 10 })
  nromov1: string | null;
}
