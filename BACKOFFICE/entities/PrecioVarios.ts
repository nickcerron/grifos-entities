import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index(
  "PK_precio_varios_1",
  ["cdcliente", "cdarticulo", "tipocli", "tiporango", "tipo", "rango1"],
  { unique: true }
)
@Entity("precio_varios", { schema: "dbo" })
export class PrecioVarios {
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { primary: true, name: "tiporango", length: 3 })
  tiporango: string;

  @Column("numeric", { primary: true, name: "rango1", precision: 10, scale: 3 })
  rango1: number;

  @Column("numeric", {
    name: "rango2",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  rango2: number | null;

  @Column("smalldatetime", { name: "fechamodificacion", nullable: true })
  fechamodificacion: Date | null;

  @Column("char", { primary: true, name: "tipo", length: 3 })
  tipo: string;

  @Column("numeric", { name: "valor", nullable: true, precision: 10, scale: 3 })
  valor: number | null;

  @Column("varchar", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("bit", { name: "flgAplicaDsctoAuto", nullable: true })
  flgAplicaDsctoAuto: boolean | null;

  @PrimaryGeneratedColumn({ type: "int", name: "cdPrecioVarios" })
  cdPrecioVarios: number;
}
