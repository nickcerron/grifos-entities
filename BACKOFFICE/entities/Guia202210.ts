import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Guia202210",
  ["cdtipodoc", "cdlocal", "cdTpGuia", "nroGuia", "rucempresa"],
  { unique: true }
)
@Entity("Guia202210", { schema: "dbo" })
export class Guia202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "CdTpGuia", length: 5 })
  cdTpGuia: string;

  @Column("char", { primary: true, name: "NroGuia", length: 10 })
  nroGuia: string;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;

  @Column("varchar", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdalmdest", nullable: true, length: 3 })
  cdalmdest: string | null;

  @Column("smalldatetime", { name: "fecGuia", nullable: true })
  fecGuia: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("bit", { name: "Transbordo", nullable: true })
  transbordo: boolean | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 250 })
  observacion: string | null;

  @Column("char", { name: "cdtpTransporte", nullable: true, length: 2 })
  cdtpTransporte: string | null;

  @Column("char", { name: "cdtpTraslado", nullable: true, length: 3 })
  cdtpTraslado: string | null;

  @Column("smalldatetime", { name: "FecInicio", nullable: true })
  fecInicio: Date | null;

  @Column("char", { name: "UbigeoOrigen", nullable: true, length: 6 })
  ubigeoOrigen: string | null;

  @Column("char", { name: "UbigeoDestino", nullable: true, length: 6 })
  ubigeoDestino: string | null;

  @Column("varchar", { name: "DireccionOrigen", nullable: true, length: 100 })
  direccionOrigen: string | null;

  @Column("varchar", { name: "DireccionDestino", nullable: true, length: 100 })
  direccionDestino: string | null;

  @Column("varchar", { name: "cdtranspor", nullable: true, length: 20 })
  cdtranspor: string | null;

  @Column("numeric", {
    name: "totalpeso",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  totalpeso: number | null;

  @Column("bit", { name: "electronico", nullable: true })
  electronico: boolean | null;

  @Column("varchar", { name: "Motivo", nullable: true, length: 100 })
  motivo: string | null;

  @Column("numeric", {
    name: "bultos",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  bultos: number | null;

  @Column("varchar", { name: "NroDAM", nullable: true, length: 20 })
  nroDam: string | null;

  @Column("varchar", { name: "cdhash", nullable: true, length: 50 })
  cdhash: string | null;

  @Column("varchar", { name: "cdQR", nullable: true, length: 50 })
  cdQr: string | null;

  @Column("varchar", { name: "NroRegMTCTrans", nullable: true, length: 20 })
  nroRegMtcTrans: string | null;

  @Column("varchar", { name: "NroAutorizaTrans", nullable: true, length: 50 })
  nroAutorizaTrans: string | null;

  @Column("char", { name: "cdAutorizaTrans", nullable: true, length: 2 })
  cdAutorizaTrans: string | null;

  @Column("varchar", { name: "NroContenedor", nullable: true, length: 11 })
  nroContenedor: string | null;

  @Column("bit", { name: "Categoria", nullable: true })
  categoria: boolean | null;
}
