import { Column, Entity, Index } from "typeorm";

@Index("PK_NCredito202211", ["cdlocal", "nroncredito", "cddocorigen"], {
  unique: true,
})
@Entity("NCredito202211", { schema: "dbo" })
export class NCredito202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroncredito", length: 10 })
  nroncredito: string;

  @Column("smalldatetime", { name: "fecncredito", nullable: true })
  fecncredito: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "NroSerie1", nullable: true, length: 15 })
  nroSerie1: string | null;

  @Column("char", { name: "NroSerie2", nullable: true, length: 10 })
  nroSerie2: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("numeric", {
    name: "tcambioorigen",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambioorigen: number | null;

  @Column("char", { name: "observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("bit", { name: "flgdevolucion", nullable: true })
  flgdevolucion: boolean | null;

  @Column("char", { name: "Tiponcredito", nullable: true, length: 1 })
  tiponcredito: string | null;

  @Column("char", { name: "cdmotncredito", nullable: true, length: 5 })
  cdmotncredito: string | null;

  @Column("varchar", { name: "pdf417", nullable: true, length: 450 })
  pdf417: string | null;

  @Column("varchar", { name: "cdhash", nullable: true, length: 50 })
  cdhash: string | null;

  @Column("varchar", { name: "sustento", nullable: true, length: 60 })
  sustento: string | null;

  @Column("varchar", { name: "docreferencia", nullable: true, length: 13 })
  docreferencia: string | null;

  @Column("datetime", { name: "fecreferencia", nullable: true })
  fecreferencia: Date | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "flgmanual", nullable: true })
  flgmanual: boolean | null;

  @Column("char", {
    primary: true,
    name: "cddocorigen",
    length: 5,
    default: () => "' '",
  })
  cddocorigen: string;

  @Column("numeric", {
    name: "mtoimp_otros",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpOtros: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("bit", { name: "contingencia", nullable: true })
  contingencia: boolean | null;

  @Column("numeric", {
    name: "mtogratuito",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtogratuito: number | null;

  @Column("numeric", {
    name: "redondea_indecopi",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  redondeaIndecopi: number | null;

  @Column("varchar", { name: "observacion_anula", nullable: true, length: 250 })
  observacionAnula: string | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;

  @Column("bit", { name: "flgLiberaND", nullable: true })
  flgLiberaNd: boolean | null;
}
