import { Column, Entity, Index } from "typeorm";

@Index("PK_ColaImpresion", ["cdtipodoc", "nrodocumento", "nropos"], {
  unique: true,
})
@Entity("ColaImpresion", { schema: "dbo" })
export class ColaImpresion {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "nropos", length: 10 })
  nropos: string;

  @Column("varchar", { name: "formato", nullable: true, length: 50 })
  formato: string | null;

  @Column("smalldatetime", { name: "fecdocumento" })
  fecdocumento: Date;

  @Column("varchar", { name: "impresora", length: 40 })
  impresora: string;

  @Column("varchar", { name: "trama", nullable: true })
  trama: string | null;

  @Column("varchar", { name: "json", nullable: true })
  json: string | null;

  @Column("varchar", { name: "hash", length: 50 })
  hash: string;

  @Column("bit", { name: "impreso" })
  impreso: boolean;

  @Column("varchar", { name: "observacion", nullable: true, length: 1000 })
  observacion: string | null;

  @Column("smalldatetime", { name: "timestamp" })
  timestamp: Date;

  @Column("bit", { name: "correoenviado", nullable: true })
  correoenviado: boolean | null;
}
