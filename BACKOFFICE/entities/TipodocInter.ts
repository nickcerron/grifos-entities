import { Column, Entity, Index } from "typeorm";

@Index("PK_tipodoc_inter", ["cdtipodoc"], { unique: true })
@Entity("tipodoc_inter", { schema: "dbo" })
export class TipodocInter {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "tipo_conta", nullable: true, length: 1 })
  tipoConta: string | null;

  @Column("char", { name: "CTDCODIGO", nullable: true, length: 2 })
  ctdcodigo: string | null;
}
