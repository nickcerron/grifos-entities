import { Column, Entity, Index } from "typeorm";

@Index("PK_compracc", ["fechora"], { unique: true })
@Entity("compracc", { schema: "dbo" })
export class Compracc {
  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("char", { name: "cdtipodoc1", nullable: true, length: 5 })
  cdtipodoc1: string | null;

  @Column("char", { name: "nrodocumento1", nullable: true, length: 10 })
  nrodocumento1: string | null;

  @Column("smalldatetime", { name: "fecdocumento1", nullable: true })
  fecdocumento1: Date | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "dsproveedor", nullable: true, length: 60 })
  dsproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "feccompra", nullable: true })
  feccompra: Date | null;

  @Column("char", { name: "observacion", nullable: true, length: 50 })
  observacion: string | null;
}
