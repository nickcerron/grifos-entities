import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_P__3214EC0799F0EC3A", ["id"], { unique: true })
@Entity("Kardex_Productos_Todos", { schema: "dbo" })
export class KardexProductosTodos {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
}
