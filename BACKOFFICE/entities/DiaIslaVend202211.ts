import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaIslaVend202211",
  ["cdlocal", "fecproceso", "turno", "isla", "cdvendedor", "lado"],
  { unique: true }
)
@Entity("DiaIslaVend202211", { schema: "dbo" })
export class DiaIslaVend202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "isla", length: 2 })
  isla: string;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", {
    primary: true,
    name: "lado",
    length: 2,
    default: () => "'01'",
  })
  lado: string;
}
