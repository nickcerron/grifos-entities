import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_F__7CFB65B276C84403", ["periodo"], { unique: true })
@Entity("Kardex_Fisico_Periodo_Inicial_UpdateStock", { schema: "dbo" })
export class KardexFisicoPeriodoInicialUpdateStock {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { primary: true, name: "Periodo", length: 6 })
  periodo: string;
}
