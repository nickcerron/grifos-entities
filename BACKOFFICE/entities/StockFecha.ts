import { Column, Entity, Index } from "typeorm";

@Index("PK_stock_fecha", ["cdalmacen", "cdarticulo", "fecproceso"], {
  unique: true,
})
@Entity("stock_fecha", { schema: "dbo" })
export class StockFecha {
  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;
}
