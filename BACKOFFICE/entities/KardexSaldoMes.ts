import { Column, Entity, Index } from "typeorm";

@Index("PK_Kardex_Saldo_mes", ["cdarticulo", "periodo", "cdTpClienteKardex"], {
  unique: true,
})
@Entity("Kardex_Saldo_mes", { schema: "dbo" })
export class KardexSaldoMes {
  @Column("varchar", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { primary: true, name: "periodo", length: 6 })
  periodo: string;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;

  @Column("numeric", {
    name: "costof",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costof: number | null;

  @Column("numeric", {
    name: "totalf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totalf: number | null;

  @Column("char", {
    primary: true,
    name: "cdTpClienteKardex",
    length: 5,
    default: () => "''",
  })
  cdTpClienteKardex: string;
}
