import { Column, Entity, Index } from "typeorm";

@Index("PK_Salida202211", ["cdlocal", "cdtpsalida", "nrosalida"], {
  unique: true,
})
@Entity("Salida202211", { schema: "dbo" })
export class Salida202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("char", { primary: true, name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdalmdest", nullable: true, length: 3 })
  cdalmdest: string | null;

  @Column("char", { name: "nroingreso", nullable: true, length: 10 })
  nroingreso: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 250 })
  observacion: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("bit", { name: "IsSAP", nullable: true, default: () => "(1)" })
  isSap: boolean | null;

  @Column("char", { name: "UbigeoOrigen", nullable: true, length: 6 })
  ubigeoOrigen: string | null;

  @Column("char", { name: "UbigeoDestino", nullable: true, length: 6 })
  ubigeoDestino: string | null;

  @Column("varchar", { name: "DireccionOrigen", nullable: true, length: 100 })
  direccionOrigen: string | null;

  @Column("varchar", { name: "DireccionDestino", nullable: true, length: 100 })
  direccionDestino: string | null;

  @Column("char", { name: "cdtpTransporte", nullable: true, length: 2 })
  cdtpTransporte: string | null;

  @Column("char", { name: "cdtpTraslado", nullable: true, length: 3 })
  cdtpTraslado: string | null;

  @Column("char", { name: "cdtpguia", nullable: true, length: 5 })
  cdtpguia: string | null;

  @Column("smalldatetime", { name: "FecInicio", nullable: true })
  fecInicio: Date | null;

  @Column("char", { name: "cdDestinatario", nullable: true, length: 20 })
  cdDestinatario: string | null;

  @Column("varchar", { name: "vehiculo", nullable: true, length: 50 })
  vehiculo: string | null;

  @Column("varchar", { name: "nroplaca", nullable: true, length: 8 })
  nroplaca: string | null;

  @Column("varchar", { name: "cdtranspor", nullable: true, length: 20 })
  cdtranspor: string | null;

  @Column("numeric", {
    name: "totalpeso",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  totalpeso: number | null;

  @Column("varchar", { name: "cdhash", nullable: true, length: 50 })
  cdhash: string | null;

  @Column("varchar", { name: "cdQR", nullable: true, length: 50 })
  cdQr: string | null;

  @Column("varchar", { name: "docreferencia", nullable: true, length: 13 })
  docreferencia: string | null;

  @Column("bit", { name: "electronico", nullable: true })
  electronico: boolean | null;

  @Column("varchar", { name: "Motivo", nullable: true, length: 100 })
  motivo: string | null;

  @Column("numeric", {
    name: "bultos",
    nullable: true,
    precision: 11,
    scale: 0,
  })
  bultos: number | null;

  @Column("varchar", { name: "NroDam", nullable: true, length: 20 })
  nroDam: string | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
