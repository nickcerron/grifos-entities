import { Column, Entity } from "typeorm";

@Entity("TmpVentasWebGNV", { schema: "dbo" })
export class TmpVentasWebGnv {
  @Column("char", { name: "CdTipoDoc", nullable: true, length: 5 })
  cdTipoDoc: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("char", { name: "Tipo", length: 1 })
  tipo: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("int", { name: "EmpresaID" })
  empresaId: number;

  @Column("int", { name: "LocalID", nullable: true })
  localId: number | null;

  @Column("char", { name: "CdProducto", length: 20 })
  cdProducto: string;

  @Column("smallint", { name: "Item", nullable: true })
  item: number | null;

  @Column("varchar", { name: "NroSerieMaq", length: 20 })
  nroSerieMaq: string;

  @Column("char", { name: "TarjAfiliacion", length: 20 })
  tarjAfiliacion: string;

  @Column("bigint", { name: "ClienteId", nullable: true })
  clienteId: string | null;

  @Column("char", { name: "NroPos", length: 10 })
  nroPos: string;

  @Column("datetime", { name: "FechaDoc", nullable: true })
  fechaDoc: Date | null;

  @Column("smalldatetime", { name: "FechaSis", nullable: true })
  fechaSis: Date | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tCambio: number | null;

  @Column("int", { name: "Impuesto" })
  impuesto: number;

  @Column("numeric", {
    name: "Precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "Cantidad",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  cantidad: number | null;

  @Column("int", { name: "MtoDescto" })
  mtoDescto: number;

  @Column("int", { name: "MtoSubTotal" })
  mtoSubTotal: number;

  @Column("int", { name: "MtoImpuesto" })
  mtoImpuesto: number;

  @Column("numeric", { name: "Total", nullable: true, precision: 10, scale: 2 })
  total: number | null;

  @Column("numeric", { name: "Puntos", precision: 11, scale: 0 })
  puntos: number;

  @Column("nchar", { name: "Usuario", nullable: true, length: 10 })
  usuario: string | null;

  @Column("numeric", { name: "Turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "Enviado", nullable: true })
  enviado: boolean | null;

  @Column("numeric", { name: "ValorAcumPto", precision: 18, scale: 2 })
  valorAcumPto: number;

  @Column("int", { name: "Almacen", nullable: true })
  almacen: number | null;

  @Column("int", { name: "Credito" })
  credito: number;

  @Column("int", { name: "NroDocumentoAnul", nullable: true })
  nroDocumentoAnul: number | null;
}
