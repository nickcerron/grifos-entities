import { Column, Entity, Index } from "typeorm";

@Index("PK_op_tran_1", ["numero", "cTurno"], { unique: true })
@Entity("op_tran_historico", { schema: "dbo" })
export class OpTranHistorico {
  @Column("bigint", { name: "c_interno" })
  cInterno: string;

  @Column("char", { primary: true, name: "numero", length: 4 })
  numero: string;

  @Column("numeric", { name: "soles", nullable: true, precision: 11, scale: 2 })
  soles: number | null;

  @Column("char", { name: "producto", nullable: true, length: 2 })
  producto: string | null;

  @Column("numeric", { name: "precio", nullable: true, precision: 6, scale: 2 })
  precio: number | null;

  @Column("numeric", {
    name: "galones",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  galones: number | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("smalldatetime", { name: "hora", nullable: true })
  hora: Date | null;

  @Column("char", { name: "turno", nullable: true, length: 1 })
  turno: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 11 })
  cdtipodoc: string | null;

  @Column("char", { name: "documento", nullable: true, length: 12 })
  documento: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("nchar", { name: "MANGUERA", nullable: true, length: 1 })
  manguera: string | null;

  @Column("bigint", { primary: true, name: "c_turno" })
  cTurno: string;

  @Column("datetime", {
    name: "fecha_hora_servidor",
    nullable: true,
    default: () => "getdate()",
  })
  fechaHoraServidor: Date | null;

  @Column("char", {
    name: "c_tipo",
    nullable: true,
    length: 2,
    default: () => "'SC'",
  })
  cTipo: string | null;

  @Column("varchar", { name: "c_punto_venta", nullable: true, length: 15 })
  cPuntoVenta: string | null;

  @Column("int", { name: "n_turno", nullable: true })
  nTurno: number | null;
}
