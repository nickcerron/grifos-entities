import { Column, Entity, Index } from "typeorm";

@Index("PK_tipoguia", ["cdtpguia"], { unique: true })
@Entity("tipoguia", { schema: "dbo" })
export class Tipoguia {
  @Column("char", { primary: true, name: "cdtpguia", length: 5 })
  cdtpguia: string;

  @Column("char", { name: "dstpguia", nullable: true, length: 40 })
  dstpguia: string | null;

  @Column("bit", { name: "flgvalorizado", nullable: true })
  flgvalorizado: boolean | null;

  @Column("bit", { name: "flgfacturable", nullable: true })
  flgfacturable: boolean | null;

  @Column("bit", { name: "flgsistema", nullable: true })
  flgsistema: boolean | null;

  @Column("char", { name: "nrocorrelativo", nullable: true, length: 10 })
  nrocorrelativo: string | null;

  @Column("bit", { name: "flgmostrar", nullable: true })
  flgmostrar: boolean | null;
}
