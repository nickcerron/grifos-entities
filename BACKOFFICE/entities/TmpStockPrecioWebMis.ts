import { Column, Entity } from "typeorm";

@Entity("TmpStockPrecioWebMIS", { schema: "dbo" })
export class TmpStockPrecioWebMis {
  @Column("int", { name: "EmpresaID" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("int", { name: "AlmacenID", nullable: true })
  almacenId: number | null;

  @Column("char", { name: "cdArticulo", length: 20 })
  cdArticulo: string;

  @Column("numeric", {
    name: "StockActual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockActual: number | null;

  @Column("numeric", { name: "Precio", precision: 12, scale: 4 })
  precio: number;
}
