import { Column, Entity } from "typeorm";

@Entity("atikulo", { schema: "dbo" })
export class Atikulo {
  @Column("nvarchar", { name: "cdarticulo", nullable: true, length: 255 })
  cdarticulo: string | null;

  @Column("nvarchar", { name: "dsarticulo", nullable: true, length: 255 })
  dsarticulo: string | null;

  @Column("float", { name: "precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("nvarchar", { name: "cdbarra", nullable: true, length: 255 })
  cdbarra: string | null;
}
