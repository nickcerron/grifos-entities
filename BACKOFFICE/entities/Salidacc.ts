import { Column, Entity, Index } from "typeorm";

@Index("PK_salidacc", ["cdtpsalida", "fechora"], { unique: true })
@Entity("salidacc", { schema: "dbo" })
export class Salidacc {
  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "dsproveedor", nullable: true, length: 60 })
  dsproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("char", { name: "observacion", nullable: true, length: 50 })
  observacion: string | null;
}
