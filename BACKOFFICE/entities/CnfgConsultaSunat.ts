import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("CnfgConsultaSunat", { schema: "dbo" })
export class CnfgConsultaSunat {
  @Column("varchar", { name: "Descripcion", length: 50 })
  descripcion: string;

  @Column("numeric", { name: "Orden", nullable: true, precision: 1, scale: 0 })
  orden: number | null;

  @Column("numeric", {
    name: "Cantidad",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  cantidad: number | null;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  @Column("bit", { name: "Local", nullable: true })
  local: boolean | null;

  @PrimaryGeneratedColumn({ type: "int", name: "ValorID" })
  valorId: number;
}
