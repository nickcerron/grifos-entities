import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_AfiliacionPtos",
  [
    "cdlocal",
    "tarjAfiliacion",
    "tipo",
    "nropos",
    "cdtipodoc",
    "nrodocumento",
    "fecha",
    "cdproducto",
  ],
  { unique: true }
)
@Entity("AfiliacionPtos", { schema: "dbo" })
export class AfiliacionPtos {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "TarjAfiliacion", length: 20 })
  tarjAfiliacion: string;

  @Column("char", { primary: true, name: "tipo", length: 1 })
  tipo: string;

  @Column("char", { primary: true, name: "nropos", length: 10 })
  nropos: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("datetime", { primary: true, name: "fecha" })
  fecha: Date;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", { name: "total", nullable: true, precision: 10, scale: 2 })
  total: number | null;

  @Column("char", { primary: true, name: "cdproducto", length: 20 })
  cdproducto: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "Puntos",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  puntos: number | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("bit", { name: "enviado", nullable: true })
  enviado: boolean | null;

  @Column("numeric", {
    name: "canjeados",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  canjeados: number | null;

  @Column("numeric", {
    name: "valoracumula",
    nullable: true,
    precision: 18,
    scale: 2,
  })
  valoracumula: number | null;

  @Column("char", {
    name: "TArjAfiliacion_Traspaso",
    nullable: true,
    length: 25,
  })
  tArjAfiliacionTraspaso: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("tinyint", { name: "item", default: () => "(1)" })
  item: number;
}
