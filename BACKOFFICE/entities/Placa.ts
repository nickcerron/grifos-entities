import { Column, Entity, Index } from "typeorm";

@Index("PK_placa", ["nroplaca"], { unique: true })
@Entity("placa", { schema: "dbo" })
export class Placa {
  @Column("varchar", { primary: true, name: "nroplaca", length: 250 })
  nroplaca: string;

  @Column("char", { name: "marca", nullable: true, length: 50 })
  marca: string | null;

  @Column("char", { name: "modelo", nullable: true, length: 50 })
  modelo: string | null;

  @Column("char", { name: "color", nullable: true, length: 50 })
  color: string | null;

  @Column("numeric", { name: "ano", nullable: true, precision: 4, scale: 0 })
  ano: number | null;

  @Column("char", { name: "nroserie", nullable: true, length: 40 })
  nroserie: string | null;

  @Column("char", { name: "nromotor", nullable: true, length: 40 })
  nromotor: string | null;

  @Column("numeric", {
    name: "kilometraje",
    nullable: true,
    precision: 8,
    scale: 0,
  })
  kilometraje: number | null;
}
