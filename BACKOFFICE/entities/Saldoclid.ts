import { Column, Entity, Index } from "typeorm";

@Index("PK_saldoclid", ["cdcliente", "nrotarjeta"], { unique: true })
@Entity("saldoclid", { schema: "dbo" })
export class Saldoclid {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { primary: true, name: "nrotarjeta", length: 20 })
  nrotarjeta: string;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "limitemto",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  limitemto: number | null;

  @Column("numeric", {
    name: "consumto",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumto: number | null;

  @Column("char", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("char", {
    name: "TipoDespacho",
    nullable: true,
    length: 1,
    default: () => "'M'",
  })
  tipoDespacho: string | null;

  @Column("smalldatetime", {
    name: "FechaAtencion",
    nullable: true,
    default: () => "getdate()",
  })
  fechaAtencion: Date | null;

  @Column("bit", { name: "Enviado", nullable: true, default: () => "(0)" })
  enviado: boolean | null;

  @Column("char", { name: "clave", nullable: true, length: 15 })
  clave: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 20 })
  nrodocumento: string | null;

  @Column("varchar", { name: "nrocontrato", nullable: true, length: 60 })
  nrocontrato: string | null;

  @Column("numeric", {
    name: "Base_Inicial",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  baseInicial: number | null;

  @Column("numeric", {
    name: "Base_Temporal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  baseTemporal: number | null;

  @Column("numeric", {
    name: "Base_Extra",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  baseExtra: number | null;

  @Column("varchar", { name: "nrotarjeta_credito", nullable: true, length: 20 })
  nrotarjetaCredito: string | null;

  @Column("char", { name: "NroInterno", nullable: true, length: 15 })
  nroInterno: string | null;

  @Column("char", { name: "cdUnidad", nullable: true, length: 5 })
  cdUnidad: string | null;

  @Column("numeric", {
    name: "DotDiaria",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  dotDiaria: number | null;

  @Column("numeric", {
    name: "Adicional",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  adicional: number | null;

  @Column("int", { name: "Periodo", nullable: true })
  periodo: number | null;

  @Column("bit", { name: "Administrativo", nullable: true })
  administrativo: boolean | null;
}
