import { Column, Entity, Index } from "typeorm";

@Index("PK__motncred__113636DB2386756E", ["cdmotncredito"], { unique: true })
@Entity("motncredito", { schema: "dbo" })
export class Motncredito {
  @Column("varchar", { primary: true, name: "cdmotncredito", length: 5 })
  cdmotncredito: string;

  @Column("varchar", { name: "dsmotncredito", nullable: true, length: 40 })
  dsmotncredito: string | null;
}
