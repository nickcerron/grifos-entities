import { Column, Entity } from "typeorm";

@Entity("Respuesta_fact_electronica", { schema: "dbo" })
export class RespuestaFactElectronica {
  @Column("varchar", { name: "serie", nullable: true, length: 20 })
  serie: string | null;

  @Column("varchar", { name: "numero", nullable: true, length: 50 })
  numero: string | null;

  @Column("varchar", { name: "codrespuesta", nullable: true, length: 10 })
  codrespuesta: string | null;

  @Column("varchar", { name: "respuesta", nullable: true, length: 500 })
  respuesta: string | null;

  @Column("varchar", { name: "fecha", nullable: true, length: 20 })
  fecha: string | null;

  @Column("smalldatetime", { name: "FecProceso", nullable: true })
  fecProceso: Date | null;

  @Column("numeric", {
    name: "NumProceso",
    nullable: true,
    precision: 18,
    scale: 0,
  })
  numProceso: number | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;
}
