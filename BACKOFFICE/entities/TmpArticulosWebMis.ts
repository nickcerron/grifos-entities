import { Column, Entity } from "typeorm";

@Entity("TmpArticulosWebMIS", { schema: "dbo" })
export class TmpArticulosWebMis {
  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("varchar", { name: "dsarticulo", nullable: true, length: 60 })
  dsarticulo: string | null;

  @Column("char", { name: "cdunimed", nullable: true, length: 5 })
  cdunimed: string | null;

  @Column("int", { name: "Kit" })
  kit: number;

  @Column("bit", { name: "StockAble", nullable: true })
  stockAble: boolean | null;

  @Column("smalldatetime", { name: "FCreacion", nullable: true })
  fCreacion: Date | null;

  @Column("numeric", { name: "FactorConv", precision: 10, scale: 5 })
  factorConv: number;

  @Column("varchar", { name: "Impuesto", length: 5 })
  impuesto: string;

  @Column("char", { name: "cdGrupo01", nullable: true, length: 5 })
  cdGrupo01: string | null;

  @Column("int", { name: "IsPromocion" })
  isPromocion: number;

  @Column("numeric", {
    name: "ValorPtoCanje",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  valorPtoCanje: number | null;

  @Column("numeric", { name: "PrecioBase", precision: 12, scale: 4 })
  precioBase: number;

  @Column("int", { name: "Status" })
  status: number;
}
