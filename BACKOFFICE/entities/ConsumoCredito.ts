import { Column, Entity, Index } from "typeorm";

@Index("PK_Consumo_credito", ["cdcliente", "nrotarjeta", "fecha"], {
  unique: true,
})
@Entity("Consumo_credito", { schema: "dbo" })
export class ConsumoCredito {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { primary: true, name: "nrotarjeta", length: 20 })
  nrotarjeta: string;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "consumto",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumto: number | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("datetime", { name: "Fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("datetime", {
    primary: true,
    name: "Fecha",
    default: () => "getdate()",
  })
  fecha: Date;

  @Column("bit", { name: "Anulado", nullable: true })
  anulado: boolean | null;

  @Column("numeric", {
    name: "adicional",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  adicional: number | null;
}
