import { Column, Entity } from "typeorm";

@Entity("loteria", { schema: "dbo" })
export class Loteria {
  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;

  @Column("smalldatetime", { name: "fecinicio", nullable: true })
  fecinicio: Date | null;

  @Column("smalldatetime", { name: "fecfin", nullable: true })
  fecfin: Date | null;

  @Column("bit", { name: "flgefectivo", nullable: true })
  flgefectivo: boolean | null;

  @Column("bit", { name: "flgtarjeta", nullable: true })
  flgtarjeta: boolean | null;

  @Column("bit", { name: "flgcheque", nullable: true })
  flgcheque: boolean | null;

  @Column("bit", { name: "flgcredito", nullable: true })
  flgcredito: boolean | null;

  @Column("bit", { name: "flgpromocion", nullable: true })
  flgpromocion: boolean | null;

  @Column("bit", { name: "nro_centralizacion", nullable: true })
  nroCentralizacion: boolean | null;
}
