import { Column, Entity, Index } from "typeorm";

@Index("PK_comprar", ["cdlocal", "fecdocumento"], { unique: true })
@Entity("comprar", { schema: "dbo" })
export class Comprar {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecdocumento" })
  fecdocumento: Date;
}
