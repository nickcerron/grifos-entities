import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index(
  "PK_preciocliente",
  ["cdcliente", "tipocli", "cdarticulo", "nrocontrato"],
  { unique: true }
)
@Entity("preciocliente", { schema: "dbo" })
export class Preciocliente {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { primary: true, name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "tipodes", nullable: true, length: 3 })
  tipodes: string | null;

  @Column("varchar", {
    primary: true,
    name: "nrocontrato",
    length: 60,
    default: () => "' '",
  })
  nrocontrato: string;

  @Column("bit", { name: "licitacion", nullable: true })
  licitacion: boolean | null;

  @PrimaryGeneratedColumn({ type: "int", name: "cdPrecioCliente" })
  cdPrecioCliente: number;
}
