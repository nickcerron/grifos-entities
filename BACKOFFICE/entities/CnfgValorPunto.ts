import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { CanjeConcepto } from "./CanjeConcepto";

@Index("PK_CnfgValorPunto", ["valorId"], { unique: true })
@Entity("CnfgValorPunto", { schema: "dbo" })
export class CnfgValorPunto {
  @PrimaryGeneratedColumn({ type: "int", name: "ValorID" })
  valorId: number;

  @Column("int", { name: "PrefijoCard" })
  prefijoCard: number;

  @Column("int", { name: "Concepto" })
  concepto: number;

  @Column("int", { name: "TipoPunto", nullable: true })
  tipoPunto: number | null;

  @Column("float", { name: "ValorPunto", precision: 53 })
  valorPunto: number;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 50 })
  descripcion: string | null;

  @Column("bit", { name: "Status", nullable: true })
  status: boolean | null;

  @Column("char", { name: "tipoacumula", nullable: true, length: 2 })
  tipoacumula: string | null;

  @Column("char", { name: "tipocanje", nullable: true, length: 2 })
  tipocanje: string | null;

  @OneToMany(() => CanjeConcepto, (canjeConcepto) => canjeConcepto.valor)
  canjeConceptos: CanjeConcepto[];
}
