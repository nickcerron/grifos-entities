import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepDiaContomManualWebS", { schema: "dbo" })
export class TmpRepDiaContomManualWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("int", { name: "Turno" })
  turno: number;

  @Column("char", { name: "Lado", length: 2 })
  lado: string;

  @Column("char", { name: "Manguera", length: 2 })
  manguera: string;

  @Column("bit", { name: "TipoRegistro", nullable: true })
  tipoRegistro: boolean | null;

  @Column("varchar", { name: "ArticuloId", nullable: true, length: 20 })
  articuloId: string | null;

  @Column("decimal", {
    name: "Inicial",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  inicial: number | null;

  @Column("decimal", { name: "Final", nullable: true, precision: 27, scale: 5 })
  final: number | null;

  @Column("decimal", {
    name: "GalonesContom",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  galonesContom: number | null;

  @Column("int", { name: "PrecioId" })
  precioId: number;

  @Column("smalldatetime", { name: "FecRegistro_Precio", nullable: true })
  fecRegistroPrecio: Date | null;

  @Column("decimal", {
    name: "Precio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  precio: number | null;

  @Column("decimal", { name: "Total", nullable: true, precision: 27, scale: 5 })
  total: number | null;

  @Column("decimal", {
    name: "GalonesPEC",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  galonesPec: number | null;

  @Column("decimal", {
    name: "TotalPEC",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  totalPec: number | null;

  @Column("varchar", { name: "Observaciones", nullable: true, length: 100 })
  observaciones: string | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
