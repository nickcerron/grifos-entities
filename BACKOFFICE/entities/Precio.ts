import { Column, Entity, Index } from "typeorm";

@Index("PK_precio", ["cdarticulo", "cdprecio"], { unique: true })
@Entity("precio", { schema: "dbo" })
export class Precio {
  //* SU ID es compuesto entre cdarticulo y cdprecio

  //referencia al producto
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  //referencia a Size
  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  //Referencia a Lista de Precio, es decir tipo de Precio
  // por ejemplo: Precio de Venta directa, PRECIO CREDITO
  @Column("char", { primary: true, name: "cdprecio", length: 5 })
  cdprecio: string;

  //referencia a moneda
  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  //referencia al monto
  @Column("numeric", {
    name: "mtoprecio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecio: number | null;

  ///*pendiente definir funcionalidad
  @Column("numeric", {
    name: "porcomision",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porcomision: number | null;

  //referencia a la moneda de la oferta
  @Column("char", { name: "cdmonoferta", nullable: true, length: 1 })
  cdmonoferta: string | null;

  //precio del monto de la oferta
  @Column("numeric", {
    name: "mtoprecioferta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecioferta: number | null;

  //fecha inicio de la oferta
  @Column("smalldatetime", { name: "fecinioferta", nullable: true })
  fecinioferta: Date | null;

  //fecha fin de la oferta
  @Column("smalldatetime", { name: "fecfinoferta", nullable: true })
  fecfinoferta: Date | null;

  //hora inicio de la oferta
  @Column("char", { name: "horinioferta", nullable: true, length: 8 })
  horinioferta: string | null;

  //hora fin de la oferta
  @Column("char", { name: "horfinoferta", nullable: true, length: 8 })
  horfinoferta: string | null;

  //fecha de edicion
  @Column("smalldatetime", { name: "fecedicion", nullable: true })
  fecedicion: Date | null;

  //! todo esta en null, para eliminar? al parecer puede tener relacion con la tabla Promo, PromoTiend y PromoTiendaBaja
  @Column("varchar", { name: "PromocionId", nullable: true, length: 20 })
  promocionId: string | null;

  //rango de pesos, limite inferior, solo valido para kg?
  //12 4
  @Column("numeric", {
    name: "balanzapesoini",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  balanzapesoini: number | null;

  //rango de pesos, limite superior, solo valido para KG?
  //12 4
  @Column("numeric", {
    name: "balanzapesofin",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  balanzapesofin: number | null;
}
