import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaContommanual202210",
  ["cdlocal", "fecproceso", "turno", "lado", "manguera"],
  { unique: true }
)
@Entity("DiaContommanual202210", { schema: "dbo" })
export class DiaContommanual202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "lado", length: 2 })
  lado: string;

  @Column("char", { primary: true, name: "manguera", length: 2 })
  manguera: string;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "Inicial",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  inicial: number | null;

  @Column("numeric", { name: "final", nullable: true, precision: 14, scale: 4 })
  final: number | null;

  @Column("numeric", {
    name: "galonesc",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  galonesc: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 12, scale: 2 })
  total: number | null;

  @Column("numeric", {
    name: "galonespec",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  galonespec: number | null;

  @Column("numeric", {
    name: "totalpec",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  totalpec: number | null;

  @Column("char", { name: "Observaciones", nullable: true, length: 100 })
  observaciones: string | null;

  @Column("char", { name: "dsusuario", nullable: true, length: 20 })
  dsusuario: string | null;
}
