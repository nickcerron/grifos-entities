import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_transferenciad",
  ["cdlocal", "movimiento", "nromov", "nroitem", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("transferenciad", { schema: "dbo" })
export class Transferenciad {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "movimiento", length: 1 })
  movimiento: string;

  @Column("char", { primary: true, name: "nromov", length: 10 })
  nromov: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;
}
