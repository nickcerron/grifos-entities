import { Column, Entity, Index } from "typeorm";

@Index("PK_Destinatario", ["cdDestinatario"], { unique: true })
@Entity("Destinatario", { schema: "dbo" })
export class Destinatario {
  @Column("char", { primary: true, name: "cdDestinatario", length: 20 })
  cdDestinatario: string;

  @Column("char", { name: "rsDestinatario", nullable: true, length: 120 })
  rsDestinatario: string | null;

  @Column("char", { name: "drDestinatario", nullable: true, length: 120 })
  drDestinatario: string | null;
}
