import { Column, Entity } from "typeorm";

@Entity("TmpRepFaltSobrWebS", { schema: "dbo" })
export class TmpRepFaltSobrWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("numeric", { name: "Turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { name: "VendedorId", length: 10 })
  vendedorId: string;

  @Column("int", { name: "TipoRegistro" })
  tipoRegistro: number;

  @Column("float", { name: "Total_Depositado", precision: 53 })
  totalDepositado: number;

  @Column("float", { name: "Total_Venta", precision: 53 })
  totalVenta: number;

  @Column("int", { name: "IsIngresado" })
  isIngresado: number;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
