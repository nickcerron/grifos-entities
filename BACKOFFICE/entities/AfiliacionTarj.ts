import { Column, Entity, Index } from "typeorm";

@Index("PK_AfiliacionTarj", ["cdlocal", "tarjAfiliacion"], { unique: true })
@Entity("AfiliacionTarj", { schema: "dbo" })
export class AfiliacionTarj {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "TarjAfiliacion", length: 20 })
  tarjAfiliacion: string;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("char", { name: "drcliente", nullable: true, length: 120 })
  drcliente: string | null;

  @Column("char", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null;

  @Column("char", { name: "tlfcliente1", nullable: true, length: 15 })
  tlfcliente1: string | null;

  @Column("char", { name: "emcliente", nullable: true, length: 60 })
  emcliente: string | null;

  @Column("numeric", {
    name: "Puntos",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  puntos: number | null;

  @Column("numeric", {
    name: "Canjeado",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  canjeado: number | null;

  @Column("numeric", {
    name: "Disponible",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  disponible: number | null;

  @Column("bit", { name: "Bloqueado", nullable: true })
  bloqueado: boolean | null;

  @Column("char", { name: "provtelefonia", nullable: true, length: 2 })
  provtelefonia: string | null;

  @Column("char", { name: "provtelefonia1", nullable: true, length: 2 })
  provtelefonia1: string | null;

  @Column("smalldatetime", { name: "fechaultconsumo" })
  fechaultconsumo: Date;

  @Column("char", {
    name: "TArjAfiliacion_Traspaso",
    nullable: true,
    length: 25,
  })
  tArjAfiliacionTraspaso: string | null;

  @Column("int", { name: "estado", nullable: true })
  estado: number | null;

  @Column("numeric", {
    name: "valoracumula",
    nullable: true,
    precision: 18,
    scale: 2,
  })
  valoracumula: number | null;

  @Column("bit", { name: "CSQL_ACTUALIZA", nullable: true })
  csqlActualiza: boolean | null;
}
