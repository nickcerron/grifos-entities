import { Column, Entity } from "typeorm";

@Entity("TmpRepVentaWebS", { schema: "dbo" })
export class TmpRepVentaWebS {
  @Column("int", { name: "EMPRESA" })
  empresa: number;

  @Column("char", { name: "RUCEMISOR", length: 15 })
  rucemisor: string;

  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("varchar", { name: "SERIEMAQ", length: 20 })
  seriemaq: string;

  @Column("varchar", { name: "TIPOD", length: 5 })
  tipod: string;

  @Column("char", { name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("datetime", { name: "FECDOCUMENTO", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "FECPROCESO", nullable: true })
  fecproceso: Date | null;

  @Column("datetime", { name: "FECSISTEMA", nullable: true })
  fecsistema: Date | null;

  @Column("varchar", { name: "NROPOS", nullable: true, length: 10 })
  nropos: string | null;

  @Column("int", { name: "ALMACENID", nullable: true })
  almacenid: number | null;

  @Column("varchar", { name: "CLIENTEID", nullable: true, length: 15 })
  clienteid: string | null;

  @Column("varchar", { name: "RUCCLIENTE", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "RSCLIENTE", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("varchar", { name: "MONEDA", nullable: true, length: 3 })
  moneda: string | null;

  @Column("numeric", {
    name: "MTODESCUENTO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtodescuento: number | null;

  @Column("numeric", {
    name: "MTOSUBTOTAL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "MTOIMPUESTO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "MTOTOTAL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtototal: number | null;

  @Column("varchar", { name: "TRANSPORTISTAID", nullable: true, length: 50 })
  transportistaid: string | null;

  @Column("varchar", { name: "NROPLACA", nullable: true, length: 250 })
  nroplaca: string | null;

  @Column("varchar", { name: "VENDEDORID", nullable: true, length: 15 })
  vendedorid: string | null;

  @Column("bit", { name: "ANULADO", nullable: true })
  anulado: boolean | null;

  @Column("numeric", {
    name: "TCAMBIO",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("int", { name: "TURNO", nullable: true })
  turno: number | null;

  @Column("varchar", { name: "ODOMETRO", nullable: true, length: 10 })
  odometro: string | null;

  @Column("char", { name: "TIPOFACTURA", nullable: true, length: 1 })
  tipofactura: string | null;

  @Column("char", { name: "TIPOVENTA", nullable: true, length: 1 })
  tipoventa: string | null;

  @Column("smalldatetime", { name: "FECANULA", nullable: true })
  fecanula: Date | null;

  @Column("varchar", { name: "C_CENTRALIZACION", nullable: true, length: 50 })
  cCentralizacion: string | null;

  @Column("varchar", { name: "MOTNOTAID", nullable: true, length: 5 })
  motnotaid: string | null;

  @Column("varchar", { name: "MOTNOTADES", nullable: true, length: 100 })
  motnotades: string | null;

  @Column("varchar", { name: "NROTARJETA", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("numeric", { name: "MTOIMP_OTROS", precision: 12, scale: 4 })
  mtoimpOtros: number;

  @Column("numeric", {
    name: "MTONOAFECTO",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("datetime", { name: "FECVENCIMIENTO", nullable: true })
  fecvencimiento: Date | null;

  @Column("varchar", { name: "CDTPFACTURA", nullable: true, length: 5 })
  cdtpfactura: string | null;

  @Column("varchar", { name: "ESTADO", nullable: true, length: 20 })
  estado: string | null;

  @Column("numeric", { name: "MTORECAUDO", precision: 27, scale: 4 })
  mtorecaudo: number;

  @Column("numeric", { name: "REDONDEA_INDECOPI", precision: 27, scale: 4 })
  redondeaIndecopi: number;
}
