import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_OP_TRAN",
  ["controlador", "numero", "cara", "producto", "fecha", "hora"],
  { unique: true }
)
@Entity("OP_TRAN", { schema: "dbo" })
export class OpTran {
  @Column("bigint", { name: "C_INTERNO" })
  cInterno: string;

  @Column("char", { primary: true, name: "CONTROLADOR", length: 2 })
  controlador: string;

  @Column("char", { primary: true, name: "NUMERO", length: 20 })
  numero: string;

  @Column("numeric", { name: "SOLES", precision: 11, scale: 2 })
  soles: number;

  @Column("char", { primary: true, name: "PRODUCTO", length: 2 })
  producto: string;

  @Column("numeric", { name: "PRECIO", precision: 6, scale: 2 })
  precio: number;

  @Column("numeric", { name: "GALONES", precision: 7, scale: 3 })
  galones: number;

  @Column("char", { primary: true, name: "CARA", length: 2 })
  cara: string;

  @Column("smalldatetime", { primary: true, name: "FECHA" })
  fecha: Date;

  @Column("smalldatetime", { primary: true, name: "HORA" })
  hora: Date;

  @Column("char", { name: "TURNO", length: 10 })
  turno: string;

  @Column("char", { name: "ESTADO", nullable: true, length: 10 })
  estado: string | null;

  @Column("char", { name: "DOCUMENTO", nullable: true, length: 10 })
  documento: string | null;

  @Column("smalldatetime", { name: "DATEPROCE", nullable: true })
  dateproce: Date | null;

  @Column("char", { name: "CDTIPODOC", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("int", { name: "MANGUERA", nullable: true })
  manguera: number | null;

  @Column("datetime", { name: "FECSISTEMA", default: () => "getdate()" })
  fecsistema: Date;

  @Column("numeric", {
    name: "VolumenFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  volumenFinal: number | null;

  @Column("numeric", {
    name: "MontoFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  montoFinal: number | null;

  @Column("int", { name: "IdTran", nullable: true })
  idTran: number | null;
}
