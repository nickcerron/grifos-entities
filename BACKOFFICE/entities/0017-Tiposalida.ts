import { Column, Entity, Index } from 'typeorm';

@Index('PK_tiposalida', ['cdtpsalida'], { unique: true })
@Entity('tiposalida', { schema: 'dbo' })
export class Tiposalida {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtpsalida', length: 5 })
  cdtpsalida: string;

  //name
  //nullable:false
  @Column('varchar', { name: 'dstpsalida', nullable: true, length: 40 })
  dstpsalida: string | null;

  //nro_correlative
  @Column('char', { name: 'nrocorrelativo', nullable: true, length: 10 })
  nrocorrelativo: string | null;

  //valued
  @Column('bit', { name: 'flgvalorizado', nullable: true })
  flgvalorizado: boolean | null;

  //show
  //default 1
  @Column('bit', { name: 'flgmostrar', nullable: true })
  flgmostrar: boolean | null;

  //system
  //?muere
  //? TODOS LOS DATOS SON => bit(0)
  @Column('bit', { name: 'flgsistema', nullable: true })
  flgsistema: boolean | null;

  //?muere
  //? los datos son => bit(0)
  //enableSalePrices
  @Column('bit', { name: 'flgprecvta', nullable: true })
  flgprecvta: boolean | null;

  //type => Listaprecio (relation)
  @Column('char', { name: 'cdprecio', nullable: true, length: 5 })
  cdprecio: string | null;

  //cal_cost
  //?NO SE PARA QUE SIRVE
  @Column('bit', { name: 'flgcalcosto', nullable: true })
  flgcalcosto: boolean | null;

  //nro_resume
  //?muere => TODOS LOS DATOS SON '000'
  @Column('char', { name: 'Bzl_nroresumen', nullable: true, length: 3 })
  bzlNroresumen: string | null;
}
