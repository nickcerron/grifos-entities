import { Column, Entity, Index } from "typeorm";

@Index("PK_Cambio_Precio", ["cdArticulo", "fecha"], { unique: true })
@Entity("Cambio_Precio", { schema: "dbo" })
export class CambioPrecio {
  @Column("char", { primary: true, name: "CdArticulo", length: 20 })
  cdArticulo: string;

  @Column("numeric", {
    name: "MtoPrecio_actual",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  mtoPrecioActual: number | null;

  @Column("numeric", {
    name: "MtoPrecio_Nuevo",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  mtoPrecioNuevo: number | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("datetime", {
    primary: true,
    name: "Fecha",
    default: () => "getdate()",
  })
  fecha: Date;
}
