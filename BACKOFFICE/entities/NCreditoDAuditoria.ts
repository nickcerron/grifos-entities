import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_NCreditoD_Auditoria",
  ["fecelimina", "cdlocal", "nroncredito", "nroitem", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("NCreditoD_Auditoria", { schema: "dbo" })
export class NCreditoDAuditoria {
  @Column("datetime", { primary: true, name: "fecelimina" })
  fecelimina: Date;

  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroncredito", length: 10 })
  nroncredito: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "dsarticulo", length: 100 })
  dsarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cddocorigen", nullable: true, length: 5 })
  cddocorigen: string | null;

  @Column("char", { name: "nrodocorigen", nullable: true, length: 10 })
  nrodocorigen: string | null;

  @Column("numeric", {
    name: "nroitemorigen",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nroitemorigen: number | null;

  @Column("smalldatetime", { name: "fecorigen", nullable: true })
  fecorigen: Date | null;

  @Column("numeric", {
    name: "tcambioorigen",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambioorigen: number | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("bit", { name: "moverstock", nullable: true })
  moverstock: boolean | null;
}
