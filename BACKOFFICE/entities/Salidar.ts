import { Column, Entity, Index } from "typeorm";

@Index("PK_salidar", ["cdlocal", "fecsalida", "fecproceso"], { unique: true })
@Entity("salidar", { schema: "dbo" })
export class Salidar {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecsalida" })
  fecsalida: Date;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;
}
