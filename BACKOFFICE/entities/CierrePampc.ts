import { Column, Entity, Index } from "typeorm";

@Index("PK_CIERRE_PAMPC", ["codigo"], { unique: true })
@Entity("CIERRE_PAMPC", { schema: "dbo" })
export class CierrePampc {
  @Column("int", { primary: true, name: "codigo" })
  codigo: number;

  @Column("varchar", { name: "tipo", nullable: true, length: 1 })
  tipo: string | null;

  @Column("varchar", { name: "fecha", nullable: true, length: 50 })
  fecha: string | null;

  @Column("varchar", { name: "hora", nullable: true, length: 50 })
  hora: string | null;

  @Column("nchar", { name: "estado", nullable: true, length: 10 })
  estado: string | null;
}
