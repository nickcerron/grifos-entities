import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_C__3214EC0771DF8F0C", ["id"], { unique: true })
@Entity("Kardex_Costo_Promedio_old_ND", { schema: "dbo" })
export class KardexCostoPromedioOldNd {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("varchar", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "tipodoc", nullable: true, length: 5 })
  tipodoc: string | null;

  @Column("varchar", { name: "serie", nullable: true, length: 5 })
  serie: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("varchar", { name: "mov", nullable: true, length: 10 })
  mov: string | null;

  @Column("char", { name: "tipo", nullable: true, length: 2 })
  tipo: string | null;

  @Column("numeric", {
    name: "cantidadi",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadi: number | null;

  @Column("numeric", {
    name: "costoi",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costoi: number | null;

  @Column("numeric", {
    name: "totali",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totali: number | null;

  @Column("numeric", {
    name: "cantidadv",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadv: number | null;

  @Column("numeric", {
    name: "costov",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costov: number | null;

  @Column("numeric", {
    name: "totalv",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totalv: number | null;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;

  @Column("numeric", {
    name: "costof",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costof: number | null;

  @Column("numeric", {
    name: "totalf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totalf: number | null;

  @Column("varchar", { name: "tipo_ope", nullable: true, length: 50 })
  tipoOpe: string | null;

  @Column("varchar", { name: "nrodocumentofac", nullable: true, length: 15 })
  nrodocumentofac: string | null;
}
