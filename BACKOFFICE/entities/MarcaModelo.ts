import { Column, Entity, Index } from "typeorm";

@Index("PK_marca_modelo", ["tipo", "descripcion"], { unique: true })
@Entity("marca_modelo", { schema: "dbo" })
export class MarcaModelo {
  @Column("char", { primary: true, name: "tipo", length: 10 })
  tipo: string;

  @Column("char", { primary: true, name: "descripcion", length: 50 })
  descripcion: string;
}
