import { Column, Entity, Index } from "typeorm";

@Index("PK_articulo_cuenta", ["cdarticulo", "cuenta"], { unique: true })
@Entity("articulo_cuenta", { schema: "dbo" })
export class ArticuloCuenta {
  @Column("char", { primary: true, name: "CDARTICULO", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "CUENTA", length: 11 })
  cuenta: string;

  @Column("char", { name: "cdgrupo", nullable: true, length: 5 })
  cdgrupo: string | null;
}
