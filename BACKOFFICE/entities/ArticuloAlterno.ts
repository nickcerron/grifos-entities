import { Column, Entity, Index } from "typeorm";

@Index("PK_articulo_alterno", ["cdarticulo", "cdalterno"], { unique: true })
@Entity("articulo_alterno", { schema: "dbo" })
export class ArticuloAlterno {
  @Column("varchar", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { primary: true, name: "cdalterno", length: 20 })
  cdalterno: string;
}
