import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_separaciond",
  ["nroseparacion", "cdalmacen", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("separaciond", { schema: "dbo" })
export class Separaciond {
  @Column("char", { primary: true, name: "nroseparacion", length: 10 })
  nroseparacion: string;

  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "mtoprecio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecio: number | null;
}
