import { Column, Entity } from "typeorm";

@Entity("ndebitodg", { schema: "dbo" })
export class Ndebitodg {
  @Column("char", { name: "nrondebito", length: 10 })
  nrondebito: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { name: "nroseriemaq", length: 15 })
  nroseriemaq: string;
}
