import { Column, Entity, Index } from "typeorm";

@Index("PK_articulomemo", ["cdarticulo"], { unique: true })
@Entity("articulomemo", { schema: "dbo" })
export class Articulomemo {
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("text", { name: "memarticulo", nullable: true })
  memarticulo: string | null;

  @Column("image", { name: "ftarticulo", nullable: true })
  ftarticulo: Buffer | null;
}
