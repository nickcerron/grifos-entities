import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_SalidaD202210",
  ["cdlocal", "cdtpsalida", "nrosalida", "nroitem", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("SalidaD202210", { schema: "dbo" })
export class SalidaD202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("char", { primary: true, name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("char", { name: "cdalternativo", nullable: true, length: 20 })
  cdalternativo: string | null;

  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 16, scale: 4 })
  costo: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", { name: "peso", nullable: true, precision: 18, scale: 5 })
  peso: number | null;

  @Column("numeric", {
    name: "totalpeso",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  totalpeso: number | null;
}
