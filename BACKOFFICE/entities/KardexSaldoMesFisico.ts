import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("IX_Kardex_Saldo_Fisico", ["cdarticulo", "periodo"], {})
@Index("IX_Kardex_Saldo_mes_Fisico1", ["cdarticulo", "cantidadf"], {})
@Index("PK_Kardex_Saldo_mes_Fisico", ["id"], { unique: true })
@Entity("Kardex_Saldo_mes_Fisico", { schema: "dbo" })
export class KardexSaldoMesFisico {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "periodo", length: 6 })
  periodo: string;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;
}
