import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_FaltSobr202210",
  ["cdlocal", "fecproceso", "turno", "cdvendedor", "nropos"],
  { unique: true }
)
@Entity("FaltSobr202210", { schema: "dbo" })
export class FaltSobr202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("numeric", { name: "total_ing", precision: 12, scale: 2 })
  totalIng: number;

  @Column("numeric", { name: "totalvta", precision: 12, scale: 2 })
  totalvta: number;

  @Column("char", { primary: true, name: "nropos", length: 10 })
  nropos: string;

  @Column("numeric", {
    name: "redondea_indecopi",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  redondeaIndecopi: number | null;
}
