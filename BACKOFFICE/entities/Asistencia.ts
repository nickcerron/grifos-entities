import { Column, Entity } from "typeorm";

@Entity("Asistencia", { schema: "dbo" })
export class Asistencia {
  @Column("nchar", { name: "proceso", nullable: true, length: 1 })
  proceso: string | null;

  @Column("nchar", { name: "codempleado", nullable: true, length: 4 })
  codempleado: string | null;

  @Column("nchar", { name: "codturno", nullable: true, length: 11 })
  codturno: string | null;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;
}
