import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepVentaDWebS", { schema: "dbo" })
export class TmpRepVentaDWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @Column("int", { name: "EMPRESA" })
  empresa: number;

  @Column("char", { name: "RUCEMISOR", length: 15 })
  rucemisor: string;

  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("varchar", { name: "SERIEMAQ", length: 20 })
  seriemaq: string;

  @Column("varchar", { name: "TIPOD", length: 5 })
  tipod: string;

  @Column("char", { name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("varchar", { name: "ARTICULOID", length: 20 })
  articuloid: string;

  @Column("int", { name: "NROITEM" })
  nroitem: number;

  @Column("numeric", {
    name: "IMPUESTO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "PORDSCTO1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "PORDSCTO2",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "CANTIDAD",
    nullable: true,
    precision: 27,
    scale: 7,
  })
  cantidad: number | null;

  @Column("varchar", { name: "TIPOPRECIO", nullable: true, length: 15 })
  tipoprecio: string | null;

  @Column("numeric", {
    name: "PRECIO",
    nullable: true,
    precision: 27,
    scale: 7,
  })
  precio: number | null;

  @Column("numeric", {
    name: "MTODESCUENTO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtodescuento: number | null;

  @Column("numeric", {
    name: "MTOSUBTOTAL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "MTOIMPUESTO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "MTOTOTAL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtototal: number | null;

  @Column("char", { name: "CARA", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "MANGUERA", nullable: true, length: 1 })
  manguera: string | null;

  @Column("varchar", { name: "NROTRANSACCION", nullable: true, length: 10 })
  nrotransaccion: string | null;

  @Column("bit", { name: "FLGMOVIMIENTO", nullable: true })
  flgmovimiento: boolean | null;

  @Column("numeric", { name: "COSTO", nullable: true, precision: 27, scale: 7 })
  costo: number | null;

  @Column("numeric", {
    name: "PRECIO_ORIG",
    nullable: true,
    precision: 27,
    scale: 7,
  })
  precioOrig: number | null;

  @Column("varchar", { name: "CDPACK", nullable: true, length: 20 })
  cdpack: string | null;

  @Column("numeric", { name: "MTOIMP_OTROS", precision: 12, scale: 4 })
  mtoimpOtros: number;

  @Column("numeric", {
    name: "MTONOAFECTO",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("varchar", { name: "C_CENTRALIZACION", nullable: true, length: 50 })
  cCentralizacion: string | null;

  @Column("bit", { name: "FLG_INAFECTO", nullable: true })
  flgInafecto: boolean | null;

  @Column("bit", { name: "FLG_EXONERADO", nullable: true })
  flgExonerado: boolean | null;
}
