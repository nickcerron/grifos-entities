import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { OrdenPedido } from "./OrdenPedido";

@Index(
  "PK_Orden_Pedido_Detalle",
  ["empresaId", "localId", "nroPedido", "nroItem"],
  { unique: true }
)
@Entity("Orden_Pedido_Detalle", { schema: "dbo" })
export class OrdenPedidoDetalle {
  @Column("int", { primary: true, name: "EmpresaId" })
  empresaId: number;

  @Column("int", { primary: true, name: "LocalId" })
  localId: number;

  @Column("varchar", { primary: true, name: "NroPedido", length: 15 })
  nroPedido: string;

  @Column("int", { primary: true, name: "NroItem" })
  nroItem: number;

  @Column("varchar", { name: "ProveedorId", length: 15 })
  proveedorId: string;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("numeric", {
    name: "Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("float", { name: "Costo_Compra", nullable: true, precision: 53 })
  costoCompra: number | null;

  @Column("varchar", { name: "cdunimed_compra", nullable: true, length: 40 })
  cdunimedCompra: string | null;

  @Column("float", { name: "factor_conver", nullable: true, precision: 53 })
  factorConver: number | null;

  @Column("float", { name: "Sugerido", nullable: true, precision: 53 })
  sugerido: number | null;

  @Column("float", { name: "NroEmpaques", nullable: true, precision: 53 })
  nroEmpaques: number | null;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  @Column("float", { name: "factor_conver_alm", nullable: true, precision: 53 })
  factorConverAlm: number | null;

  @Column("float", { name: "IGV", nullable: true, precision: 53 })
  igv: number | null;

  @ManyToOne(
    () => OrdenPedido,
    (ordenPedido) => ordenPedido.ordenPedidoDetalles
  )
  @JoinColumn([
    { name: "EmpresaId", referencedColumnName: "empresaId" },
    { name: "LocalId", referencedColumnName: "localId" },
    { name: "NroPedido", referencedColumnName: "nroPedido" },
  ])
  ordenPedido: OrdenPedido;
}
