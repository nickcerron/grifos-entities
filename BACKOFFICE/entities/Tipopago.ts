import { Column, Entity, Index } from "typeorm";

@Index("PK_tipopago", ["cdtppago"], { unique: true })
@Entity("tipopago", { schema: "dbo" })
export class Tipopago {
  @Column("char", { primary: true, name: "cdtppago", length: 5 })
  cdtppago: string;

  @Column("char", { name: "dstppago", nullable: true, length: 60 })
  dstppago: string | null;

  @Column("bit", { name: "flgpago", nullable: true })
  flgpago: boolean | null;

  @Column("bit", { name: "flgsistema", nullable: true })
  flgsistema: boolean | null;
}
