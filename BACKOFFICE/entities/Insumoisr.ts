import { Column, Entity } from "typeorm";

@Entity("insumoisr", { schema: "dbo" })
export class Insumoisr {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "FECPROCESO", nullable: true })
  fecproceso: Date | null;
}
