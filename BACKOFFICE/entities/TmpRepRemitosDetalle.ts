import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepRemitosDetalle", { schema: "dbo" })
export class TmpRepRemitosDetalle {
  @PrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "CDREMITO", nullable: true, length: 5 })
  cdremito: string | null;

  @Column("varchar", { name: "NROREMITO", nullable: true, length: 12 })
  nroremito: string | null;

  @Column("datetime", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "DSTURNO", nullable: true, length: 5 })
  dsturno: string | null;

  @Column("int", { name: "NROPAQUETES", nullable: true })
  nropaquetes: number | null;

  @Column("numeric", {
    name: "MTOSOLES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "MTODOLARES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("numeric", { name: "TURNO", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("int", { name: "NROPAQSOLES", nullable: true })
  nropaqsoles: number | null;

  @Column("int", { name: "NROPAQDOLARES", nullable: true })
  nropaqdolares: number | null;
}
