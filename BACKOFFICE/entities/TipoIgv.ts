import { Column, Entity } from "typeorm";

@Entity("TIPO_IGV", { schema: "dbo" })
export class TipoIgv {
  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "IGV", nullable: true, length: 5 })
  igv: string | null;
}
