import { Column, Entity } from "typeorm";

@Entity("TmpVentaEliminados", { schema: "dbo" })
export class TmpVentaEliminados {
  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "SerieMaq", length: 20 })
  serieMaq: string;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;
}
