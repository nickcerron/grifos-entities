import { Column, Entity, Index } from "typeorm";

@Index("PK__PesajesB__93F5095C86299A18", ["nroticket"], { unique: true })
@Entity("PesajesBalanza", { schema: "dbo" })
export class PesajesBalanza {
  @Column("char", { primary: true, name: "nroticket", length: 10 })
  nroticket: string;

  @Column("char", { name: "tiposervicio", nullable: true, length: 5 })
  tiposervicio: string | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  precio: number | null;

  @Column("varchar", {
    name: "cdarticulo_primer_peso",
    nullable: true,
    length: 20,
  })
  cdarticuloPrimerPeso: string | null;

  @Column("int", { name: "turno", nullable: true })
  turno: number | null;

  @Column("smalldatetime", { name: "fechaproceso", nullable: true })
  fechaproceso: Date | null;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "placa", nullable: true, length: 20 })
  placa: string | null;

  @Column("varchar", { name: "descripcion", nullable: true, length: 120 })
  descripcion: string | null;

  @Column("varchar", { name: "obs_entrada", nullable: true, length: 120 })
  obsEntrada: string | null;

  @Column("varchar", { name: "obs_salida", nullable: true, length: 120 })
  obsSalida: string | null;

  @Column("int", { name: "primer_peso", nullable: true })
  primerPeso: number | null;

  @Column("int", { name: "segundo_peso", nullable: true })
  segundoPeso: number | null;

  @Column("int", { name: "turno_segundo_peso", nullable: true })
  turnoSegundoPeso: number | null;

  @Column("smalldatetime", { name: "fecha_segundo_peso", nullable: true })
  fechaSegundoPeso: Date | null;

  @Column("numeric", {
    name: "precio_segundo_peso",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  precioSegundoPeso: number | null;

  @Column("varchar", {
    name: "cdarticulo_segundo_peso",
    nullable: true,
    length: 20,
  })
  cdarticuloSegundoPeso: string | null;

  @Column("int", { name: "pesoneto", nullable: true })
  pesoneto: number | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "cdusumodifica", nullable: true, length: 10 })
  cdusumodifica: string | null;

  @Column("smalldatetime", { name: "fecmodifica", nullable: true })
  fecmodifica: Date | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "convenio", nullable: true, length: 3 })
  convenio: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;

  @Column("smalldatetime", {
    name: "fechaproceso_segundo_peso",
    nullable: true,
  })
  fechaprocesoSegundoPeso: Date | null;
}
