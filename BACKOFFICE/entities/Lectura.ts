import { Column, Entity } from "typeorm";

@Entity("LECTURA", { schema: "dbo" })
export class Lectura {
  @Column("varchar", { name: "CDCLIENTE", nullable: true, length: 11 })
  cdcliente: string | null;

  @Column("varchar", { name: "RSCLIENTE", nullable: true, length: 200 })
  rscliente: string | null;

  @Column("varchar", {
    name: "Tipo_de_Contribuyente",
    nullable: true,
    length: 50,
  })
  tipoDeContribuyente: string | null;

  @Column("nvarchar", { name: "Profesion", nullable: true })
  profesion: string | null;

  @Column("varchar", { name: "Nombre_Comercial", nullable: true, length: 50 })
  nombreComercial: string | null;

  @Column("varchar", { name: "Condicion", nullable: true, length: 50 })
  condicion: string | null;

  @Column("varchar", { name: "Estado", nullable: true, length: 50 })
  estado: string | null;

  @Column("varchar", { name: "Fecha_Inscripcion", nullable: true, length: 50 })
  fechaInscripcion: string | null;

  @Column("varchar", {
    name: "Fecha_Inicio_Actividades",
    nullable: true,
    length: 50,
  })
  fechaInicioActividades: string | null;

  @Column("varchar", { name: "Departamento", nullable: true, length: 50 })
  departamento: string | null;

  @Column("varchar", { name: "Provincia", nullable: true, length: 50 })
  provincia: string | null;

  @Column("varchar", { name: "Distrito", nullable: true, length: 50 })
  distrito: string | null;

  @Column("varchar", { name: "Direccion", nullable: true, length: 150 })
  direccion: string | null;

  @Column("nvarchar", { name: "OTROS", nullable: true })
  otros: string | null;
}
