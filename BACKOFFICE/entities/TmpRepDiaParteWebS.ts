import { Column, Entity } from "typeorm";

@Entity("TmpRepDiaParteWebS", { schema: "dbo" })
export class TmpRepDiaParteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("smalldatetime", { name: "FecAnterior", nullable: true })
  fecAnterior: Date | null;

  @Column("bit", { name: "Cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("bit", { name: "IslaVend", nullable: true })
  islaVend: boolean | null;

  @Column("bit", { name: "DepVend", nullable: true })
  depVend: boolean | null;

  @Column("bit", { name: "Contom", nullable: true })
  contom: boolean | null;

  @Column("bit", { name: "Cobranza", nullable: true })
  cobranza: boolean | null;

  @Column("bit", { name: "Varillaje", nullable: true })
  varillaje: boolean | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;

  @Column("varchar", { name: "NroPos", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("varchar", { name: "UserModifica", nullable: true, length: 10 })
  userModifica: string | null;

  @Column("datetime", { name: "FechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
