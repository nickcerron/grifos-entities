import { Column, Entity } from "typeorm";

@Entity("correlativo", { schema: "dbo" })
export class Correlativo {
  @Column("char", { name: "factura", nullable: true, length: 10 })
  factura: string | null;

  @Column("char", { name: "boleta", nullable: true, length: 10 })
  boleta: string | null;

  @Column("char", { name: "ticket", nullable: true, length: 10 })
  ticket: string | null;

  @Column("char", { name: "nventa", nullable: true, length: 10 })
  nventa: string | null;

  @Column("char", { name: "promocion", nullable: true, length: 10 })
  promocion: string | null;

  @Column("char", { name: "ncredito", nullable: true, length: 10 })
  ncredito: string | null;

  @Column("char", { name: "ndebito", nullable: true, length: 10 })
  ndebito: string | null;

  @Column("char", { name: "guia", nullable: true, length: 10 })
  guia: string | null;

  @Column("char", { name: "proforma", nullable: true, length: 10 })
  proforma: string | null;

  @Column("char", { name: "letra", nullable: true, length: 10 })
  letra: string | null;

  @Column("char", { name: "otrabajo", nullable: true, length: 10 })
  otrabajo: string | null;

  @Column("char", { name: "ncreditoboleta", nullable: true, length: 10 })
  ncreditoboleta: string | null;

  @Column("char", { name: "ndebitoboleta", nullable: true, length: 10 })
  ndebitoboleta: string | null;
}
