import { Column, Entity, Index } from "typeorm";

@Index("PK_tipoletra", ["cdletra"], { unique: true })
@Entity("tipoletra", { schema: "dbo" })
export class Tipoletra {
  @Column("char", { primary: true, name: "cdletra", length: 2 })
  cdletra: string;

  @Column("char", { name: "dsletra", nullable: true, length: 30 })
  dsletra: string | null;

  @Column("bit", { name: "flgbanco", nullable: true })
  flgbanco: boolean | null;
}
