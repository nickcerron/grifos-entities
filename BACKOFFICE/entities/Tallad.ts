import { Column, Entity, Index } from "typeorm";

@Index("PK_tallad", ["cdtalla", "talla"], { unique: true })
@Entity("tallad", { schema: "dbo" })
export class Tallad {
  @Column("char", { primary: true, name: "cdtalla", length: 5 })
  cdtalla: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", { name: "orden", nullable: true, precision: 3, scale: 0 })
  orden: number | null;
}
