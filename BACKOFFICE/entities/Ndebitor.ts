import { Column, Entity } from "typeorm";

@Entity("ndebitor", { schema: "dbo" })
export class Ndebitor {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { name: "fecndebito" })
  fecndebito: Date;

  @Column("smalldatetime", { name: "fecproceso" })
  fecproceso: Date;
}
