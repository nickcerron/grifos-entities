import { Column, Entity } from "typeorm";

@Entity("saldo_transacciones_siges_cobranza_anulado", { schema: "dbo" })
export class SaldoTransaccionesSigesCobranzaAnulado {
  @Column("varchar", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("varchar", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("varchar", { name: "nrotarjeta", nullable: true, length: 15 })
  nrotarjeta: string | null;

  @Column("numeric", {
    name: "montosoles",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  montosoles: number | null;

  @Column("varchar", { name: "seriemaq", nullable: true, length: 15 })
  seriemaq: string | null;

  @Column("varchar", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;
}
