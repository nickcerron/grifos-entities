import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoTraslado", ["cdtpTraslado"], { unique: true })
@Entity("TipoTraslado", { schema: "dbo" })
//* TIPOS DE TRASLADO
export class TipoTraslado {
  @Column("char", { primary: true, name: "cdtpTraslado", length: 3 })
  cdtpTraslado: string;

  @Column("varchar", { name: "dstpTraslado", length: 80 })
  dstpTraslado: string;

  @Column("bit", { name: "FlgMostrar", nullable: true })
  flgMostrar: boolean | null;
}
