import { Column, Entity, Index } from "typeorm";

@Index("PK_separacion", ["nroseparacion"], { unique: true })
@Entity("separacion", { schema: "dbo" })
export class Separacion {
  @Column("char", { primary: true, name: "nroseparacion", length: 10 })
  nroseparacion: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "fecseparacion", nullable: true })
  fecseparacion: Date | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 3 })
  cdcliente: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;
}
