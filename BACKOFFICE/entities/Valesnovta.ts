import { Column, Entity } from "typeorm";

@Entity("valesnovta", { schema: "dbo" })
export class Valesnovta {
  @Column("char", { name: "nrovale", nullable: true, length: 10 })
  nrovale: string | null;

  @Column("smalldatetime", { name: "fecvale", nullable: true })
  fecvale: Date | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtototal: number | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "nroseriemaq", nullable: true, length: 15 })
  nroseriemaq: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "nroseriemaqfac", nullable: true, length: 15 })
  nroseriemaqfac: string | null;

  @Column("char", { name: "cdtipodocfac", nullable: true, length: 5 })
  cdtipodocfac: string | null;

  @Column("char", { name: "nrodocumentofac", nullable: true, length: 10 })
  nrodocumentofac: string | null;

  @Column("datetime", { name: "fecdocumentofac", nullable: true })
  fecdocumentofac: Date | null;

  @Column("smalldatetime", { name: "fecprocesofac", nullable: true })
  fecprocesofac: Date | null;

  @Column("char", { name: "placa", nullable: true, length: 10 })
  placa: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;
}
