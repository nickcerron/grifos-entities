import { Column, Entity } from "typeorm";

@Entity("Tmp_Tarjetas", { schema: "dbo" })
export class TmpTarjetas {
  @Column("varchar", { name: "TarjAfiliacion", length: 25 })
  tarjAfiliacion: string;

  @Column("bigint", { name: "ClienteId" })
  clienteId: string;

  @Column("int", { name: "LocalId", nullable: true })
  localId: number | null;

  @Column("smalldatetime", { name: "FechaRegistro", nullable: true })
  fechaRegistro: Date | null;

  @Column("int", { name: "PtosAcumulado", nullable: true })
  ptosAcumulado: number | null;

  @Column("int", { name: "PtosCanjeados", nullable: true })
  ptosCanjeados: number | null;

  @Column("int", { name: "PtosDisponibles", nullable: true })
  ptosDisponibles: number | null;

  @Column("bit", { name: "Status", nullable: true })
  status: boolean | null;

  @Column("smalldatetime", { name: "Fechaultconsumo", nullable: true })
  fechaultconsumo: Date | null;

  @Column("nvarchar", { name: "Poseedor", nullable: true, length: 50 })
  poseedor: string | null;

  @Column("float", { name: "ValorAcumulado", nullable: true, precision: 53 })
  valorAcumulado: number | null;

  @Column("bit", { name: "IsIngresado", nullable: true, default: () => "(0)" })
  isIngresado: boolean | null;
}
