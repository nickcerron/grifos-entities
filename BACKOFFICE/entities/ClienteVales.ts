import { Column, Entity } from "typeorm";

@Entity("cliente_vales", { schema: "dbo" })
export class ClienteVales {
  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "Rangovale1", nullable: true, length: 10 })
  rangovale1: string | null;

  @Column("char", { name: "rangovale2", nullable: true, length: 10 })
  rangovale2: string | null;

  @Column("date", { name: "HastaFecha_activo", nullable: true })
  hastaFechaActivo: Date | null;

  @Column("bit", { name: "nousar_rango", nullable: true })
  nousarRango: boolean | null;
}
