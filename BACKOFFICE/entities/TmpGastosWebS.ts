import { Column, Entity } from "typeorm";

@Entity("TmpGastosWebS", { schema: "dbo" })
export class TmpGastosWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "Numero_Gasto", nullable: true, length: 10 })
  numeroGasto: string | null;

  @Column("smalldatetime", { name: "Fecha_Proceso" })
  fechaProceso: Date;

  @Column("int", { name: "TipoRegistro" })
  tipoRegistro: number;

  @Column("char", { name: "Tipo_Gasto", nullable: true, length: 5 })
  tipoGasto: string | null;

  @Column("char", { name: "TipoDocId", nullable: true, length: 5 })
  tipoDocId: string | null;

  @Column("varchar", { name: "NroDocumento", length: 15 })
  nroDocumento: string;

  @Column("char", { name: "ProveedorId", length: 11 })
  proveedorId: string;

  @Column("varchar", { name: "Encargado", nullable: true, length: 40 })
  encargado: string | null;

  @Column("float", { name: "Impuesto", nullable: true, precision: 53 })
  impuesto: number | null;

  @Column("float", { name: "MtoSubTotal", nullable: true, precision: 53 })
  mtoSubTotal: number | null;

  @Column("float", { name: "MtoImpuesto", nullable: true, precision: 53 })
  mtoImpuesto: number | null;

  @Column("float", { name: "MtoTotal", nullable: true, precision: 53 })
  mtoTotal: number | null;

  @Column("varchar", { name: "Observaciones", nullable: true, length: 250 })
  observaciones: string | null;

  @Column("bit", { name: "Cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
