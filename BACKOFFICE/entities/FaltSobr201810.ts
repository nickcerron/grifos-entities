import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_FaltSobr201810",
  ["cdlocal", "fecproceso", "turno", "cdvendedor", "nropos"],
  { unique: true }
)
@Entity("FaltSobr201810", { schema: "dbo" })
export class FaltSobr201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("int", { primary: true, name: "turno" })
  turno: number;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("float", { name: "total_ing", precision: 53 })
  totalIng: number;

  @Column("float", { name: "totalvta", precision: 53 })
  totalvta: number;

  @Column("char", { primary: true, name: "nropos", length: 10 })
  nropos: string;
}
