import { Column, Entity } from "typeorm";

@Entity("stock_back", { schema: "dbo" })
export class StockBack {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("smalldatetime", { name: "fecinicial", nullable: true })
  fecinicial: Date | null;

  @Column("numeric", {
    name: "stockinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinicial: number | null;

  @Column("char", { name: "monctoinicial", nullable: true, length: 1 })
  monctoinicial: string | null;

  @Column("numeric", {
    name: "ctoinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinicial: number | null;

  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("numeric", {
    name: "stockinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinventario: number | null;

  @Column("char", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column("numeric", {
    name: "stockminimo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockminimo: number | null;

  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;

  @Column("numeric", {
    name: "stockseparado",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockseparado: number | null;

  @Column("numeric", {
    name: "stockmaximo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockmaximo: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;

  @Column("char", { name: "usuingreso", nullable: true, length: 10 })
  usuingreso: string | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("char", { name: "ususalida", nullable: true, length: 10 })
  ususalida: string | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("char", { name: "usuventa", nullable: true, length: 10 })
  usuventa: string | null;

  @Column("smalldatetime", { name: "fecventa", nullable: true })
  fecventa: Date | null;

  @Column("bit", { name: "IS_RECALCULO", nullable: true })
  isRecalculo: boolean | null;
}
