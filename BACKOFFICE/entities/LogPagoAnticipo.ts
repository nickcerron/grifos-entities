import { Column, Entity } from "typeorm";

@Entity("LOG_PagoAnticipo", { schema: "dbo" })
export class LogPagoAnticipo {
  @Column("char", { name: "operacion", length: 40 })
  operacion: string;

  @Column("char", { name: "ruccliente", length: 15 })
  ruccliente: string;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "cdBanco", nullable: true, length: 4 })
  cdBanco: string | null;

  @Column("char", { name: "NroCuenta", nullable: true, length: 25 })
  nroCuenta: string | null;

  @Column("numeric", {
    name: "mtoanticipo",
    nullable: true,
    precision: 16,
    scale: 2,
  })
  mtoanticipo: number | null;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  @Column("char", { name: "Accion", nullable: true, length: 1 })
  accion: string | null;

  @Column("smalldatetime", { name: "fecSistema", nullable: true })
  fecSistema: Date | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;
}
