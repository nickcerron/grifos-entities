import { Column, Entity, Index } from "typeorm";

@Index("PK_cinventario", ["cdlocal", "cdalmacen", "cdinventario"], {
  unique: true,
})
@Entity("cinventario", { schema: "dbo" })
export class Cinventario {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { primary: true, name: "cdinventario", length: 5 })
  cdinventario: string;

  @Column("char", { name: "dsinventario", length: 60 })
  dsinventario: string;

  @Column("datetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;
}
