import { Column, Entity } from "typeorm";

@Entity("Tmp_Precio_GNV", { schema: "dbo" })
export class TmpPrecioGnv {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("char", { name: "cdprecio", length: 5 })
  cdprecio: string;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtoprecio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecio: number | null;

  @Column("numeric", {
    name: "porcomision",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porcomision: number | null;

  @Column("char", { name: "cdmonoferta", nullable: true, length: 1 })
  cdmonoferta: string | null;

  @Column("numeric", {
    name: "mtoprecioferta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecioferta: number | null;

  @Column("smalldatetime", { name: "fecinioferta", nullable: true })
  fecinioferta: Date | null;

  @Column("smalldatetime", { name: "fecfinoferta", nullable: true })
  fecfinoferta: Date | null;

  @Column("char", { name: "horinioferta", nullable: true, length: 8 })
  horinioferta: string | null;

  @Column("char", { name: "horfinoferta", nullable: true, length: 8 })
  horfinoferta: string | null;

  @Column("smalldatetime", { name: "fecedicion", nullable: true })
  fecedicion: Date | null;
}
