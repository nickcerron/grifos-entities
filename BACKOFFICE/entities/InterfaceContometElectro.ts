import { Column, Entity, Index } from "typeorm";

@Index("PK_interface_contomet_electro", ["fechaCierreDia", "linea", "cTurno"], {
  unique: true,
})
@Entity("interface_contomet_electro", { schema: "dbo" })
export class InterfaceContometElectro {
  @Column("smalldatetime", { primary: true, name: "fecha_cierre_dia" })
  fechaCierreDia: Date;

  @Column("int", { primary: true, name: "linea" })
  linea: number;

  @Column("bigint", { primary: true, name: "c_turno" })
  cTurno: string;

  @Column("int", { name: "CodLado", nullable: true })
  codLado: number | null;

  @Column("int", { name: "CodManguera", nullable: true })
  codManguera: number | null;

  @Column("varchar", { name: "CodArticulo", nullable: true, length: 20 })
  codArticulo: string | null;

  @Column("smalldatetime", { name: "FechaTurno", nullable: true })
  fechaTurno: Date | null;

  @Column("numeric", {
    name: "LecturaFinal",
    nullable: true,
    precision: 13,
    scale: 3,
  })
  lecturaFinal: number | null;

  @Column("numeric", {
    name: "PrecioVenta",
    nullable: true,
    precision: 19,
    scale: 3,
  })
  precioVenta: number | null;

  @Column("smalldatetime", {
    name: "fecha_registro",
    nullable: true,
    default: () => "getdate()",
  })
  fechaRegistro: Date | null;

  @Column("char", { name: "flag_automatico", nullable: true, length: 1 })
  flagAutomatico: string | null;

  @Column("numeric", {
    name: "galones",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  galones: number | null;

  @Column("numeric", {
    name: "inporte",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  inporte: number | null;
}
