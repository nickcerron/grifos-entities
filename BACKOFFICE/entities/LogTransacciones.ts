import { Column, Entity } from "typeorm";

@Entity("logTransacciones", { schema: "dbo" })
export class LogTransacciones {
  @Column("char", { name: "TipoTrn", nullable: true, length: 1 })
  tipoTrn: string | null;

  @Column("varchar", { name: "Tabla", nullable: true, length: 128 })
  tabla: string | null;

  @Column("varchar", { name: "PK", nullable: true, length: 1000 })
  pk: string | null;

  @Column("varchar", { name: "Campo", nullable: true, length: 128 })
  campo: string | null;

  @Column("varchar", { name: "ValorOriginal", nullable: true, length: 1000 })
  valorOriginal: string | null;

  @Column("varchar", { name: "ValorNuevo", nullable: true, length: 1000 })
  valorNuevo: string | null;

  @Column("datetime", { name: "FechaTrn", nullable: true })
  fechaTrn: Date | null;

  @Column("varchar", { name: "Usuario", nullable: true, length: 128 })
  usuario: string | null;

  @Column("varchar", { name: "OBS", nullable: true, length: 128 })
  obs: string | null;
}
