import { Column, Entity } from "typeorm";

@Entity("TmpAlmacenWebS", { schema: "dbo" })
export class TmpAlmacenWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("int", { name: "AlmacenId" })
  almacenId: number;

  @Column("varchar", { name: "Nombre", nullable: true, length: 50 })
  nombre: string | null;
}
