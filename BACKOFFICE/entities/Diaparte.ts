import { Column, Entity, Index } from "typeorm";

@Index("PK_diaparte", ["cdlocal", "fecproceso"], { unique: true })
@Entity("diaparte", { schema: "dbo" })
export class Diaparte {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("smalldatetime", { name: "fecanterior", nullable: true })
  fecanterior: Date | null;

  @Column("bit", { name: "cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("bit", { name: "pdislavend", nullable: true })
  pdislavend: boolean | null;

  @Column("bit", { name: "pddepvend", nullable: true })
  pddepvend: boolean | null;

  @Column("bit", { name: "pdcontom", nullable: true })
  pdcontom: boolean | null;

  @Column("bit", { name: "pdcobranza", nullable: true })
  pdcobranza: boolean | null;

  @Column("bit", { name: "pdVarillaje", nullable: true })
  pdVarillaje: boolean | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("varchar", { name: "NroPos", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("varchar", { name: "UserModifica", nullable: true, length: 10 })
  userModifica: string | null;

  @Column("datetime", {
    name: "FechaModifica",
    nullable: true,
    default: () => "getdate()",
  })
  fechaModifica: Date | null;

  @Column("varchar", { name: "user_reg", nullable: true, length: 10 })
  userReg: string | null;

  @Column("smalldatetime", { name: "fec_registra", nullable: true })
  fecRegistra: Date | null;

  @Column("varchar", { name: "User_Autoriza", nullable: true, length: 10 })
  userAutoriza: string | null;

  @Column("datetime", { name: "Fecha_Autoriza", nullable: true })
  fechaAutoriza: Date | null;

  @Column("varchar", { name: "Obs_Faltante", nullable: true, length: 90 })
  obsFaltante: string | null;

  @Column("varchar", { name: "Obs_Stock", nullable: true, length: 400 })
  obsStock: string | null;

  @Column("varchar", { name: "Obs_Contometros", nullable: true, length: 100 })
  obsContometros: string | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
