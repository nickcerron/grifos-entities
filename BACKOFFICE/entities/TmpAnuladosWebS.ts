import { Column, Entity } from "typeorm";

@Entity("TmpAnuladosWebS", { schema: "dbo" })
export class TmpAnuladosWebS {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "SerieMaq", length: 20 })
  serieMaq: string;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("bit", { name: "Anulado", nullable: true })
  anulado: boolean | null;

  @Column("smalldatetime", { name: "FecAnula", nullable: true })
  fecAnula: Date | null;
}
