import { Column, Entity } from "typeorm";

@Entity("terminalAutomatico", { schema: "dbo" })
export class TerminalAutomatico {
  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("bit", { name: "CierreX", nullable: true })
  cierreX: boolean | null;

  @Column("bit", { name: "CierreZ", nullable: true })
  cierreZ: boolean | null;

  @Column("bit", { name: "Obligatorio", nullable: true })
  obligatorio: boolean | null;
}
