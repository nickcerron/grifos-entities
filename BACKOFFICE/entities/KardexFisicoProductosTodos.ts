import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_F__3214EC07457F8F50", ["id"], { unique: true })
@Entity("Kardex_Fisico_Productos_Todos", { schema: "dbo" })
export class KardexFisicoProductosTodos {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
}
