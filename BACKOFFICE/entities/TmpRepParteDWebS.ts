import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepParteDWebS", { schema: "dbo" })
export class TmpRepParteDWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("datetime", { name: "Fecha" })
  fecha: Date;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("char", { name: "Grupo", nullable: true, length: 1 })
  grupo: string | null;

  @Column("varchar", { name: "dsarticulo", nullable: true, length: 40 })
  dsarticulo: string | null;

  @Column("decimal", {
    name: "inicial",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  inicial: number | null;

  @Column("decimal", {
    name: "compra",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  compra: number | null;

  @Column("decimal", {
    name: "Consumo",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  consumo: number | null;

  @Column("decimal", { name: "Venta", nullable: true, precision: 27, scale: 2 })
  venta: number | null;

  @Column("decimal", {
    name: "cFinal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  cFinal: number | null;

  @Column("decimal", {
    name: "varillaje",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  varillaje: number | null;

  @Column("decimal", {
    name: "difdia",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  difdia: number | null;

  @Column("decimal", {
    name: "difmes",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  difmes: number | null;

  @Column("decimal", {
    name: "Variacion",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  variacion: number | null;

  @Column("decimal", { name: "Total", nullable: true, precision: 27, scale: 2 })
  total: number | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
