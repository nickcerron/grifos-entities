import { Column, Entity, Index } from "typeorm";

@Index("PK_Ingreso201810", ["cdlocal", "cdtpingreso", "nroingreso"], {
  unique: true,
})
@Entity("Ingreso201810", { schema: "dbo" })
export class Ingreso201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("char", { primary: true, name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdalmorig", nullable: true, length: 3 })
  cdalmorig: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("char", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "FECVENCIMIENTO", nullable: true })
  fecvencimiento: Date | null;

  @Column("char", { name: "SCOP", nullable: true, length: 11 })
  scop: string | null;

  @Column("char", {
    name: "NroPedido",
    nullable: true,
    length: 15,
    default: () => "''",
  })
  nroPedido: string | null;

  @Column("bit", { name: "Cancelado", nullable: true, default: () => "(0)" })
  cancelado: boolean | null;

  @Column("numeric", {
    name: "mtopercepcion",
    nullable: true,
    precision: 12,
    scale: 4,
    default: () => "(0)",
  })
  mtopercepcion: number | null;

  @Column("numeric", {
    name: "FISE",
    nullable: true,
    precision: 14,
    scale: 4,
    default: () => "(0)",
  })
  fise: number | null;

  @Column("char", {
    name: "nroseriedoc",
    nullable: true,
    length: 5,
    default: () => "''",
  })
  nroseriedoc: string | null;

  @Column("char", {
    name: "NumSAP",
    nullable: true,
    length: 20,
    default: () => "''",
  })
  numSap: string | null;

  @Column("numeric", {
    name: "Redondeo",
    nullable: true,
    precision: 14,
    scale: 4,
    default: () => "(0)",
  })
  redondeo: number | null;

  @Column("bit", { name: "IsSAP", nullable: true, default: () => "(1)" })
  isSap: boolean | null;

  @Column("numeric", {
    name: "Flete",
    nullable: true,
    precision: 12,
    scale: 4,
    default: () => "(0)",
  })
  flete: number | null;
}
