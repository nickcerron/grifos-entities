import { Column, Entity } from "typeorm";

@Entity("Anomalias", { schema: "dbo" })
export class Anomalias {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "nroanomalia", nullable: true, length: 12 })
  nroanomalia: string | null;

  @Column("char", { name: "nroremito", nullable: true, length: 12 })
  nroremito: string | null;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 8,
    scale: 4,
  })
  tcambio: number | null;

  @Column("numeric", {
    name: "mtofaltsoles",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltsoles: number | null;

  @Column("numeric", {
    name: "mtofaltdolares",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltdolares: number | null;

  @Column("numeric", {
    name: "mtosobsoles",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobsoles: number | null;

  @Column("numeric", {
    name: "mtosobdolares",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobdolares: number | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 200 })
  observacion: string | null;

  @Column("datetime", { name: "fecregistra", nullable: true })
  fecregistra: Date | null;

  @Column("datetime", { name: "fecmodifica", nullable: true })
  fecmodifica: Date | null;

  @Column("varchar", { name: "usuregistra", nullable: true, length: 20 })
  usuregistra: string | null;

  @Column("varchar", { name: "usumodifica", nullable: true, length: 20 })
  usumodifica: string | null;

  @Column("char", { name: "nroHoja", nullable: true, length: 15 })
  nroHoja: string | null;

  @Column("char", { name: "cdremito", nullable: true, length: 5 })
  cdremito: string | null;
}
