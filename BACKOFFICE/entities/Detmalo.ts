import { Column, Entity } from "typeorm";

@Entity("detmalo", { schema: "dbo" })
export class Detmalo {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "nroseriemaq", nullable: true, length: 15 })
  nroseriemaq: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nroitem: number | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdalterna", nullable: true, length: 20 })
  cdalterna: string | null;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "cant_ncredito",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  cantNcredito: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoservicio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoservicio: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtototal: number | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "nrogasboy", nullable: true, length: 4 })
  nrogasboy: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nroguia", nullable: true, length: 10 })
  nroguia: string | null;

  @Column("char", { name: "nroproforma", nullable: true, length: 10 })
  nroproforma: string | null;

  @Column("bit", { name: "moverstock", nullable: true })
  moverstock: boolean | null;

  @Column("text", { name: "glosa", nullable: true })
  glosa: string | null;

  @Column("char", { name: "manguera", nullable: true, length: 1 })
  manguera: string | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 11, scale: 4 })
  costo: number | null;
}
