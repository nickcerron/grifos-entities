import { Column, Entity } from "typeorm";

@Entity("LOG_PrecioGrupoCli", { schema: "dbo" })
export class LogPrecioGrupoCli {
  @Column("char", { name: "CDGRUPOCLI", nullable: true, length: 5 })
  cdgrupocli: string | null;

  @Column("char", { name: "TIPOCLI", nullable: true, length: 3 })
  tipocli: string | null;

  @Column("char", { name: "CDARTICULO", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "PRECIO",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "TIPODES", nullable: true, length: 3 })
  tipodes: string | null;

  @Column("int", { name: "cdPrecioGCliente", nullable: true })
  cdPrecioGCliente: number | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
