import { Column, Entity, Index } from "typeorm";

@Index("PK_stocklocal", ["cdlocal", "cdarticulo", "talla"], { unique: true })
@Entity("stocklocal", { schema: "dbo" })
export class Stocklocal {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;
}
