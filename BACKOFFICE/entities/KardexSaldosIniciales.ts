import { Column, Entity } from "typeorm";

@Entity("Kardex_Saldos_Iniciales", { schema: "dbo" })
export class KardexSaldosIniciales {
  @Column("varchar", { name: "Periodo", nullable: true, length: 6 })
  periodo: string | null;

  @Column("varchar", { name: "CdArticulo", nullable: true, length: 20 })
  cdArticulo: string | null;

  @Column("numeric", {
    name: "cantidadi",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadi: number | null;

  @Column("numeric", {
    name: "costoi",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costoi: number | null;

  @Column("numeric", {
    name: "totali",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totali: number | null;
}
