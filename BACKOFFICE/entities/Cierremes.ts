import { Column, Entity, Index } from "typeorm";

@Index("PK_cierremes_1", ["anno", "mes", "cdarticulo"], { unique: true })
@Entity("cierremes", { schema: "dbo" })
export class Cierremes {
  @Column("numeric", { primary: true, name: "anno", precision: 4, scale: 0 })
  anno: number;

  @Column("numeric", { primary: true, name: "mes", precision: 2, scale: 0 })
  mes: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 8,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 11, scale: 4 })
  costo: number | null;

  @Column("numeric", {
    name: "saldoinicial",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  saldoinicial: number | null;

  @Column("numeric", {
    name: "compra_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  compraUnid: number | null;

  @Column("numeric", {
    name: "compra_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  compraVal: number | null;

  @Column("numeric", {
    name: "venta_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaUnid: number | null;

  @Column("numeric", {
    name: "venta_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaVal: number | null;

  @Column("numeric", {
    name: "venta_inc",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaInc: number | null;

  @Column("numeric", {
    name: "ingreso_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ingresoUnid: number | null;

  @Column("numeric", {
    name: "ingreso_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ingresoVal: number | null;

  @Column("numeric", {
    name: "salida_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  salidaUnid: number | null;

  @Column("numeric", {
    name: "salida_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  salidaVal: number | null;

  @Column("numeric", {
    name: "transfer_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  transferUnid: number | null;

  @Column("numeric", {
    name: "transfer_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  transferVal: number | null;

  @Column("numeric", {
    name: "ajuste_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ajusteUnid: number | null;

  @Column("numeric", {
    name: "ajuste_val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ajusteVal: number | null;

  @Column("numeric", {
    name: "saldo_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  saldoUnid: number | null;

  @Column("numeric", {
    name: "margen",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  margen: number | null;
}
