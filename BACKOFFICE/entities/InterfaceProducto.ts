import { Column, Entity, Index } from "typeorm";

@Index("PK_interface_producto", ["idProducto"], { unique: true })
@Entity("interface_producto", { schema: "dbo" })
export class InterfaceProducto {
  @Column("tinyint", { primary: true, name: "id_producto" })
  idProducto: number;

  @Column("varchar", { name: "desc_producto", length: 20 })
  descProducto: string;

  @Column("float", { name: "venta", nullable: true, precision: 53 })
  venta: number | null;

  @Column("float", { name: "precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("varchar", { name: "CodArticulo", length: 20 })
  codArticulo: string;

  @Column("float", { name: "equivalencia", nullable: true, precision: 53 })
  equivalencia: number | null;
}
