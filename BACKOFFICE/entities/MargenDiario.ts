import { Column, Entity } from "typeorm";

@Entity("MargenDiario", { schema: "dbo" })
export class MargenDiario {
  @Column("date", { name: "Fecha", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "Articulo", nullable: true, length: 20 })
  articulo: string | null;

  @Column("numeric", {
    name: "StockGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  stockGls: number | null;

  @Column("numeric", {
    name: "UltCosto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ultCosto: number | null;

  @Column("numeric", {
    name: "CompraGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  compraGls: number | null;

  @Column("numeric", { name: "Costo", nullable: true, precision: 12, scale: 2 })
  costo: number | null;

  @Column("numeric", { name: "Total", nullable: true, precision: 12, scale: 2 })
  total: number | null;

  @Column("numeric", {
    name: "CostoSinIGV",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  costoSinIgv: number | null;

  @Column("numeric", {
    name: "PVPPizarra",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  pvpPizarra: number | null;

  @Column("numeric", {
    name: "VentaGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaGls: number | null;

  @Column("numeric", {
    name: "VentaSIGES",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaSiges: number | null;

  @Column("numeric", {
    name: "VentaSinIGV",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaSinIgv: number | null;

  @Column("numeric", {
    name: "CostoTotSinIGV",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  costoTotSinIgv: number | null;

  @Column("numeric", {
    name: "MgRetailTotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgRetailTotal: number | null;

  @Column("numeric", {
    name: "MgUnitRetail",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgUnitRetail: number | null;

  @Column("numeric", {
    name: "VentaCalleGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCalleGls: number | null;

  @Column("numeric", {
    name: "VentaCalleTotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCalleTotal: number | null;

  @Column("numeric", {
    name: "VentaCalleCosto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCalleCosto: number | null;

  @Column("numeric", {
    name: "MgUnitRetailCalle",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgUnitRetailCalle: number | null;

  @Column("numeric", {
    name: "VentaEspGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaEspGls: number | null;

  @Column("numeric", {
    name: "VentaEspTotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaEspTotal: number | null;

  @Column("numeric", {
    name: "VentaEspCosto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaEspCosto: number | null;

  @Column("numeric", {
    name: "MgUnitRetailEspecial",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgUnitRetailEspecial: number | null;

  @Column("numeric", {
    name: "VentaCredGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCredGls: number | null;

  @Column("numeric", {
    name: "VentaCredTotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCredTotal: number | null;

  @Column("numeric", {
    name: "VentaCredCosto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaCredCosto: number | null;

  @Column("numeric", {
    name: "MgUnitRetailCredito",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgUnitRetailCredito: number | null;

  @Column("numeric", {
    name: "VentaLicGls",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaLicGls: number | null;

  @Column("numeric", {
    name: "VentaLicTotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaLicTotal: number | null;

  @Column("numeric", {
    name: "VentaLicCosto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  ventaLicCosto: number | null;

  @Column("numeric", {
    name: "MgUnitRetailLicitacion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mgUnitRetailLicitacion: number | null;

  @Column("char", { name: "estado", nullable: true, length: 1 })
  estado: string | null;
}
