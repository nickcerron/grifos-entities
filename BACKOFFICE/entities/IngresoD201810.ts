import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_IngresoD201810",
  ["cdlocal", "cdtpingreso", "nroingreso", "nroitem", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("IngresoD201810", { schema: "dbo" })
export class IngresoD201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("char", { primary: true, name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("char", { name: "cdalternativo", nullable: true, length: 20 })
  cdalternativo: string | null;

  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 12, scale: 4 })
  costo: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctopromedio: number | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "impuesto1",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto1: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "precactual",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  precactual: number | null;

  @Column("numeric", {
    name: "mgutilidad",
    nullable: true,
    precision: 7,
    scale: 2,
  })
  mgutilidad: number | null;

  @Column("numeric", {
    name: "preciosug",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  preciosug: number | null;

  @Column("bit", { name: "flgcambprec", nullable: true })
  flgcambprec: boolean | null;

  @Column("char", { name: "categoria", nullable: true, length: 5 })
  categoria: string | null;

  @Column("int", { name: "Linea", nullable: true, default: () => "(0)" })
  linea: number | null;
}
