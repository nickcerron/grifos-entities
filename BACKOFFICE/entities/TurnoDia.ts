import { Column, Entity } from "typeorm";

@Entity("turno_dia", { schema: "dbo" })
export class TurnoDia {
  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "turno", nullable: true, length: 10 })
  turno: string | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "seriehd", nullable: true, length: 10 })
  seriehd: string | null;
}
