import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("Kardex_Fisico_NC", { schema: "dbo" })
export class KardexFisicoNc {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "nroncredito", nullable: true, length: 10 })
  nroncredito: string | null;

  @Column("char", { name: "cdtipodocrefe", nullable: true, length: 5 })
  cdtipodocrefe: string | null;

  @Column("varchar", { name: "docreferencia", nullable: true, length: 10 })
  docreferencia: string | null;

  @Column("datetime", { name: "fecreferencia", nullable: true })
  fecreferencia: Date | null;

  @Column("char", { name: "tipofactura", nullable: true, length: 1 })
  tipofactura: string | null;
}
