import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Variables__7C8480AE", ["varId"], { unique: true })
@Entity("Variables", { schema: "dbo" })
export class Variables {
  @PrimaryGeneratedColumn({ type: "int", name: "VarId" })
  varId: number;

  @Column("varchar", { name: "TipoVar", nullable: true, length: 15 })
  tipoVar: string | null;

  @Column("varchar", { name: "Clave", nullable: true, length: 30 })
  clave: string | null;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 200 })
  descripcion: string | null;

  @Column("float", {
    name: "ValorPto",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  valorPto: number | null;

  @Column("varchar", { name: "Config", nullable: true, length: 250 })
  config: string | null;

  @Column("char", { name: "valor", nullable: true, length: 2 })
  valor: string | null;

  @Column("bit", { name: "flgelimina", nullable: true })
  flgelimina: boolean | null;
}
