import { Column, Entity } from "typeorm";

@Entity("logdet", { schema: "dbo" })
export class Logdet {
  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "limitemto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  limitemto: number | null;

  @Column("numeric", {
    name: "consumto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  consumto: number | null;

  @Column("char", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("char", { name: "TipoDespacho", nullable: true, length: 1 })
  tipoDespacho: string | null;

  @Column("smalldatetime", { name: "FechaAtencion", nullable: true })
  fechaAtencion: Date | null;

  @Column("bit", { name: "Enviado", nullable: true })
  enviado: boolean | null;

  @Column("char", { name: "nrocontrato", nullable: true, length: 10 })
  nrocontrato: string | null;
}
