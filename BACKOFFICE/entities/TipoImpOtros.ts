import { Column, Entity } from "typeorm";

@Entity("TIPO_IMP_OTROS", { schema: "dbo" })
export class TipoImpOtros {
  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", { name: "MONTO", nullable: true, precision: 12, scale: 4 })
  monto: number | null;
}
