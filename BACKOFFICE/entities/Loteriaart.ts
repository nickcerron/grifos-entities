import { Column, Entity, Index } from "typeorm";

@Index("PK_loteriaart", ["item", "cdarticulo"], { unique: true })
@Entity("loteriaart", { schema: "dbo" })
export class Loteriaart {
  @Column("numeric", { primary: true, name: "item", precision: 2, scale: 0 })
  item: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;
}
