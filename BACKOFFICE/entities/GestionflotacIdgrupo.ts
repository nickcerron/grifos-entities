import { Column, Entity, Index } from "typeorm";

@Index("fk_gestionflotac_IDGRUPO", ["cdcliente", "cdgrupoId"], { unique: true })
@Entity("gestionflotac_idgrupo", { schema: "dbo" })
export class GestionflotacIdgrupo {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("int", { primary: true, name: "cdgrupoID" })
  cdgrupoId: number;
}
