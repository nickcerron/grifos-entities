import { Column, Entity, Index } from "typeorm";

@Index("PK_NoVenta202302", ["nropos", "fecproceso"], { unique: true })
@Entity("NoVenta202302", { schema: "dbo" })
export class NoVenta202302 {
  @Column("char", { primary: true, name: "nropos", length: 10 })
  nropos: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 6,
    scale: 0,
  })
  cantidad: number | null;
}
