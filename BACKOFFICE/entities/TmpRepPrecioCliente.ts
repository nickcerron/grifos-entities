import { Column, Entity } from "typeorm";

@Entity("TmpRepPrecioCliente", { schema: "dbo" })
export class TmpRepPrecioCliente {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("char", { name: "CdCliente", length: 15 })
  cdCliente: string;

  @Column("char", { name: "TipoCli", length: 3 })
  tipoCli: string;

  @Column("char", { name: "CdArticulo", length: 20 })
  cdArticulo: string;

  @Column("numeric", {
    name: "Precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "TipoDes", nullable: true, length: 3 })
  tipoDes: string | null;

  @Column("varchar", { name: "NroContrato", length: 60 })
  nroContrato: string;

  @Column("bit", { name: "Licitacion", nullable: true })
  licitacion: boolean | null;
}
