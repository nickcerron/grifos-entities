import { Column, Entity } from "typeorm";

@Entity("preciohist", { schema: "dbo" })
export class Preciohist {
  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdprecio", nullable: true, length: 5 })
  cdprecio: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 10 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "precioante",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precioante: number | null;

  @Column("numeric", {
    name: "precionuevo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precionuevo: number | null;

  @Column("char", { name: "cdmonoferta", nullable: true, length: 1 })
  cdmonoferta: string | null;

  @Column("numeric", {
    name: "mtoprecioferta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoprecioferta: number | null;

  @Column("smalldatetime", { name: "fecinioferta", nullable: true })
  fecinioferta: Date | null;

  @Column("smalldatetime", { name: "fecfinoferta", nullable: true })
  fecfinoferta: Date | null;

  @Column("char", { name: "horinioferta", nullable: true, length: 8 })
  horinioferta: string | null;

  @Column("char", { name: "horfinoferta", nullable: true, length: 8 })
  horfinoferta: string | null;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;
}
