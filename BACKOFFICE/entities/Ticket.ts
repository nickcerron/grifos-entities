import { Column, Entity } from "typeorm";

@Entity("ticket", { schema: "dbo" })
export class Ticket {
  @Column("char", { name: "documento", nullable: true, length: 6 })
  documento: string | null;

  @Column("char", { name: "tipo", nullable: true, length: 1 })
  tipo: string | null;

  @Column("numeric", { name: "linea", nullable: true, precision: 2, scale: 0 })
  linea: number | null;

  @Column("char", { name: "texto", nullable: true, length: 39 })
  texto: string | null;
}
