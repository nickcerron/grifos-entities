import { Column, Entity, Index } from "typeorm";

@Index("PK_region", ["cdregion"], { unique: true })
@Entity("region", { schema: "dbo" })
export class Region {
  @Column("char", { primary: true, name: "cdregion", length: 5 })
  cdregion: string;

  @Column("varchar", { name: "dsregion", length: 40 })
  dsregion: string;
}
