import { Column, Entity, Index } from "typeorm";

@Index("PK_almacen", ["cdalmacen"], { unique: true })
@Entity("almacen", { schema: "dbo" })
export class Almacen {
  // ahora ser id, bigint, autoincremental
  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  //name
  @Column("char", { name: "dsalmacen", nullable: true, length: 50 })
  dsalmacen: string | null;

  //description
  @Column("varchar", { name: "daalmacen", nullable: true, length: 100 })
  daalmacen: string | null;

  // la mayoria en null y uno que otro en 1 o 0
  @Column("bit", { name: "inventario", nullable: true })
  inventario: boolean | null;

  //?fecha de inventario, 99% en null, deberia morir
  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  //* tipo de almacen, todo en null, tal vez deberiamos crear otra entidad
  @Column("char", { name: "TIPO", nullable: true, length: 1 })
  tipo: string | null;

  //estado, todo en 1
  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;

  //!muere, ya se podra ver en el distrito
  @Column("char", { name: "cdDepartamento", nullable: true, length: 2 })
  cdDepartamento: string | null;


  //!muere, ya se podra ver en el distrito
  @Column("char", { name: "cdProvincia", nullable: true, length: 5 })
  cdProvincia: string | null;

  //referencia a distrito
  @Column("char", { name: "cdDistrito", nullable: true, length: 5 })
  cdDistrito: string | null;

  //!muere, ya se podra ver en el distrito
  @Column("char", { name: "cdUbigeo", nullable: true, length: 6 })
  cdUbigeo: string | null;

  //address
  @Column("varchar", { name: "Direccion", nullable: true, length: 200 })
  direccion: string | null;
}
