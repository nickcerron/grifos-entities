import { Column, Entity } from "typeorm";

@Entity("TmpClientesWebS", { schema: "dbo" })
export class TmpClientesWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("varchar", { name: "RazonSocial", nullable: true, length: 60 })
  razonSocial: string | null;

  @Column("varchar", { name: "RUC", nullable: true, length: 15 })
  ruc: string | null;

  @Column("char", { name: "UbigeoId", length: 6 })
  ubigeoId: string;

  @Column("char", { name: "ZonaId", length: 5 })
  zonaId: string;

  @Column("varchar", { name: "Direccion", nullable: true, length: 60 })
  direccion: string | null;

  @Column("varchar", { name: "Correo", nullable: true, length: 60 })
  correo: string | null;

  @Column("smalldatetime", { name: "Fecha_Nacimiento", nullable: true })
  fechaNacimiento: Date | null;

  @Column("varchar", { name: "Telefono1", nullable: true, length: 15 })
  telefono1: string | null;

  @Column("varchar", { name: "Telefono2", nullable: true, length: 15 })
  telefono2: string | null;

  @Column("varchar", { name: "Fax", nullable: true, length: 15 })
  fax: string | null;

  @Column("varchar", { name: "Dir_Cobranza", nullable: true, length: 60 })
  dirCobranza: string | null;

  @Column("varchar", { name: "Dir_Entrega", nullable: true, length: 60 })
  dirEntrega: string | null;

  @Column("varchar", { name: "Contacto", nullable: true, length: 30 })
  contacto: string | null;

  @Column("char", { name: "TipoCliente", nullable: true, length: 1 })
  tipoCliente: string | null;

  @Column("int", { name: "Dias_Credito", nullable: true })
  diasCredito: number | null;

  @Column("int", { name: "DiasMax_ND", nullable: true })
  diasMaxNd: number | null;

  @Column("char", { name: "GrupoIdCliente", length: 5 })
  grupoIdCliente: string;

  @Column("char", { name: "TipoSaldo", nullable: true, length: 1 })
  tipoSaldo: string | null;

  @Column("numeric", {
    name: "LimiteMto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  limiteMto: number | null;

  @Column("numeric", {
    name: "ConsuMto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  consuMto: number | null;

  @Column("bit", { name: "FlgIlimitado", nullable: true })
  flgIlimitado: boolean | null;

  @Column("bit", { name: "FlgBloquea", nullable: true })
  flgBloquea: boolean | null;
}
