import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_P__7CFB65B238822CE3", ["periodo"], { unique: true })
@Entity("Kardex_Periodo_Inicial", { schema: "dbo" })
export class KardexPeriodoInicial {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { primary: true, name: "Periodo", length: 6 })
  periodo: string;
}
