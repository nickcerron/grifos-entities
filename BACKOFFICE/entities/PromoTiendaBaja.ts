import { Column, Entity } from "typeorm";

@Entity("promo_tienda_baja", { schema: "dbo" })
export class PromoTiendaBaja {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdamarre", length: 20 })
  cdamarre: string;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "descuento",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  descuento: number | null;

  @Column("smalldatetime", { name: "fechainicial", nullable: true })
  fechainicial: Date | null;

  @Column("smalldatetime", { name: "fechafinal", nullable: true })
  fechafinal: Date | null;
}
