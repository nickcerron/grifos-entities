import { Column, Entity } from "typeorm";

@Entity("cierre", { schema: "dbo" })
export class Cierre {
  @Column("char", { name: "cdcierre", nullable: true, length: 1 })
  cdcierre: string | null;

  @Column("bit", { name: "flggrupo01", nullable: true })
  flggrupo01: boolean | null;

  @Column("bit", { name: "flggrupo02", nullable: true })
  flggrupo02: boolean | null;

  @Column("bit", { name: "flggrupo03", nullable: true })
  flggrupo03: boolean | null;

  @Column("bit", { name: "flggrupo04", nullable: true })
  flggrupo04: boolean | null;

  @Column("bit", { name: "flggrupo05", nullable: true })
  flggrupo05: boolean | null;

  @Column("bit", { name: "flgvendedor", nullable: true })
  flgvendedor: boolean | null;

  @Column("bit", { name: "flgarticulo", nullable: true })
  flgarticulo: boolean | null;

  @Column("bit", { name: "flgpago", nullable: true })
  flgpago: boolean | null;

  @Column("bit", { name: "flgcara", nullable: true })
  flgcara: boolean | null;

  @Column("bit", { name: "flgdocmanual", nullable: true })
  flgdocmanual: boolean | null;

  @Column("bit", { name: "flgusuario", nullable: true })
  flgusuario: boolean | null;

  @Column("bit", { name: "FLGCANJEARTICULO", nullable: true })
  flgcanjearticulo: boolean | null;

  @Column("bit", { name: "flgdesc", nullable: true })
  flgdesc: boolean | null;

  @Column("bit", { name: "flgarticulodesc", nullable: true })
  flgarticulodesc: boolean | null;

  @Column("bit", { name: "flgdepositogrifero", nullable: true })
  flgdepositogrifero: boolean | null;

  @Column("bit", { name: "flgconsolidarlados", nullable: true })
  flgconsolidarlados: boolean | null;

  @Column("bit", { name: "flggastogrifero", nullable: true })
  flggastogrifero: boolean | null;

  @Column("bit", { name: "flgstknegativo", default: () => "(0)" })
  flgstknegativo: boolean;
}
