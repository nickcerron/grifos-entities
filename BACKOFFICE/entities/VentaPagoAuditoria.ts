import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_VENTA_PAGO_AUDITORIA",
  ["cdtipodoc", "nrodocumento", "nroseriemaq"],
  { unique: true }
)
@Entity("VENTA_PAGO_AUDITORIA", { schema: "dbo" })
export class VentaPagoAuditoria {
  @Column("varchar", { primary: true, name: "CDTIPODOC", length: 5 })
  cdtipodoc: string;

  @Column("varchar", { primary: true, name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("varchar", { primary: true, name: "NROSERIEMAQ", length: 20 })
  nroseriemaq: string;

  @Column("smalldatetime", { name: "FECPROCESO", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;
}
