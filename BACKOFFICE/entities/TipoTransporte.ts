import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoTransporte", ["cdtpTransporte"], { unique: true })
@Entity("TipoTransporte", { schema: "dbo" })
export class TipoTransporte {
  @Column("char", { primary: true, name: "cdtpTransporte", length: 2 })
  cdtpTransporte: string;

  @Column("varchar", { name: "dstpTransporte", length: 20 })
  dstpTransporte: string;
}
