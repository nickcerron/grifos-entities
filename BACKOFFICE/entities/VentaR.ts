import { Column, Entity, Index } from "typeorm";

@Index("PK_ventar", ["cdlocal", "fecdocumento", "fecproceso"], { unique: true })
@Entity("ventar", { schema: "dbo" })
export class Ventar {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecdocumento" })
  fecdocumento: Date;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;
}
