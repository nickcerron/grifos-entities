import { Column, Entity } from "typeorm";

@Entity("HCredCliente_WEBMIS", { schema: "dbo" })
export class HCredClienteWebmis {
  @Column("char", { name: "docpago", length: 1 })
  docpago: string;

  @Column("char", { name: "CdTipoDoc", nullable: true, length: 5 })
  cdTipoDoc: string | null;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("smalldatetime", { name: "fecpago", nullable: true })
  fecpago: Date | null;

  @Column("smalldatetime", { name: "fecsistema" })
  fecsistema: Date;

  @Column("bigint", { name: "ClienteId", nullable: true })
  clienteId: string | null;

  @Column("varchar", { name: "Moneda", length: 3 })
  moneda: string;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtoemision",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtoemision: number | null;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolares",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cddocaplica", nullable: true, length: 5 })
  cddocaplica: string | null;

  @Column("char", { name: "nrodocaplica", nullable: true, length: 10 })
  nrodocaplica: string | null;

  @Column("char", { name: "cdcobrador", nullable: true, length: 10 })
  cdcobrador: string | null;

  @Column("numeric", {
    name: "nropago",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nropago: number | null;

  @Column("char", { name: "nroplanilla", nullable: true, length: 10 })
  nroplanilla: string | null;

  @Column("char", { name: "nrorecibo", nullable: true, length: 10 })
  nrorecibo: string | null;

  @Column("char", { name: "cdtppago", nullable: true, length: 5 })
  cdtppago: string | null;

  @Column("char", { name: "cdbanco", nullable: true, length: 5 })
  cdbanco: string | null;

  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("char", { name: "nrocheque", nullable: true, length: 20 })
  nrocheque: string | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("varchar", { name: "referencia", nullable: true, length: 60 })
  referencia: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;
}
