import { Column, Entity, Index } from "typeorm";

@Index("PK_importaciong", ["cdlocal", "nroimportacion", "fecha"], {
  unique: true,
})
@Entity("importaciong", { schema: "dbo" })
export class Importaciong {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroimportacion", length: 10 })
  nroimportacion: string;

  @Column("smalldatetime", { primary: true, name: "fecha" })
  fecha: Date;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;
}
