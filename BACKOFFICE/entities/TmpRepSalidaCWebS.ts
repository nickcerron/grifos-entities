import { Column, Entity } from "typeorm";

@Entity("TmpRepSalidaCWebS", { schema: "dbo" })
export class TmpRepSalidaCWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "SalidaId", length: 5 })
  salidaId: string;

  @Column("varchar", { name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("varchar", { name: "TipoDocId", nullable: true, length: 5 })
  tipoDocId: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("varchar", { name: "ProveedorId", nullable: true, length: 15 })
  proveedorId: string | null;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("char", { name: "AlmacenId_Destino", nullable: true, length: 3 })
  almacenIdDestino: string | null;

  @Column("varchar", { name: "nroingreso", nullable: true, length: 10 })
  nroingreso: string | null;

  @Column("char", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("varchar", { name: "UserIdReg", nullable: true, length: 20 })
  userIdReg: string | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("varchar", { name: "UserIdAnula", nullable: true, length: 20 })
  userIdAnula: string | null;

  @Column("bit", { name: "IsSAP", nullable: true })
  isSap: boolean | null;

  @Column("varchar", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
