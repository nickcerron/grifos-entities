import { Column, Entity, Index } from "typeorm";

@Index("PK_mascara", ["variable"], { unique: true })
@Entity("mascara", { schema: "dbo" })
export class Mascara {
  @Column("char", { primary: true, name: "variable", length: 10 })
  variable: string;

  @Column("char", { name: "nombre", nullable: true, length: 12 })
  nombre: string | null;

  @Column("char", { name: "valor", nullable: true, length: 20 })
  valor: string | null;
}
