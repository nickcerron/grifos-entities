import { Column, Entity } from "typeorm";

@Entity("logpre", { schema: "dbo" })
export class Logpre {
  @Column("datetime", { name: "fecha" })
  fecha: Date;

  @Column("char", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "tipodes", nullable: true, length: 3 })
  tipodes: string | null;

  @Column("char", { name: "nrocontrato", nullable: true, length: 10 })
  nrocontrato: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;
}
