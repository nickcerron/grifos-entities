import { Column, Entity, Index } from "typeorm";

@Index("PK_GrupoProdclienteDesDetalle", ["cdgrupoId", "cdgrupo"], {
  unique: true,
})
@Entity("GrupoProdclienteDesDetalle", { schema: "dbo" })
export class GrupoProdclienteDesDetalle {
  @Column("int", { primary: true, name: "cdgrupoID" })
  cdgrupoId: number;

  @Column("char", { primary: true, name: "cdgrupo", length: 5 })
  cdgrupo: string;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("varchar", { name: "DsGrupo", nullable: true, length: 50 })
  dsGrupo: string | null;
}
