import { Column, Entity } from "typeorm";

@Entity("TmpRepRemitos", { schema: "dbo" })
export class TmpRepRemitos {
  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "CDREMITO", nullable: true, length: 5 })
  cdremito: string | null;

  @Column("varchar", { name: "NROREMITO", nullable: true, length: 12 })
  nroremito: string | null;

  @Column("datetime", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "NROCUENTA", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("numeric", {
    name: "TCAMBIO",
    nullable: true,
    precision: 8,
    scale: 4,
  })
  tcambio: number | null;

  @Column("int", { name: "NROPAQUETES", nullable: true })
  nropaquetes: number | null;

  @Column("numeric", {
    name: "MTOSOLES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "MTODOLARES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("varchar", { name: "OBSERVACION", nullable: true, length: 200 })
  observacion: string | null;

  @Column("datetime", { name: "FECREGISTRA", nullable: true })
  fecregistra: Date | null;

  @Column("datetime", { name: "FECMODIFICA", nullable: true })
  fecmodifica: Date | null;

  @Column("varchar", { name: "USUREGISTRA", nullable: true, length: 20 })
  usuregistra: string | null;

  @Column("varchar", { name: "USUMODIFICA", nullable: true, length: 20 })
  usumodifica: string | null;

  @Column("varchar", { name: "ESTADO", nullable: true, length: 1 })
  estado: string | null;

  @Column("int", { name: "NROPAQSOLES", nullable: true })
  nropaqsoles: number | null;

  @Column("int", { name: "NROPAQDOLARES", nullable: true })
  nropaqdolares: number | null;

  @Column("varchar", { name: "NROBOLSA", nullable: true, length: 15 })
  nrobolsa: string | null;

  @Column("varchar", { name: "NROCUENTADOLARES", nullable: true, length: 20 })
  nrocuentadolares: string | null;
}
