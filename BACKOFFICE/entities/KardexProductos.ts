import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_P__3214EC077C9FEC28", ["id"], { unique: true })
@Entity("Kardex_Productos", { schema: "dbo" })
export class KardexProductos {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
}
