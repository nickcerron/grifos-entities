import { Column, Entity } from "typeorm";

@Entity("TmpSaldoCliCWebS", { schema: "dbo" })
export class TmpSaldoCliCWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("varchar", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { name: "tpsaldo", nullable: true, length: 1 })
  tpsaldo: string | null;

  @Column("float", { name: "limitemto", nullable: true, precision: 53 })
  limitemto: number | null;

  @Column("float", { name: "consumto", nullable: true, precision: 53 })
  consumto: number | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("bit", { name: "IsIngresado", nullable: true, default: () => "(0)" })
  isIngresado: boolean | null;
}
