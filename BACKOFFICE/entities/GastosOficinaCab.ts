import { Column, Entity } from "typeorm";

@Entity("Gastos_Oficina_Cab", { schema: "dbo" })
export class GastosOficinaCab {
  @Column("char", { name: "CdLocal", length: 3 })
  cdLocal: string;

  @Column("smalldatetime", { name: "Fecha" })
  fecha: Date;

  @Column("numeric", {
    name: "TotalAutorizado",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  totalAutorizado: number | null;

  @Column("bit", { name: "cerrado", nullable: true })
  cerrado: boolean | null;
}
