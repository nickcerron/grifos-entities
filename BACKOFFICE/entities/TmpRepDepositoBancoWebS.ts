import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepDepositoBancoWebS", { schema: "dbo" })
export class TmpRepDepositoBancoWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("int", { name: "Turno", nullable: true })
  turno: number | null;

  @Column("varchar", { name: "NroCuenta", length: 20 })
  nroCuenta: string;

  @Column("varchar", { name: "NroOperacion", length: 20 })
  nroOperacion: string;

  @Column("smalldatetime", { name: "FecDeposito", nullable: true })
  fecDeposito: Date | null;

  @Column("char", { name: "GrupoId01", length: 5 })
  grupoId01: string;

  @Column("decimal", {
    name: "MtoTotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoTotal: number | null;

  @Column("decimal", {
    name: "TCambio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  tCambio: number | null;

  @Column("decimal", {
    name: "Valores",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  valores: number | null;

  @Column("decimal", {
    name: "Cheques",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  cheques: number | null;

  @Column("varchar", { name: "Observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("smalldatetime", { name: "FecConciliacion", nullable: true })
  fecConciliacion: Date | null;

  @Column("bit", { name: "Conciliado", nullable: true })
  conciliado: boolean | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;

  @Column("bit", { name: "IsIngresado", nullable: true })
  isIngresado: boolean | null;
}
