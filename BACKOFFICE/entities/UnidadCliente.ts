import { Column, Entity, Index } from "typeorm";

@Index("PK_Unidad_Cliente", ["cdUnidad"], { unique: true })
@Entity("Unidad_Cliente", { schema: "dbo" })
export class UnidadCliente {
  @Column("char", { primary: true, name: "cdUnidad", length: 5 })
  cdUnidad: string;

  @Column("char", { name: "dsUnidad", length: 80 })
  dsUnidad: string;

  @Column("bit", { name: "Activo", nullable: true })
  activo: boolean | null;
}
