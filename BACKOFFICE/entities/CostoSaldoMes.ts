import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("IX_Costo_Saldo_mes", ["cdarticulo", "periodo"], {})
@Index("PK_Costo_Saldo_mes", ["id"], { unique: true })
@Entity("Costo_Saldo_mes", { schema: "dbo" })
export class CostoSaldoMes {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "periodo", length: 6 })
  periodo: string;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;

  @Column("numeric", {
    name: "costof",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costof: number | null;

  @Column("numeric", {
    name: "totalf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totalf: number | null;
}
