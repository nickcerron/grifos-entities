import { Column, Entity, Index, OneToMany } from "typeorm";
import { OrdenPedidoDetalle } from "./OrdenPedidoDetalle";

@Index("PK_Orden_Pedido", ["empresaId", "localId", "nroPedido"], {
  unique: true,
})
@Entity("Orden_Pedido", { schema: "dbo" })
export class OrdenPedido {
  @Column("int", { primary: true, name: "EmpresaId" })
  empresaId: number;

  @Column("int", { primary: true, name: "LocalId" })
  localId: number;

  @Column("varchar", { primary: true, name: "NroPedido", length: 15 })
  nroPedido: string;

  @Column("smalldatetime", { name: "Fecha_Pedido" })
  fechaPedido: Date;

  @Column("varchar", { name: "SCOP", length: 15 })
  scop: string;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("varchar", { name: "TransportistaId", length: 15 })
  transportistaId: string;

  @Column("varchar", { name: "RUC_Transportista", nullable: true, length: 15 })
  rucTransportista: string | null;

  @Column("varchar", { name: "NroLicencia", length: 15 })
  nroLicencia: string;

  @Column("varchar", { name: "DNI_Chofer", nullable: true, length: 10 })
  dniChofer: string | null;

  @Column("varchar", { name: "Placa_Vehiculo", length: 10 })
  placaVehiculo: string;

  @Column("varchar", { name: "Placa_Cisterna", length: 10 })
  placaCisterna: string;

  @Column("char", { name: "BancoId", length: 4 })
  bancoId: string;

  @Column("varchar", { name: "NroCheque", nullable: true, length: 20 })
  nroCheque: string | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 10 })
  userRegistra: string | null;

  @Column("smalldatetime", { name: "Fecha_Registra", nullable: true })
  fechaRegistra: Date | null;

  @Column("varchar", { name: "User_Modifica", nullable: true, length: 10 })
  userModifica: string | null;

  @Column("smalldatetime", { name: "Fecha_Modifica", nullable: true })
  fechaModifica: Date | null;

  @Column("bit", { name: "Pendiente", nullable: true })
  pendiente: boolean | null;

  @Column("varchar", { name: "NumSAP", nullable: true, length: 20 })
  numSap: string | null;

  @Column("char", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  tCambio: number | null;

  @Column("smalldatetime", { name: "Fecha_Vencimiento", nullable: true })
  fechaVencimiento: Date | null;

  @OneToMany(
    () => OrdenPedidoDetalle,
    (ordenPedidoDetalle) => ordenPedidoDetalle.ordenPedido
  )
  ordenPedidoDetalles: OrdenPedidoDetalle[];
}
