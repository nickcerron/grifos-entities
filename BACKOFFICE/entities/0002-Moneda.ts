import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BankCurrentAccount } from '../../bank-current-account/entities/bank-current-account.entity';

@Entity()
export class Currency {
  @ApiProperty({ uniqueItems: true })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ uniqueItems: true, maxLength: 75, title: 'Nombre' })
  @Column('varchar', { unique: true, length: 75 })
  name: string;

  @ApiProperty({ uniqueItems: true, maxLength: 3, title: 'Símbolo' })
  @Column('varchar', { unique: true, length: 3 })
  symbol: string;

  @ApiProperty({ default: true, title: 'Estado' })
  @Column('boolean', { default: true })
  isActive: boolean;

  @OneToMany(
    () => BankCurrentAccount,
    (bankCurrentAccount) => bankCurrentAccount.currency,
  )
  bankCurrentAccounts: BankCurrentAccount[];
}
