import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaDepVend",
  ["cdlocal", "nrodeposito", "fecproceso", "turno", "cdvendedor", "cdtppago"],
  { unique: true }
)
@Entity("DiaDepVend", { schema: "dbo" })
export class DiaDepVend {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", { primary: true, name: "cdtppago", length: 5 })
  cdtppago: string;

  @Column("numeric", {
    primary: true,
    name: "nrodeposito",
    precision: 10,
    scale: 0,
  })
  nrodeposito: number;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolar",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolar: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("bit", { name: "glp", nullable: true })
  glp: boolean | null;

  @Column("numeric", {
    name: "nrosobres",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nrosobres: number | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;
}
