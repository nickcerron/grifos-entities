import { Column, Entity } from "typeorm";

@Entity("TmpRepIngresoCWebS", { schema: "dbo" })
export class TmpRepIngresoCWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "IngresoId", length: 5 })
  ingresoId: string;

  @Column("varchar", { name: "NroIngreso", length: 15 })
  nroIngreso: string;

  @Column("varchar", { name: "TipoDocId", length: 5 })
  tipoDocId: string;

  @Column("varchar", { name: "NroDocumento", length: 15 })
  nroDocumento: string;

  @Column("varchar", { name: "ProveedorId", length: 15 })
  proveedorId: string;

  @Column("int", { name: "AlmacenId" })
  almacenId: number;

  @Column("int", { name: "AlmacenId_Origen", nullable: true })
  almacenIdOrigen: number | null;

  @Column("varchar", { name: "Moneda", nullable: true, length: 1 })
  moneda: string | null;

  @Column("numeric", {
    name: "MtoSubtotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoSubtotal: number | null;

  @Column("numeric", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoImpuesto: number | null;

  @Column("numeric", {
    name: "MtoImpuesto1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoImpuesto1: number | null;

  @Column("numeric", {
    name: "MtoTotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoTotal: number | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  tCambio: number | null;

  @Column("smalldatetime", { name: "FecIngreso", nullable: true })
  fecIngreso: Date | null;

  @Column("smalldatetime", { name: "FecProceso", nullable: true })
  fecProceso: Date | null;

  @Column("smalldatetime", { name: "FecVencimiento", nullable: true })
  fecVencimiento: Date | null;

  @Column("datetime", { name: "FecSistema", nullable: true })
  fecSistema: Date | null;

  @Column("varchar", { name: "Observacion", nullable: true, length: 80 })
  observacion: string | null;

  @Column("varchar", { name: "Scop", nullable: true, length: 15 })
  scop: string | null;

  @Column("varchar", { name: "NroPedido", nullable: true, length: 15 })
  nroPedido: string | null;

  @Column("smalldatetime", { name: "FecAnulaSis", nullable: true })
  fecAnulaSis: Date | null;

  @Column("varchar", { name: "UserIdAnula", nullable: true, length: 20 })
  userIdAnula: string | null;

  @Column("numeric", {
    name: "mtopercepcion",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtopercepcion: number | null;

  @Column("numeric", { name: "FISE", nullable: true, precision: 14, scale: 4 })
  fise: number | null;

  @Column("varchar", { name: "NumSAP", nullable: true, length: 20 })
  numSap: string | null;

  @Column("numeric", {
    name: "Redondeo",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  redondeo: number | null;

  @Column("bit", { name: "IsSAP", nullable: true })
  isSap: boolean | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;
}
