import { Column, Entity } from "typeorm";

@Entity("vc01", { schema: "dbo" })
export class Vc01 {
  @Column("numeric", {
    name: "totalc",
    nullable: true,
    precision: 38,
    scale: 4,
  })
  totalc: number | null;

  @Column("char", { name: "nroc", length: 10 })
  nroc: string;

  @Column("char", { name: "tipoc", length: 5 })
  tipoc: string;
}
