import { Column, Entity } from "typeorm";

@Entity("Turnos", { schema: "dbo" })
export class Turnos {
  @Column("numeric", {
    name: "codturno",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  codturno: number | null;

  @Column("nchar", { name: "desturno", nullable: true, length: 20 })
  desturno: string | null;

  @Column("time", { name: "desde", nullable: true })
  desde: Date | null;

  @Column("time", { name: "hasta", nullable: true })
  hasta: Date | null;
}
