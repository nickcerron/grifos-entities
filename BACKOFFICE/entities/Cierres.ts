import { Column, Entity } from "typeorm";

@Entity("cierres", { schema: "dbo" })
export class Cierres {
  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;
}
