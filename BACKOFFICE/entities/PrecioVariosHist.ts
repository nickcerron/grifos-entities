import { Column, Entity } from "typeorm";

@Entity("precio_varios_hist", { schema: "dbo" })
export class PrecioVariosHist {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { name: "tiporango", length: 3 })
  tiporango: string;

  @Column("numeric", {
    name: "rango1",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  rango1: number | null;

  @Column("numeric", {
    name: "rango2",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  rango2: number | null;

  @Column("smalldatetime", { name: "fechamodificacion" })
  fechamodificacion: Date;

  @Column("char", { name: "tipo", nullable: true, length: 3 })
  tipo: string | null;

  @Column("numeric", { name: "valor", nullable: true, precision: 10, scale: 3 })
  valor: number | null;

  @Column("varchar", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;
}
