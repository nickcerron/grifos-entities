import { Column, Entity } from "typeorm";

@Entity("TmpRepInsumoWebS", { schema: "dbo" })
export class TmpRepInsumoWebS {
  @Column("int", { name: "EMPRESA" })
  empresa: number;

  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("varchar", { name: "TIPOD", length: 5 })
  tipod: string;

  @Column("char", { name: "NRODOCUMENTO", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("char", { name: "MOVIMIENTO", nullable: true, length: 1 })
  movimiento: string | null;

  @Column("int", { name: "NROITEM" })
  nroitem: number;

  @Column("varchar", { name: "ARTICULOID", length: 20 })
  articuloid: string;

  @Column("float", { name: "CANTIDAD", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("bit", { name: "ANULADO", nullable: true })
  anulado: boolean | null;

  @Column("datetime", { name: "FECSISTEMA", nullable: true })
  fecsistema: Date | null;

  @Column("float", { name: "PRECIO", nullable: true, precision: 53 })
  precio: number | null;
}
