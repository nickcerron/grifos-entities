import { Column, Entity, Index } from "typeorm";

@Index("PK_terminal", ["nroseriemaq"], { unique: true })
@Entity("terminal", { schema: "dbo" })
export class Terminal {
  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "seriehd", nullable: true, length: 10 })
  seriehd: string | null;

  @Column("char", { primary: true, name: "nroseriemaq", length: 15 })
  nroseriemaq: string;

  @Column("char", { name: "nroserie1", nullable: true, length: 15 })
  nroserie1: string | null;

  @Column("char", { name: "nroserie2", nullable: true, length: 10 })
  nroserie2: string | null;

  @Column("numeric", {
    name: "nrozeta",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nrozeta: number | null;

  @Column("numeric", {
    name: "mtozeta",
    nullable: true,
    precision: 16,
    scale: 2,
  })
  mtozeta: number | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdprecio", nullable: true, length: 5 })
  cdprecio: string | null;

  @Column("char", { name: "factura", nullable: true, length: 10 })
  factura: string | null;

  @Column("varchar", { name: "facturaimpre", nullable: true, length: 40 })
  facturaimpre: string | null;

  @Column("char", { name: "facturafmt", nullable: true, length: 20 })
  facturafmt: string | null;

  @Column("char", { name: "boleta", nullable: true, length: 10 })
  boleta: string | null;

  @Column("varchar", { name: "boletaimpre", nullable: true, length: 40 })
  boletaimpre: string | null;

  @Column("char", { name: "boletafmt", nullable: true, length: 20 })
  boletafmt: string | null;

  @Column("varchar", { name: "gavetachr", nullable: true, length: 50 })
  gavetachr: string | null;

  @Column("char", { name: "gavetaimpre", nullable: true, length: 10 })
  gavetaimpre: string | null;

  @Column("char", { name: "ticket", nullable: true, length: 10 })
  ticket: string | null;

  @Column("char", { name: "ticketimpre", nullable: true, length: 10 })
  ticketimpre: string | null;

  @Column("numeric", {
    name: "ticketfeed",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  ticketfeed: number | null;

  @Column("char", { name: "ticketchrini", nullable: true, length: 30 })
  ticketchrini: string | null;

  @Column("char", { name: "ticketchrfin", nullable: true, length: 30 })
  ticketchrfin: string | null;

  @Column("numeric", {
    name: "ticketlineacorte",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  ticketlineacorte: number | null;

  @Column("numeric", {
    name: "ticket2columnas",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  ticket2columnas: number | null;

  @Column("char", { name: "nventa", nullable: true, length: 10 })
  nventa: string | null;

  @Column("char", { name: "nventaimpre", nullable: true, length: 10 })
  nventaimpre: string | null;

  @Column("numeric", {
    name: "nventafeed",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  nventafeed: number | null;

  @Column("char", { name: "nventachrini", nullable: true, length: 30 })
  nventachrini: string | null;

  @Column("char", { name: "nventachrfin", nullable: true, length: 30 })
  nventachrfin: string | null;

  @Column("char", { name: "promocion", nullable: true, length: 10 })
  promocion: string | null;

  @Column("varchar", { name: "promocionimpre", nullable: true, length: 40 })
  promocionimpre: string | null;

  @Column("char", { name: "promocionfmt", nullable: true, length: 20 })
  promocionfmt: string | null;

  @Column("numeric", {
    name: "promocionfeed",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  promocionfeed: number | null;

  @Column("char", { name: "promocionchrini", nullable: true, length: 30 })
  promocionchrini: string | null;

  @Column("char", { name: "promocionchrfin", nullable: true, length: 30 })
  promocionchrfin: string | null;

  @Column("char", { name: "ncredito", nullable: true, length: 10 })
  ncredito: string | null;

  @Column("varchar", { name: "ncreditoimpre", nullable: true, length: 40 })
  ncreditoimpre: string | null;

  @Column("char", { name: "ncreditofmt", nullable: true, length: 20 })
  ncreditofmt: string | null;

  @Column("char", { name: "ndebito", nullable: true, length: 10 })
  ndebito: string | null;

  @Column("varchar", { name: "ndebitoimpre", nullable: true, length: 40 })
  ndebitoimpre: string | null;

  @Column("char", { name: "ndebitofmt", nullable: true, length: 20 })
  ndebitofmt: string | null;

  @Column("char", { name: "guia", nullable: true, length: 10 })
  guia: string | null;

  @Column("varchar", { name: "guiaimpre", nullable: true, length: 40 })
  guiaimpre: string | null;

  @Column("char", { name: "guiafmt", nullable: true, length: 20 })
  guiafmt: string | null;

  @Column("char", { name: "proforma", nullable: true, length: 10 })
  proforma: string | null;

  @Column("varchar", { name: "proformaimpre", nullable: true, length: 40 })
  proformaimpre: string | null;

  @Column("char", { name: "proformafmt", nullable: true, length: 20 })
  proformafmt: string | null;

  @Column("char", { name: "letra", nullable: true, length: 10 })
  letra: string | null;

  @Column("varchar", { name: "letraimpre", nullable: true, length: 40 })
  letraimpre: string | null;

  @Column("char", { name: "letrafmt", nullable: true, length: 20 })
  letrafmt: string | null;

  @Column("bit", { name: "tktfactura", nullable: true })
  tktfactura: boolean | null;

  @Column("bit", { name: "tktboleta", nullable: true })
  tktboleta: boolean | null;

  @Column("bit", { name: "tktpromocion", nullable: true })
  tktpromocion: boolean | null;

  @Column("bit", { name: "facturapreimpre", nullable: true })
  facturapreimpre: boolean | null;

  @Column("bit", { name: "boletapreimpre", nullable: true })
  boletapreimpre: boolean | null;

  @Column("bit", { name: "promocionpreimpre", nullable: true })
  promocionpreimpre: boolean | null;

  @Column("bit", { name: "modfecha", nullable: true })
  modfecha: boolean | null;

  @Column("bit", { name: "modmoneda", nullable: true })
  modmoneda: boolean | null;

  @Column("bit", { name: "modvendedor", nullable: true })
  modvendedor: boolean | null;

  @Column("bit", { name: "modalmacen", nullable: true })
  modalmacen: boolean | null;

  @Column("bit", { name: "modlistap", nullable: true })
  modlistap: boolean | null;

  @Column("bit", { name: "modprecio", nullable: true })
  modprecio: boolean | null;

  @Column("bit", { name: "modocompra", nullable: true })
  modocompra: boolean | null;

  @Column("bit", { name: "modservicio", nullable: true })
  modservicio: boolean | null;

  @Column("bit", { name: "modobservacion", nullable: true })
  modobservacion: boolean | null;

  @Column("bit", { name: "moddsctogral", nullable: true })
  moddsctogral: boolean | null;

  @Column("bit", { name: "moddsctoitem", nullable: true })
  moddsctoitem: boolean | null;

  @Column("numeric", {
    name: "mtodsctomax",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  mtodsctomax: number | null;

  @Column("bit", { name: "preciocero", nullable: true })
  preciocero: boolean | null;

  @Column("bit", { name: "flghotkey", nullable: true })
  flghotkey: boolean | null;

  @Column("bit", { name: "flgfacturacion", nullable: true })
  flgfacturacion: boolean | null;

  @Column("bit", { name: "grabarcliente", nullable: true })
  grabarcliente: boolean | null;

  @Column("bit", { name: "flgautomatica", nullable: true })
  flgautomatica: boolean | null;

  @Column("varchar", { name: "path_loteria", nullable: true, length: 70 })
  pathLoteria: string | null;

  @Column("bit", { name: "flgaperturacaja", nullable: true })
  flgaperturacaja: boolean | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "flgpagoefectivo", nullable: true })
  flgpagoefectivo: boolean | null;

  @Column("bit", { name: "flgpagotarjeta", nullable: true })
  flgpagotarjeta: boolean | null;

  @Column("bit", { name: "flgpagocheque", nullable: true })
  flgpagocheque: boolean | null;

  @Column("bit", { name: "flgpagocredito", nullable: true })
  flgpagocredito: boolean | null;

  @Column("bit", { name: "flgpagoncredito", nullable: true })
  flgpagoncredito: boolean | null;

  @Column("bit", { name: "flgvalidaz", nullable: true })
  flgvalidaz: boolean | null;

  @Column("bit", { name: "flgcierrezok", nullable: true })
  flgcierrezok: boolean | null;

  @Column("char", { name: "displayimpre", nullable: true, length: 40 })
  displayimpre: string | null;

  @Column("char", { name: "tipoterminal", nullable: true, length: 1 })
  tipoterminal: string | null;

  @Column("bit", { name: "rucinvalido", nullable: true })
  rucinvalido: boolean | null;

  @Column("numeric", {
    name: "tranvirtual",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  tranvirtual: number | null;

  @Column("bit", { name: "virtual", nullable: true })
  virtual: boolean | null;

  @Column("bit", { name: "tktnotadespacho", nullable: true })
  tktnotadespacho: boolean | null;

  @Column("bit", { name: "flgtransferencia", nullable: true })
  flgtransferencia: boolean | null;

  @Column("bit", { name: "CONEXION_DISPENSADOR", nullable: true })
  conexionDispensador: boolean | null;

  @Column("bit", { name: "PLAYA_FORMASDEPAGO", nullable: true })
  playaFormasdepago: boolean | null;

  @Column("bit", { name: "modif_corr", nullable: true })
  modifCorr: boolean | null;

  @Column("char", { name: "Ticketfactura", nullable: true, length: 10 })
  ticketfactura: string | null;

  @Column("bit", { name: "activa_boton_playa", nullable: true })
  activaBotonPlaya: boolean | null;

  @Column("int", { name: "fe_idpos", nullable: true })
  feIdpos: number | null;

  @Column("numeric", {
    name: "nrodeposito",
    nullable: true,
    precision: 12,
    scale: 0,
  })
  nrodeposito: number | null;

  @Column("bit", { name: "flg_pdf417", nullable: true })
  flgPdf417: boolean | null;

  @Column("char", { name: "ncreditoboleta", nullable: true, length: 10 })
  ncreditoboleta: string | null;

  @Column("bit", { name: "flg_nc_correlativo", nullable: true })
  flgNcCorrelativo: boolean | null;

  @Column("char", { name: "ndebitoboleta", nullable: true, length: 10 })
  ndebitoboleta: string | null;

  @Column("bit", { name: "flg_nd_correlativo", nullable: true })
  flgNdCorrelativo: boolean | null;

  @Column("varchar", { name: "fe_nompos", nullable: true, length: 20 })
  feNompos: string | null;

  @Column("bit", { name: "flg_print_qr", nullable: true })
  flgPrintQr: boolean | null;

  @Column("bit", { name: "flg_formato_A4", nullable: true })
  flgFormatoA4: boolean | null;

  @Column("char", { name: "dinterno", nullable: true, length: 10 })
  dinterno: string | null;

  @Column("varchar", { name: "dinternoimpre", nullable: true, length: 40 })
  dinternoimpre: string | null;

  @Column("char", { name: "dinternofmt", nullable: true, length: 20 })
  dinternofmt: string | null;

  @Column("bit", { name: "tktdinterno", nullable: true })
  tktdinterno: boolean | null;

  @Column("bit", { name: "dinternopreimpre", nullable: true })
  dinternopreimpre: boolean | null;

  @Column("char", { name: "dinternochrini", nullable: true, length: 30 })
  dinternochrini: string | null;

  @Column("char", { name: "dinternochrfin", nullable: true, length: 30 })
  dinternochrfin: string | null;

  @Column("numeric", {
    name: "dinternofeed",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  dinternofeed: number | null;

  @Column("char", { name: "FacturaContingencia", nullable: true, length: 10 })
  facturaContingencia: string | null;

  @Column("char", { name: "BoletaContingencia", nullable: true, length: 10 })
  boletaContingencia: string | null;

  @Column("bit", { name: "flg_pos_visa", nullable: true })
  flgPosVisa: boolean | null;

  @Column("varchar", {
    name: "Ruta_Print_Service",
    nullable: true,
    length: 120,
  })
  rutaPrintService: string | null;

  @Column("bit", { name: "flg_Print_Service", nullable: true })
  flgPrintService: boolean | null;

  @Column("bit", { name: "flg_pos_izi", nullable: true })
  flgPosIzi: boolean | null;

  @Column("char", { name: "Lado_Autoriza", nullable: true, length: 2 })
  ladoAutoriza: string | null;

  @Column("varchar", { name: "serviceip", nullable: true, length: 250 })
  serviceip: string | null;

  @Column("bit", { name: "active", nullable: true })
  active: boolean | null;

  @Column("varchar", { name: "boleta_selva", nullable: true, length: 10 })
  boletaSelva: string | null;

  @Column("varchar", { name: "factura_selva", nullable: true, length: 10 })
  facturaSelva: string | null;

  @Column("varchar", { name: "bal_puerto", nullable: true, length: 10 })
  balPuerto: string | null;

  @Column("char", { name: "bal_bitsxsegundo", nullable: true, length: 6 })
  balBitsxsegundo: string | null;

  @Column("char", { name: "bal_bitsdatos", nullable: true, length: 1 })
  balBitsdatos: string | null;

  @Column("varchar", { name: "bal_paridad", nullable: true, length: 20 })
  balParidad: string | null;

  @Column("varchar", { name: "bal_bitsparada", nullable: true, length: 5 })
  balBitsparada: string | null;

  @Column("bit", { name: "bal_pesomanual", nullable: true })
  balPesomanual: boolean | null;

  @Column("bit", { name: "flg_autoriza_imp_tck", nullable: true })
  flgAutorizaImpTck: boolean | null;
}
