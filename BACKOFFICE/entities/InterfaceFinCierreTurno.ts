import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Interface_fin_cierre_turno",
  ["cPuntoVenta", "fechaTurno", "codLado", "codManguera", "codArticulo"],
  { unique: true }
)
@Entity("Interface_fin_cierre_turno", { schema: "dbo" })
export class InterfaceFinCierreTurno {
  @Column("varchar", { primary: true, name: "c_punto_venta", length: 10 })
  cPuntoVenta: string;

  @Column("smalldatetime", { name: "fecha_cierre_dia", nullable: true })
  fechaCierreDia: Date | null;

  @Column("smalldatetime", { primary: true, name: "FechaTurno" })
  fechaTurno: Date;

  @Column("int", { name: "c_turno", nullable: true })
  cTurno: number | null;

  @Column("int", { name: "linea", nullable: true })
  linea: number | null;

  @Column("int", { name: "n_turno", nullable: true })
  nTurno: number | null;

  @Column("int", { primary: true, name: "CodLado" })
  codLado: number;

  @Column("int", { primary: true, name: "CodManguera" })
  codManguera: number;

  @Column("varchar", { primary: true, name: "CodArticulo", length: 20 })
  codArticulo: string;

  @Column("numeric", {
    name: "LecturaFinal",
    nullable: true,
    precision: 19,
    scale: 3,
    default: () => "(0)",
  })
  lecturaFinal: number | null;

  @Column("float", {
    name: "PrecioVenta",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  precioVenta: number | null;

  @Column("numeric", {
    name: "galones",
    nullable: true,
    precision: 13,
    scale: 3,
    default: () => "(0)",
  })
  galones: number | null;

  @Column("numeric", {
    name: "monto",
    nullable: true,
    precision: 13,
    scale: 3,
    default: () => "(0)",
  })
  monto: number | null;

  @Column("char", {
    name: "flag_automatico",
    nullable: true,
    length: 1,
    default: () => "'&'",
  })
  flagAutomatico: string | null;

  @Column("smalldatetime", {
    name: "fecha_registro",
    nullable: true,
    default: () => "getdate()",
  })
  fechaRegistro: Date | null;
}
