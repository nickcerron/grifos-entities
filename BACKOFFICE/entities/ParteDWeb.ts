import { Column, Entity, Index } from "typeorm";

@Index("PK_ParteDWeb", ["cdlocal", "fecha", "cdarticulo"], { unique: true })
@Entity("ParteDWeb", { schema: "dbo" })
export class ParteDWeb {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("datetime", { primary: true, name: "Fecha" })
  fecha: Date;

  @Column("char", { name: "Grupo", nullable: true, length: 1 })
  grupo: string | null;

  @Column("varchar", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "dsarticulo", nullable: true, length: 40 })
  dsarticulo: string | null;

  @Column("numeric", {
    name: "inicial",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  inicial: number | null;

  @Column("numeric", {
    name: "compra",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  compra: number | null;

  @Column("numeric", {
    name: "Consumo",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumo: number | null;

  @Column("numeric", { name: "Venta", nullable: true, precision: 12, scale: 3 })
  venta: number | null;

  @Column("numeric", { name: "Final", nullable: true, precision: 12, scale: 3 })
  final: number | null;

  @Column("numeric", {
    name: "varillaje",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  varillaje: number | null;

  @Column("numeric", {
    name: "difdia",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difdia: number | null;

  @Column("numeric", {
    name: "difmes",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difmes: number | null;

  @Column("numeric", {
    name: "Variacion",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  variacion: number | null;

  @Column("numeric", { name: "Total", nullable: true, precision: 12, scale: 2 })
  total: number | null;
}
