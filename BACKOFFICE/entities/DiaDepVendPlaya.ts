import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index(
  "PK_DiaDepVendPlaya",
  ["cdlocal", "fecproceso", "turno", "cdvendedor", "cdtppago", "nrodeposito"],
  { unique: true }
)
@Entity("DiaDepVendPlaya", { schema: "dbo" })
export class DiaDepVendPlaya {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", { primary: true, name: "cdtppago", length: 5 })
  cdtppago: string;

  @PrimaryGeneratedColumn({
    type: "numeric",
    name: "nrodeposito",
    precision: 14,
    scale: 0,
  })
  nrodeposito: number;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolar",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolar: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("datetime", {
    name: "fecsistema",
    nullable: true,
    default: () => "getdate()",
  })
  fecsistema: Date | null;

  @Column("bit", { name: "anexado", nullable: true, default: () => "(0)" })
  anexado: boolean | null;
}
