import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepDiaDepVendWebS", { schema: "dbo" })
export class TmpRepDiaDepVendWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "fecproceso" })
  fecproceso: Date;

  @Column("int", { name: "turno" })
  turno: number;

  @Column("varchar", { name: "VendedorId", length: 20 })
  vendedorId: string;

  @Column("varchar", { name: "TipoPagoId", length: 5 })
  tipoPagoId: string;

  @Column("bigint", { name: "nrodeposito" })
  nrodeposito: string;

  @Column("int", { name: "TipoRegistro" })
  tipoRegistro: number;

  @Column("decimal", {
    name: "mtosoles",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("decimal", {
    name: "mtodolar",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtodolar: number | null;

  @Column("decimal", {
    name: "tcambio",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  tcambio: number | null;

  @Column("varchar", { name: "TarjetaCreditoId", nullable: true, length: 2 })
  tarjetaCreditoId: string | null;

  @Column("varchar", { name: "NroTarjetaCredito", nullable: true, length: 20 })
  nroTarjetaCredito: string | null;

  @Column("bit", { name: "glp", nullable: true })
  glp: boolean | null;

  @Column("int", { name: "nrosobres", nullable: true })
  nrosobres: number | null;

  @Column("varchar", { name: "nrodocumento", length: 15 })
  nrodocumento: string;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;

  @Column("int", { name: "cantmonedas", nullable: true })
  cantmonedas: number | null;

  @Column("int", { name: "cantbilletes", nullable: true })
  cantbilletes: number | null;

  @Column("decimal", {
    name: "mtomonedas",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtomonedas: number | null;

  @Column("decimal", {
    name: "mtobilletessol",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtobilletessol: number | null;

  @Column("decimal", {
    name: "mtobilletesdol",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtobilletesdol: number | null;
}
