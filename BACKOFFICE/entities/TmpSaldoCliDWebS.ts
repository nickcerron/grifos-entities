import { Column, Entity } from "typeorm";

@Entity("TmpSaldoCliDWebS", { schema: "dbo" })
export class TmpSaldoCliDWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("varchar", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("varchar", { name: "nrotarjeta", length: 20 })
  nrotarjeta: string;

  @Column("char", { name: "TipoDespacho", nullable: true, length: 1 })
  tipoDespacho: string | null;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("float", { name: "limitemto", nullable: true, precision: 53 })
  limitemto: number | null;

  @Column("float", { name: "consumto", nullable: true, precision: 53 })
  consumto: number | null;

  @Column("varchar", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("smalldatetime", { name: "FechaAtencion", nullable: true })
  fechaAtencion: Date | null;

  @Column("bit", { name: "IsIngresado", nullable: true, default: () => "(0)" })
  isIngresado: boolean | null;
}
