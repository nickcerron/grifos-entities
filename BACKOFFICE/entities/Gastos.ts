import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Gastos_1",
  ["cdLocal", "cdTipoDoc", "numeroDocumento", "cdProveedor"],
  { unique: true }
)
@Entity("Gastos", { schema: "dbo" })
export class Gastos {
  @Column("char", { primary: true, name: "CdLocal", length: 3 })
  cdLocal: string;

  @Column("char", { primary: true, name: "CdTipoDoc", length: 5 })
  cdTipoDoc: string;

  @Column("varchar", { primary: true, name: "Numero_Documento", length: 15 })
  numeroDocumento: string;

  @Column("char", { primary: true, name: "CdProveedor", length: 20 })
  cdProveedor: string;

  @Column("smalldatetime", { name: "Fecha_Documento" })
  fechaDocumento: Date;

  @Column("varchar", { name: "RsProveedor", nullable: true, length: 150 })
  rsProveedor: string | null;

  @Column("numeric", {
    name: "Impuesto",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "MtoSubTotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoSubTotal: number | null;

  @Column("numeric", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoImpuesto: number | null;

  @Column("numeric", {
    name: "MtoTotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoTotal: number | null;

  @Column("varchar", { name: "Observaciones", nullable: true, length: 254 })
  observaciones: string | null;

  @Column("char", { name: "Tipo_Gasto", nullable: true, length: 5 })
  tipoGasto: string | null;

  @Column("char", { name: "Numero_Gasto", nullable: true, length: 10 })
  numeroGasto: string | null;

  @Column("bit", { name: "cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("datetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("varchar", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;
}
