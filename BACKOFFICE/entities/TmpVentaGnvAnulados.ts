import { Column, Entity } from "typeorm";

@Entity("Tmp_VentaGNV_Anulados", { schema: "dbo" })
export class TmpVentaGnvAnulados {
  @Column("smalldatetime", { name: "FECHA_TURNO", nullable: true })
  fechaTurno: Date | null;

  @Column("varchar", { name: "TIPO_COMPROBANTE", nullable: true, length: 10 })
  tipoComprobante: string | null;

  @Column("varchar", { name: "SERIE", nullable: true, length: 10 })
  serie: string | null;

  @Column("varchar", { name: "PRODUCTO", nullable: true, length: 50 })
  producto: string | null;

  @Column("varchar", {
    name: "COMPROBANTE_INICIAL",
    nullable: true,
    length: 20,
  })
  comprobanteInicial: string | null;

  @Column("varchar", { name: "COMPROBANTE_FINAL", nullable: true, length: 20 })
  comprobanteFinal: string | null;

  @Column("varchar", { name: "NRO_SERI_MAQ", nullable: true, length: 20 })
  nroSeriMaq: string | null;

  @Column("varchar", { name: "RUC", nullable: true, length: 11 })
  ruc: string | null;

  @Column("varchar", { name: "RAZON_SOCIAL", nullable: true, length: 120 })
  razonSocial: string | null;

  @Column("numeric", {
    name: "OPERACION_INAFECTA",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  operacionInafecta: number | null;

  @Column("numeric", {
    name: "BASE_IMPONIBLE",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  baseImponible: number | null;

  @Column("numeric", { name: "IGV", nullable: true, precision: 12, scale: 2 })
  igv: number | null;

  @Column("numeric", {
    name: "TOTAL_VENTA",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  totalVenta: number | null;

  @Column("numeric", {
    name: "RECAUDO",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  recaudo: number | null;

  @Column("numeric", {
    name: "CANTIDAD_M3",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  cantidadM3: number | null;

  @Column("numeric", { name: "valor", nullable: true, precision: 12, scale: 2 })
  valor: number | null;

  @Column("numeric", {
    name: "redondeo",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  redondeo: number | null;

  @Column("numeric", {
    name: "donacion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  donacion: number | null;
}
