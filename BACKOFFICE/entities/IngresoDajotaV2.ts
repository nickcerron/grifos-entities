import { Column, Entity } from "typeorm";

@Entity("ingreso_dajota_v2", { schema: "dbo" })
export class IngresoDajotaV2 {
  @Column("varchar", { name: "cdarticulo", nullable: true, length: 60 })
  cdarticulo: string | null;

  @Column("numeric", { name: "stock", nullable: true, precision: 12, scale: 4 })
  stock: number | null;
}
