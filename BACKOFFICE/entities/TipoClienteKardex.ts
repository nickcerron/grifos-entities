import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoClienteKardex", ["cdTpClienteKardex"], { unique: true })
@Entity("TipoClienteKardex", { schema: "dbo" })
export class TipoClienteKardex {
  @Column("char", { primary: true, name: "cdTpClienteKardex", length: 5 })
  cdTpClienteKardex: string;

  @Column("char", { name: "dsClienteKardex", length: 50 })
  dsClienteKardex: string;

  @Column("varchar", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;

  @Column("varchar", { name: "cdproveedor", nullable: true, length: 20 })
  cdproveedor: string | null;
}
