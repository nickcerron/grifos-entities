import { Column, Entity } from "typeorm";

@Entity("AnomaliasDetalle", { schema: "dbo" })
export class AnomaliasDetalle {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "nroanomalia", nullable: true, length: 12 })
  nroanomalia: string | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtofaltante",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltante: number | null;

  @Column("numeric", {
    name: "mtosobrante",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobrante: number | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("int", { name: "turno", nullable: true })
  turno: number | null;

  @Column("varchar", { name: "dsfechaturno", nullable: true, length: 20 })
  dsfechaturno: string | null;
}
