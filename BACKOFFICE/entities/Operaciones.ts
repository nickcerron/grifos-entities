import { Column, Entity, Index } from "typeorm";

@Index("PK_operaciones", ["ctopcodigo"], { unique: true })
@Entity("operaciones", { schema: "dbo" })
export class Operaciones {
  @Column("char", { primary: true, name: "ctopcodigo", length: 11 })
  ctopcodigo: string;

  @Column("varchar", { name: "ctopdescri", length: 38 })
  ctopdescri: string;

  @Column("char", { name: "ctopfiltro", length: 1 })
  ctopfiltro: string;

  @Column("tinyint", { name: "ltopautoma" })
  ltopautoma: number;

  @Column("tinyint", { name: "itopactivo" })
  itopactivo: number;
}
