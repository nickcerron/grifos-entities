import { Column, Entity } from "typeorm";

@Entity("TmpDepositoBancoWebS", { schema: "dbo" })
export class TmpDepositoBancoWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("varchar", { name: "NroCuenta", length: 20 })
  nroCuenta: string;

  @Column("varchar", { name: "NroOperacion", length: 20 })
  nroOperacion: string;

  @Column("smalldatetime", { name: "FecDeposito", nullable: true })
  fecDeposito: Date | null;

  @Column("char", { name: "GrupoId01", length: 5 })
  grupoId01: string;

  @Column("float", { name: "MtoTotal", nullable: true, precision: 53 })
  mtoTotal: number | null;

  @Column("float", { name: "TCambio", nullable: true, precision: 53 })
  tCambio: number | null;

  @Column("float", { name: "Valores", nullable: true, precision: 53 })
  valores: number | null;

  @Column("float", { name: "Cheques", nullable: true, precision: 53 })
  cheques: number | null;

  @Column("varchar", { name: "Observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("smalldatetime", { name: "FecConciliacion", nullable: true })
  fecConciliacion: Date | null;

  @Column("bit", { name: "Conciliado", nullable: true })
  conciliado: boolean | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;

  @Column("bit", { name: "IsIngresado", nullable: true })
  isIngresado: boolean | null;
}
