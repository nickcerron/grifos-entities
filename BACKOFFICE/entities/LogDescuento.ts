import { Column, Entity } from "typeorm";

@Entity("LOG_Descuento", { schema: "dbo" })
export class LogDescuento {
  @Column("char", { name: "cdarticulo", nullable: true, length: 10 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nroitem: number | null;

  @Column("numeric", {
    name: "cantidad1",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  cantidad1: number | null;

  @Column("numeric", {
    name: "cantidad2",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  cantidad2: number | null;

  @Column("numeric", {
    name: "porcentaje",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  porcentaje: number | null;

  @Column("numeric", {
    name: "descuento",
    nullable: true,
    precision: 8,
    scale: 2,
  })
  descuento: number | null;

  @Column("int", { name: "cdDescuento", nullable: true })
  cdDescuento: number | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
