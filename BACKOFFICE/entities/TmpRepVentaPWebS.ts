import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepVentaPWebS", { schema: "dbo" })
export class TmpRepVentaPWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @Column("int", { name: "EMPRESA" })
  empresa: number;

  @Column("char", { name: "RUCEMISOR", length: 15 })
  rucemisor: string;

  @Column("int", { name: "LOCAL" })
  local: number;

  @Column("varchar", { name: "SERIEMAQ", length: 20 })
  seriemaq: string;

  @Column("varchar", { name: "TIPOD", length: 5 })
  tipod: string;

  @Column("char", { name: "NRODOCUMENTO", length: 10 })
  nrodocumento: string;

  @Column("char", { name: "TIPOPAGOID", length: 5 })
  tipopagoid: string;

  @Column("varchar", { name: "NROCUENTA", length: 20 })
  nrocuenta: string;

  @Column("varchar", { name: "NROCHEQUE", length: 20 })
  nrocheque: string;

  @Column("char", { name: "TARJETACREDITOID", length: 2 })
  tarjetacreditoid: string;

  @Column("varchar", { name: "NROTARJETACREDITO", length: 20 })
  nrotarjetacredito: string;

  @Column("numeric", {
    name: "MTOPAGO",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtopago: number | null;

  @Column("numeric", {
    name: "MTOPAGODOL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtopagodol: number | null;

  @Column("numeric", {
    name: "MTOTOTALDOL",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtototaldol: number | null;

  @Column("char", { name: "BANCOID", nullable: true, length: 4 })
  bancoid: string | null;
}
