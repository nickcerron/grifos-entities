import { Column, Entity } from "typeorm";

@Entity("diaparte_auditoria", { schema: "dbo" })
export class DiaparteAuditoria {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecanterior", nullable: true })
  fecanterior: Date | null;

  @Column("bit", { name: "cerrado", nullable: true })
  cerrado: boolean | null;

  @Column("bit", { name: "pdislavend", nullable: true })
  pdislavend: boolean | null;

  @Column("bit", { name: "pddepvend", nullable: true })
  pddepvend: boolean | null;

  @Column("bit", { name: "pdcontom", nullable: true })
  pdcontom: boolean | null;

  @Column("bit", { name: "pdcobranza", nullable: true })
  pdcobranza: boolean | null;

  @Column("bit", { name: "pdVarillaje", nullable: true })
  pdVarillaje: boolean | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", {
    name: "fecsistema",
    nullable: true,
    default: () => "getdate()",
  })
  fecsistema: Date | null;

  @Column("varchar", { name: "Proceso", nullable: true, length: 15 })
  proceso: string | null;
}
