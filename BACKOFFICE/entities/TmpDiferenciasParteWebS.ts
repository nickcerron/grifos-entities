import { Column, Entity } from "typeorm";

@Entity("TmpDiferenciasParteWebS", { schema: "dbo" })
export class TmpDiferenciasParteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "fecproceso" })
  fecproceso: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("float", { name: "Contometropec", precision: 53 })
  contometropec: number;

  @Column("float", { name: "Contometromanual", precision: 53 })
  contometromanual: number;

  @Column("float", { name: "diferenciacontom", precision: 53 })
  diferenciacontom: number;

  @Column("float", { name: "Depbanco", precision: 53 })
  depbanco: number;

  @Column("float", { name: "Ventas", precision: 53 })
  ventas: number;

  @Column("float", { name: "diferenciaventas", precision: 53 })
  diferenciaventas: number;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
