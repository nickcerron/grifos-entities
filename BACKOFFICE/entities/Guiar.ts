import { Column, Entity } from "typeorm";

@Entity("guiar", { schema: "dbo" })
export class Guiar {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("smalldatetime", { name: "fecguia", nullable: true })
  fecguia: Date | null;
}
