import { Column, Entity } from "typeorm";

@Entity("hvale_anula", { schema: "dbo" })
export class HvaleAnula {
  @Column("char", { name: "nrovale", length: 10 })
  nrovale: string;

  @Column("smalldatetime", { name: "fecvale", nullable: true })
  fecvale: Date | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtovale",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtovale: number | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "nroseriemaq", nullable: true, length: 15 })
  nroseriemaq: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "nroseriemaqfac", nullable: true, length: 15 })
  nroseriemaqfac: string | null;

  @Column("char", { name: "cdtipodocfac", nullable: true, length: 5 })
  cdtipodocfac: string | null;

  @Column("char", { name: "nrodocumentofac", nullable: true, length: 10 })
  nrodocumentofac: string | null;

  @Column("smalldatetime", { name: "fecdocumentofac", nullable: true })
  fecdocumentofac: Date | null;

  @Column("smalldatetime", { name: "fecprocesofac", nullable: true })
  fecprocesofac: Date | null;

  @Column("char", { name: "placa", nullable: true, length: 10 })
  placa: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;

  @Column("char", { name: "docvale", nullable: true, length: 10 })
  docvale: string | null;

  @Column("numeric", {
    name: "ConsumoMtoIni",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumoMtoIni: number | null;

  @Column("numeric", {
    name: "ConsumoMtoFin",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumoMtoFin: number | null;

  @Column("varchar", { name: "nrocontrato", nullable: true, length: 60 })
  nrocontrato: string | null;

  @Column("bit", { name: "licitacion", nullable: true })
  licitacion: boolean | null;

  @Column("char", { name: "area", nullable: true, length: 5 })
  area: string | null;

  @Column("char", { name: "dsarea", nullable: true, length: 60 })
  dsarea: string | null;

  @Column("bit", { name: "EnlaceFactura", nullable: true })
  enlaceFactura: boolean | null;

  @Column("varchar", { name: "NroVoucher", nullable: true, length: 15 })
  nroVoucher: string | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
