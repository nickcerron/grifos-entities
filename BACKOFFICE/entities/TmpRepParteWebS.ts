import { Column, Entity } from "typeorm";

@Entity("TmpRepParteWebS", { schema: "dbo" })
export class TmpRepParteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "Fecha" })
  fecha: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("decimal", {
    name: "Tcontom",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  tcontom: number | null;

  @Column("decimal", {
    name: "Dsctotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  dsctotal: number | null;

  @Column("decimal", {
    name: "CnsInterno",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  cnsInterno: number | null;

  @Column("decimal", {
    name: "Grupo1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo1: number | null;

  @Column("decimal", {
    name: "Grupo2",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo2: number | null;

  @Column("decimal", {
    name: "Grupo3",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo3: number | null;

  @Column("decimal", {
    name: "Grupo4",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo4: number | null;

  @Column("decimal", {
    name: "Grupo5",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo5: number | null;

  @Column("decimal", {
    name: "Grupo6",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo6: number | null;

  @Column("decimal", {
    name: "Grupo7",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  grupo7: number | null;

  @Column("decimal", {
    name: "CnsInternoTienda",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  cnsInternoTienda: number | null;

  @Column("decimal", {
    name: "Serafines",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  serafines: number | null;

  @Column("decimal", {
    name: "Valecontado",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  valecontado: number | null;

  @Column("decimal", {
    name: "Valcredcomb",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  valcredcomb: number | null;

  @Column("decimal", {
    name: "Valcredprod",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  valcredprod: number | null;

  @Column("decimal", {
    name: "Tarjcredito",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  tarjcredito: number | null;

  @Column("decimal", {
    name: "Efectsoles",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  efectsoles: number | null;

  @Column("decimal", {
    name: "Efectdolar",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  efectdolar: number | null;

  @Column("decimal", {
    name: "OtrosIngre",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  otrosIngre: number | null;

  @Column("decimal", {
    name: "Faltantes",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  faltantes: number | null;

  @Column("decimal", {
    name: "Sobrantes",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  sobrantes: number | null;

  @Column("decimal", { name: "Otros", nullable: true, precision: 27, scale: 2 })
  otros: number | null;

  @Column("smalldatetime", { name: "Fecha_cierre", nullable: true })
  fechaCierre: Date | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
