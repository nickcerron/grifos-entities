import { Column, Entity } from "typeorm";

@Entity("TmpDiaContomManualWebS", { schema: "dbo" })
export class TmpDiaContomManualWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("int", { name: "Turno" })
  turno: number;

  @Column("char", { name: "Lado", length: 2 })
  lado: string;

  @Column("char", { name: "Manguera", length: 2 })
  manguera: string;

  @Column("bit", { name: "TipoRegistro", nullable: true })
  tipoRegistro: boolean | null;

  @Column("varchar", { name: "ArticuloId", nullable: true, length: 20 })
  articuloId: string | null;

  @Column("float", { name: "Inicial", nullable: true, precision: 53 })
  inicial: number | null;

  @Column("float", { name: "Final", nullable: true, precision: 53 })
  final: number | null;

  @Column("float", { name: "GalonesContom", nullable: true, precision: 53 })
  galonesContom: number | null;

  @Column("int", { name: "PrecioId" })
  precioId: number;

  @Column("smalldatetime", { name: "FecRegistro_Precio", nullable: true })
  fecRegistroPrecio: Date | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "Total", nullable: true, precision: 53 })
  total: number | null;

  @Column("float", { name: "GalonesPEC", nullable: true, precision: 53 })
  galonesPec: number | null;

  @Column("float", { name: "TotalPEC", nullable: true, precision: 53 })
  totalPec: number | null;

  @Column("varchar", { name: "Observaciones", nullable: true, length: 100 })
  observaciones: string | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
