import { Column, Entity, Index } from "typeorm";

@Index("PK_Resumen", ["cdTipodoc", "nrodocumento"], { unique: true })
@Entity("Resumen", { schema: "dbo" })
export class Resumen {
  @Column("char", { primary: true, name: "cdTipodoc", length: 5 })
  cdTipodoc: string;

  @Column("varchar", { primary: true, name: "Nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("varchar", { name: "ResumenId", nullable: true, length: 17 })
  resumenId: string | null;

  @Column("varchar", { name: "TipoDocumento", nullable: true, length: 2 })
  tipoDocumento: string | null;

  @Column("varchar", { name: "NumeroCorrelativo", nullable: true, length: 13 })
  numeroCorrelativo: string | null;
}
