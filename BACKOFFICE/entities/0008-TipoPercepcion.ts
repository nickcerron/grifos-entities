import { Column, Entity, Index } from 'typeorm';

@Index('PK_TipoPercepcion', ['cdtpPercepcion'], { unique: true })
@Entity('TipoPercepcion', { schema: 'dbo' })
export class TipoPercepcion {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdtpPercepcion', length: 3 })
  cdtpPercepcion: string;

  //name
  //nullable => false
  @Column('varchar', { name: 'dstpPercepcion', length: 100 })
  dstpPercepcion: string;

  //rate
  //precision => 6
  @Column('numeric', { name: 'tasa', nullable: true, precision: 5, scale: 2 })
  tasa: number | null;

  //view
  //default => true
  @Column('bit', { name: 'flg_mostrar', nullable: true })
  flgMostrar: boolean | null;

  //sele añadira una relacion con compañia
  //campo en db: company_id
}
