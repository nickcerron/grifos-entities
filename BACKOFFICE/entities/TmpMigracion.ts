import { Column, Entity } from "typeorm";

@Entity("TmpMigracion", { schema: "dbo" })
export class TmpMigracion {
  @Column("char", { name: "sucursal", nullable: true, length: 3 })
  sucursal: string | null;

  @Column("char", { name: "tipocompro", nullable: true, length: 5 })
  tipocompro: string | null;

  @Column("char", { name: "seriecompr", nullable: true, length: 5 })
  seriecompr: string | null;

  @Column("char", { name: "nrocompr", nullable: true, length: 7 })
  nrocompr: string | null;

  @Column("char", { name: "moneda", nullable: true, length: 5 })
  moneda: string | null;

  @Column("char", { name: "seriedocmod", nullable: true, length: 10 })
  seriedocmod: string | null;

  @Column("char", { name: "nrodocmod", nullable: true, length: 10 })
  nrodocmod: string | null;

  @Column("datetime", { name: "fecemision", nullable: true })
  fecemision: Date | null;

  @Column("datetime", { name: "fecvenc", nullable: true })
  fecvenc: Date | null;

  @Column("char", { name: "tipoide", nullable: true, length: 1 })
  tipoide: string | null;

  @Column("char", { name: "nroide", nullable: true, length: 11 })
  nroide: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 250 })
  rscliente: string | null;

  @Column("varchar", { name: "appaterno", nullable: true, length: 60 })
  appaterno: string | null;

  @Column("varchar", { name: "apmaterno", nullable: true, length: 60 })
  apmaterno: string | null;

  @Column("varchar", { name: "nombre", nullable: true, length: 60 })
  nombre: string | null;

  @Column("varchar", { name: "unidneg", nullable: true, length: 20 })
  unidneg: string | null;

  @Column("varchar", { name: "tippago", nullable: true, length: 60 })
  tippago: string | null;

  @Column("varchar", { name: "glosa", nullable: true, length: 120 })
  glosa: string | null;

  @Column("char", { name: "tipo", nullable: true, length: 3 })
  tipo: string | null;

  @Column("char", { name: "transf", nullable: true, length: 60 })
  transf: string | null;

  @Column("numeric", {
    name: "percepcion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  percepcion: number | null;

  @Column("char", { name: "isc", nullable: true, length: 3 })
  isc: string | null;

  @Column("char", { name: "retiro", nullable: true, length: 3 })
  retiro: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 6,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "porcentaje",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  porcentaje: number | null;

  @Column("numeric", {
    name: "tvalorvta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  tvalorvta: number | null;

  @Column("numeric", {
    name: "tpreciovta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  tpreciovta: number | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;
}
