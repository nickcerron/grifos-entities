import { Column, Entity, Index } from "typeorm";

@Index("pk_Vehiculo", ["cdplaca"], { unique: true })
@Entity("Vehiculo", { schema: "dbo" })
export class Vehiculo {
  @Column("char", { primary: true, name: "cdplaca", length: 10 })
  cdplaca: string;

  @Column("varchar", { name: "nroTarjeta", nullable: true, length: 15 })
  nroTarjeta: string | null;

  @Column("varchar", { name: "NroAutoriza", nullable: true, length: 50 })
  nroAutoriza: string | null;

  @Column("varchar", { name: "CodAutoriza", nullable: true, length: 2 })
  codAutoriza: string | null;
}
