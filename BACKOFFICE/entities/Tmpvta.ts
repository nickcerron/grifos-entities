import { Column, Entity } from "typeorm";

@Entity("tmpvta", { schema: "dbo" })
export class Tmpvta {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "nroseriemaq", nullable: true, length: 15 })
  nroseriemaq: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("datetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("numeric", {
    name: "mtovueltosol",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtovueltosol: number | null;

  @Column("numeric", {
    name: "mtovueltodol",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtovueltodol: number | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("char", { name: "nroproforma", nullable: true, length: 10 })
  nroproforma: string | null;

  @Column("char", { name: "cdprecio", nullable: true, length: 5 })
  cdprecio: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "porservicio",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porservicio: number | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoservicio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoservicio: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtototal: number | null;

  @Column("char", { name: "cdtranspor", nullable: true, length: 20 })
  cdtranspor: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "drpartida", nullable: true, length: 60 })
  drpartida: string | null;

  @Column("char", { name: "drdestino", nullable: true, length: 60 })
  drdestino: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "tipofactura", nullable: true, length: 1 })
  tipofactura: string | null;

  @Column("bit", { name: "flgmanual", nullable: true })
  flgmanual: boolean | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 9,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "nroocompra", nullable: true, length: 15 })
  nroocompra: string | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("char", { name: "observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("bit", { name: "flgmovimiento", nullable: true })
  flgmovimiento: boolean | null;

  @Column("char", { name: "referencia", nullable: true, length: 60 })
  referencia: string | null;

  @Column("char", { name: "nroserie1", nullable: true, length: 15 })
  nroserie1: string | null;

  @Column("char", { name: "nroserie2", nullable: true, length: 10 })
  nroserie2: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "odometro", nullable: true, length: 10 })
  odometro: string | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;
}
