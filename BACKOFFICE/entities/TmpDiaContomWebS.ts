import { Column, Entity } from "typeorm";

@Entity("TmpDiaContomWebS", { schema: "dbo" })
export class TmpDiaContomWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "FecProceso" })
  fecProceso: Date;

  @Column("int", { name: "Turno" })
  turno: number;

  @Column("char", { name: "Lado", length: 2 })
  lado: string;

  @Column("char", { name: "Manguera", length: 2 })
  manguera: string;

  @Column("bit", { name: "TipoRegistro", nullable: true })
  tipoRegistro: boolean | null;

  @Column("varchar", { name: "ArticuloId", nullable: true, length: 20 })
  articuloId: string | null;

  @Column("float", { name: "Inicial", nullable: true, precision: 53 })
  inicial: number | null;

  @Column("float", { name: "Final", nullable: true, precision: 53 })
  final: number | null;

  @Column("float", { name: "GalonesContom", nullable: true, precision: 53 })
  galonesContom: number | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "Total", nullable: true, precision: 53 })
  total: number | null;

  @Column("float", { name: "GalonesPEC", nullable: true, precision: 53 })
  galonesPec: number | null;

  @Column("float", { name: "TotalPEC", nullable: true, precision: 53 })
  totalPec: number | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
