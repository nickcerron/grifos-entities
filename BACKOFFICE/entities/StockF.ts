import { Column, Entity, Index } from "typeorm";

@Index("PK_STOCK_F", ["codigo"], { unique: true })
@Entity("STOCK_F", { schema: "dbo" })
export class StockF {
  @Column("varchar", { primary: true, name: "CODIGO", length: 20 })
  codigo: string;

  @Column("varchar", { name: "DESCRIPCION", nullable: true, length: 100 })
  descripcion: string | null;

  @Column("varchar", { name: "MEDIDA", nullable: true, length: 5 })
  medida: string | null;

  @Column("numeric", { name: "COSTO", nullable: true, precision: 12, scale: 6 })
  costo: number | null;

  @Column("numeric", { name: "STOCK", nullable: true, precision: 12, scale: 2 })
  stock: number | null;

  @Column("numeric", {
    name: "COSTO_TOTAL",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  costoTotal: number | null;
}
