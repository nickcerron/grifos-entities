import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("descuento", { schema: "dbo" })
export class Descuento {
  @Column("char", { name: "cdarticulo", nullable: true, length: 10 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nroitem: number | null;

  @Column("numeric", {
    name: "cantidad1",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  cantidad1: number | null;

  @Column("numeric", {
    name: "cantidad2",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  cantidad2: number | null;

  @Column("numeric", {
    name: "porcentaje",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  porcentaje: number | null;

  @Column("numeric", {
    name: "descuento",
    nullable: true,
    precision: 8,
    scale: 2,
  })
  descuento: number | null;

  @PrimaryGeneratedColumn({ type: "int", name: "cdDescuento" })
  cdDescuento: number;
}
