import { Column, Entity } from "typeorm";

@Entity("TmpRepStockWebS", { schema: "dbo" })
export class TmpRepStockWebS {
  @Column("int", { name: "EmpresaId", nullable: true })
  empresaId: number | null;

  @Column("int", { name: "LocalId", nullable: true })
  localId: number | null;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("varchar", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("smalldatetime", { name: "fecinicial", nullable: true })
  fecinicial: Date | null;

  @Column("numeric", {
    name: "stockinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinicial: number | null;

  @Column("varchar", { name: "monctoinicial", nullable: true, length: 1 })
  monctoinicial: string | null;

  @Column("numeric", {
    name: "ctoinicial",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinicial: number | null;

  @Column("smalldatetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("numeric", {
    name: "stockinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinventario: number | null;

  @Column("varchar", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column("numeric", { name: "stockminimo", precision: 12, scale: 4 })
  stockminimo: number;

  @Column("numeric", {
    name: "stockactual",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;

  @Column("numeric", {
    name: "stockseparado",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockseparado: number | null;

  @Column("numeric", { name: "stockmaximo", precision: 12, scale: 4 })
  stockmaximo: number;

  @Column("varchar", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;

  @Column("varchar", { name: "usuingreso", nullable: true, length: 10 })
  usuingreso: string | null;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("varchar", { name: "ususalida", nullable: true, length: 10 })
  ususalida: string | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("varchar", { name: "usuventa", nullable: true, length: 10 })
  usuventa: string | null;

  @Column("smalldatetime", { name: "fecventa", nullable: true })
  fecventa: Date | null;
}
