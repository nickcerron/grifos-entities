import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_GuiaD202302",
  ["cdlocal", "cdtpGuia", "nroGuia", "rucempresa", "nroitem", "cdarticulo"],
  { unique: true }
)
@Entity("GuiaD202302", { schema: "dbo" })
export class GuiaD202302 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpGuia", length: 5 })
  cdtpGuia: string;

  @Column("char", { primary: true, name: "nroGuia", length: 10 })
  nroGuia: string;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fecGuia", nullable: true })
  fecGuia: Date | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "peso", nullable: true, precision: 18, scale: 5 })
  peso: number | null;

  @Column("numeric", {
    name: "totalpeso",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  totalpeso: number | null;
}
