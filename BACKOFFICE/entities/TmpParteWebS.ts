import { Column, Entity } from "typeorm";

@Entity("TmpParteWebS", { schema: "dbo" })
export class TmpParteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "Fecha" })
  fecha: Date;

  @Column("bit", { name: "TipoRegistro" })
  tipoRegistro: boolean;

  @Column("float", { name: "Tcontom", nullable: true, precision: 53 })
  tcontom: number | null;

  @Column("float", { name: "Dsctotal", nullable: true, precision: 53 })
  dsctotal: number | null;

  @Column("float", { name: "CnsInterno", nullable: true, precision: 53 })
  cnsInterno: number | null;

  @Column("float", { name: "Grupo1", nullable: true, precision: 53 })
  grupo1: number | null;

  @Column("float", { name: "Grupo2", nullable: true, precision: 53 })
  grupo2: number | null;

  @Column("float", { name: "Grupo3", nullable: true, precision: 53 })
  grupo3: number | null;

  @Column("float", { name: "Grupo4", nullable: true, precision: 53 })
  grupo4: number | null;

  @Column("float", { name: "Grupo5", nullable: true, precision: 53 })
  grupo5: number | null;

  @Column("float", { name: "Grupo6", nullable: true, precision: 53 })
  grupo6: number | null;

  @Column("float", { name: "Grupo7", nullable: true, precision: 53 })
  grupo7: number | null;

  @Column("float", { name: "CnsInternoTienda", nullable: true, precision: 53 })
  cnsInternoTienda: number | null;

  @Column("float", { name: "Serafines", nullable: true, precision: 53 })
  serafines: number | null;

  @Column("float", { name: "Valecontado", nullable: true, precision: 53 })
  valecontado: number | null;

  @Column("float", { name: "Valcredcomb", nullable: true, precision: 53 })
  valcredcomb: number | null;

  @Column("float", { name: "Valcredprod", nullable: true, precision: 53 })
  valcredprod: number | null;

  @Column("float", { name: "Tarjcredito", nullable: true, precision: 53 })
  tarjcredito: number | null;

  @Column("float", { name: "Efectsoles", nullable: true, precision: 53 })
  efectsoles: number | null;

  @Column("float", { name: "Efectdolar", nullable: true, precision: 53 })
  efectdolar: number | null;

  @Column("float", { name: "OtrosIngre", nullable: true, precision: 53 })
  otrosIngre: number | null;

  @Column("float", { name: "Faltantes", nullable: true, precision: 53 })
  faltantes: number | null;

  @Column("float", { name: "Sobrantes", nullable: true, precision: 53 })
  sobrantes: number | null;

  @Column("float", { name: "Otros", nullable: true, precision: 53 })
  otros: number | null;

  @Column("smalldatetime", { name: "Fecha_cierre", nullable: true })
  fechaCierre: Date | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
