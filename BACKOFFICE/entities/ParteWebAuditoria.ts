import { Column, Entity } from "typeorm";

@Entity("ParteWeb_auditoria", { schema: "dbo" })
export class ParteWebAuditoria {
  @Column("char", { name: "cdlocal", nullable: true, length: 4 })
  cdlocal: string | null;

  @Column("datetime", { name: "Fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "Tcontom",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  tcontom: number | null;

  @Column("numeric", {
    name: "Dsctotal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  dsctotal: number | null;

  @Column("numeric", {
    name: "CnsInterno",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  cnsInterno: number | null;

  @Column("numeric", {
    name: "Grupo1",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo1: number | null;

  @Column("numeric", {
    name: "Grupo2",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo2: number | null;

  @Column("numeric", {
    name: "Grupo3",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo3: number | null;

  @Column("numeric", {
    name: "Grupo4",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo4: number | null;

  @Column("numeric", {
    name: "Grupo5",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo5: number | null;

  @Column("numeric", {
    name: "Grupo6",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo6: number | null;

  @Column("numeric", {
    name: "Grupo7",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  grupo7: number | null;

  @Column("numeric", {
    name: "Serafines",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  serafines: number | null;

  @Column("numeric", {
    name: "Valecontado",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  valecontado: number | null;

  @Column("numeric", {
    name: "Valcredcomb",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  valcredcomb: number | null;

  @Column("numeric", {
    name: "Valcredprod",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  valcredprod: number | null;

  @Column("numeric", {
    name: "Tarjcredito",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  tarjcredito: number | null;

  @Column("numeric", {
    name: "Efectsoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  efectsoles: number | null;

  @Column("numeric", {
    name: "Efectdolar",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  efectdolar: number | null;

  @Column("numeric", {
    name: "OtrosIngre",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  otrosIngre: number | null;

  @Column("numeric", {
    name: "Faltantes",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  faltantes: number | null;

  @Column("numeric", {
    name: "Sobrantes",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  sobrantes: number | null;

  @Column("numeric", { name: "Otros", nullable: true, precision: 12, scale: 2 })
  otros: number | null;

  @Column("smalldatetime", {
    name: "Fecsistema",
    nullable: true,
    default: () => "getdate()",
  })
  fecsistema: Date | null;

  @Column("varchar", { name: "proceso", nullable: true, length: 15 })
  proceso: string | null;
}
