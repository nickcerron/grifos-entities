import { Column, Entity } from "typeorm";

@Entity("TmpVentaDWebS", { schema: "dbo" })
export class TmpVentaDWebS {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "SerieMaq", length: 20 })
  serieMaq: string;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("int", { name: "NroItem" })
  nroItem: number;

  @Column("float", { name: "Impuesto", nullable: true, precision: 53 })
  impuesto: number | null;

  @Column("float", { name: "PorDscto1", nullable: true, precision: 53 })
  porDscto1: number | null;

  @Column("float", { name: "PorDscto2", nullable: true, precision: 53 })
  porDscto2: number | null;

  @Column("float", { name: "Cantidad", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("varchar", { name: "TipoPrecio", nullable: true, length: 15 })
  tipoPrecio: string | null;

  @Column("float", { name: "Precio", nullable: true, precision: 53 })
  precio: number | null;

  @Column("float", { name: "MtoDescuento", nullable: true, precision: 53 })
  mtoDescuento: number | null;

  @Column("float", { name: "mtosubtotal", nullable: true, precision: 53 })
  mtosubtotal: number | null;

  @Column("float", { name: "mtoimpuesto", nullable: true, precision: 53 })
  mtoimpuesto: number | null;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("char", { name: "Cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "Manguera", nullable: true, length: 1 })
  manguera: string | null;

  @Column("varchar", { name: "NroTransaccion", nullable: true, length: 10 })
  nroTransaccion: string | null;

  @Column("bit", { name: "FlgMovimiento", nullable: true })
  flgMovimiento: boolean | null;

  @Column("float", { name: "Costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("float", { name: "Precio_Orig", nullable: true, precision: 53 })
  precioOrig: number | null;

  @Column("varchar", { name: "cdpack", nullable: true, length: 20 })
  cdpack: string | null;

  @Column("numeric", {
    name: "mtoimp_otros",
    precision: 12,
    scale: 4,
    default: () => "(0)",
  })
  mtoimpOtros: number;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;
}
