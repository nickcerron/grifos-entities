import { Column, Entity } from "typeorm";

@Entity("Timer_Procesos", { schema: "dbo" })
export class TimerProcesos {
  @Column("char", { name: "Proceso", length: 3 })
  proceso: string;

  @Column("datetime", { name: "Fecha" })
  fecha: Date;

  @Column("char", { name: "Hora", nullable: true, length: 8 })
  hora: string | null;

  @Column("int", { name: "veces", nullable: true, default: () => "(0)" })
  veces: number | null;
}
