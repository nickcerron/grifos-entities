import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_TmpMovPuntos",
  [
    "nrodocumento",
    "cdtipodoc",
    "tipoTran",
    "localId",
    "cdproducto",
    "item",
    "seriePrinter",
  ],
  { unique: true }
)
@Entity("TmpMovPuntos", { schema: "dbo" })
export class TmpMovPuntos {
  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "TipoTran", length: 1 })
  tipoTran: string;

  @Column("char", { primary: true, name: "LocalID", length: 4 })
  localId: string;

  @Column("char", { primary: true, name: "cdproducto", length: 20 })
  cdproducto: string;

  @Column("smallint", { primary: true, name: "Item" })
  item: number;

  @Column("char", { primary: true, name: "SeriePrinter", length: 15 })
  seriePrinter: string;

  @Column("char", { name: "Almacen", nullable: true, length: 4 })
  almacen: string | null;

  @Column("varchar", { name: "TarjAfiliacion", length: 25 })
  tarjAfiliacion: string;

  @Column("char", { name: "ClienteId", nullable: true, length: 15 })
  clienteId: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("smalldatetime", { name: "FechaDoc", nullable: true })
  fechaDoc: Date | null;

  @Column("smalldatetime", { name: "FechaSis", nullable: true })
  fechaSis: Date | null;

  @Column("char", { name: "Moneda", nullable: true, length: 3 })
  moneda: string | null;

  @Column("float", {
    name: "TCambio",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  tCambio: number | null;

  @Column("float", {
    name: "Impuesto",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  impuesto: number | null;

  @Column("float", {
    name: "Precio",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  precio: number | null;

  @Column("float", {
    name: "cantidad",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  cantidad: number | null;

  @Column("float", {
    name: "MtoDescto",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoDescto: number | null;

  @Column("float", {
    name: "MtoSubtotal",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoSubtotal: number | null;

  @Column("float", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoImpuesto: number | null;

  @Column("float", {
    name: "MtoTotal",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoTotal: number | null;

  @Column("float", {
    name: "Puntos",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  puntos: number | null;

  @Column("float", {
    name: "ValorAcumulado",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  valorAcumulado: number | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("bit", { name: "enviado", nullable: true, default: () => "(0)" })
  enviado: boolean | null;

  @Column("tinyint", { name: "Turno", nullable: true })
  turno: number | null;

  @Column("bit", { name: "Credito", nullable: true, default: () => "(0)" })
  credito: boolean | null;

  @Column("nvarchar", { name: "Glosa", nullable: true, length: 150 })
  glosa: string | null;

  @Column("nvarchar", { name: "Referencia", nullable: true, length: 25 })
  referencia: string | null;

  @Column("nchar", { name: "Usuario", length: 10 })
  usuario: string;

  @Column("timestamp", { name: "TmsTamp" })
  tmsTamp: Date;
}
