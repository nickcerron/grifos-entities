import { Column, Entity, Index } from "typeorm";

@Index("PK_loteriahora", ["item"], { unique: true })
@Entity("loteriahora", { schema: "dbo" })
export class Loteriahora {
  @Column("numeric", { primary: true, name: "item", precision: 2, scale: 0 })
  item: number;

  @Column("char", { name: "horainicio", nullable: true, length: 5 })
  horainicio: string | null;

  @Column("char", { name: "horafin", nullable: true, length: 5 })
  horafin: string | null;

  @Column("bit", { name: "flglunes", nullable: true })
  flglunes: boolean | null;

  @Column("bit", { name: "flgmartes", nullable: true })
  flgmartes: boolean | null;

  @Column("bit", { name: "flgmiercoles", nullable: true })
  flgmiercoles: boolean | null;

  @Column("bit", { name: "flgjueves", nullable: true })
  flgjueves: boolean | null;

  @Column("bit", { name: "flgviernes", nullable: true })
  flgviernes: boolean | null;

  @Column("bit", { name: "flgsabado", nullable: true })
  flgsabado: boolean | null;

  @Column("bit", { name: "flgdomingo", nullable: true })
  flgdomingo: boolean | null;

  @Column("bit", { name: "flgprod", nullable: true })
  flgprod: boolean | null;

  @Column("numeric", {
    name: "nroganador",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nroganador: number | null;

  @Column("char", { name: "monmto", nullable: true, length: 1 })
  monmto: string | null;

  @Column("numeric", {
    name: "mtomax",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtomax: number | null;

  @Column("numeric", {
    name: "frecuencia",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  frecuencia: number | null;
}
