import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_Kardex_Saldo_mes_Fisico_UpdateStock", ["id"], { unique: true })
@Entity("Kardex_Saldo_mes_Fisico_UpdateStock", { schema: "dbo" })
export class KardexSaldoMesFisicoUpdateStock {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "periodo", length: 6 })
  periodo: string;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;
}
