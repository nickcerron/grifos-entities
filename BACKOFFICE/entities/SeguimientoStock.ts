import { Column, Entity } from "typeorm";

@Entity("SeguimientoStock", { schema: "dbo" })
export class SeguimientoStock {
  @Column("int", { name: "CDLOCAL" })
  cdlocal: number;

  @Column("int", { name: "ALMACENID" })
  almacenid: number;

  @Column("char", { name: "ARTICULOID", length: 20 })
  articuloid: string;

  @Column("char", { name: "CODIGO", nullable: true, length: 10 })
  codigo: string | null;

  @Column("varchar", { name: "MENSAJE", nullable: true, length: 250 })
  mensaje: string | null;

  @Column("datetime", { name: "FECHAERROR", nullable: true })
  fechaerror: Date | null;
}
