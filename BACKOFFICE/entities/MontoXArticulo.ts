import { Column, Entity, Index } from "typeorm";

@Index("PK_MontoXArticulo", ["cdarticulo"], { unique: true })
@Entity("MontoXArticulo", { schema: "dbo" })
export class MontoXArticulo {
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", { name: "MtoBase", precision: 16, scale: 2 })
  mtoBase: number;
}
