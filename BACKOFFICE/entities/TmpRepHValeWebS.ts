import { Column, Entity } from "typeorm";

@Entity("TmpRepHValeWebS", { schema: "dbo" })
export class TmpRepHValeWebS {
  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("char", { name: "RUCEMISOR", length: 15 })
  rucemisor: string;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "NROVALE", length: 15 })
  nrovale: string;

  @Column("smalldatetime", { name: "FECVALE", nullable: true })
  fecvale: Date | null;

  @Column("char", { name: "MONEDA", nullable: true, length: 1 })
  moneda: string | null;

  @Column("numeric", {
    name: "MTOVALE",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtovale: number | null;

  @Column("varchar", { name: "CLIENTEID", length: 15 })
  clienteid: string;

  @Column("char", { name: "NROPLACA", nullable: true, length: 250 })
  nroplaca: string | null;

  @Column("varchar", { name: "NROSERIEMAQ", length: 15 })
  nroseriemaq: string;

  @Column("varchar", { name: "TIPODOCID", length: 5 })
  tipodocid: string;

  @Column("varchar", { name: "NRODOCUMENTO", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("smalldatetime", { name: "FECDOCUMENTO", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "FECPROCESO", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "NROSERIEMAQFAC", nullable: true, length: 15 })
  nroseriemaqfac: string | null;

  @Column("char", { name: "CDTIPODOCFAC", nullable: true, length: 5 })
  cdtipodocfac: string | null;

  @Column("varchar", { name: "NRODOCUMENTOFAC", nullable: true, length: 15 })
  nrodocumentofac: string | null;

  @Column("smalldatetime", { name: "FECDOCUMENTOFAC", nullable: true })
  fecdocumentofac: Date | null;

  @Column("smalldatetime", { name: "FECPROCESOFAC", nullable: true })
  fecprocesofac: Date | null;

  @Column("char", { name: "PLACA", nullable: true, length: 10 })
  placa: string | null;

  @Column("int", { name: "TURNO", nullable: true })
  turno: number | null;

  @Column("bit", { name: "ARCHTURNO", nullable: true })
  archturno: boolean | null;

  @Column("varchar", { name: "DOCVALE", nullable: true, length: 10 })
  docvale: string | null;

  @Column("numeric", {
    name: "CONSUMOMTOINI",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumomtoini: number | null;

  @Column("numeric", {
    name: "CONSUMOMTOFIN",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumomtofin: number | null;

  @Column("bit", { name: "LICITACION", nullable: true })
  licitacion: boolean | null;

  @Column("varchar", { name: "NROCONTRATO", nullable: true, length: 60 })
  nrocontrato: string | null;

  @Column("varchar", { name: "USER_REGISTRA", nullable: true, length: 20 })
  userRegistra: string | null;

  @Column("char", { name: "AREAID", nullable: true, length: 5 })
  areaid: string | null;
}
