import { Column, Entity, Index } from "typeorm";

@Index("PK_Envio_trama", ["cdtipodoc", "nrodocumento"], { unique: true })
@Entity("Envio_trama", { schema: "dbo" })
export class EnvioTrama {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("datetime", { name: "fecdocumento" })
  fecdocumento: Date;

  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("varchar", { name: "trama", nullable: true })
  trama: string | null;

  @Column("varchar", { name: "mensaje", nullable: true })
  mensaje: string | null;

  @Column("varchar", { name: "cdhash", nullable: true, length: 50 })
  cdhash: string | null;

  @Column("bit", { name: "Enviado", nullable: true })
  enviado: boolean | null;

  @Column("datetime", { name: "FechaEnvio", nullable: true })
  fechaEnvio: Date | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 1000 })
  observacion: string | null;
}
