import { Column, Entity } from "typeorm";

@Entity("LOG_Precio_Varios", { schema: "dbo" })
export class LogPrecioVarios {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { name: "tiporango", length: 3 })
  tiporango: string;

  @Column("numeric", { name: "rango1", precision: 10, scale: 3 })
  rango1: number;

  @Column("numeric", {
    name: "rango2",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  rango2: number | null;

  @Column("smalldatetime", { name: "fechamodificacion", nullable: true })
  fechamodificacion: Date | null;

  @Column("char", { name: "tipo", length: 3 })
  tipo: string;

  @Column("numeric", { name: "valor", nullable: true, precision: 10, scale: 3 })
  valor: number | null;

  @Column("varchar", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("bit", { name: "flgAplicaDsctoAuto", nullable: true })
  flgAplicaDsctoAuto: boolean | null;

  @Column("int", { name: "cdPrecioVarios", nullable: true })
  cdPrecioVarios: number | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
