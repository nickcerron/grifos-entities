import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Salida202302",
  ["cdlocal", "cdtpsalida", "nrosalida", "rucempresa"],
  { unique: true }
)
@Entity("Salida202302", { schema: "dbo" })
export class Salida202302 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("char", { primary: true, name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 15 })
  nrodocumento: string | null;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdalmdest", nullable: true, length: 3 })
  cdalmdest: string | null;

  @Column("char", { name: "nroingreso", nullable: true, length: 10 })
  nroingreso: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtoimpuesto1",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtoimpuesto1: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 16,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 250 })
  observacion: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("bit", { name: "IsSAP", nullable: true, default: () => "(1)" })
  isSap: boolean | null;
}
