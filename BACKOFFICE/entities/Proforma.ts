import { Column, Entity, Index } from "typeorm";

@Index("PK_proforma", ["cdlocal", "nroproforma"], { unique: true })
@Entity("proforma", { schema: "dbo" })
export class Proforma {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroproforma", length: 10 })
  nroproforma: string;

  @Column("smalldatetime", { name: "fecproforma", nullable: true })
  fecproforma: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("varchar", { name: "drcliente", nullable: true, length: 60 })
  drcliente: string | null;

  @Column("char", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null;

  @Column("char", { name: "tlfcliente1", nullable: true, length: 15 })
  tlfcliente1: string | null;

  @Column("char", { name: "emcliente", nullable: true, length: 60 })
  emcliente: string | null;

  @Column("char", { name: "cdprecio", nullable: true, length: 5 })
  cdprecio: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("numeric", { name: "estado", nullable: true, precision: 1, scale: 0 })
  estado: number | null;

  @Column("smalldatetime", { name: "fecaprobado", nullable: true })
  fecaprobado: Date | null;

  @Column("char", { name: "cdusuaprobado", nullable: true, length: 10 })
  cdusuaprobado: string | null;

  @Column("char", { name: "nroocompra", nullable: true, length: 15 })
  nroocompra: string | null;

  @Column("char", { name: "validez", nullable: true, length: 50 })
  validez: string | null;

  @Column("varchar", { name: "tiempo", nullable: true, length: 100 })
  tiempo: string | null;

  @Column("char", { name: "formapago", nullable: true, length: 50 })
  formapago: string | null;

  @Column("char", { name: "garantia", nullable: true, length: 50 })
  garantia: string | null;

  @Column("char", { name: "cdencabezado", nullable: true, length: 5 })
  cdencabezado: string | null;

  @Column("char", { name: "cdpiepagina", nullable: true, length: 5 })
  cdpiepagina: string | null;

  @Column("char", { name: "cdfirma", nullable: true, length: 5 })
  cdfirma: string | null;
}
