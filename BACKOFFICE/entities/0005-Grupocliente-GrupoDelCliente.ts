import { Column, Entity, Index } from "typeorm";

@Index("PK_GRUPOCLIENTE", ["cdgrupocli"], { unique: true })
@Entity("GRUPOCLIENTE", { schema: "dbo" })
export class Grupocliente {
  //id:  ahora sera identity
  //bigint
  @Column("char", { primary: true, name: "CDGRUPOCLI", length: 5 })
  cdgrupocli: string;

  //nombre del grupo
  //nombre en la db: name
  // varchar(50)
  // nullable false
  // unique true
  @Column("varchar", { name: "DSGRUPOCLI", nullable: true, length: 50 })
  dsgrupocli: string | null;
}
