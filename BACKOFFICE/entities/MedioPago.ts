import { Column, Entity, Index } from "typeorm";

@Index("PK__Medio_Pa__9DA0D4BFEB6C91F2", ["cdMedioPago"], { unique: true })
@Entity("Medio_Pago", { schema: "dbo" })
export class MedioPago {
  @Column("char", { primary: true, name: "cdMedio_Pago", length: 3 })
  cdMedioPago: string;

  @Column("varchar", { name: "dsMedio_pago", nullable: true, length: 250 })
  dsMedioPago: string | null;
}
