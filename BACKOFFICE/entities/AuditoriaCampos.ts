import { Column, Entity, Index } from "typeorm";

@Index("PK_auditoria_campos", ["tabla", "columna"], { unique: true })
@Entity("auditoria_campos", { schema: "dbo" })
export class AuditoriaCampos {
  @Column("varchar", { primary: true, name: "tabla", length: 128 })
  tabla: string;

  @Column("varchar", { primary: true, name: "columna", length: 128 })
  columna: string;

  @Column("smallint", { name: "pk", nullable: true })
  pk: number | null;

  @Column("smallint", { name: "prec", nullable: true })
  prec: number | null;

  @Column("smallint", { name: "scale", nullable: true })
  scale: number | null;

  @Column("varchar", { name: "tipo", length: 128 })
  tipo: string;

  @Column("tinyint", { name: "sel", default: () => "(1)" })
  sel: number;

  @Column("smallint", { name: "norden", default: () => "(0)" })
  norden: number;
}
