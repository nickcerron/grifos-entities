import { Column, Entity } from "typeorm";

@Entity("Afiliacionprefijos", { schema: "dbo" })
export class Afiliacionprefijos {
  @Column("char", { name: "cdprefijo", nullable: true, length: 2 })
  cdprefijo: string | null;

  @Column("char", { name: "prefijo", nullable: true, length: 20 })
  prefijo: string | null;

  @Column("varchar", { name: "dsprefijo", nullable: true, length: 50 })
  dsprefijo: string | null;
}
