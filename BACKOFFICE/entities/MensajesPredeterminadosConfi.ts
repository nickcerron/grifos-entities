import { Column, Entity, Index } from "typeorm";

@Index("PK_Mensajes_Predeterminados_Confi", ["cdcliente"], { unique: true })
@Entity("Mensajes_Predeterminados_Confi", { schema: "dbo" })
export class MensajesPredeterminadosConfi {
  @Column("char", { primary: true, name: "cdcliente", length: 20 })
  cdcliente: string;

  @Column("varchar", { name: "Rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("int", { name: "Id_Mensaje1", nullable: true })
  idMensaje1: number | null;

  @Column("varchar", { name: "Descripcion1", nullable: true, length: 200 })
  descripcion1: string | null;

  @Column("bit", { name: "Habilitado1", nullable: true })
  habilitado1: boolean | null;

  @Column("int", { name: "Id_Mensaje2", nullable: true })
  idMensaje2: number | null;

  @Column("varchar", { name: "Descripcion2", nullable: true, length: 200 })
  descripcion2: string | null;

  @Column("bit", { name: "Habilitado2", nullable: true })
  habilitado2: boolean | null;

  @Column("int", { name: "Id_Mensaje3", nullable: true })
  idMensaje3: number | null;

  @Column("varchar", { name: "Descripcion3", nullable: true, length: 200 })
  descripcion3: string | null;

  @Column("bit", { name: "Habilitado3", nullable: true })
  habilitado3: boolean | null;
}
