import { Column, Entity } from "typeorm";

@Entity("CnfgParteDiario", { schema: "dbo" })
export class CnfgParteDiario {
  @Column("char", { name: "codigo", nullable: true, length: 15 })
  codigo: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", { name: "valor", nullable: true, precision: 12, scale: 2 })
  valor: number | null;
}
