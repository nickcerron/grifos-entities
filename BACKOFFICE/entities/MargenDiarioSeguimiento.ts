import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK_MARGEN_DIARIO_SEGUIMIENTO", ["id"], { unique: true })
@Entity("MARGEN_DIARIO_SEGUIMIENTO", { schema: "dbo" })
export class MargenDiarioSeguimiento {
  @PrimaryGeneratedColumn({ type: "bigint", name: "ID" })
  id: string;

  @Column("date", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "ARTICULO", nullable: true, length: 25 })
  articulo: string | null;

  @Column("varchar", { name: "MENSAJE", nullable: true, length: 5000 })
  mensaje: string | null;

  @Column("varchar", { name: "FECHAERROR", nullable: true, length: 25 })
  fechaerror: string | null;
}
