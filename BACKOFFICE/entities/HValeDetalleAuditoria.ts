import { Column, Entity } from "typeorm";

@Entity("HVale_Detalle_Auditoria", { schema: "dbo" })
export class HValeDetalleAuditoria {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("numeric", { name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "precio_orig",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precioOrig: number | null;

  @Column("numeric", {
    name: "mtototal_orig",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototalOrig: number | null;

  @Column("numeric", {
    name: "mtoimpuesto_orig",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuestoOrig: number | null;

  @Column("numeric", {
    name: "mtosubtotal_orig",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotalOrig: number | null;

  @Column("char", { name: "Usuario", nullable: true, length: 20 })
  usuario: string | null;

  @Column("smalldatetime", { name: "fecModificacion", nullable: true })
  fecModificacion: Date | null;
}
