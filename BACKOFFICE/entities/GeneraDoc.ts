import { Column, Entity } from "typeorm";

@Entity("GeneraDoc", { schema: "dbo" })
export class GeneraDoc {
  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "nrodocumento", nullable: true, length: 10 })
  nrodocumento: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("char", { name: "nrodocumentonuevo", nullable: true, length: 10 })
  nrodocumentonuevo: string | null;

  @Column("datetime", { name: "fecdocumentonuevo", nullable: true })
  fecdocumentonuevo: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;
}
