import { Column, Entity } from "typeorm";

@Entity("TmpClientesGNV", { schema: "dbo" })
export class TmpClientesGnv {
  @Column("varchar", { name: "LocalId", length: 3 })
  localId: string;

  @Column("varchar", { name: "ClienteID", nullable: true, length: 17 })
  clienteId: string | null;

  @Column("int", { name: "TipoCliente" })
  tipoCliente: number;

  @Column("varchar", { name: "RazSoc", nullable: true, length: 50 })
  razSoc: string | null;

  @Column("varchar", { name: "Ruc", nullable: true, length: 17 })
  ruc: string | null;

  @Column("char", { name: "drcliente", nullable: true, length: 60 })
  drcliente: string | null;

  @Column("varchar", { name: "cddistrito", length: 1 })
  cddistrito: string;

  @Column("varchar", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null;

  @Column("varchar", { name: "provtelef1", length: 1 })
  provtelef1: string;

  @Column("varchar", { name: "tlfcliente1", length: 1 })
  tlfcliente1: string;

  @Column("varchar", { name: "provtelef2", length: 1 })
  provtelef2: string;

  @Column("varchar", { name: "Correo", length: 30 })
  correo: string;

  @Column("varchar", { name: "Contacto", length: 1 })
  contacto: string;

  @Column("int", { name: "Afil" })
  afil: number;

  @Column("varchar", { name: "FechaAf", length: 10 })
  fechaAf: string;

  @Column("smalldatetime", { name: "FecNac", nullable: true })
  fecNac: Date | null;

  @Column("varchar", { name: "tipocli", length: 1 })
  tipocli: string;

  @Column("int", { name: "LmtCrd" })
  lmtCrd: number;

  @Column("int", { name: "Status" })
  status: number;
}
