import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaDepBanco202211",
  ["cdlocal", "fecproceso", "turno", "nrocuenta"],
  { unique: true }
)
@Entity("DiaDepBanco202211", { schema: "dbo" })
export class DiaDepBanco202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { primary: true, name: "Turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { primary: true, name: "nrocuenta", length: 20 })
  nrocuenta: string;

  @Column("char", { name: "nrodeposito", length: 20 })
  nrodeposito: string;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "TCambio",
    nullable: true,
    precision: 8,
    scale: 4,
  })
  tCambio: number | null;

  @Column("numeric", {
    name: "valores",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  valores: number | null;

  @Column("numeric", {
    name: "cheques",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  cheques: number | null;

  @Column("char", { name: "Observacion", nullable: true, length: 60 })
  observacion: string | null;

  @Column("bit", { name: "FlgGnv", nullable: true })
  flgGnv: boolean | null;

  @Column("bit", { name: "FlgGlp", nullable: true })
  flgGlp: boolean | null;
}
