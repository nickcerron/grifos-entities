import { Column, Entity, Index } from "typeorm";

@Index("pbcate_x", ["pbeName", "pbeSeqn"], {})
@Entity("pbcatedt", { schema: "dbo" })
export class Pbcatedt {
  @Column("varchar", { name: "pbe_name", length: 30 })
  pbeName: string;

  @Column("varchar", { name: "pbe_edit", nullable: true, length: 254 })
  pbeEdit: string | null;

  @Column("smallint", { name: "pbe_type", nullable: true })
  pbeType: number | null;

  @Column("int", { name: "pbe_cntr", nullable: true })
  pbeCntr: number | null;

  @Column("smallint", { name: "pbe_seqn" })
  pbeSeqn: number;

  @Column("int", { name: "pbe_flag", nullable: true })
  pbeFlag: number | null;

  @Column("varchar", { name: "pbe_work", nullable: true, length: 32 })
  pbeWork: string | null;
}
