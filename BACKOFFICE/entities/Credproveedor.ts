import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_credproveedor",
  ["docpago", "cdtipodoc", "nrodocumento", "renovacion"],
  { unique: true }
)
@Entity("credproveedor", { schema: "dbo" })
export class Credproveedor {
  @Column("char", { primary: true, name: "docpago", length: 1 })
  docpago: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "renovacion", length: 2 })
  renovacion: string;

  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("smalldatetime", { name: "fecpago", nullable: true })
  fecpago: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtoemision",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtoemision: number | null;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolares",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("numeric", {
    name: "mtodifcambio",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodifcambio: number | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cddocaplica", nullable: true, length: 5 })
  cddocaplica: string | null;

  @Column("char", { name: "nrodocaplica", nullable: true, length: 10 })
  nrodocaplica: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("numeric", {
    name: "nropago",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nropago: number | null;

  @Column("char", { name: "nroplanilla", nullable: true, length: 10 })
  nroplanilla: string | null;

  @Column("char", { name: "nrorecibo", nullable: true, length: 10 })
  nrorecibo: string | null;

  @Column("char", { name: "cdtppago", nullable: true, length: 5 })
  cdtppago: string | null;

  @Column("char", { name: "cdbanco", nullable: true, length: 5 })
  cdbanco: string | null;

  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("char", { name: "nrocheque", nullable: true, length: 20 })
  nrocheque: string | null;

  @Column("char", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "referencia", nullable: true, length: 60 })
  referencia: string | null;
}
