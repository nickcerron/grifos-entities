import { Column, Entity } from "typeorm";

@Entity("ventag", { schema: "dbo" })
export class Ventag {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { name: "nroseriemaq", length: 15 })
  nroseriemaq: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("bit", { name: "declarado", nullable: true })
  declarado: boolean | null;

  @Column("bit", { name: "anulado", nullable: true })
  anulado: boolean | null;

  @Column("char", { name: "estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("int", {
    name: "reintentos_replicacion",
    nullable: true,
    default: () => "(0)",
  })
  reintentosReplicacion: number | null;

  @Column("varchar", { name: "EstadoRegistro", nullable: true, length: 1 })
  estadoRegistro: string | null;

  @Column("varchar", { name: "EstadoProceso", nullable: true, length: 15 })
  estadoProceso: string | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
