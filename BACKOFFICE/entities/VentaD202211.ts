import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_VentaD202211",
  [
    "cdlocal",
    "nroseriemaq",
    "cdtipodoc",
    "nrodocumento",
    "nroitem",
    "cdarticulo",
    "talla",
  ],
  { unique: true }
)
@Entity("VentaD202211", { schema: "dbo" })
export class VentaD202211 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroseriemaq", length: 20 })
  nroseriemaq: string;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("numeric", { primary: true, name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdalterna", nullable: true, length: 20 })
  cdalterna: string | null;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "cant_ncredito",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantNcredito: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoservicio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoservicio: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "nrogasboy", nullable: true, length: 20 })
  nrogasboy: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nroguia", nullable: true, length: 10 })
  nroguia: string | null;

  @Column("char", { name: "nroproforma", nullable: true, length: 10 })
  nroproforma: string | null;

  @Column("bit", { name: "moverstock", nullable: true })
  moverstock: boolean | null;

  @Column("text", { name: "glosa", nullable: true })
  glosa: string | null;

  @Column("char", { name: "manguera", nullable: true, length: 2 })
  manguera: string | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 12, scale: 4 })
  costo: number | null;

  @Column("numeric", {
    name: "precio_orig",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precioOrig: number | null;

  @Column("numeric", {
    name: "precioafiliacion",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precioafiliacion: number | null;

  @Column("numeric", {
    name: "PtosGanados",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  ptosGanados: number | null;

  @Column("varchar", { name: "TipoAcumula", nullable: true, length: 50 })
  tipoAcumula: string | null;

  @Column("numeric", {
    name: "ValorAcumula",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  valorAcumula: number | null;

  @Column("varchar", { name: "TipoSuma", nullable: true, length: 50 })
  tipoSuma: string | null;

  @Column("numeric", {
    name: "Costo_Venta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  costoVenta: number | null;

  @Column("numeric", {
    name: "porcPercepcion",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porcPercepcion: number | null;

  @Column("numeric", {
    name: "MtoPercepcion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtoPercepcion: number | null;

  @Column("char", { name: "cdpack", nullable: true, length: 20 })
  cdpack: string | null;

  @Column("numeric", {
    name: "mtodetraccion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtodetraccion: number | null;

  @Column("numeric", {
    name: "porcdetraccion",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porcdetraccion: number | null;

  @Column("numeric", {
    name: "redondea_indecopi",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  redondeaIndecopi: number | null;

  @Column("varchar", { name: "cdarticulosunat", nullable: true, length: 20 })
  cdarticulosunat: string | null;

  @Column("numeric", {
    name: "mtoimp_otros",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpOtros: number | null;

  @Column("numeric", {
    name: "VolumenFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  volumenFinal: number | null;

  @Column("numeric", {
    name: "MontoFinal",
    nullable: true,
    precision: 18,
    scale: 3,
  })
  montoFinal: number | null;

  @Column("int", { name: "IdTran", nullable: true })
  idTran: number | null;

  @Column("numeric", {
    name: "mtogratuito",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtogratuito: number | null;

  @Column("numeric", {
    name: "cantidad_conversion",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadConversion: number | null;

  @Column("numeric", {
    name: "precio_orig_conversion",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precioOrigConversion: number | null;

  @Column("numeric", {
    name: "precio_conversion",
    nullable: true,
    precision: 18,
    scale: 10,
  })
  precioConversion: number | null;

  @Column("char", { name: "factor_conversion", nullable: true, length: 1 })
  factorConversion: string | null;

  @Column("text", { name: "glosa2", nullable: true })
  glosa2: string | null;

  @Column("int", { name: "cdDescuento", nullable: true })
  cdDescuento: number | null;

  @Column("char", { name: "cdTPDescuento", nullable: true, length: 4 })
  cdTpDescuento: string | null;

  @Column("bit", { name: "Flg_Exonerado", nullable: true })
  flgExonerado: boolean | null;

  @Column("bit", { name: "Flg_Inafecto", nullable: true })
  flgInafecto: boolean | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
