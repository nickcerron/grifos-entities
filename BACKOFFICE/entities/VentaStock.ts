import { Column, Entity } from "typeorm";

@Entity("Venta_Stock", { schema: "dbo" })
export class VentaStock {
  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", { name: "total", nullable: true, precision: 12, scale: 4 })
  total: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;
}
