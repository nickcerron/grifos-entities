import { Column, Entity } from "typeorm";

@Entity("TmpVentaByTipoWebS", { schema: "dbo" })
export class TmpVentaByTipoWebS {
  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "TipoTran", length: 1 })
  tipoTran: string;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("char", { name: "NroSerieMaq", length: 15 })
  nroSerieMaq: string;

  @Column("char", { name: "NroPOS", nullable: true, length: 10 })
  nroPos: string | null;

  @Column("char", { name: "TipoPago", length: 5 })
  tipoPago: string;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "FecProceso", nullable: true })
  fecProceso: Date | null;

  @Column("char", { name: "cdbanco", length: 4 })
  cdbanco: string;

  @Column("char", { name: "nrocuenta", length: 20 })
  nrocuenta: string;

  @Column("char", { name: "nrocheque", length: 20 })
  nrocheque: string;

  @Column("char", { name: "CdTarj", length: 2 })
  cdTarj: string;

  @Column("char", { name: "NroTarj", length: 20 })
  nroTarj: string;

  @Column("float", { name: "TCambio", nullable: true, precision: 53 })
  tCambio: number | null;

  @Column("float", { name: "mtopagosol", nullable: true, precision: 53 })
  mtopagosol: number | null;

  @Column("float", { name: "mtopagodol", nullable: true, precision: 53 })
  mtopagodol: number | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("int", { name: "turno", nullable: true })
  turno: number | null;

  @Column("char", { name: "nroncredito", nullable: true, length: 10 })
  nroncredito: string | null;

  @Column("varchar", { name: "tipofactura", length: 1 })
  tipofactura: string;

  @Column("char", { name: "tipoventa", nullable: true, length: 1 })
  tipoventa: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("varchar", { name: "User_Registra", length: 20 })
  userRegistra: string;
}
