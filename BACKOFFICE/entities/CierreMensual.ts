import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Cierre_Mensual",
  ["ano", "mes", "cdLocal", "cdAlmacen", "cdArticulo"],
  { unique: true }
)
@Entity("Cierre_Mensual", { schema: "dbo" })
export class CierreMensual {
  @Column("numeric", { primary: true, name: "Ano", precision: 4, scale: 0 })
  ano: number;

  @Column("numeric", { primary: true, name: "Mes", precision: 2, scale: 0 })
  mes: number;

  @Column("char", { primary: true, name: "CdLocal", length: 3 })
  cdLocal: string;

  @Column("char", { primary: true, name: "CdAlmacen", length: 3 })
  cdAlmacen: string;

  @Column("char", { primary: true, name: "CdArticulo", length: 20 })
  cdArticulo: string;

  @Column("smalldatetime", { name: "fecha_Inicio_Cierre", nullable: true })
  fechaInicioCierre: Date | null;

  @Column("smalldatetime", { name: "Fecha_Final_Cierre", nullable: true })
  fechaFinalCierre: Date | null;

  @Column("numeric", {
    name: "Saldo_Inicial_Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoInicialCantidad: number | null;

  @Column("numeric", {
    name: "Saldo_Inicial_Costo_Unitario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoInicialCostoUnitario: number | null;

  @Column("numeric", {
    name: "Saldo_Inicial_Costo_Total",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoInicialCostoTotal: number | null;

  @Column("numeric", {
    name: "Entrada_Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  entradaCantidad: number | null;

  @Column("numeric", {
    name: "Entrada_Costo_Unitario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  entradaCostoUnitario: number | null;

  @Column("numeric", {
    name: "Entrada_Costo_Total",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  entradaCostoTotal: number | null;

  @Column("numeric", {
    name: "Salida_Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  salidaCantidad: number | null;

  @Column("numeric", {
    name: "Salida_Costo_Unitario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  salidaCostoUnitario: number | null;

  @Column("numeric", {
    name: "Salida_Costo_Total",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  salidaCostoTotal: number | null;

  @Column("numeric", {
    name: "Saldo_Final_Cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoFinalCantidad: number | null;

  @Column("numeric", {
    name: "Saldo_Final_Costo_Unitario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoFinalCostoUnitario: number | null;

  @Column("numeric", {
    name: "Saldo_Final_Costo_Total",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  saldoFinalCostoTotal: number | null;
}
