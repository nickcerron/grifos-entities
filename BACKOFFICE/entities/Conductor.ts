import { Column, Entity, Index } from "typeorm";

@Index("pk_Conductor", ["cdConductor"], { unique: true })
@Entity("Conductor", { schema: "dbo" })
export class Conductor {
  @Column("char", { primary: true, name: "cdConductor", length: 20 })
  cdConductor: string;

  @Column("varchar", { name: "dsNombres", nullable: true, length: 100 })
  dsNombres: string | null;

  @Column("varchar", { name: "dsApellidos", nullable: true, length: 100 })
  dsApellidos: string | null;

  @Column("varchar", { name: "nrolicencia", nullable: true, length: 10 })
  nrolicencia: string | null;

  @Column("char", { name: "cdtranspor", nullable: true, length: 20 })
  cdtranspor: string | null;
}
