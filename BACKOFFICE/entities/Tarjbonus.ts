import { Column, Entity } from "typeorm";

@Entity("tarjbonus", { schema: "dbo" })
export class Tarjbonus {
  @Column("char", { name: "cdestacion", length: 6 })
  cdestacion: string;

  @Column("char", { name: "nrobonus", length: 11 })
  nrobonus: string;

  @Column("char", { name: "fecha", length: 8 })
  fecha: string;

  @Column("char", { name: "hora", length: 4 })
  hora: string;

  @Column("char", { name: "nroequipo", length: 6 })
  nroequipo: string;

  @Column("char", { name: "nrotransac", length: 6 })
  nrotransac: string;

  @Column("varchar", { name: "detallexml", nullable: true })
  detallexml: string | null;

  @Column("char", { name: "totalvta", nullable: true, length: 7 })
  totalvta: string | null;

  @Column("bit", { name: "enviado", nullable: true })
  enviado: boolean | null;

  @Column("char", { name: "total", nullable: true, length: 7 })
  total: string | null;

  @Column("char", { name: "cdproducto", nullable: true, length: 14 })
  cdproducto: string | null;

  @Column("char", { name: "cantidad", nullable: true, length: 8 })
  cantidad: string | null;
}
