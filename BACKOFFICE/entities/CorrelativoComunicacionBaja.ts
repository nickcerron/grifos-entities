import { Column, Entity } from "typeorm";

@Entity("correlativo_comunicacion_baja", { schema: "dbo" })
export class CorrelativoComunicacionBaja {
  @Column("datetime", { name: "fecgeneracion", nullable: true })
  fecgeneracion: Date | null;

  @Column("int", { name: "correlativo", nullable: true })
  correlativo: number | null;

  @Column("int", { name: "correlativoBol", nullable: true })
  correlativoBol: number | null;
}
