import { Column, Entity } from "typeorm";

@Entity("hotkey", { schema: "dbo" })
export class Hotkey {
  @Column("char", { name: "tecla", nullable: true, length: 1 })
  tecla: string | null;

  @Column("numeric", { name: "tipo", nullable: true, precision: 1, scale: 0 })
  tipo: number | null;

  @Column("char", { name: "codigo", nullable: true, length: 20 })
  codigo: string | null;
}
