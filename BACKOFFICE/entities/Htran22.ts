import { Column, Entity } from "typeorm";

@Entity("Htran22", { schema: "dbo" })
export class Htran22 {
  @Column("char", { name: "Numero", nullable: true, length: 20 })
  numero: string | null;

  @Column("char", { name: "Soles", nullable: true, length: 6 })
  soles: string | null;

  @Column("char", { name: "producto", nullable: true, length: 2 })
  producto: string | null;

  @Column("char", { name: "Precio", nullable: true, length: 4 })
  precio: string | null;

  @Column("char", { name: "galones", nullable: true, length: 8 })
  galones: string | null;

  @Column("char", { name: "Cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "Fecha", nullable: true, length: 6 })
  fecha: string | null;

  @Column("char", { name: "Hora", nullable: true, length: 4 })
  hora: string | null;

  @Column("char", { name: "Turno", nullable: true, length: 1 })
  turno: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "Documento", nullable: true, length: 10 })
  documento: string | null;

  @Column("datetime", { name: "Fecproceso", nullable: true })
  fecproceso: Date | null;
}
