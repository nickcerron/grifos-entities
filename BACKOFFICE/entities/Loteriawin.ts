import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_loteriawin",
  ["fecha", "item", "nroseriemaq", "cdtipodoc", "nrodocumento"],
  { unique: true }
)
@Entity("loteriawin", { schema: "dbo" })
export class Loteriawin {
  @Column("smalldatetime", { primary: true, name: "fecha" })
  fecha: Date;

  @Column("numeric", { primary: true, name: "item", precision: 2, scale: 0 })
  item: number;

  @Column("char", { primary: true, name: "nroseriemaq", length: 15 })
  nroseriemaq: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "nroganador",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nroganador: number | null;

  @Column("numeric", {
    name: "frecuencia",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  frecuencia: number | null;
}
