import { Column, Entity } from "typeorm";

@Entity("promo", { schema: "dbo" })
export class Promo {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdamarre", length: 6 })
  cdamarre: string;

  @Column("numeric", { name: "cantidad", precision: 12, scale: 4 })
  cantidad: number;

  @Column("smalldatetime", { name: "fechafinal" })
  fechafinal: Date;
}
