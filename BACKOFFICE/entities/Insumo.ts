import { Column, Entity, Index } from "typeorm";

@Index("PK_insumo", ["cdarticulo", "cdinsumo"], { unique: true })
@Entity("insumo", { schema: "dbo" })
export class Insumo {
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "cdinsumo", length: 20 })
  cdinsumo: string;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "porcprecio",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  porcprecio: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 14, scale: 4 })
  costo: number | null;
}
