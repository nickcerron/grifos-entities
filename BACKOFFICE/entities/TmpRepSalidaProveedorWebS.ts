import { Column, Entity, Index } from "typeorm";

@Index("PK_TmpRepSalidaProveedorWebS", ["proveedorId"], { unique: true })
@Entity("TmpRepSalidaProveedorWebS", { schema: "dbo" })
export class TmpRepSalidaProveedorWebS {
  @Column("varchar", { primary: true, name: "ProveedorId", length: 20 })
  proveedorId: string;

  @Column("varchar", { name: "Nombre", nullable: true, length: 120 })
  nombre: string | null;

  @Column("varchar", { name: "Ruc", nullable: true, length: 15 })
  ruc: string | null;

  @Column("varchar", { name: "Direccion", nullable: true, length: 120 })
  direccion: string | null;
}
