import { Column, Entity, Index } from "typeorm";
import {Tab0s10} from "../../BACKOFFICESETUP/entities/0002-Tab0s10-Distrito";
import { Zona } from './0004-Zona-Zona';
import { Almacen } from './0006-Almacen-Almacen';
import { Grupocliente } from './0005-Grupocliente-GrupoDelCliente';
@Index("PK_cliente", ["cdcliente"], { unique: true })
@Entity("cliente", { schema: "dbo" })
export class Cliente {
  //id:  ahora sera identity
  //bigint
  @Column("char"
  , { primary: true, name: "cdcliente", length: 20 })
  cdcliente: string;

  //ruc
  // varchar(20)
  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  //nombre del cliente
  //varchar
  @Column("char", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  //direccion del cliente
  //varchar
  @Column("char", { name: "drcliente", nullable: true, length: 120 })
  drcliente: string | null;

  //distrito: ahora sera un FK
  //campo en db: district_id
  // con saber el districto ya se sabe la provincia, el departamento y el pais
  @Column("char", { name: "cddistrito", nullable: true, length: 2 })
  cddistrito: string | null | Tab0s10;

  //departamente: esto muere
  // @Column("char", { name: "cddepartamento", nullable: true, length: 2 })
  // cddepartamento: string | null;

  //zona: ahora sera un FK, zona como   (centro, norte, noreste, residencial)?, no estara relacionado directamente con distrito
  // nombre en la db: zone_id
  @Column("char", { name: "cdzona", nullable: true, length: 5 })
  cdzona: string | null | Zona;

  //direccion de cobranza
  @Column("char", { name: "drcobranza", nullable: true, length: 60 })
  drcobranza: string | null;

  //direccion de entrega
  @Column("char", { name: "drentrega", nullable: true, length: 60 })
  drentrega: string | null;

  //LOS CAMPOS TELEFONOS SERAN REEMPLAZADOS POR UN NUEVO CAMPO
  //array varchar(20)
  phones: string[];
  // 
  @Column("char", { name: "tlfcliente", nullable: true, length: 15 })
  tlfcliente: string | null; //esto muere

  @Column("char", { name: "tlfcliente1", nullable: true, length: 15 })
  tlfcliente1: string | null; //esto muere
  //

  //fax 
  @Column("char", { name: "faxcliente", nullable: true, length: 15 })
  faxcliente: string | null;

  //?monto_limite (Bandera?)
  //?pendiente, en la db la mayoria tiene valor '0' y algunos 'S'
  @Column("char", { name: "monlimite", nullable: true, length: 1 })
  monlimite: string | null;

  //?monto_limite (numero)
  //? todos los valores en 0.00
  @Column("numeric", {
    name: "mtolimite",
    nullable: true,
    precision: 13,
    scale: 2,
  })
  mtolimite: number | null;

  //?monto_disponible 
  //?la mayoria de valores en 0.00
  //?el resto son negativos (-9440.00, -22921.81, -11806.90, etc)
  @Column("numeric", {
    name: "mtodisponible",
    nullable: true,
    precision: 13,
    scale: 2,
  })
  mtodisponible: number | null;

  //?bloqueo de credito, bandera
  //? solo valores de 0 y 1
  @Column("bit", { name: "bloqcredito", nullable: true })
  bloqcredito: boolean | null;

  //?email
  //varchar(120)
  @Column("varchar", { name: "emcliente", nullable: true, length: 220 })
  emcliente: string | null;

  //fecha de nacimiento
  // ahora sera date en la db
  @Column("smalldatetime", { name: "fecnacimiento", nullable: true })
  fecnacimiento: Date | null;

  //? codigo de almacen, referencia id no explicita en la db
  //? todos los campos en null
  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null | Almacen;

  //? tipo_cliente
  //? La mayoria de valores en '', algunos 'A', 'D' y 'C'
  @Column("char", { name: "tipocli", nullable: true, length: 1 })
  tipocli: string | null;

  //? dias de credito
  @Column("int", { name: "diascredito", nullable: true })
  diascredito: number | null;

  // ahora sera un FK
  // nombre en la db: client_group_id
  // la mayoria de valores en '',  y algunos '1' (con referencia no explicita en la db)
  @Column("char", { name: "CDGRUPOCLI", nullable: true, length: 5 })
  cdgrupocli: string | null | Grupocliente;

  //?sunat participa, solo valores 1, 0 y null
  @Column("tinyint", { name: "Sunat_Actualiza", nullable: true })
  sunatActualiza: number | null;

  //? todo esta en null, probablemente muera
  @Column("varchar", { name: "cliente", nullable: true, length: 60 })
  cliente: string | null;

  //contacto de entrega, delivery_contact
  @Column("varchar", { name: "contacto", nullable: true, length: 60 })
  contacto: string | null;

  //ahora se llamara en la db => created_at;
  //timestamp
  @Column("smalldatetime", {
    name: "fecha_creacion",
    nullable: true,
    default: () => "getdate()",
  })
  fechaCreacion: Date | null;

  //en la db todo esta en null, muere
  @Column("char", { name: "GRUPORUTA", nullable: true, length: 10 })
  gruporuta: string | null;

  //* en la db todo esta en 0, muere
  //* parece ser 'Dias Maximo Nota de Despacho'
  @Column("int", { name: "DIASMAX_ND", nullable: true, default: () => "(0)" })
  diasmaxNd: number | null;

  //* en la db todo esta en 0, muere
  //* parece ser 'NO Mostrar Precio en Nota de Despacho'
  @Column("bit", { name: "flgpreciond", nullable: true, default: () => "(0)" })
  flgpreciond: boolean | null;

  //? en la db la mayoria esta en null (~85%)
  //?probablemente muera
  @Column("bit", { name: "consulta_sunat", nullable: true })
  consultaSunat: boolean | null;

  //* en la db la mayoria esta en null (~95%)
  //* parace ser 'Clave para descuentos'
  @Column("bit", { name: "flg_pideclave", nullable: true })
  flgPideclave: boolean | null;

  // * en la db la mayoria esta en null (~95%)
  // * parece ser 'NO Mostrar total en Nota de Despacho'
  @Column("bit", { name: "flgtotalnd", nullable: true })
  flgtotalnd: boolean | null;

  // ! estos dos se combinan al parecer
  // ! parecen hacer referencia a 
  // ! 'Mostrar Saldo de Consumo en Nota de Despacho'
  @Column("bit", { name: "flgMostrarSaldo", nullable: true })
  flgMostrarSaldo: boolean | null;
  //* la mayoria en ambos tiene valor null y algunos en 0
  @Column("bit", { name: "FLGLNOMOSTRARSALDO", nullable: true })
  flglnomostrarsaldo: boolean | null;
  //! se creara un nuevo campo

  //? pendiente? aplica descuento automatico,
  //? la mayoria en null (~95%) algunos en 0 y otros en 1
  //? probablemente muera, no veo su campo en el formulario de la plataforma actual
  @Column("bit", { name: "flgAplicaDsctoAuto", nullable: true })
  flgAplicaDsctoAuto: boolean | null;

  //? pendiente, no se que es, la mayoria en 0 (99%) el resto en 1 y null
  @Column("bit", { name: "flgFactCentral", nullable: true })
  flgFactCentral: boolean | null;

  //no modificable, bandera
  //ahora se llamara is_modificable (ojo) pq se invierte el valor
  @Column("bit", { name: "flgNoModificable", nullable: true })
  flgNoModificable: boolean | null;

  // cuetas creditos, todo esta en 1
  //ahora sera un integer, default 1
  @Column("numeric", {
    name: "nrocuota",
    precision: 3,
    scale: 0,
    default: () => "(1)",
  })
  nrocuota: number;

  //? pendiente de saber para que es,
  //? todo en la db esta en 0
  // ahora sera boolean
  @Column("bit", { name: "retencion", default: () => "(0)" })
  retencion: boolean;
  
}
