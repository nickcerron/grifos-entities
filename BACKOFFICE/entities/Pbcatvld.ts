import { Column, Entity, Index } from "typeorm";

@Index("pbcatv_x", ["pbvName"], {})
@Entity("pbcatvld", { schema: "dbo" })
export class Pbcatvld {
  @Column("varchar", { name: "pbv_name", length: 30 })
  pbvName: string;

  @Column("varchar", { name: "pbv_vald", nullable: true, length: 254 })
  pbvVald: string | null;

  @Column("smallint", { name: "pbv_type", nullable: true })
  pbvType: number | null;

  @Column("int", { name: "pbv_cntr", nullable: true })
  pbvCntr: number | null;

  @Column("varchar", { name: "pbv_msg", nullable: true, length: 254 })
  pbvMsg: string | null;
}
