import { Column, Entity } from "typeorm";

@Entity("TmpIngresoDWebS", { schema: "dbo" })
export class TmpIngresoDWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "IngresoId", length: 5 })
  ingresoId: string;

  @Column("varchar", { name: "NroIngreso", length: 15 })
  nroIngreso: string;

  @Column("varchar", { name: "TipoDocId", length: 5 })
  tipoDocId: string;

  @Column("int", { name: "NroItem" })
  nroItem: number;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("float", { name: "Cantidad", nullable: true, precision: 53 })
  cantidad: number | null;

  @Column("float", { name: "Costo", nullable: true, precision: 53 })
  costo: number | null;

  @Column("float", { name: "MtoSubtotal", nullable: true, precision: 53 })
  mtoSubtotal: number | null;

  @Column("float", { name: "MtoImpuesto", nullable: true, precision: 53 })
  mtoImpuesto: number | null;

  @Column("float", { name: "MtoImpuesto1", nullable: true, precision: 53 })
  mtoImpuesto1: number | null;

  @Column("float", { name: "MtoTotal", nullable: true, precision: 53 })
  mtoTotal: number | null;

  @Column("int", { name: "Linea", nullable: true })
  linea: number | null;
}
