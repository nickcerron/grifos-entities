import { Column, Entity, Index } from "typeorm";

@Index("PK_AfiliacionValorPto", ["cdarticulo"], { unique: true })
@Entity("AfiliacionValorPto", { schema: "dbo" })
export class AfiliacionValorPto {
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("numeric", {
    name: "ValorPunto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  valorPunto: number | null;
}
