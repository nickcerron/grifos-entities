import { Column, Entity, Index } from 'typeorm';

@Index('PK_grupo04', ['cdgrupo04'], { unique: true })
@Entity('grupo04', { schema: 'dbo' })
export class Grupo04 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgrupo04', length: 5 })
  cdgrupo04: string;
  //name
  @Column('char', { name: 'dsgrupo04', nullable: true, length: 40 })
  dsgrupo04: string | null;
  //description
  @Column('varchar', { name: 'dagrupo04', nullable: true, length: 100 })
  dagrupo04: string | null;
}
