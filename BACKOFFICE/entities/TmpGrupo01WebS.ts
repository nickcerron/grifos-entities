import { Column, Entity } from "typeorm";

@Entity("TmpGrupo01WebS", { schema: "dbo" })
export class TmpGrupo01WebS {
  @Column("char", { name: "GrupoId01", length: 5 })
  grupoId01: string;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 40 })
  descripcion: string | null;
}
