import { Column, Entity } from "typeorm";

@Entity("LOG_PrecioCliente", { schema: "dbo" })
export class LogPrecioCliente {
  @Column("char", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("char", { name: "tipodes", nullable: true, length: 3 })
  tipodes: string | null;

  @Column("bit", { name: "licitacion", nullable: true, default: () => "(0)" })
  licitacion: boolean | null;

  @Column("varchar", { name: "nrocontrato", length: 120, default: () => "' '" })
  nrocontrato: string;

  @Column("int", { name: "cdPrecioCliente", nullable: true })
  cdPrecioCliente: number | null;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
