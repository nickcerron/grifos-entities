import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_guiag",
  ["cdlocal", "cdtipodoc", "cdTpGuia", "nroguia", "rucempresa"],
  { unique: true }
)
@Entity("guiag", { schema: "dbo" })
export class Guiag {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroguia", length: 10 })
  nroguia: string;

  @Column("smalldatetime", { name: "fecguia", nullable: true })
  fecguia: Date | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "nropos", length: 10 })
  nropos: string;

  @Column("char", { name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { primary: true, name: "CdTpGuia", length: 5 })
  cdTpGuia: string;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("char", { name: "EstadoRegistro", nullable: true, length: 1 })
  estadoRegistro: string | null;

  @Column("varchar", { name: "EstadoProceso", nullable: true, length: 15 })
  estadoProceso: string | null;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;
}
