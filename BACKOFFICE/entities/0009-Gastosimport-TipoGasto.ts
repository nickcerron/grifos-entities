import { Column, Entity, Index } from 'typeorm';

@Index('PK_gastosimport', ['cdgasto'], { unique: true })
@Entity('gastosimport', { schema: 'dbo' })
export class Gastosimport {
  //?TIPO DE GASTO

  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgasto', length: 5 })
  cdgasto: string;

  //name
  //nullable => false
  //
  @Column('char', { name: 'dsgasto', nullable: true, length: 40 })
  dsgasto: string | null;

  //?muere
  @Column('bit', { name: 'flgsistema', nullable: true })
  flgsistema: boolean | null;
}
