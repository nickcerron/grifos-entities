import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_F__3214EC0727BE5A24", ["id"], { unique: true })
@Entity("Kardex_Fisico_Productos", { schema: "dbo" })
export class KardexFisicoProductos {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;
}
