import { Column, Entity } from "typeorm";

@Entity("tab0s05", { schema: "dbo" })
export class Tab0s05 {
  @Column("char", { name: "cdvendedor", length: 10 })
  cdvendedor: string;

  @Column("char", { name: "dsvendedor", nullable: true, length: 60 })
  dsvendedor: string | null;

  @Column("char", { name: "drvendedor", nullable: true, length: 60 })
  drvendedor: string | null;

  @Column("char", { name: "rucvendedor", nullable: true, length: 15 })
  rucvendedor: string | null;

  @Column("char", { name: "tlfvendedor", nullable: true, length: 15 })
  tlfvendedor: string | null;

  @Column("char", { name: "tlfvendedor1", nullable: true, length: 15 })
  tlfvendedor1: string | null;
}
