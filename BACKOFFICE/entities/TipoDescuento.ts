import { Column, Entity, Index } from "typeorm";

@Index("PK_TipoDescuento", ["cdTpDescuento"], { unique: true })
@Entity("TipoDescuento", { schema: "dbo" })
export class TipoDescuento {
  @Column("char", { primary: true, name: "cdTPDescuento", length: 4 })
  cdTpDescuento: string;

  @Column("varchar", { name: "dsTPDescuento", length: 40 })
  dsTpDescuento: string;
}
