import { Column, Entity } from "typeorm";

@Entity("PARAMETRO_BAK_2018", { schema: "dbo" })
export class ParametroBak_2018 {
  @Column("char", { name: "monsistema", nullable: true, length: 1 })
  monsistema: string | null;

  @Column("char", { name: "monticket", nullable: true, length: 1 })
  monticket: string | null;

  @Column("char", { name: "tpsalida", nullable: true, length: 5 })
  tpsalida: string | null;

  @Column("char", { name: "tpingreso", nullable: true, length: 5 })
  tpingreso: string | null;

  @Column("numeric", {
    name: "coloron",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  coloron: number | null;

  @Column("numeric", {
    name: "coloroff",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  coloroff: number | null;

  @Column("numeric", {
    name: "colorgrid",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  colorgrid: number | null;

  @Column("char", { name: "masccantidad", nullable: true, length: 15 })
  masccantidad: string | null;

  @Column("char", { name: "masccantidadf", nullable: true, length: 15 })
  masccantidadf: string | null;

  @Column("char", { name: "mascprecio", nullable: true, length: 15 })
  mascprecio: string | null;

  @Column("char", { name: "masccosto", nullable: true, length: 15 })
  masccosto: string | null;

  @Column("char", { name: "masctotal", nullable: true, length: 15 })
  masctotal: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("bit", { name: "flgtalla", nullable: true })
  flgtalla: boolean | null;

  @Column("bit", { name: "flgformula", nullable: true })
  flgformula: boolean | null;

  @Column("bit", { name: "precioconigv", nullable: true })
  precioconigv: boolean | null;

  @Column("bit", { name: "precioconservicio", nullable: true })
  precioconservicio: boolean | null;

  @Column("numeric", {
    name: "porservicio",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  porservicio: number | null;

  @Column("smalldatetime", { name: "fecinstall", nullable: true })
  fecinstall: Date | null;

  @Column("char", { name: "tpcompra", nullable: true, length: 5 })
  tpcompra: string | null;

  @Column("char", { name: "nrocompra", nullable: true, length: 10 })
  nrocompra: string | null;

  @Column("char", { name: "tpcambiotalla", nullable: true, length: 5 })
  tpcambiotalla: string | null;

  @Column("char", { name: "tpanulacion", nullable: true, length: 5 })
  tpanulacion: string | null;

  @Column("char", { name: "tpinicial", nullable: true, length: 5 })
  tpinicial: string | null;

  @Column("char", { name: "tptransferencia", nullable: true, length: 5 })
  tptransferencia: string | null;

  @Column("char", { name: "tptransferenciainterna", nullable: true, length: 5 })
  tptransferenciainterna: string | null;

  @Column("bit", { name: "utilidadcosto", nullable: true })
  utilidadcosto: boolean | null;

  @Column("bit", { name: "flgintegrador", nullable: true })
  flgintegrador: boolean | null;

  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("numeric", {
    name: "nropago",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nropago: number | null;

  @Column("bit", { name: "stknegativo", nullable: true })
  stknegativo: boolean | null;

  @Column("numeric", {
    name: "nrocdbarra",
    nullable: true,
    precision: 7,
    scale: 0,
  })
  nrocdbarra: number | null;

  @Column("char", { name: "cdgrupocombustible", nullable: true, length: 5 })
  cdgrupocombustible: string | null;

  @Column("char", { name: "cdgrupovtaplaya", nullable: true, length: 5 })
  cdgrupovtaplaya: string | null;

  @Column("numeric", {
    name: "digitoruc",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  digitoruc: number | null;

  @Column("bit", { name: "conexiondispensador", nullable: true })
  conexiondispensador: boolean | null;

  @Column("numeric", {
    name: "nrocara",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nrocara: number | null;

  @Column("char", { name: "tpguiatransferencia", nullable: true, length: 5 })
  tpguiatransferencia: string | null;

  @Column("bit", { name: "flggrifo", nullable: true })
  flggrifo: boolean | null;

  @Column("char", { name: "nrovale", nullable: true, length: 10 })
  nrovale: string | null;

  @Column("numeric", {
    name: "nrotransgasboy",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  nrotransgasboy: number | null;

  @Column("bit", { name: "zenpantalla", nullable: true })
  zenpantalla: boolean | null;

  @Column("char", { name: "cdletrainicial", nullable: true, length: 2 })
  cdletrainicial: string | null;

  @Column("char", { name: "nroimportacion", nullable: true, length: 10 })
  nroimportacion: string | null;

  @Column("numeric", {
    name: "digitocdcliente",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  digitocdcliente: number | null;

  @Column("bit", { name: "flgcreaprodmov", nullable: true })
  flgcreaprodmov: boolean | null;

  @Column("numeric", {
    name: "porcipm",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porcipm: number | null;

  @Column("char", { name: "tpingimportacion", nullable: true, length: 5 })
  tpingimportacion: string | null;

  @Column("bit", { name: "flgvalidaruc", nullable: true })
  flgvalidaruc: boolean | null;

  @Column("numeric", {
    name: "cant_turno",
    nullable: true,
    precision: 1,
    scale: 0,
  })
  cantTurno: number | null;

  @Column("char", { name: "tppgocanje", nullable: true, length: 5 })
  tppgocanje: string | null;

  @Column("char", { name: "prefcredlocal", nullable: true, length: 10 })
  prefcredlocal: string | null;

  @Column("char", { name: "prefcredcorp", nullable: true, length: 10 })
  prefcredcorp: string | null;

  @Column("char", { name: "prefbonus", nullable: true, length: 10 })
  prefbonus: string | null;

  @Column("numeric", {
    name: "ptobonus",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  ptobonus: number | null;

  @Column("bit", { name: "bloqventaplaya", nullable: true })
  bloqventaplaya: boolean | null;

  @Column("char", { name: "cdestacion", nullable: true, length: 6 })
  cdestacion: string | null;

  @Column("char", { name: "nivelserafin", nullable: true, length: 2 })
  nivelserafin: string | null;

  @Column("char", { name: "cd_estacion", nullable: true, length: 10 })
  cdEstacion: string | null;

  @Column("numeric", {
    name: "intervaltimer",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  intervaltimer: number | null;

  @Column("numeric", {
    name: "minutosxtktbol",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  minutosxtktbol: number | null;

  @Column("char", { name: "tptransftienda", nullable: true, length: 5 })
  tptransftienda: string | null;

  @Column("numeric", {
    name: "nrodeposito",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nrodeposito: number | null;

  @Column("numeric", {
    name: "longtarjeta",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  longtarjeta: number | null;

  @Column("bit", { name: "saltoauto", nullable: true })
  saltoauto: boolean | null;

  @Column("char", { name: "lin1display", nullable: true, length: 40 })
  lin1display: string | null;

  @Column("char", { name: "lin2display", nullable: true, length: 40 })
  lin2display: string | null;

  @Column("char", { name: "tipocontrol", nullable: true, length: 10 })
  tipocontrol: string | null;

  @Column("numeric", {
    name: "mtominimodni",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtominimodni: number | null;

  @Column("bit", { name: "tarjcredplaca", nullable: true })
  tarjcredplaca: boolean | null;

  @Column("bit", { name: "flgprintndespacho", nullable: true })
  flgprintndespacho: boolean | null;

  @Column("char", { name: "cdcliprintndespacho", nullable: true, length: 15 })
  cdcliprintndespacho: string | null;

  @Column("char", { name: "cdtipodocautomatico", nullable: true, length: 5 })
  cdtipodocautomatico: string | null;

  @Column("char", { name: "cdclienteautomatico", nullable: true, length: 15 })
  cdclienteautomatico: string | null;

  @Column("bit", { name: "flgSistema01", nullable: true })
  flgSistema01: boolean | null;

  @Column("bit", { name: "flgSistema02", nullable: true })
  flgSistema02: boolean | null;

  @Column("bit", { name: "flgSistema03", nullable: true })
  flgSistema03: boolean | null;

  @Column("bit", { name: "flgcontometro", nullable: true })
  flgcontometro: boolean | null;

  @Column("bit", { name: "flgsolotienda", nullable: true })
  flgsolotienda: boolean | null;

  @Column("numeric", {
    name: "mtominautomatico",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtominautomatico: number | null;

  @Column("varchar", { name: "VERSIONBO", nullable: true, length: 250 })
  versionbo: string | null;

  @Column("varchar", { name: "VERSIONtInven", nullable: true, length: 250 })
  versioNtInven: string | null;

  @Column("varchar", { name: "VERSIONplaya", nullable: true, length: 250 })
  versioNplaya: string | null;

  @Column("varchar", { name: "VERSIONtienda", nullable: true, length: 250 })
  versioNtienda: string | null;

  @Column("bit", { name: "flgmostrar_precio_orig", nullable: true })
  flgmostrarPrecioOrig: boolean | null;

  @Column("bit", { name: "VERTIPOSVENTA", nullable: true })
  vertiposventa: boolean | null;

  @Column("char", { name: "TipoAfiliacion", nullable: true, length: 1 })
  tipoAfiliacion: string | null;

  @Column("char", { name: "TipoPtoAfiliacion", nullable: true, length: 1 })
  tipoPtoAfiliacion: string | null;

  @Column("int", { name: "CantDigitos_Tarjpromo", nullable: true })
  cantDigitosTarjpromo: number | null;

  @Column("bit", { name: "redondeaSolesCombus", nullable: true })
  redondeaSolesCombus: boolean | null;

  @Column("tinyint", { name: "TipoCierreXTienda", nullable: true })
  tipoCierreXTienda: number | null;

  @Column("tinyint", { name: "Cursor_tienda", nullable: true })
  cursorTienda: number | null;

  @Column("tinyint", { name: "repite_articulo", nullable: true })
  repiteArticulo: number | null;

  @Column("numeric", {
    name: "mtocupon",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtocupon: number | null;

  @Column("numeric", {
    name: "MOdifica_precio_tienda",
    nullable: true,
    precision: 10,
    scale: 2,
  })
  mOdificaPrecioTienda: number | null;

  @Column("tinyint", { name: "imprime_canjeweb", nullable: true })
  imprimeCanjeweb: number | null;

  @Column("numeric", {
    name: "precio_varios",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precioVarios: number | null;

  @Column("char", {
    name: "SerieHd_Imprime_Ticket_Web",
    nullable: true,
    length: 15,
  })
  serieHdImprimeTicketWeb: string | null;

  @Column("bit", { name: "pantalland_cliprintnd", nullable: true })
  pantallandCliprintnd: boolean | null;

  @Column("bit", { name: "imprime_total_dispensado", nullable: true })
  imprimeTotalDispensado: boolean | null;

  @Column("tinyint", { name: "imprime_ptosacumulados", nullable: true })
  imprimePtosacumulados: number | null;

  @Column("tinyint", { name: "tarjeta_actu_cdcliente", nullable: true })
  tarjetaActuCdcliente: number | null;

  @Column("tinyint", { name: "boton_transfer_gratuita", nullable: true })
  botonTransferGratuita: number | null;

  @Column("bit", { name: "imprime_clientes_credito", nullable: true })
  imprimeClientesCredito: boolean | null;

  @Column("tinyint", { name: "tienda_cant_neg", nullable: true })
  tiendaCantNeg: number | null;

  @Column("tinyint", { name: "imprime_tiketera", nullable: true })
  imprimeTiketera: number | null;

  @Column("tinyint", { name: "imprime_nvta", nullable: true })
  imprimeNvta: number | null;

  @Column("tinyint", { name: "Modifica_depositos_parte", nullable: true })
  modificaDepositosParte: number | null;

  @Column("tinyint", { name: "mostrar_local_gastos", nullable: true })
  mostrarLocalGastos: number | null;

  @Column("char", { name: "Notad_cdarticulo", nullable: true, length: 10 })
  notadCdarticulo: string | null;

  @Column("tinyint", { name: "imprimir_cdarticulo_config", nullable: true })
  imprimirCdarticuloConfig: number | null;

  @Column("tinyint", { name: "Mostrar_IGV_Pantalla", nullable: true })
  mostrarIgvPantalla: number | null;

  @Column("tinyint", { name: "tipo_menu", nullable: true })
  tipoMenu: number | null;

  @Column("tinyint", { name: "nd_playa", nullable: true })
  ndPlaya: number | null;

  @Column("tinyint", { name: "valida_cdarticulo", nullable: true })
  validaCdarticulo: number | null;

  @Column("tinyint", { name: "Conf_cierrex", nullable: true })
  confCierrex: number | null;

  @Column("tinyint", { name: "cierreX_formatos", nullable: true })
  cierreXFormatos: number | null;

  @Column("tinyint", { name: "Imprime_fact_playa", nullable: true })
  imprimeFactPlaya: number | null;

  @Column("tinyint", { name: "credplaca", nullable: true })
  credplaca: number | null;

  @Column("tinyint", { name: "cierre_especial", nullable: true })
  cierreEspecial: number | null;

  @Column("tinyint", { name: "parte_tienda", nullable: true })
  parteTienda: number | null;

  @Column("tinyint", { name: "FLG_DESC_PREFIJO", nullable: true })
  flgDescPrefijo: number | null;

  @Column("varchar", { name: "RUTA_BACKUP", nullable: true, length: 50 })
  rutaBackup: string | null;

  @Column("tinyint", { name: "cierre_kardex", nullable: true })
  cierreKardex: number | null;

  @Column("varchar", { name: "valorcanje_regvta", nullable: true, length: 20 })
  valorcanjeRegvta: string | null;

  @Column("bit", { name: "ACTIVASAWA", nullable: true })
  activasawa: boolean | null;

  @Column("bit", { name: "ACTIVADISPENSADOR", nullable: true })
  activadispensador: boolean | null;

  @Column("nchar", { name: "BBDDSETUP", nullable: true, length: 25 })
  bbddsetup: string | null;

  @Column("bit", { name: "DESANULAR", nullable: true })
  desanular: boolean | null;

  @Column("char", { name: "GUIA", nullable: true, length: 10 })
  guia: string | null;

  @Column("char", { name: "GUIAIMPR", nullable: true, length: 10 })
  guiaimpr: string | null;

  @Column("char", { name: "GUIAFMT", nullable: true, length: 10 })
  guiafmt: string | null;

  @Column("bit", { name: "punto_nd", nullable: true })
  puntoNd: boolean | null;

  @Column("tinyint", { name: "noconectawpuntos", nullable: true })
  noconectawpuntos: number | null;

  @Column("char", { name: "cddepart_base", nullable: true, length: 2 })
  cddepartBase: string | null;

  @Column("bit", { name: "triveno", nullable: true })
  triveno: boolean | null;

  @Column("char", { name: "factura_c", nullable: true, length: 10 })
  facturaC: string | null;

  @Column("varchar", { name: "facturaimpre_c", nullable: true, length: 100 })
  facturaimpreC: string | null;

  @Column("varchar", { name: "facturafmt_c", nullable: true, length: 50 })
  facturafmtC: string | null;

  @Column("bit", { name: "pd_separaglp", nullable: true })
  pdSeparaglp: boolean | null;

  @Column("numeric", {
    name: "diasresetptos",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  diasresetptos: number | null;

  @Column("bit", { name: "cambioturno", nullable: true })
  cambioturno: boolean | null;

  @Column("bit", { name: "iniciodia", nullable: true })
  iniciodia: boolean | null;

  @Column("bit", { name: "flgvalida_nrovale", nullable: true })
  flgvalidaNrovale: boolean | null;

  @Column("bit", { name: "arequipa", nullable: true })
  arequipa: boolean | null;

  @Column("bit", { name: "cancelado", nullable: true })
  cancelado: boolean | null;

  @Column("bit", { name: "correlativos2_ticket", nullable: true })
  correlativos2Ticket: boolean | null;

  @Column("bit", { name: "chkclienteDia", nullable: true })
  chkclienteDia: boolean | null;

  @Column("bit", { name: "escirsa", nullable: true })
  escirsa: boolean | null;

  @Column("bit", { name: "FLGCIERRATURNOXCAJA", nullable: true })
  flgcierraturnoxcaja: boolean | null;

  @Column("bit", { name: "flgruta", nullable: true })
  flgruta: boolean | null;

  @Column("int", { name: "GALONES_DECIMALES", nullable: true })
  galonesDecimales: number | null;

  @Column("bit", { name: "flg_prefij_seriesdoc", nullable: true })
  flgPrefijSeriesdoc: boolean | null;

  @Column("bit", { name: "Mostrar_Articulo_Kardex", nullable: true })
  mostrarArticuloKardex: boolean | null;

  @Column("int", { name: "tiendagazel", nullable: true })
  tiendagazel: number | null;

  @Column("varchar", { name: "tpsalmerma", nullable: true, length: 5 })
  tpsalmerma: string | null;

  @Column("varchar", { name: "tpsaldev", nullable: true, length: 5 })
  tpsaldev: string | null;

  @Column("numeric", {
    name: "Redondeo",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  redondeo: number | null;

  @Column("int", { name: "terminalserver", nullable: true })
  terminalserver: number | null;

  @Column("bit", { name: "flg_BotonTiendaenPlaya", nullable: true })
  flgBotonTiendaenPlaya: boolean | null;

  @Column("bit", { name: "Valida_Correlativo", nullable: true })
  validaCorrelativo: boolean | null;

  @Column("varchar", { name: "ruta_websaldos", nullable: true, length: 200 })
  rutaWebsaldos: string | null;

  @Column("bit", { name: "flg_invent_fisicoteorico", nullable: true })
  flgInventFisicoteorico: boolean | null;

  @Column("bit", { name: "flg_botoncredito", nullable: true })
  flgBotoncredito: boolean | null;

  @Column("bit", { name: "flg_nobuscar_nombre", nullable: true })
  flgNobuscarNombre: boolean | null;

  @Column("bit", { name: "consulta_sunat", nullable: true })
  consultaSunat: boolean | null;

  @Column("bit", { name: "clubgazel", nullable: true })
  clubgazel: boolean | null;

  @Column("varchar", { name: "ruta_webclubgazel", nullable: true, length: 200 })
  rutaWebclubgazel: string | null;

  @Column("bit", { name: "activa_camedi", nullable: true })
  activaCamedi: boolean | null;

  @Column("bit", { name: "tarjeta_mascara", nullable: true })
  tarjetaMascara: boolean | null;

  @Column("char", { name: "tipo_canje", nullable: true, length: 1 })
  tipoCanje: string | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("varchar", { name: "ruta_ws_easytaxy", nullable: true, length: 200 })
  rutaWsEasytaxy: string | null;

  @Column("bit", { name: "activa_elsol", nullable: true })
  activaElsol: boolean | null;

  @Column("varchar", { name: "cod_viatico", nullable: true, length: 20 })
  codViatico: string | null;

  @Column("bit", { name: "flg_boton_facturacionmanual", nullable: true })
  flgBotonFacturacionmanual: boolean | null;

  @Column("bit", { name: "mostrar_ptos_ganados", nullable: true })
  mostrarPtosGanados: boolean | null;

  @Column("bit", { name: "activa_formas_pago", nullable: true })
  activaFormasPago: boolean | null;

  @Column("numeric", {
    name: "mtominimodni_sunat",
    nullable: true,
    precision: 7,
    scale: 2,
  })
  mtominimodniSunat: number | null;

  @Column("bit", { name: "flg_pideplaca", nullable: true })
  flgPideplaca: boolean | null;

  @Column("bit", { name: "depositos_playa", nullable: true })
  depositosPlaya: boolean | null;

  @Column("bit", { name: "flg_anula_easytaxi", nullable: true })
  flgAnulaEasytaxi: boolean | null;

  @Column("numeric", {
    name: "mtominideposito",
    nullable: true,
    precision: 7,
    scale: 2,
  })
  mtominideposito: number | null;

  @Column("bit", { name: "ConIGV", nullable: true })
  conIgv: boolean | null;

  @Column("numeric", {
    name: "ValorIGV",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  valorIgv: number | null;

  @Column("bit", { name: "flg_pideplacatb", nullable: true })
  flgPideplacatb: boolean | null;

  @Column("varchar", { name: "horacierrepd", nullable: true, length: 5 })
  horacierrepd: string | null;

  @Column("bit", { name: "activa_repro_stock", nullable: true })
  activaReproStock: boolean | null;

  @Column("bit", { name: "flg_nc_liberand", nullable: true })
  flgNcLiberand: boolean | null;

  @Column("bit", { name: "flg_valfecpos", nullable: true })
  flgValfecpos: boolean | null;

  @Column("int", { name: "rango_valfecpos", nullable: true })
  rangoValfecpos: number | null;

  @Column("bit", { name: "flg_anulapos", nullable: true })
  flgAnulapos: boolean | null;

  @Column("varchar", { name: "interfaz", nullable: true, length: 10 })
  interfaz: string | null;

  @Column("bit", { name: "flg_fecsrv", nullable: true })
  flgFecsrv: boolean | null;

  @Column("char", { name: "prefflotlocal", nullable: true, length: 10 })
  prefflotlocal: string | null;

  @Column("bit", { name: "flg_transfer_gratuita_cero", nullable: true })
  flgTransferGratuitaCero: boolean | null;

  @Column("bit", { name: "flg_valdscto", nullable: true })
  flgValdscto: boolean | null;

  @Column("char", { name: "tppgogasto", nullable: true, length: 15 })
  tppgogasto: string | null;

  @Column("bit", { name: "flg_pideclavecred", nullable: true })
  flgPideclavecred: boolean | null;

  @Column("bit", { name: "flg_pideodometro", nullable: true })
  flgPideodometro: boolean | null;

  @Column("bit", { name: "flg_modo_fact", nullable: true })
  flgModoFact: boolean | null;

  @Column("numeric", {
    name: "colum_termica",
    nullable: true,
    precision: 2,
    scale: 0,
  })
  columTermica: number | null;

  @Column("bit", { name: "Label_Bellavista", nullable: true })
  labelBellavista: boolean | null;

  @Column("numeric", {
    name: "mtominimodetraccion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtominimodetraccion: number | null;

  @Column("bit", { name: "Nd_imp_saldoyconsumo", nullable: true })
  ndImpSaldoyconsumo: boolean | null;

  @Column("int", { name: "nro_caracteres_rsocial", nullable: true })
  nroCaracteresRsocial: number | null;

  @Column("char", { name: "fe_fecValida", nullable: true, length: 8 })
  feFecValida: string | null;

  @Column("bit", { name: "valida_fecha_playa", nullable: true })
  validaFechaPlaya: boolean | null;

  @Column("bit", { name: "flg_valida_fecproce_dia", nullable: true })
  flgValidaFecproceDia: boolean | null;

  @Column("int", { name: "impr_Veces_ND", nullable: true })
  imprVecesNd: number | null;

  @Column("int", { name: "impr_Veces_fac", nullable: true })
  imprVecesFac: number | null;

  @Column("int", { name: "impr_Veces_bol", nullable: true })
  imprVecesBol: number | null;

  @Column("bit", { name: "flg_canjend", nullable: true })
  flgCanjend: boolean | null;

  @Column("varchar", { name: "nroversionBO", nullable: true, length: 50 })
  nroversionBo: string | null;

  @Column("varchar", { name: "nroversionplaya", nullable: true, length: 50 })
  nroversionplaya: string | null;

  @Column("varchar", { name: "nroversiontienda", nullable: true, length: 50 })
  nroversiontienda: string | null;

  @Column("varchar", { name: "nroversionTI", nullable: true, length: 50 })
  nroversionTi: string | null;

  @Column("bit", { name: "flg_nrodias", nullable: true })
  flgNrodias: boolean | null;

  @Column("bit", { name: "flg_credito_centralizado", nullable: true })
  flgCreditoCentralizado: boolean | null;

  @Column("bit", { name: "flgsoloterminal", nullable: true })
  flgsoloterminal: boolean | null;

  @Column("bit", { name: "flg_round_dec_indecopi", nullable: true })
  flgRoundDecIndecopi: boolean | null;

  @Column("bit", { name: "flg_round_indecopi_1_9", nullable: true })
  flgRoundIndecopi_1_9: boolean | null;

  @Column("varchar", { name: "CONEXIONWEB", nullable: true, length: 50 })
  conexionweb: string | null;

  @Column("varchar", { name: "INSTANCIAWEB", nullable: true, length: 50 })
  instanciaweb: string | null;

  @Column("varchar", { name: "BDWEB", nullable: true, length: 50 })
  bdweb: string | null;

  @Column("varchar", { name: "USERWEB", nullable: true, length: 50 })
  userweb: string | null;

  @Column("varchar", { name: "PASSWORDWEB", nullable: true, length: 50 })
  passwordweb: string | null;

  @Column("bit", { name: "FLG_PDF417", nullable: true })
  flgPdf417: boolean | null;

  @Column("varchar", { name: "ruta_qr_jpg", nullable: true, length: 220 })
  rutaQrJpg: string | null;

  @Column("varchar", {
    name: "Mto_desc_Descripcion",
    nullable: true,
    length: 67,
  })
  mtoDescDescripcion: string | null;

  @Column("numeric", {
    name: "nroinventario",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  nroinventario: number | null;

  @Column("bit", { name: "flg_kardex_unalinea", nullable: true })
  flgKardexUnalinea: boolean | null;

  @Column("char", { name: "flg_invent_2", nullable: true, length: 10 })
  flgInvent_2: string | null;

  @Column("bit", { name: "flg_facturacion_automatica", nullable: true })
  flgFacturacionAutomatica: boolean | null;

  @Column("numeric", {
    name: "mto_facturacion_automatica",
    nullable: true,
    precision: 10,
    scale: 0,
  })
  mtoFacturacionAutomatica: number | null;

  @Column("bit", { name: "flg_btn_credito_playa", nullable: true })
  flgBtnCreditoPlaya: boolean | null;

  @Column("bit", { name: "flg_validateclas_cdcliente", nullable: true })
  flgValidateclasCdcliente: boolean | null;

  @Column("bit", { name: "flg_activa_TI_todosProd", nullable: true })
  flgActivaTiTodosProd: boolean | null;

  @Column("bit", { name: "flg_boton_promo", nullable: true })
  flgBotonPromo: boolean | null;

  @Column("bit", { name: "flg_gastos_playa", nullable: true })
  flgGastosPlaya: boolean | null;

  @Column("bit", { name: "flg_ocultar_campos_tck", nullable: true })
  flgOcultarCamposTck: boolean | null;

  @Column("bit", { name: "flg_print_qr", nullable: true })
  flgPrintQr: boolean | null;

  @Column("bit", { name: "flg_repx_terminal", nullable: true })
  flgRepxTerminal: boolean | null;

  @Column("bit", { name: "flg_cliente_automatico", nullable: true })
  flgClienteAutomatico: boolean | null;

  @Column("varchar", {
    name: "cdcliente_automatico",
    nullable: true,
    length: 20,
  })
  cdclienteAutomatico: string | null;

  @Column("varchar", {
    name: "rscliente_automatico",
    nullable: true,
    length: 120,
  })
  rsclienteAutomatico: string | null;

  @Column("bit", { name: "Desactivar_FoxyPreviewer", nullable: true })
  desactivarFoxyPreviewer: boolean | null;

  @Column("varchar", {
    name: "Msg_Anula_Documento",
    nullable: true,
    length: 200,
  })
  msgAnulaDocumento: string | null;

  @Column("bit", { name: "flg_notas_multiref", nullable: true })
  flgNotasMultiref: boolean | null;

  @Column("bit", { name: "FLG_AFECTARCOSTO_FLETECOMPRAS", nullable: true })
  flgAfectarcostoFletecompras: boolean | null;

  @Column("bit", { name: "flg_ImprimirND_Menos5S", nullable: true })
  flgImprimirNdMenos5S: boolean | null;

  @Column("bit", { name: "flg_OcultarVta_Menos5S", nullable: true })
  flgOcultarVtaMenos5S: boolean | null;

  @Column("bit", { name: "flg_noaplica_desc_tarj", nullable: true })
  flgNoaplicaDescTarj: boolean | null;

  @Column("bit", { name: "flg_activar_clientes_varios", nullable: true })
  flgActivarClientesVarios: boolean | null;
}
