import { Column, Entity, Index } from "typeorm";

@Index("PK_ndebito", ["cdlocal", "nrondebito"], { unique: true })
@Entity("ndebito", { schema: "dbo" })
export class Ndebito {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nrondebito", length: 10 })
  nrondebito: string;

  @Column("smalldatetime", { name: "fecndebito", nullable: true })
  fecndebito: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 12,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("varchar", { name: "observacion_anula", nullable: true, length: 250 })
  observacionAnula: string | null;
}
