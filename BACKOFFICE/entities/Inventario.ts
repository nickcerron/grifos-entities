import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_inventario",
  ["cdlocal", "cdalmacen", "cdinventario", "cdarticulo", "talla"],
  { unique: true }
)
@Entity("inventario", { schema: "dbo" })
export class Inventario {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { primary: true, name: "cdinventario", length: 5 })
  cdinventario: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdgrupo01", length: 5 })
  cdgrupo01: string;

  @Column("char", { name: "cdgrupo02", length: 5 })
  cdgrupo02: string;

  @Column("char", { name: "cdgrupo03", length: 5 })
  cdgrupo03: string;

  @Column("char", { primary: true, name: "talla", length: 10 })
  talla: string;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "stockteorico",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockteorico: number | null;

  @Column("numeric", {
    name: "stockfisico",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockfisico: number | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;

  @Column("datetime", { name: "fecinventario", nullable: true })
  fecinventario: Date | null;

  @Column("numeric", {
    name: "stockinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockinventario: number | null;

  @Column("char", { name: "monctoinventario", nullable: true, length: 1 })
  monctoinventario: string | null;

  @Column("numeric", {
    name: "ctoinventario",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoinventario: number | null;

  @Column("char", { name: "monctoprom", nullable: true, length: 1 })
  monctoprom: string | null;

  @Column("numeric", {
    name: "ctopromedio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctopromedio: number | null;

  @Column("char", { name: "monctorepo", nullable: true, length: 1 })
  monctorepo: string | null;

  @Column("numeric", {
    name: "ctoreposicion",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ctoreposicion: number | null;

  @Column("datetime", { name: "fecimportado", nullable: true })
  fecimportado: Date | null;
}
