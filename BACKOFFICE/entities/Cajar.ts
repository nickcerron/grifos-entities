import { Column, Entity, Index } from "typeorm";

@Index("PK_cajar", ["cdlocal", "fecproceso"], { unique: true })
@Entity("cajar", { schema: "dbo" })
export class Cajar {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;
}
