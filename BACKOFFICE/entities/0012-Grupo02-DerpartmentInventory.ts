import { Column, Entity, Index } from 'typeorm';

@Index('PK_grupo02', ['cdgrupo02'], { unique: true })
@Entity('grupo02', { schema: 'dbo' })
export class Grupo02 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgrupo02', length: 5 })
  cdgrupo02: string;

  //name
  //length => 40
  @Column('varchar', { name: 'dsgrupo02', nullable: true, length: 100 })
  dsgrupo02: string | null;
  //description
  @Column('varchar', { name: 'dagrupo02', nullable: true, length: 100 })
  dagrupo02: string | null;
}
