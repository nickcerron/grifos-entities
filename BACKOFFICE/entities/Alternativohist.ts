import { Column, Entity } from "typeorm";

@Entity("alternativohist", { schema: "dbo" })
export class Alternativohist {
  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdalterna", nullable: true, length: 20 })
  cdalterna: string | null;

  @Column("smalldatetime", { name: "fecmodifica", nullable: true })
  fecmodifica: Date | null;

  @Column("char", { name: "usumodifica", nullable: true, length: 10 })
  usumodifica: string | null;
}
