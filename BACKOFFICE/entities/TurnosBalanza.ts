import { Column, Entity } from "typeorm";

@Entity("TurnosBalanza", { schema: "dbo" })
export class TurnosBalanza {
  @Column("int", { name: "turno", nullable: true })
  turno: number | null;

  @Column("char", { name: "desde", nullable: true, length: 5 })
  desde: string | null;

  @Column("char", { name: "hasta", nullable: true, length: 5 })
  hasta: string | null;
}
