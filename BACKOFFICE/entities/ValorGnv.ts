import { Column, Entity, Index } from "typeorm";

@Index("PK_Valor_GNV", ["varId"], { unique: true })
@Entity("Valor_GNV", { schema: "dbo" })
export class ValorGnv {
  @Column("int", { primary: true, name: "VarId" })
  varId: number;

  @Column("varchar", { name: "TipoVar", nullable: true, length: 15 })
  tipoVar: string | null;

  @Column("varchar", { name: "Clave", nullable: true, length: 30 })
  clave: string | null;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 30 })
  descripcion: string | null;

  @Column("float", {
    name: "ValorPto",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  valorPto: number | null;

  @Column("nchar", {
    name: "Config",
    nullable: true,
    length: 40,
    default: () => "''",
  })
  config: string | null;
}
