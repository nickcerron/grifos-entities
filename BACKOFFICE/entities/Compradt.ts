import { Column, Entity, Index } from "typeorm";

@Index("PK_compradt", ["ubicacion", "cdarticulo"], { unique: true })
@Entity("compradt", { schema: "dbo" })
export class Compradt {
  @Column("char", { primary: true, name: "ubicacion", length: 10 })
  ubicacion: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;
}
