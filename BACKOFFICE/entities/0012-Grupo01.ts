import { Column, Entity, Index } from 'typeorm';

@Index('PK_grupo01', ['cdgrupo01'], { unique: true })
@Entity('grupo01', { schema: 'dbo' })
export class Grupo01 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgrupo01', length: 5 })
  cdgrupo01: string;

  //name
  @Column('varchar', { name: 'dsgrupo01', nullable: true, length: 100 })
  dsgrupo01: string | null;

  //description
  @Column('varchar', { name: 'dagrupo01', nullable: true, length: 100 })
  dagrupo01: string | null;
}
