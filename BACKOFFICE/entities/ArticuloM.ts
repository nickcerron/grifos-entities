import { Column, Entity } from "typeorm";

@Entity("articulo_m", { schema: "dbo" })
export class ArticuloM {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;
}
