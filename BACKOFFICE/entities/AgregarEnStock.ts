import { Column, Entity } from "typeorm";

@Entity("AGREGAR_EN_STOCK", { schema: "dbo" })
export class AgregarEnStock {
  @Column("varchar", { name: "CDARTICULO", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "STOCKACTUAL",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  stockactual: number | null;

  @Column("numeric", {
    name: "CANTIDAD",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;
}
