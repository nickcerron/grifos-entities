import { Column, Entity } from "typeorm";

@Entity("TmpVentaPWebS", { schema: "dbo" })
export class TmpVentaPWebS {
  @Column("int", { name: "Empresa" })
  empresa: number;

  @Column("int", { name: "Local" })
  local: number;

  @Column("varchar", { name: "SerieMaq", length: 20 })
  serieMaq: string;

  @Column("varchar", { name: "TipoD", length: 5 })
  tipoD: string;

  @Column("char", { name: "NroDocumento", length: 10 })
  nroDocumento: string;

  @Column("char", { name: "TipoPagoId", length: 5 })
  tipoPagoId: string;

  @Column("varchar", { name: "NroCuenta", length: 20 })
  nroCuenta: string;

  @Column("varchar", { name: "NroCheque", length: 20 })
  nroCheque: string;

  @Column("char", { name: "TarjetaCreditoId", length: 2 })
  tarjetaCreditoId: string;

  @Column("varchar", { name: "NroTarjetaCredito", length: 20 })
  nroTarjetaCredito: string;

  @Column("float", { name: "mtopago", nullable: true, precision: 53 })
  mtopago: number | null;

  @Column("float", {
    name: "mtoPagoDol",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoPagoDol: number | null;

  @Column("float", {
    name: "mtoTotalDol",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  mtoTotalDol: number | null;
}
