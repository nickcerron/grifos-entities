import { Column, Entity } from "typeorm";

@Entity("TipoRemito", { schema: "dbo" })
export class TipoRemito {
  @Column("char", { name: "cdremito", nullable: true, length: 5 })
  cdremito: string | null;

  @Column("varchar", { name: "dsremito", nullable: true, length: 30 })
  dsremito: string | null;
}
