import { Column, Entity, Index } from "typeorm";

@Index("PK_saldoclic", ["cdcliente"], { unique: true })
@Entity("saldoclic", { schema: "dbo" })
export class Saldoclic {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { name: "tpsaldo", nullable: true, length: 1 })
  tpsaldo: string | null;

  @Column("numeric", {
    name: "limitemto",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  limitemto: number | null;

  @Column("numeric", {
    name: "consumto",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  consumto: number | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("varchar", { name: "NroContrato", nullable: true, length: 60 })
  nroContrato: string | null;

  @Column("datetime", { name: "fechainicio", nullable: true })
  fechainicio: Date | null;

  @Column("char", { name: "TipoDespacho", nullable: true, length: 1 })
  tipoDespacho: string | null;

  @Column("char", { name: "TipoCredito", nullable: true, length: 1 })
  tipoCredito: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 20 })
  nrodocumento: string | null;

  @Column("char", { name: "cdlocal", nullable: true, length: 4 })
  cdlocal: string | null;

  @Column("datetime", { name: "FechaFinal", nullable: true })
  fechaFinal: Date | null;

  @Column("numeric", {
    name: "montolicitacion",
    nullable: true,
    precision: 12,
    scale: 3,
  })
  montolicitacion: number | null;

  @Column("bit", { name: "Licitacion", nullable: true })
  licitacion: boolean | null;
}
