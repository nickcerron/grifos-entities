import { Column, Entity, Index } from "typeorm";

@Index("PK_Precio_Promo", ["promocionId", "articuloId"], { unique: true })
@Entity("Precio_Promo", { schema: "dbo" })
export class PrecioPromo {
  @Column("varchar", { primary: true, name: "PromocionId", length: 20 })
  promocionId: string;

  @Column("varchar", { primary: true, name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("varchar", { name: "DsArticulo", nullable: true, length: 60 })
  dsArticulo: string | null;

  @Column("datetime", { name: "FechaInicial", nullable: true })
  fechaInicial: Date | null;

  @Column("datetime", { name: "FechaFinal", nullable: true })
  fechaFinal: Date | null;

  @Column("numeric", {
    name: "Costo_Compra",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  costoCompra: number | null;

  @Column("numeric", {
    name: "Costo_IGV",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  costoIgv: number | null;

  @Column("numeric", {
    name: "Precio_Promedio",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  precioPromedio: number | null;

  @Column("numeric", {
    name: "Precio_Promocion",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  precioPromocion: number | null;

  @Column("numeric", {
    name: "Margen",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  margen: number | null;

  @Column("numeric", {
    name: "MargenPorc",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  margenPorc: number | null;

  @Column("varchar", { name: "Hora_Inicial", nullable: true, length: 8 })
  horaInicial: string | null;

  @Column("varchar", { name: "Hora_Final", nullable: true, length: 8 })
  horaFinal: string | null;
}
