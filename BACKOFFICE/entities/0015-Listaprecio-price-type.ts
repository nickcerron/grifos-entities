import { Column, Entity, Index } from 'typeorm';

@Index('PK_listaprecio', ['cdprecio'], { unique: true })
@Entity('listaprecio', { schema: 'dbo' })
export class Listaprecio {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdprecio', length: 5 })
  cdprecio: string;

  //name
  //nullable => false
  @Column('char', { name: 'dsprecio', nullable: true, length: 40 })
  dsprecio: string | null;
}
