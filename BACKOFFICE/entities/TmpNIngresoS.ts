import { Column, Entity } from "typeorm";

@Entity("TMP_NIngresoS", { schema: "dbo" })
export class TmpNIngresoS {
  @Column("char", { name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("char", { name: "nroingreso", length: 10 })
  nroingreso: string;

  @Column("smalldatetime", { name: "fecingreso", nullable: true })
  fecingreso: Date | null;

  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", { name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 16,
    scale: 6,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 17,
    scale: 6,
  })
  mtoimpuesto: number | null;
}
