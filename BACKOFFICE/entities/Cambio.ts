import { Column, Entity } from "typeorm";

@Entity("cambio", { schema: "dbo" })
export class Cambio {
  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "cambio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cambio: number | null;
}
