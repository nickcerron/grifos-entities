import { Column, Entity } from "typeorm";

@Entity("cventad", { schema: "dbo" })
export class Cventad {
  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("smalldatetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nroitem: number | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 5,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoservicio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoservicio: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  mtototal: number | null;

  @Column("numeric", {
    name: "mtoimp_otros",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpOtros: number | null;

  @Column("bit", { name: "vtaxmonto", nullable: true })
  vtaxmonto: boolean | null;

  @Column("bit", { name: "movimiento", nullable: true })
  movimiento: boolean | null;

  @Column("bit", { name: "flgglosa", nullable: true })
  flgglosa: boolean | null;

  @Column("char", { name: "cdgrupo01", nullable: true, length: 5 })
  cdgrupo01: string | null;

  @Column("char", { name: "CDUNIMED", nullable: true, length: 5 })
  cdunimed: string | null;

  @Column("bit", { name: "trfGratuita", nullable: true })
  trfGratuita: boolean | null;

  @Column("bit", { name: "impplastico", nullable: true })
  impplastico: boolean | null;
}
