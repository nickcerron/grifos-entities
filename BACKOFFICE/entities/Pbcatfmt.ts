import { Column, Entity, Index } from "typeorm";

@Index("pbcatf_x", ["pbfName"], {})
@Entity("pbcatfmt", { schema: "dbo" })
export class Pbcatfmt {
  @Column("varchar", { name: "pbf_name", length: 30 })
  pbfName: string;

  @Column("varchar", { name: "pbf_frmt", nullable: true, length: 254 })
  pbfFrmt: string | null;

  @Column("smallint", { name: "pbf_type", nullable: true })
  pbfType: number | null;

  @Column("int", { name: "pbf_cntr", nullable: true })
  pbfCntr: number | null;
}
