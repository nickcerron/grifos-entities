import { Column, Entity } from "typeorm";

@Entity("TmpCredClienteWebS", { schema: "dbo" })
export class TmpCredClienteWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("char", { name: "docpago", length: 1 })
  docpago: string;

  @Column("char", { name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("varchar", { name: "nrodocumento", length: 20 })
  nrodocumento: string;

  @Column("smalldatetime", { name: "fecsistema" })
  fecsistema: Date;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("int", { name: "AlmacenId", nullable: true })
  almacenId: number | null;

  @Column("smalldatetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("smalldatetime", { name: "fecpago", nullable: true })
  fecpago: Date | null;

  @Column("varchar", { name: "ClienteId", length: 15 })
  clienteId: string;

  @Column("char", { name: "Moneda", nullable: true, length: 3 })
  moneda: string | null;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("float", { name: "mtoemision", nullable: true, precision: 53 })
  mtoemision: number | null;

  @Column("float", { name: "mtosoles", nullable: true, precision: 53 })
  mtosoles: number | null;

  @Column("float", { name: "mtodolares", nullable: true, precision: 53 })
  mtodolares: number | null;

  @Column("float", { name: "tcambio", nullable: true, precision: 53 })
  tcambio: number | null;

  @Column("char", { name: "cddocaplica", nullable: true, length: 5 })
  cddocaplica: string | null;

  @Column("char", { name: "nrodocaplica", nullable: true, length: 10 })
  nrodocaplica: string | null;

  @Column("char", { name: "cdcobrador", nullable: true, length: 10 })
  cdcobrador: string | null;

  @Column("bigint", { name: "nropago", nullable: true })
  nropago: string | null;

  @Column("char", { name: "nroplanilla", nullable: true, length: 10 })
  nroplanilla: string | null;

  @Column("char", { name: "nrorecibo", nullable: true, length: 10 })
  nrorecibo: string | null;

  @Column("char", { name: "cdtppago", nullable: true, length: 5 })
  cdtppago: string | null;

  @Column("char", { name: "cdbanco", nullable: true, length: 5 })
  cdbanco: string | null;

  @Column("varchar", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("varchar", { name: "nrocheque", nullable: true, length: 20 })
  nrocheque: string | null;

  @Column("varchar", { name: "cdtarjeta", nullable: true, length: 2 })
  cdtarjeta: string | null;

  @Column("varchar", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("varchar", { name: "referencia", nullable: true, length: 60 })
  referencia: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
