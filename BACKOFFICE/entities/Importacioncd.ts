import { Column, Entity, Index } from "typeorm";

@Index("PK_importacioncd", ["fechora"], { unique: true })
@Entity("importacioncd", { schema: "dbo" })
export class Importacioncd {
  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("numeric", {
    name: "nroitem",
    nullable: true,
    precision: 4,
    scale: 0,
  })
  nroitem: number | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdalternativo", nullable: true, length: 20 })
  cdalternativo: string | null;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 14, scale: 6 })
  costo: number | null;
}
