import { Column, Entity } from "typeorm";

@Entity("PROVTELEFONIA", { schema: "dbo" })
export class Provtelefonia {
  @Column("char", { name: "cdProvTelefonia", length: 2 })
  cdProvTelefonia: string;

  @Column("varchar", { name: "dsProvtelefonia", nullable: true, length: 50 })
  dsProvtelefonia: string | null;
}
