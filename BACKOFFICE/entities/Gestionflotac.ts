import { Column, Entity, Index } from "typeorm";

@Index("fk_gestionflotac", ["cdcliente"], { unique: true })
@Entity("gestionflotac", { schema: "dbo" })
export class Gestionflotac {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("char", { name: "NroContrato", nullable: true, length: 10 })
  nroContrato: string | null;

  @Column("datetime", { name: "fechainicio", nullable: true })
  fechainicio: Date | null;

  @Column("int", { name: "cdgrupoID", nullable: true })
  cdgrupoId: number | null;

  @Column("bit", { name: "FlgConfirmadoCentral", nullable: true })
  flgConfirmadoCentral: boolean | null;
}
