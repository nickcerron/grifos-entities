import { Column, Entity } from "typeorm";

@Entity("Log_MontoXArticulo", { schema: "dbo" })
export class LogMontoXArticulo {
  @Column("char", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", { name: "MtoBase", precision: 16, scale: 2 })
  mtoBase: number;

  @Column("smalldatetime", { name: "fechaModifica", nullable: true })
  fechaModifica: Date | null;

  @Column("char", { name: "cdUsuarioModifica", nullable: true, length: 10 })
  cdUsuarioModifica: string | null;
}
