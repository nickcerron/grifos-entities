import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index(
  "PK_GrupoProdclienteDes",
  ["cdgrupoId", "tipocli", "tipogrupo", "tipodes"],
  { unique: true }
)
@Entity("GrupoProdclienteDes", { schema: "dbo" })
export class GrupoProdclienteDes {
  @Column("int", { primary: true, name: "cdgrupoID" })
  cdgrupoId: number;

  @Column("varchar", { name: "descripcion", nullable: true, length: 50 })
  descripcion: string | null;

  @Column("char", { primary: true, name: "tipocli", length: 3 })
  tipocli: string;

  @Column("char", { primary: true, name: "tipogrupo", length: 10 })
  tipogrupo: string;

  @Column("char", { primary: true, name: "tipodes", length: 3 })
  tipodes: string;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 10,
    scale: 3,
  })
  precio: number | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @PrimaryGeneratedColumn({ type: "int", name: "cdGrupoProCli" })
  cdGrupoProCli: number;
}
