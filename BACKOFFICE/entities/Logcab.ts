import { Column, Entity } from "typeorm";

@Entity("Logcab", { schema: "dbo" })
export class Logcab {
  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "tpsaldo", nullable: true, length: 1 })
  tpsaldo: string | null;

  @Column("numeric", {
    name: "limitemto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  limitemto: number | null;

  @Column("numeric", {
    name: "consumto",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  consumto: number | null;

  @Column("bit", { name: "flgilimit", nullable: true })
  flgilimit: boolean | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;

  @Column("char", { name: "NroContrato", nullable: true, length: 10 })
  nroContrato: string | null;

  @Column("datetime", { name: "fechainicio", nullable: true })
  fechainicio: Date | null;

  @Column("datetime", { name: "fechafin", nullable: true })
  fechafin: Date | null;
}
