import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("PK__Kardex_M__3214EC07C389BE28", ["id"], { unique: true })
@Entity("Kardex_Movi_New", { schema: "dbo" })
export class KardexMoviNew {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("varchar", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("varchar", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("varchar", { name: "serie", nullable: true, length: 5 })
  serie: string | null;

  @Column("varchar", { name: "nrodocumento", nullable: true, length: 20 })
  nrodocumento: string | null;

  @Column("varchar", { name: "mov", nullable: true, length: 10 })
  mov: string | null;

  @Column("char", { name: "tipo", nullable: true, length: 2 })
  tipo: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 18, scale: 4 })
  costo: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 18, scale: 4 })
  total: number | null;

  @Column("varchar", { name: "tipo_ope", nullable: true, length: 50 })
  tipoOpe: string | null;

  @Column("bit", { name: "flgmanual", nullable: true })
  flgmanual: boolean | null;

  @Column("char", { name: "nrocelular", nullable: true, length: 12 })
  nrocelular: string | null;

  @Column("varchar", { name: "razonsocial", nullable: true, length: 120 })
  razonsocial: string | null;
}
