import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_ncreditog",
  ["cdlocal", "nroncredito", "cddocorigen", "rucempresa"],
  { unique: true }
)
@Entity("ncreditog", { schema: "dbo" })
export class Ncreditog {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroncredito", length: 10 })
  nroncredito: string;

  @Column("smalldatetime", { name: "fecncredito", nullable: true })
  fecncredito: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("bit", { name: "declarado", nullable: true })
  declarado: boolean | null;

  @Column("bit", { name: "anulado", nullable: true })
  anulado: boolean | null;

  @Column("char", {
    primary: true,
    name: "cddocorigen",
    length: 5,
    default: () => "' '",
  })
  cddocorigen: string;

  @Column("char", { name: "estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("varchar", { name: "EstadoRegistro", nullable: true, length: 1 })
  estadoRegistro: string | null;

  @Column("varchar", { name: "EstadoProceso", nullable: true, length: 15 })
  estadoProceso: string | null;

  @Column("int", {
    name: "reintentos_replicacion",
    nullable: true,
    default: () => "(0)",
  })
  reintentosReplicacion: number | null;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;
}
