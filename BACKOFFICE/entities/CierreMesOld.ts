import { Column, Entity } from "typeorm";

@Entity("CierreMes_old", { schema: "dbo" })
export class CierreMesOld {
  @Column("numeric", { name: "mes", nullable: true, precision: 2, scale: 0 })
  mes: number | null;

  @Column("numeric", { name: "anno", nullable: true, precision: 4, scale: 0 })
  anno: number | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 8,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 11, scale: 4 })
  costo: number | null;

  @Column("numeric", {
    name: "SaldoInicial",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  saldoInicial: number | null;

  @Column("numeric", {
    name: "Compra_unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  compraUnid: number | null;

  @Column("numeric", {
    name: "Compra_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  compraVal: number | null;

  @Column("numeric", {
    name: "Venta_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaUnid: number | null;

  @Column("numeric", {
    name: "Venta_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaVal: number | null;

  @Column("numeric", {
    name: "Venta_Inc",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ventaInc: number | null;

  @Column("numeric", {
    name: "Ingreso_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ingresoUnid: number | null;

  @Column("numeric", {
    name: "Ingreso_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ingresoVal: number | null;

  @Column("numeric", {
    name: "Salida_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  salidaUnid: number | null;

  @Column("numeric", {
    name: "Salida_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  salidaVal: number | null;

  @Column("numeric", {
    name: "Transfer_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  transferUnid: number | null;

  @Column("numeric", {
    name: "Transfer_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  transferVal: number | null;

  @Column("numeric", {
    name: "Ajuste_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ajusteUnid: number | null;

  @Column("numeric", {
    name: "Ajuste_Val",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  ajusteVal: number | null;

  @Column("numeric", {
    name: "Saldo_Unid",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  saldoUnid: number | null;

  @Column("numeric", {
    name: "Margen",
    nullable: true,
    precision: 11,
    scale: 4,
  })
  margen: number | null;
}
