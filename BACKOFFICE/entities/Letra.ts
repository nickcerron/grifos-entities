import { Column, Entity, Index } from "typeorm";

@Index("PK_letra", ["cdlocal", "nroletra", "renovacion"], { unique: true })
@Entity("letra", { schema: "dbo" })
export class Letra {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroletra", length: 10 })
  nroletra: string;

  @Column("char", { primary: true, name: "renovacion", length: 2 })
  renovacion: string;

  @Column("smalldatetime", { name: "fecletra", nullable: true })
  fecletra: Date | null;

  @Column("smalldatetime", { name: "fecvencimiento", nullable: true })
  fecvencimiento: Date | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("char", { name: "rscliente", nullable: true, length: 60 })
  rscliente: string | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "cdletra", nullable: true, length: 2 })
  cdletra: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("smalldatetime", { name: "fecaceptada", nullable: true })
  fecaceptada: Date | null;

  @Column("smalldatetime", { name: "fecprotesto", nullable: true })
  fecprotesto: Date | null;

  @Column("char", { name: "cdbanco", nullable: true, length: 4 })
  cdbanco: string | null;

  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  @Column("char", { name: "nroletrabanco", nullable: true, length: 15 })
  nroletrabanco: string | null;

  @Column("smalldatetime", { name: "fecanulacion", nullable: true })
  fecanulacion: Date | null;

  @Column("bit", { name: "flgrenovada", nullable: true })
  flgrenovada: boolean | null;
}
