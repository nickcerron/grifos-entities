import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepIngresoDWebS", { schema: "dbo" })
export class TmpRepIngresoDWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("varchar", { name: "IngresoId", length: 5 })
  ingresoId: string;

  @Column("varchar", { name: "NroIngreso", length: 15 })
  nroIngreso: string;

  @Column("varchar", { name: "TipoDocId", length: 5 })
  tipoDocId: string;

  @Column("int", { name: "NroItem" })
  nroItem: number;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("numeric", {
    name: "Cantidad",
    nullable: true,
    precision: 27,
    scale: 5,
  })
  cantidad: number | null;

  @Column("numeric", { name: "Costo", nullable: true, precision: 27, scale: 5 })
  costo: number | null;

  @Column("numeric", {
    name: "MtoSubtotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoSubtotal: number | null;

  @Column("numeric", {
    name: "MtoImpuesto",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoImpuesto: number | null;

  @Column("numeric", {
    name: "MtoImpuesto1",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoImpuesto1: number | null;

  @Column("numeric", {
    name: "MtoTotal",
    nullable: true,
    precision: 27,
    scale: 2,
  })
  mtoTotal: number | null;

  @Column("int", { name: "Linea", nullable: true })
  linea: number | null;
}
