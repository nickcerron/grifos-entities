import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_DiaDepBanco201810",
  ["cdlocal", "fecproceso", "turno", "nrocuenta"],
  { unique: true }
)
@Entity("DiaDepBanco201810", { schema: "dbo" })
export class DiaDepBanco201810 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;

  @Column("int", { primary: true, name: "Turno" })
  turno: number;

  @Column("char", { primary: true, name: "nrocuenta", length: 20 })
  nrocuenta: string;

  @Column("char", { name: "nrodeposito", length: 20 })
  nrodeposito: string;

  @Column("float", { name: "mtototal", nullable: true, precision: 53 })
  mtototal: number | null;

  @Column("float", { name: "TCambio", nullable: true, precision: 53 })
  tCambio: number | null;

  @Column("float", { name: "valores", nullable: true, precision: 53 })
  valores: number | null;

  @Column("float", { name: "cheques", nullable: true, precision: 53 })
  cheques: number | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 60 })
  observacion: string | null;
}
