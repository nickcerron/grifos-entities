import { Column, Entity } from "typeorm";

@Entity("ultimos_registros", { schema: "dbo" })
export class UltimosRegistros {
  @Column("char", { name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { name: "fecproceso" })
  fecproceso: Date;

  @Column("numeric", { name: "turno", precision: 2, scale: 0 })
  turno: number;

  @Column("char", { name: "lado", length: 2 })
  lado: string;

  @Column("char", { name: "manguera", length: 2 })
  manguera: string;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "Inicial",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  inicial: number | null;

  @Column("numeric", { name: "final", nullable: true, precision: 14, scale: 4 })
  final: number | null;

  @Column("numeric", {
    name: "galonesc",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  galonesc: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("numeric", { name: "total", nullable: true, precision: 12, scale: 2 })
  total: number | null;

  @Column("numeric", {
    name: "galonespec",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  galonespec: number | null;

  @Column("numeric", {
    name: "totalpec",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  totalpec: number | null;

  @Column("char", { name: "Observaciones", nullable: true, length: 100 })
  observaciones: string | null;

  @Column("char", { name: "dsusuario", nullable: true, length: 20 })
  dsusuario: string | null;
}
