import { Column, Entity } from "typeorm";

@Entity("RemitosDetalle", { schema: "dbo" })
export class RemitosDetalle {
  @Column("char", { name: "cdlocal", nullable: true, length: 3 })
  cdlocal: string | null;

  @Column("char", { name: "cdremito", nullable: true, length: 5 })
  cdremito: string | null;

  @Column("char", { name: "nroremito", nullable: true, length: 12 })
  nroremito: string | null;

  @Column("datetime", { name: "fecha", nullable: true })
  fecha: Date | null;

  @Column("varchar", { name: "dsturno", nullable: true, length: 5 })
  dsturno: string | null;

  @Column("int", { name: "nropaquetes", nullable: true })
  nropaquetes: number | null;

  @Column("numeric", {
    name: "mtosoles",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosoles: number | null;

  @Column("numeric", {
    name: "mtodolares",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtodolares: number | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("int", { name: "nroPaqSoles", nullable: true })
  nroPaqSoles: number | null;

  @Column("int", { name: "nroPaqDolares", nullable: true })
  nroPaqDolares: number | null;
}
