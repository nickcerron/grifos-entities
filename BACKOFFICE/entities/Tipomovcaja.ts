import { Column, Entity, Index } from "typeorm";

@Index("PK_tipomovcaja", ["cdtpmovcaja"], { unique: true })
@Entity("tipomovcaja", { schema: "dbo" })
export class Tipomovcaja {
  @Column("char", { primary: true, name: "cdtpmovcaja", length: 5 })
  cdtpmovcaja: string;

  @Column("char", { name: "dstpmovcaja", nullable: true, length: 30 })
  dstpmovcaja: string | null;

  @Column("char", { name: "tipomov", nullable: true, length: 1 })
  tipomov: string | null;

  @Column("bit", { name: "flgsistema", nullable: true })
  flgsistema: boolean | null;
}
