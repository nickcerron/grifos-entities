import { Column, Entity, Index } from "typeorm";

@Index("PK_SerieDoc", ["cdtipodoc", "serie"], { unique: true })
@Entity("SerieDoc", { schema: "dbo" })
export class SerieDoc {
  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "serie", length: 5 })
  serie: string;

  @Column("char", { name: "correlativo", nullable: true, length: 10 })
  correlativo: string | null;
}
