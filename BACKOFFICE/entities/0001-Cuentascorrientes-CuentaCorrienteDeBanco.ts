import { Column, Entity } from "typeorm";
// ahora se llamara BankAccount
@Entity("cuentascorrientes", { schema: "dbo" })
export class Cuentascorrientes {

  // no tiene id, ahora tendra uno identity
  // id: number

  // ahora => varchar
  @Column("char", { name: "nrocuenta", nullable: true, length: 20 })
  nrocuenta: string | null;

  // ahora sera una referencia FK a la tabla de bancos
  //campo en db: bank_id
  @Column("char", { name: "cdbanco", nullable: true, length: 4 })
  cdbanco: string | null;

  // descripcion: length => 100
  @Column("char", { name: "dscuenta", nullable: true, length: 50 })
  dscuenta: string | null;

  //esto se convertira en una tabla y sera referencia aqui: Tabla Moneda
  // con el campo id, name, symbol (simbolo) y isActive (estado),
  //campo en db: currency_id
  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;
  
  //default: false
  @Column("bit", { name: "detraccion", nullable: true })
  detraccion: boolean | null;

  //sele añadira una relacion con compañia
  //campo en db: company_id
}
