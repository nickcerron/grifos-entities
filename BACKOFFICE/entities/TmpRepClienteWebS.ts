import { Column, Entity, Index } from "typeorm";

@Index("PK_TmpRepClienteWebS", ["cdcliente"], { unique: true })
@Entity("TmpRepClienteWebS", { schema: "dbo" })
export class TmpRepClienteWebS {
  @Column("int", { name: "empresa" })
  empresa: number;

  @Column("int", { name: "local" })
  local: number;

  @Column("varchar", { primary: true, name: "cdcliente", length: 20 })
  cdcliente: string;

  @Column("varchar", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;
}
