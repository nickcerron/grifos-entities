import { Column, Entity, Index } from 'typeorm';

@Index('PK_grupo05', ['cdgrupo05'], { unique: true })
@Entity('grupo05', { schema: 'dbo' })
export class Grupo05 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgrupo05', length: 5 })
  cdgrupo05: string;

  //name
  @Column('char', { name: 'dsgrupo05', nullable: true, length: 40 })
  dsgrupo05: string | null;

  //description
  @Column('varchar', { name: 'dagrupo05', nullable: true, length: 100 })
  dagrupo05: string | null;
}
