import { Column, Entity } from "typeorm";

@Entity("saldocliente", { schema: "dbo" })
export class Saldocliente {
  @Column("char", { name: "cdcliente", nullable: true, length: 15 })
  cdcliente: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "cdgrupo02", nullable: true, length: 5 })
  cdgrupo02: string | null;

  @Column("char", { name: "cdarticulo", nullable: true, length: 20 })
  cdarticulo: string | null;

  @Column("numeric", {
    name: "limitecantidad",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  limitecantidad: number | null;

  @Column("numeric", {
    name: "saldocantidad",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  saldocantidad: number | null;

  @Column("numeric", {
    name: "limitemonto",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  limitemonto: number | null;

  @Column("numeric", {
    name: "saldomonto",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  saldomonto: number | null;

  @Column("char", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("char", { name: "tpsaldo", nullable: true, length: 1 })
  tpsaldo: string | null;

  @Column("char", { name: "flgilimitado", nullable: true, length: 1 })
  flgilimitado: string | null;

  @Column("bit", { name: "blq", nullable: true })
  blq: boolean | null;
}
