import { Column, Entity, Index } from "typeorm";

@Index("PK_Tarjeta_Concepto_1", ["tarjAfiliacion", "ruccliente", "valorId"], {
  unique: true,
})
@Entity("Tarjeta_Concepto", { schema: "dbo" })
export class TarjetaConcepto {
  @Column("varchar", { primary: true, name: "TarjAfiliacion", length: 25 })
  tarjAfiliacion: string;

  @Column("char", { primary: true, name: "ruccliente", length: 15 })
  ruccliente: string;

  @Column("int", { primary: true, name: "ValorID" })
  valorId: number;

  @Column("char", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;
}
