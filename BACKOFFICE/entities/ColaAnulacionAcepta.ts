import { Column, Entity, Index } from "typeorm";

@Index("PK_Cola_Anulacion_Acepta", ["nrodocumento"], { unique: true })
@Entity("Cola_Anulacion_Acepta", { schema: "dbo" })
export class ColaAnulacionAcepta {
  @Column("char", { primary: true, name: "NRODOCUMENTO", length: 15 })
  nrodocumento: string;

  @Column("varchar", { name: "TRAMA", nullable: true })
  trama: string | null;

  @Column("smalldatetime", { name: "FECHA_EMISION" })
  fechaEmision: Date;

  @Column("smalldatetime", { name: "FECHA_ENVIO", nullable: true })
  fechaEnvio: Date | null;

  @Column("varchar", { name: "HASH", nullable: true, length: 100 })
  hash: string | null;

  @Column("int", { name: "ESTADO", nullable: true })
  estado: number | null;

  @Column("nvarchar", { name: "RESPUESTA", nullable: true })
  respuesta: string | null;

  @Column("varchar", { name: "OBSERVACION", nullable: true, length: 1000 })
  observacion: string | null;

  @Column("smalldatetime", { name: "TIMESTAMP" })
  timestamp: Date;

  @Column("bit", { name: "CORREOENVIADO", nullable: true })
  correoenviado: boolean | null;
}
