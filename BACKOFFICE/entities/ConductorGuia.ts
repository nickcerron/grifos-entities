import { Column, Entity, Index } from "typeorm";

@Index("pk_Conductor_Guia", ["cdConductor", "cdtpGuia", "nroGuia"], {
  unique: true,
})
@Entity("Conductor_Guia", { schema: "dbo" })
export class ConductorGuia {
  @Column("char", { primary: true, name: "cdConductor", length: 20 })
  cdConductor: string;

  @Column("char", { primary: true, name: "cdtpGuia", length: 5 })
  cdtpGuia: string;

  @Column("char", { primary: true, name: "nroGuia", length: 10 })
  nroGuia: string;
}
