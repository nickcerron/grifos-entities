import { Column, Entity, Index } from "typeorm";

@Index("PK_interface_puntos_venta_lado", ["cPuntoVenta", "codLado"], {
  unique: true,
})
@Entity("interface_puntos_venta_lado", { schema: "dbo" })
export class InterfacePuntosVentaLado {
  @Column("varchar", { primary: true, name: "c_punto_venta", length: 10 })
  cPuntoVenta: string;

  @Column("int", { primary: true, name: "CodLado" })
  codLado: number;

  @Column("float", {
    name: "galones",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  galones: number | null;

  @Column("float", {
    name: "inporte",
    nullable: true,
    precision: 53,
    default: () => "(0)",
  })
  inporte: number | null;

  @Column("int", { name: "n_turno", nullable: true })
  nTurno: number | null;

  @Column("smalldatetime", { name: "fecha_dia", nullable: true })
  fechaDia: Date | null;

  @Column("smalldatetime", { name: "fecha_Mov", nullable: true })
  fechaMov: Date | null;

  @Column("varchar", { name: "imagen", nullable: true, length: 70 })
  imagen: string | null;

  @Column("varchar", { name: "imagen1", nullable: true, length: 70 })
  imagen1: string | null;

  @Column("varchar", { name: "imagen2", nullable: true, length: 70 })
  imagen2: string | null;
}
