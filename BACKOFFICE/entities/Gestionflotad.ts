import { Column, Entity, Index } from "typeorm";

@Index("fk_gestionflotad", ["cdcliente", "nrotarjeta", "nrobonus"], {
  unique: true,
})
@Entity("gestionflotad", { schema: "dbo" })
export class Gestionflotad {
  @Column("char", { primary: true, name: "cdcliente", length: 15 })
  cdcliente: string;

  @Column("char", { primary: true, name: "nrotarjeta", length: 20 })
  nrotarjeta: string;

  @Column("char", { primary: true, name: "nrobonus", length: 20 })
  nrobonus: string;

  @Column("char", { name: "nroplaca", nullable: true, length: 10 })
  nroplaca: string | null;

  @Column("bit", { name: "flgbloquea", nullable: true })
  flgbloquea: boolean | null;
}
