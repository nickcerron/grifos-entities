import { Column, Entity, Index } from "typeorm";

@Index("PK_salidag", ["cdlocal", "cdtpsalida", "nrosalida", "rucempresa"], {
  unique: true,
})
@Entity("salidag", { schema: "dbo" })
export class Salidag {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdtpsalida", length: 5 })
  cdtpsalida: string;

  @Column("char", { primary: true, name: "nrosalida", length: 10 })
  nrosalida: string;

  @Column("char", { name: "cdproveedor", nullable: true, length: 15 })
  cdproveedor: string | null;

  @Column("smalldatetime", { name: "fecsalida", nullable: true })
  fecsalida: Date | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("char", { name: "Estado", nullable: true, length: 1 })
  estado: string | null;

  @Column("varchar", { name: "EstadoRegistro", nullable: true, length: 1 })
  estadoRegistro: string | null;

  @Column("varchar", { name: "EstadoProceso", nullable: true, length: 15 })
  estadoProceso: string | null;

  @Column("char", {
    primary: true,
    name: "rucempresa",
    length: 15,
    default: () => "''",
  })
  rucempresa: string;
}
