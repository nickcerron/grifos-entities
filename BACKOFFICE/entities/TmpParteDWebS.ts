import { Column, Entity } from "typeorm";

@Entity("TmpParteDWebS", { schema: "dbo" })
export class TmpParteDWebS {
  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("datetime", { name: "Fecha" })
  fecha: Date;

  @Column("varchar", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("char", { name: "Grupo", nullable: true, length: 1 })
  grupo: string | null;

  @Column("varchar", { name: "dsarticulo", nullable: true, length: 40 })
  dsarticulo: string | null;

  @Column("float", { name: "inicial", nullable: true, precision: 53 })
  inicial: number | null;

  @Column("float", { name: "compra", nullable: true, precision: 53 })
  compra: number | null;

  @Column("float", { name: "Consumo", nullable: true, precision: 53 })
  consumo: number | null;

  @Column("float", { name: "Venta", nullable: true, precision: 53 })
  venta: number | null;

  @Column("float", { name: "Final", nullable: true, precision: 53 })
  final: number | null;

  @Column("float", { name: "varillaje", nullable: true, precision: 53 })
  varillaje: number | null;

  @Column("float", { name: "difdia", nullable: true, precision: 53 })
  difdia: number | null;

  @Column("float", { name: "difmes", nullable: true, precision: 53 })
  difmes: number | null;

  @Column("float", { name: "Variacion", nullable: true, precision: 53 })
  variacion: number | null;

  @Column("float", { name: "Total", nullable: true, precision: 53 })
  total: number | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
