import { Column, Entity, Index } from "typeorm";

@Index("PK_ingresocd", ["cdtpingreso", "fechora", "nroitem", "cdarticulo"], {
  unique: true,
})
@Entity("ingresocd", { schema: "dbo" })
export class Ingresocd {
  @Column("char", { primary: true, name: "cdtpingreso", length: 5 })
  cdtpingreso: string;

  @Column("smalldatetime", { primary: true, name: "fechora" })
  fechora: Date;

  @Column("numeric", { primary: true, name: "nroitem", precision: 4, scale: 0 })
  nroitem: number;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { name: "cdalternativo", nullable: true, length: 20 })
  cdalternativo: string | null;

  @Column("char", { name: "talla", nullable: true, length: 10 })
  talla: string | null;

  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", { name: "costo", nullable: true, precision: 14, scale: 6 })
  costo: number | null;
}
