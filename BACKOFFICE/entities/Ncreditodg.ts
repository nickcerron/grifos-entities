import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_ncreditodg",
  ["nroncredito", "cdtipodoc", "nrodocumento", "nroseriemaq"],
  { unique: true }
)
@Entity("ncreditodg", { schema: "dbo" })
export class Ncreditodg {
  @Column("char", { primary: true, name: "nroncredito", length: 10 })
  nroncredito: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("char", { primary: true, name: "nroseriemaq", length: 15 })
  nroseriemaq: string;
}
