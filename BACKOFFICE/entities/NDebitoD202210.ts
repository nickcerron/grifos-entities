import { Column, Entity, Index } from "typeorm";

@Index("PK_NDebitoD202210", ["cdlocal", "nrondebito", "nroitem"], {
  unique: true,
})
@Entity("NDebitoD202210", { schema: "dbo" })
export class NDebitoD202210 {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nrondebito", length: 10 })
  nrondebito: string;

  @Column("numeric", { primary: true, name: "nroitem", precision: 3, scale: 0 })
  nroitem: number;

  @Column("char", { name: "dsarticulo", nullable: true, length: 60 })
  dsarticulo: string | null;

  @Column("numeric", {
    name: "impuesto",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  impuesto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("varchar", { name: "cdarticulosunat", nullable: true, length: 20 })
  cdarticulosunat: string | null;

  @Column("numeric", {
    name: "mtoimp_otros",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpOtros: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "cantidad",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  cantidad: number | null;

  @Column("numeric", {
    name: "precio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  precio: number | null;

  @Column("char", { name: "rucempresa", length: 15, default: () => "''" })
  rucempresa: string;
}
