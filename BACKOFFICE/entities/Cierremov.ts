import { Column, Entity, Index } from "typeorm";

@Index("PK_cierremov", ["cdlocal", "cdalmacen", "cdarticulo"], { unique: true })
@Entity("cierremov", { schema: "dbo" })
export class Cierremov {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "cdalmacen", length: 3 })
  cdalmacen: string;

  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("numeric", {
    name: "ventas",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ventas: number | null;

  @Column("numeric", {
    name: "ingresos",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  ingresos: number | null;

  @Column("numeric", {
    name: "salidas",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  salidas: number | null;
}
