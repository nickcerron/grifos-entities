import { Column, Entity, Index } from "typeorm";

@Index("PK_profpiepagina", ["cdpiepagina"], { unique: true })
@Entity("profpiepagina", { schema: "dbo" })
export class Profpiepagina {
  @Column("char", { primary: true, name: "cdpiepagina", length: 5 })
  cdpiepagina: string;

  @Column("text", { name: "mempiepagina", nullable: true })
  mempiepagina: string | null;

  @Column("bit", { name: "flgactivo", nullable: true })
  flgactivo: boolean | null;
}
