import { Column, Entity, Index } from "typeorm";

@Index("PK_provarticulo", ["cdarticulo", "cdproveedor"], { unique: true })
@Entity("provarticulo", { schema: "dbo" })
export class Provarticulo {
  //! id compuesto
  @Column("char", { primary: true, name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("char", { primary: true, name: "cdproveedor", length: 15 })
  cdproveedor: string;
  //!hasta aqui
  
  //! codigo de compra muere, lo mismo que codigo producto
  //! eliminar
  @Column("char", { name: "cdcompra", nullable: true, length: 20 })
  cdcompra: string | null;

  //! desc de prod
  @Column("varchar", { name: "dscompra", nullable: true, length: 60 })
  dscompra: string | null;

  //referencia a moneda, moneda que acepta la empresa?
  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  // mto de que? si las compras no van en esta tabla, sera el costo por unidad 
  // al comprarlo? no tiene sentido pq esto es variable y depende de la cantidad
  // quizas ya hay un standard de cantidad para comprar y este es el ultimo costo
  @Column("numeric", {
    name: "mtocosto",
    nullable: true,
    precision: 16,
    scale: 8,
  })
  mtocosto: number | null;


  //! todo en null
  @Column("char", { name: "cdmonfob", nullable: true, length: 1 })
  cdmonfob: string | null;

  //! todo en null
  @Column("numeric", {
    name: "mtocostofob",
    nullable: true,
    precision: 14,
    scale: 4,
  })
  mtocostofob: number | null;

  @Column("numeric", {
    name: "CostoMaximo",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  costoMaximo: number | null;

  @Column("numeric", {
    name: "CostoMinimo",
    nullable: true,
    precision: 14,
    scale: 6,
  })
  costoMinimo: number | null;
}
