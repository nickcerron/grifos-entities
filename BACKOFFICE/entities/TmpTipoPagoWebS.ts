import { Column, Entity } from "typeorm";

@Entity("TmpTipo_PagoWebS", { schema: "dbo" })
export class TmpTipoPagoWebS {
  @Column("char", { name: "TipoPagoId", length: 5 })
  tipoPagoId: string;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 60 })
  descripcion: string | null;

  @Column("bit", { name: "FlgPago", nullable: true })
  flgPago: boolean | null;

  @Column("bit", { name: "FlgSistema", nullable: true })
  flgSistema: boolean | null;
}
