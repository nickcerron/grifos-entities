import { Column, Entity, Index } from 'typeorm';

@Index('PK_grupo03', ['cdgrupo03'], { unique: true })
@Entity('grupo03', { schema: 'dbo' })
export class Grupo03 {
  // el id, sera reemplazado por un id autogenerado
  // este campo será bigint
  @Column('char', { primary: true, name: 'cdgrupo03', length: 5 })
  cdgrupo03: string;

  //name
  //length => 40
  @Column('varchar', { name: 'dsgrupo03', nullable: true, length: 100 })
  dsgrupo03: string | null;
  //description
  @Column('varchar', { name: 'dagrupo03', nullable: true, length: 100 })
  dagrupo03: string | null;
}
