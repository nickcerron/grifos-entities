import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TmpRepDiaVarillajeWebS", { schema: "dbo" })
export class TmpRepDiaVarillajeWebS {
  @PrimaryGeneratedColumn({ type: "int", name: "Id" })
  id: number;

  @Column("int", { name: "EmpresaId" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("smalldatetime", { name: "Fecproceso" })
  fecproceso: Date;

  @Column("char", { name: "ArticuloId", length: 20 })
  articuloId: string;

  @Column("numeric", {
    name: "MtoInicial",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  mtoInicial: number | null;

  @Column("numeric", {
    name: "Compra",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  compra: number | null;

  @Column("numeric", { name: "Venta", nullable: true, precision: 15, scale: 3 })
  venta: number | null;

  @Column("numeric", {
    name: "MtoFinal",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  mtoFinal: number | null;

  @Column("numeric", {
    name: "Varillaje",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  varillaje: number | null;

  @Column("numeric", {
    name: "DifDia",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difDia: number | null;

  @Column("numeric", {
    name: "DifMes",
    nullable: true,
    precision: 15,
    scale: 3,
  })
  difMes: number | null;

  @Column("numeric", {
    name: "Variacion",
    nullable: true,
    precision: 7,
    scale: 3,
  })
  variacion: number | null;

  @Column("numeric", { name: "Total", nullable: true, precision: 15, scale: 2 })
  total: number | null;
}
