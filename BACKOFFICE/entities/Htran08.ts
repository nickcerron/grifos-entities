import { Column, Entity } from "typeorm";

@Entity("htran08", { schema: "dbo" })
export class Htran08 {
  @Column("char", { name: "numero", nullable: true, length: 20 })
  numero: string | null;

  @Column("char", { name: "soles", nullable: true, length: 6 })
  soles: string | null;

  @Column("char", { name: "producto", nullable: true, length: 2 })
  producto: string | null;

  @Column("char", { name: "precio", nullable: true, length: 4 })
  precio: string | null;

  @Column("char", { name: "galones", nullable: true, length: 8 })
  galones: string | null;

  @Column("char", { name: "cara", nullable: true, length: 2 })
  cara: string | null;

  @Column("char", { name: "fecha", nullable: true, length: 6 })
  fecha: string | null;

  @Column("char", { name: "hora", nullable: true, length: 4 })
  hora: string | null;

  @Column("char", { name: "turno", nullable: true, length: 1 })
  turno: string | null;

  @Column("char", { name: "cdtipodoc", nullable: true, length: 5 })
  cdtipodoc: string | null;

  @Column("char", { name: "documento", nullable: true, length: 10 })
  documento: string | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;
}
