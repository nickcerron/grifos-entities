import { Column, Entity, Index } from "typeorm";

@Index("PK_tipocredito", ["cdtpcredito"], { unique: true })
@Entity("tipocredito", { schema: "dbo" })
export class Tipocredito {
  @Column("char", { primary: true, name: "cdtpcredito", length: 5 })
  cdtpcredito: string;

  @Column("char", { name: "dstpcredito", nullable: true, length: 60 })
  dstpcredito: string | null;

  @Column("numeric", {
    name: "nrodias",
    nullable: true,
    precision: 3,
    scale: 0,
  })
  nrodias: number | null;
}
