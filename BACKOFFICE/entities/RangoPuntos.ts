import { Column, Entity } from "typeorm";

@Entity("RANGO_PUNTOS", { schema: "dbo" })
export class RangoPuntos {
  @Column("varchar", { name: "Programa", length: 50 })
  programa: string;

  @Column("numeric", {
    name: "MtoInicio",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  mtoInicio: number | null;

  @Column("numeric", {
    name: "MtoFin",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  mtoFin: number | null;

  @Column("numeric", {
    name: "Puntos",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  puntos: number | null;
}
