import { Column, Entity, Index } from "typeorm";

@Index("PK_tipootrabajo", ["cdtipootrabajo"], { unique: true })
@Entity("tipootrabajo", { schema: "dbo" })
export class Tipootrabajo {
  @Column("char", { primary: true, name: "cdtipootrabajo", length: 2 })
  cdtipootrabajo: string;

  @Column("char", { name: "dstipootrabajo", nullable: true, length: 20 })
  dstipootrabajo: string | null;
}
