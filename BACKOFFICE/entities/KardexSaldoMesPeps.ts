import { Column, Entity } from "typeorm";

@Entity("Kardex_Saldo_mes_PEPS", { schema: "dbo" })
export class KardexSaldoMesPeps {
  @Column("varchar", { name: "cdarticulo", length: 20 })
  cdarticulo: string;

  @Column("varchar", { name: "periodo", length: 6 })
  periodo: string;

  @Column("numeric", {
    name: "cantidadf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  cantidadf: number | null;

  @Column("numeric", {
    name: "costof",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  costof: number | null;

  @Column("numeric", {
    name: "totalf",
    nullable: true,
    precision: 18,
    scale: 4,
  })
  totalf: number | null;

  @Column("char", { name: "cdTpClienteKardex", length: 5, default: () => "''" })
  cdTpClienteKardex: string;
}
