import { Column, Entity, Index } from "typeorm";

@Index(
  "PK_Venta_Auditoria",
  ["fecelimina", "cdlocal", "nroseriemaq", "cdtipodoc", "nrodocumento"],
  { unique: true }
)
@Entity("Venta_Auditoria", { schema: "dbo" })
export class VentaAuditoria {
  @Column("datetime", { primary: true, name: "fecelimina" })
  fecelimina: Date;

  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("char", { primary: true, name: "nroseriemaq", length: 20 })
  nroseriemaq: string;

  @Column("char", { primary: true, name: "cdtipodoc", length: 5 })
  cdtipodoc: string;

  @Column("char", { primary: true, name: "nrodocumento", length: 10 })
  nrodocumento: string;

  @Column("datetime", { name: "fecdocumento", nullable: true })
  fecdocumento: Date | null;

  @Column("smalldatetime", { name: "fecproceso", nullable: true })
  fecproceso: Date | null;

  @Column("smalldatetime", { name: "fecsistema", nullable: true })
  fecsistema: Date | null;

  @Column("char", { name: "nropos", nullable: true, length: 10 })
  nropos: string | null;

  @Column("numeric", {
    name: "mtovueltosol",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtovueltosol: number | null;

  @Column("numeric", {
    name: "mtovueltodol",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtovueltodol: number | null;

  @Column("char", { name: "cdalmacen", nullable: true, length: 3 })
  cdalmacen: string | null;

  @Column("char", { name: "cdcliente", nullable: true, length: 20 })
  cdcliente: string | null;

  @Column("char", { name: "ruccliente", nullable: true, length: 15 })
  ruccliente: string | null;

  @Column("varchar", { name: "rscliente", nullable: true, length: 120 })
  rscliente: string | null;

  @Column("char", { name: "nroproforma", nullable: true, length: 10 })
  nroproforma: string | null;

  @Column("char", { name: "cdprecio", nullable: true, length: 5 })
  cdprecio: string | null;

  @Column("char", { name: "cdmoneda", nullable: true, length: 1 })
  cdmoneda: string | null;

  @Column("numeric", {
    name: "porservicio",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  porservicio: number | null;

  @Column("numeric", {
    name: "pordscto1",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto1: number | null;

  @Column("numeric", {
    name: "pordscto2",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto2: number | null;

  @Column("numeric", {
    name: "pordscto3",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordscto3: number | null;

  @Column("numeric", {
    name: "pordsctoeq",
    nullable: true,
    precision: 6,
    scale: 2,
  })
  pordsctoeq: number | null;

  @Column("numeric", {
    name: "mtonoafecto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtonoafecto: number | null;

  @Column("numeric", {
    name: "valorvta",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  valorvta: number | null;

  @Column("numeric", {
    name: "mtodscto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtodscto: number | null;

  @Column("numeric", {
    name: "mtosubtotal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtosubtotal: number | null;

  @Column("numeric", {
    name: "mtoservicio",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoservicio: number | null;

  @Column("numeric", {
    name: "mtoimpuesto",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtoimpuesto: number | null;

  @Column("numeric", {
    name: "mtototal",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtototal: number | null;

  @Column("char", { name: "cdtranspor", nullable: true, length: 50 })
  cdtranspor: string | null;

  @Column("char", { name: "ructranspor", nullable: true, length: 15 })
  ructranspor: string | null;

  @Column("varchar", { name: "nroplaca", nullable: true, length: 250 })
  nroplaca: string | null;

  @Column("char", { name: "drpartida", nullable: true, length: 60 })
  drpartida: string | null;

  @Column("char", { name: "MarcaVehic", nullable: true, length: 15 })
  marcaVehic: string | null;

  @Column("char", { name: "drdestino", nullable: true, length: 60 })
  drdestino: string | null;

  @Column("char", { name: "cdusuario", nullable: true, length: 10 })
  cdusuario: string | null;

  @Column("char", { name: "cdvendedor", nullable: true, length: 10 })
  cdvendedor: string | null;

  @Column("char", { name: "cdusuanula", nullable: true, length: 10 })
  cdusuanula: string | null;

  @Column("smalldatetime", { name: "fecanula", nullable: true })
  fecanula: Date | null;

  @Column("smalldatetime", { name: "fecanulasis", nullable: true })
  fecanulasis: Date | null;

  @Column("char", { name: "tipofactura", nullable: true, length: 1 })
  tipofactura: string | null;

  @Column("bit", { name: "flgmanual", nullable: true })
  flgmanual: boolean | null;

  @Column("numeric", {
    name: "tcambio",
    nullable: true,
    precision: 10,
    scale: 6,
  })
  tcambio: number | null;

  @Column("char", { name: "nroocompra", nullable: true, length: 15 })
  nroocompra: string | null;

  @Column("bit", { name: "flgcierrez", nullable: true })
  flgcierrez: boolean | null;

  @Column("varchar", { name: "observacion", nullable: true, length: 250 })
  observacion: string | null;

  @Column("bit", { name: "flgmovimiento", nullable: true })
  flgmovimiento: boolean | null;

  @Column("varchar", { name: "referencia", nullable: true, length: 250 })
  referencia: string | null;

  @Column("char", { name: "nroserie1", nullable: true, length: 15 })
  nroserie1: string | null;

  @Column("char", { name: "nroserie2", nullable: true, length: 10 })
  nroserie2: string | null;

  @Column("numeric", { name: "turno", nullable: true, precision: 2, scale: 0 })
  turno: number | null;

  @Column("char", { name: "nrobonus", nullable: true, length: 20 })
  nrobonus: string | null;

  @Column("char", { name: "nrotarjeta", nullable: true, length: 20 })
  nrotarjeta: string | null;

  @Column("char", { name: "odometro", nullable: true, length: 10 })
  odometro: string | null;

  @Column("bit", { name: "archturno", nullable: true })
  archturno: boolean | null;

  @Column("numeric", {
    name: "mtorecaudo",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtorecaudo: number | null;

  @Column("char", { name: "inscripcion", nullable: true, length: 15 })
  inscripcion: string | null;

  @Column("char", { name: "chofer", nullable: true, length: 40 })
  chofer: string | null;

  @Column("char", { name: "nrolicencia", nullable: true, length: 15 })
  nrolicencia: string | null;

  @Column("bit", { name: "chkespecial", nullable: true })
  chkespecial: boolean | null;

  @Column("char", { name: "tipoventa", nullable: true, length: 1 })
  tipoventa: string | null;

  @Column("char", { name: "nrocelular", nullable: true, length: 12 })
  nrocelular: string | null;

  @Column("numeric", {
    name: "PtosGanados",
    nullable: true,
    precision: 11,
    scale: 3,
  })
  ptosGanados: number | null;

  @Column("char", { name: "TipoAcumula", nullable: true, length: 2 })
  tipoAcumula: string | null;

  @Column("numeric", {
    name: "ValorAcumula",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  valorAcumula: number | null;

  @Column("varchar", { name: "c_centralizacion", nullable: true, length: 50 })
  cCentralizacion: string | null;

  @Column("numeric", {
    name: "cantcupon",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  cantcupon: number | null;

  @Column("numeric", {
    name: "mtocanje",
    nullable: true,
    precision: 12,
    scale: 4,
  })
  mtocanje: number | null;

  @Column("numeric", {
    name: "MtoPercepcion",
    nullable: true,
    precision: 12,
    scale: 2,
  })
  mtoPercepcion: number | null;

  @Column("char", { name: "cdruta", nullable: true, length: 10 })
  cdruta: string | null;
}
