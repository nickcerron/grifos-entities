import { Column, Entity } from "typeorm";

@Entity("TmpAlmacenWebMIS", { schema: "dbo" })
export class TmpAlmacenWebMis {
  @Column("int", { name: "EmpresaID" })
  empresaId: number;

  @Column("int", { name: "LocalId" })
  localId: number;

  @Column("int", { name: "AlmacenID", nullable: true })
  almacenId: number | null;

  @Column("varchar", { name: "Nombre", nullable: true, length: 30 })
  nombre: string | null;
}
