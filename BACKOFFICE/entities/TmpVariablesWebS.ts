import { Column, Entity } from "typeorm";

@Entity("TmpVariablesWebS", { schema: "dbo" })
export class TmpVariablesWebS {
  @Column("varchar", { name: "TipoVar", nullable: true, length: 20 })
  tipoVar: string | null;

  @Column("varchar", { name: "Clave", nullable: true, length: 30 })
  clave: string | null;

  @Column("varchar", { name: "Descripcion", nullable: true, length: 50 })
  descripcion: string | null;

  @Column("float", { name: "ValorPto", nullable: true, precision: 53 })
  valorPto: number | null;

  @Column("varchar", { name: "Config", nullable: true, length: 40 })
  config: string | null;

  @Column("varchar", { name: "User_Registra", nullable: true, length: 20 })
  userRegistra: string | null;
}
