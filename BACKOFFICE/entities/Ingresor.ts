import { Column, Entity, Index } from "typeorm";

@Index("PK_ingresor", ["cdlocal", "fecingreso", "fecproceso"], { unique: true })
@Entity("ingresor", { schema: "dbo" })
export class Ingresor {
  @Column("char", { primary: true, name: "cdlocal", length: 3 })
  cdlocal: string;

  @Column("smalldatetime", { primary: true, name: "fecingreso" })
  fecingreso: Date;

  @Column("smalldatetime", { primary: true, name: "fecproceso" })
  fecproceso: Date;
}
