import { Column, Entity } from "typeorm";

@Entity("TmpRepAnomalias", { schema: "dbo" })
export class TmpRepAnomalias {
  @Column("int", { name: "EMPRESAID" })
  empresaid: number;

  @Column("int", { name: "LOCALID" })
  localid: number;

  @Column("varchar", { name: "NROANOMALIA", nullable: true, length: 12 })
  nroanomalia: string | null;

  @Column("varchar", { name: "NROREMITO", nullable: true, length: 12 })
  nroremito: string | null;

  @Column("datetime", { name: "FECHA", nullable: true })
  fecha: Date | null;

  @Column("numeric", {
    name: "TCAMBIO",
    nullable: true,
    precision: 8,
    scale: 4,
  })
  tcambio: number | null;

  @Column("numeric", {
    name: "MTOFALTSOLES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltsoles: number | null;

  @Column("numeric", {
    name: "MTOFALTDOLARES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtofaltdolares: number | null;

  @Column("numeric", {
    name: "MTOSOBSOLES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobsoles: number | null;

  @Column("numeric", {
    name: "MTOSOBDOLARES",
    nullable: true,
    precision: 11,
    scale: 2,
  })
  mtosobdolares: number | null;

  @Column("varchar", { name: "OBSERVACION", nullable: true, length: 200 })
  observacion: string | null;

  @Column("datetime", { name: "FECREGISTRA", nullable: true })
  fecregistra: Date | null;

  @Column("datetime", { name: "FECMODIFICA", nullable: true })
  fecmodifica: Date | null;

  @Column("varchar", { name: "USUREGISTRA", nullable: true, length: 20 })
  usuregistra: string | null;

  @Column("varchar", { name: "USUMODIFICA", nullable: true, length: 20 })
  usumodifica: string | null;

  @Column("varchar", { name: "NROHOJA", nullable: true, length: 15 })
  nrohoja: string | null;

  @Column("varchar", { name: "CDREMITO", nullable: true, length: 5 })
  cdremito: string | null;
}
